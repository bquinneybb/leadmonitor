<?php
error_reporting(E_ALL);
ini_set("display_errors", 1); 
include('../_config/config.php');

$form = array();
$form['custom_action'] = array();
$form['custom_action']['recipients'] = array();

$form['custom_action']['recipients'][] = array('db_quote_type', array(
	array('Standard Photography', 'orders@urbanangles.com')
	,array('Prestige Photography', 'orders@urbanangles.com')
	,array('3D Visualisation', 'visualisation@urbanangles.com')
	,array('Web Development', 'web@urbanangles.com')
	,array('Floorplans &amp; Drawing', 'floorplans@urbanangles.com')
	
	));
//echo json_encode($form);

?>

 <p><strong style="color: red;">FYI Values are hardcoded and email is used for from address</strong></p>

 <form id="registrationForm" name="registrationForm" action="/_emailsave/saveemail.php" method="post" enctype="multipart/form-data">
  
  <input type="hidden" name="campaign" id="campaign" value="fFiITjYEj15t2AOAeQ7D9eDhr7KJ1hWwIbkGIUIJ4wM=" />
  <input type="hidden" name="testmode" value="true" />

  <table>
    <tr>
      <td class="left"><label>form</label></td>
      <td class="right"><input type="text" name="form" id="form" value="fFiITjYEj15t2AOAeQ7D9eDhr7KJ1hWwIbkGIUIJ4wM="/></td>
    </tr>
    <tr>
      <td class="left"><label>db_name</label></td>
      <td class="right"><input type="text" name="db_name" id="db_name" class="text-input" value="Nigel" /></td>
    </tr>
    <tr>
      <td class="left"><label>db_email</label></td>
      <td class="right"><input type="text" name="db_email" id="db_email" class="text-input" value="tech@barkingbird.com.au" /></td>
    </tr>
    <tr>
      <td class="left"><label>db_phone</label></td>
      <td class="right"><input type="text" name="db_phone" id="db_phone" class="text-input" value="0000" /></td>
    </tr>
    <tr>
      <td class="left"><label>db_message</label></td>
      <td class="right"><input type="text" name="db_message" id="db_message" class="text-input" value="This is a test" /></td>
    </tr>
    <tr>
      <td class="left">&nbsp;</td>
      <td class="submit"><input type="submit" /></td>
    </tr>
  </table>

</form>
<style>
table {
  width: 400px;
}
td.left {
  width: 100px;
}
td.right {
  width: 300px;
}
td.right input {
  width: 100%;
}
td.submit {
  text-align: right;
}
</style>