<?php
/*
# Enable Error Reporting and Display:
error_reporting(~0);
ini_set('display_errors', 1);

$test_url = 'http://www.estiahealth.com.au/assets/media/uploads/ben_test.docx';
$userAgent = 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0';
//$test_url = 'http://www.estiahealth.com.au/phpinfo.php';

$ch = curl_init();

// set url
curl_setopt($ch, CURLOPT_URL, $test_url);

// return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// set the user agent
curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

// $output contains the output string
$ben_test = curl_exec($ch); 
//file_put_contents("blah.docx", $doc);

// close curl resource to free up system resources
curl_close($ch);

echo "<p>Testing File Get Contents</p>";
echo '<p><a href="'.$test_url.'">'.$test_url.'</a></p>';
//$ben_test = file_get_contents($test_url);

if($ben_test !== FALSE)
{
	var_dump($ben_test);
}
else
{
	echo '<p>Get Contents False</p>';
}

die();
*/

$_POST = array_merge($_GET, $_POST);
include('../_config/config.php');
//error_reporting(1);
error_reporting(E_ALL);
ini_set('display_errors', 'On');

$campaignId 			= isset($_POST['campaign']) ? $_POST['campaign'] : null;
$formId 				= isset($_POST['form']) ? $_POST['form'] : null;
$testing 				= isset($_POST['testmode']) ? $_POST['testmode'] : 'false';
$botCheck 				= isset($_POST['db_botbot']) ? $_POST['db_botbot'] : null;
$email 					= isset($_POST['db_email']) ? $_POST['db_email'] : null;
$name 					= isset($_POST['db_name']) ? $_POST['db_name'] : null;
$additional_recipient 	= isset($_POST['additional']) ? $_POST['additional'] : @$_POST['db_additional'];
$overwrite_subject 		= isset($_POST['db_overwrite_subject']) ? $_POST['db_overwrite_subject'] : FALSE;
$add_attachments 		= isset($_POST['db_add_attachments']) ? $_POST['db_add_attachments'] : FALSE;
$debug_mode 			= isset($_POST['db_debug_mode']) ? $_POST['db_debug_mode'] : FALSE;
//$debug_mode = TRUE;
if($overwrite_subject)
{
	unset($_POST['db_overwrite_subject']);	
}
if($debug_mode)
{
	unset($_POST['db_debug_mode']);
	//print_r($add_attachments); die;
}

if($add_attachments)
{
	unset($_POST['add_attachments']);
	unset($_POST['db_add_attachments']);
}

// echo "saveemail_ben<br/><br/>"; 
// var_dump($add_attachments);
// die();

//check and log spam
if($botCheck != null)
{	
	$email = new ModuleCampaignsLeads();
	$email->logSpam(decrypt($campaignId), decrypt($formId), 'Bot Field');
	die;	
}
//if no campain or form then stop.
if($campaignId == null || $formId == null)
{	
	die;
	//die('no id#');
}

$email = new ModuleCampaignsLeads();

// is it being sent by a spam bot?
if($spam = $email->checkForPossibleSpam(decrypt($campaignId), @$_SERVER['HTTP_REFERER']))
{
	//$email->logSpam(decrypt($campaignId), decrypt($formId), 'Referrer');		
} 
// spam check
$akismet	= new MicroAkismet( '752341d02fce', 'http://www.leadmonitor.com.au/_emailsave/saveemail.php', 'barkingbird' );

$vars 							= array();
$vars["user_ip"]           		= @$_SERVER["REMOTE_ADDR"];
$vars["user_agent"]        		= @$_SERVER["HTTP_USER_AGENT"];
$vars["referrer"] 				= @$_SERVER['HTTP_REFERER'];
$vars['comment_type'] 			= 'Registration';
$vars["comment_content"]   		= @$_POST["db_message"];
$vars["comment_author"]			= @$_POST["db_name"];
$vars["comment_author_email"]	= @$_POST["db_email"];
$akismet->check( $vars );

if ( $akismet->check( $vars ) ) 
{
	$email->logSpam(decrypt($campaignId), decrypt($formId), 'Akismet');
	die('Sorry but your email appears to be SPAM');
}
else 
{
	//var_dump($additional_recipient); die;
	$result = $email->saveWebLead($campaignId, $formId, $testing, $additional_recipient, $overwrite_subject, $add_attachments, $debug_mode);
}








?>