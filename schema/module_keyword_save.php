<?php
$schema = array(

	array('i', 'module_keyword_save_id_pk', 'insert')
	,array('i', 'group_id_pk', 'protected')
	,array('i', 'module_campaign_id_pk')
	,array('s', 'website_url', 'protected')
	,array('s', 'notes')
	,array('i', 'confirmed', 'protected')
	,array('i', 'active')
	,array('s', 'date_created', )
);
