<?php
/** group configuration schema
 * we are separating a lot of the group information into it's own table
 * so we're not constantly dealing with a cumbersome table when we're generally
 * just checking id and active
 */
$schema = array(
array('i', 'user_id_pk', 'insert')
,array('i', 'group_id_pk', 'protected')
,array('s', 'password')
,array('s', 'firstname')
,array('s', 'lastname')
,array('s', 'jobtitle')
,array('s', 'email')
,array('s','telephone')
,array('s', 'address1')
,array('s', 'address2')
,array('s', 'state')
,array('i', 'postcode')
,array('i', 'active')
,array('s', 'date_created', 'insert_now')
,array('i', 'created_by_user_id_pk', 'protected')

);