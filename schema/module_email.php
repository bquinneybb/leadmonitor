<?php
$schema = array(
	
	array('i', 'module_email_id_pk', 'protected')
	,array('i', 'module_campaign_id_pk', 'protected')
	,array('i', 'group_id_pk', 'protected')
	,array('s', 'email_label')
	,array('s', 'email_body')
	,array('s', 'use_custom_script')
	,array('i', 'complete')
	,array('i', 'sent')
	,array('s', 'history')
	,array('i', 'active')
);