<?php
/** logging schema
 * we are keeping logs of transactions and actions both for
 * accountability and quicklinks back to pages
 */
$schema = array(
array('i', 'log_action_id_pk', 'insert')
,array('i', 'user_id_pk', 'protected')
,array('s', 'module_identifier')
,array('s', 'action_identifier')
,array('s', 'item_identifier')
,array('s', 'action_url')
,array('s', 'date_created', 'insert_now')
);