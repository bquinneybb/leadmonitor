<?php
$schema =array(
	array('i', 'proxy_id_pk', 'insert')
	,array('s', 'address')
	,array('i', 'port')
	,array('i', 'latency')
	,array('i', 'active')
	,array('s', 'last_checked', 'insert_now')
);