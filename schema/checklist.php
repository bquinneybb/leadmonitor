<?php
/** checklist rules schema
 * this is designed for workflow
 * rules are expected to be in json format
 */
$schema = array(

array('i', 'checklist_id_pk', 'insert')
,array('i', 'group_parent_id_pk', 'protected')
,array('i','group_id_pk', 'protected')
,array('s', 'model')
,array('s', 'view')
,array('s', 'function')
,array('s', 'checklist_rules')
,array('i', 'active')
);