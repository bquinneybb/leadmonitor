<?php
$schema = array(
array('i', 'group_id_pk', 'insert')
,array('s', 'group_name')
,array('s', 'group_configuration')
,array('i', 'administrator')
,array('s', 'date_created', 'insert_now')
,array('i', 'active')
);