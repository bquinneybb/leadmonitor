<?php
/** configuration rules schema
 * this is design for pulling configuration such as image sizes
 * rules are expected to be in json format
 */
$schema = array(
array('i', 'configuration_id_pk', 'insert')

,array('i', 'group_id_pk', 'protected')
,array('i', 'user_id_pk', 'protected')
,array('s', 'configuration_type')
,array('s', 'configuration_rules')
,array('i', 'active')
);