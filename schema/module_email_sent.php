<?php
$schema = array(
	array('i', 'module_email_sent_id_pk', 'insert')
	,array('i', 'module_campaign_id_pk', 'protected')
	,array('i', 'module_lead_id_pk', 'protected')
	,array('i', 'module_emailer_id_pk', 'protected')
	,array('i', 'group_id_pk', 'protected')
	,array('i', 'email_opened', 'protected')
	,array('s', 'date_sent', 'insert_now')
	,array('s', 'history')
);