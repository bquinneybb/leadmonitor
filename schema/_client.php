<?php
$schema = array(
array('i', 'client_id_pk', 'insert')
,array('i', 'group_id_pk', 'protected')
,array('s', 'client_name')
,array('s', 'group_configuration')
,array('i', 'administrator')
,array('s', 'date_created', 'insert_now')
,array('i', 'active')
);