<?php
$schema = array(
	
	array('i', 'module_lead_id_pk', 'insert')
	,array('i', 'module_project_id_pk', 'protected')
	,array('i', 'module_campaign_id_pk', 'protected')
	,array('i', 'module_form_id_pk', 'protected')
	,array('i', 'group_id_pk', 'protected')
	,array('s', 'uid', 'protected')
	,array('s', 'referer_url', 'protected')
	,array('s', 'split_test')
	,array('s', 'leadsource')
	,array('i', 'followed_up')
	,array('s', 'my_comments')
	
	,array('i','active')
	,array('s', 'date_created','insert_now')

);
