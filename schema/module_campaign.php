<?php
$schema = array(

	array('i', 'module_campaign_id_pk', 'protected')
	,array('i', 'module_project_id_pk', 'protected')
	,array('i', 'group_id_pk', 'protected')
	,array('i', 'module_campaign_form_id_pk')
	,array('i', 'client_id_pk')
	,array('i', 'campaign_type')
	,array('s', 'campaign_name')
	,array('s', 'website')
	,array('s', 'return_url')
	,array('s', 'analytics_id')
	,array('s', 'report_id')

	,array('i', 'active')
	,array('s', 'date_created')

	,array('s', 'lead_frequency')
	,array('s', 'lead_email')
	,array('s', 'lead_last_sent')

);

//,array('s', 'adwords_tracking_code')