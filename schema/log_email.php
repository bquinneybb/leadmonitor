<?php
/** logging email schema
 * we are keeping logs of emails both for
 * accountability and access to lost emails
 */
$schema = array(
array('i', 'log_email_id_pk', 'insert')
,array('s', 'module_identifier')
,array('s', 'action_identifier')
,array('s', 'email_content')
,array('s', 'email_config')
,array('s', 'date_created', 'insert_now')
);