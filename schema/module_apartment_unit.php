<?php
$schema = array(
	array('i', 'module_apartment_unit_id_pk', 'insert')
	,array('i', 'module_apartment_level_id_pk')
	,array('s', 'uid', 'protected')
	,array('s', 'apartment_name')
	,array('s', 'status')
	,array('s', 'coords', 'value', '[0,0]')
	,array('i', 'active')
);
?>