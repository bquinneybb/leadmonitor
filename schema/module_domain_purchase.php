<?php
$schema = array(
	
	array('i', 'module_domain_purchase_id_pk', 'insert')
	,array('i', 'group_id_pk', 'protected')
	,array('i', 'user_id_pk', 'protected')
	,array('i', 'company_id_pk', 'protected')
	,array('s', 'domain_name', 'protected')
	,array('i', 'registration_period', 'protected')
	,array('s', 'date_created', 'protected')

);