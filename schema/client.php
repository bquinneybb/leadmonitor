<?php
$schema = array(
	array('i', 'client_id_pk', 'insert')
	,array('i', 'group_id_pk', 'protected')
	,array('s', 'client_name')
	,array('s', 'companytype')
	,array('s', 'eligibilitytype')
	,array('s', 'eligibilitynumber')
	,array('s', 'contact_first_name')
	,array('s', 'contact_last_name')
	,array('s', 'address1')
	,array('s', 'address2')
	,array('s', 'suburb')
	,array('i', 'postcode')
	,array('s', 'state')
	,array('s', 'phone')
	,array('s', 'email')
	,array('i', 'active')
);