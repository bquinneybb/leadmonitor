<?php
$schema = array(
	array('i', 'module_site_analysis_id_pk', 'insert')
	,array('i', 'group_id_pk', 'protected')
	,array('i', 'module_campaign_id_pk', 'protected')
	,array('s', 'data')
	,array('i', 'active')
	,array('s', 'date_created', 'insert_now')
);