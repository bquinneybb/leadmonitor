<?php
$schema = array(
	array('i', 'module_email_list_member_id_pk', 'insert')
	,array('i', 'module_email_list_id_pk', 'protected')
	,array('i', 'module_lead_id_pk', 'protected')
	,array('i', 'subscribed', 'protected')
	,array('s', 'date_subscribed')
	,array('s', 'date_unsubscribed')
);