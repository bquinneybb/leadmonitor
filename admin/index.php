<?php
ob_start("ob_gzhandler");
ob_implicit_flush(0);

error_reporting(E_ALL);
ini_set('display_errors','On');

define('ROOT', dirname(dirname(__FILE__)));

/**
 * CORE INCLUDES
 */
include (ROOT . '/_config/config.php');

//require_once (ROOT . '/shared/helpers.php');



// this will divert to the appropriate controller
function callHook(){

	global $url,$subcontentType, $subcontentId, $reservedControllers;

	// reserved controllers



	$urlArray = array();
	$urlArray = explode("/",$url);



	$controller = $urlArray[0];
	$action = $urlArray[1];

	$queryString = $urlArray;

	// if the last component is blank, delete it
	if($queryString[count($queryString)-1] == ''){
		unset($queryString[count($queryString)-1]);

	}
	//print_r($queryString);
	$parameters = array();



	$subcontentId = '';
	if(count($queryString) > 2){

		for($i=2;$i<count($queryString);$i++){

			if($queryString[$i] != ''){

				$parameters[] = $queryString[$i];
			}
		}
		//$subcontentId = $queryString[count($queryString)-1];
	}

	// we are accessing offset 0 and 1 all the time, so set them to avoid trap errors
	if(!isset($parameters[0])){

		$parameters[0] = '';
	}
	if(!isset($parameters[1])){

		$parameters[1] = '';
	}

	$model = ucwords($controller);

	$controllerName = ucwords($controller.'Controller');


	if($controller == 'default'){
		$controller = 'default';
		$controllerName = ucwords('Controller');
		$model = 'Model';

	}


	//echo $action;die();
	//echo $controllerName . '|' . $action;die();


	if (method_exists($controllerName, $action)) {
		$dispatch = new $controllerName($model, $controller, $action);
		call_user_func_array(array($dispatch,$action),array($queryString,$parameters));

	} else {
		/* Error Generation Code Here */
		echo 'THIS CONTROLLER DOESN\'T EXIST :admin/ ' . $controllerName ;die();
	}


}

function bugv($obj, $stop = NULL)
{
	if(!isset($stop))
		$stop = TRUE;

	echo '<pre>';
	print_r($obj);
	echo '</pre><br/>';

	if($stop === TRUE)
		die("end");
}

/**
 * THIRD PARTY INCLUDES WHICH WE'LL USE A LOT
 */


$url = isset($_GET['url']) ? $_GET['url'] : null;

if($url == null){
	$url = '';
	header('location: ' . SITE_PATH . '/admin/home/dashboard');
	die();
}



// clean any urls
foreach($_GET as $get){
	if($get != ''){
		$_GET[$get] = escapeshellarg($get);
	}
}
foreach($_POST as $post){


	if($post != ''){

		$_POST[$post] = escapeshellarg($post);

	} 
}

$session = new SessionManager();
session_start();

callHook();


$templateItems->returnOnly = false;


print_gzipped_page();
ob_flush();

?>