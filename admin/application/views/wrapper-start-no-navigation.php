<?php 
// we are expecting thay they can't view modal windows without being logged in so die if they aren't
if(isset($_SESSION['user']['user_id_pk'])){
	$thisUser = new User();
	$thisUser->getUserSimple($_SESSION['user']['user_id_pk'] ,$_SESSION['user']['group_id_pk'], false);
} else {
	
	die();
}
?>
<?php Template::loadCSS('/public/css/modal.css.php'); ?>
<div class="modal">