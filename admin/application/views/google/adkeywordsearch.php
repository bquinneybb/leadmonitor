<?php 
$adwords = new ModuleAdwords();
$keywordSearch = isset($this->variables['keywordSearch']) ? $this->variables['keywordSearch'] : '';

if($keywordSearch != ''){
	
	$suggestions = $adwords->getRelatedKeywords($keywordSearch, 0, 10);
	//print_r($suggestions);die();
}
?>
<form id="adminform" name="adminform" method="post" action="/admin/google/adkeywordsearch/">
<input type="text" name="keyword_search" id="keyword_search" value="<?php echo sanitiseOutput($keywordSearch);  ?>" />

<?php $templateItems->drawSubmit('SEARCH'); ?>
</form>

<?php if($suggestions){ ?>

<table class="formTable" id="requestList" width="100%">
	<thead>
		<tr>
			<th width="20%">Keyword Suggestion</th>
			<th width="10%">Match Type</th>
			<th width="10%">Monthly Searches</th>
		<th width="60%">CPC Data</th>
		</tr>

	</thead>

	<tbody>
<?php foreach($suggestions as $data){ ?>

		<tr>
			<td><?php echo $data['keywords']; ?></td>
			<td><?php echo $data['search_type']; ?></td>
			<td><?php echo $data['total_search'] ?></td>
			<td>
			Estimated average CPC: $<?php echo money_format('%i', $data['cpc']['average_cpc'] / 1000000) ; ?><br />
			Estimated ad position: <?php echo $data['cpc']['average_position'] ; ?><br />
			Estimated daily clicks: <?php echo $data['cpc']['average_clicks'] ; ?><br />
			Estimated daily cost: $<?php echo money_format('%i', $data['cpc']['total_cost'] / 1000000); ?><br />
			<?php //print_r( $data['cpc']) ?></td>
		</tr>
	
		<?php } ?>
	</tbody>

</table>
<?php } ?>