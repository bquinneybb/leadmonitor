<?php if($ads = $adwords->getAllAdsForAdgroup($adGroupId)){ ?>
	<?php foreach($ads as $ad){ ?>
		<?php if($ad->ad->AdType == 'TextAd'){ ?>
	
			<h1>Headline :  <?php echo sanitiseOutput($ad->ad->headline);  ?></h1>
			<p>Line 1 :  <?php echo sanitiseOutput($ad->ad->description1);  ?></p>
			<p>Line 2 :  <?php echo sanitiseOutput($ad->ad->description2);  ?></p>
			<p>Displayed URL :  <?php echo sanitiseOutput($ad->ad->displayUrl);  ?></p>
			<p>Clickthrough URL :  <?php echo sanitiseOutput($ad->ad->url);  ?></p>
		<?php } ?>
	
		<?php if($ad->ad->AdType == 'ImageAd'){ ?>
		<h1>Headline :  <?php echo sanitiseOutput($ad->ad->name);  ?></h1>
		<table>
			<tr>
			
			<td><img src="<?php echo $ad->ad->image->urls[0]->value ?>" /></td>
			<td><p>Displayed URL :  <?php echo sanitiseOutput($ad->ad->displayUrl);  ?></p>
			<p>Clickthrough URL :  <?php echo sanitiseOutput($ad->ad->url);  ?></p>
			</td>
			</tr>
		
		</table>
		
		<?php } ?>
	<?php } ?>
<?php } ?>