<?php 
$projectId = $this->variables['projectId'];
$apartment = $this->variables['apartments']->{0}; 
//print_r($this->variables);die();
$apartmentsInfo =  new ModuleProjectDeveloperApartments();
//$apartment = $apartmentsInfo->{0};


?>

<div id="container">
	<form id="adminform" method="post" action="/admin/apartments/apartmentsavestatus/<?php echo $projectId ?>/<?php echo $apartment['module_apartment_level_id_pk'] ?>" >
	
	
	<div id="apartments">
	<h1>View Level Plans</h1>
	<p><strong>Building : <?php echo sanitiseOutput($apartment['building_name']); ?></strong></p>
	<p><strong>Level : <?php echo sanitiseOutput($apartment['level_name']); ?></strong></p>
	
	
	</div>
	
	<div id=floorplans>
	<?php $checkFile =  '/fileserver/apartment/' . $apartment['module_project_id_pk'] . '/'. $apartment['module_apartment_level_id_pk'] . '/levelplan.png'; ?>
		<?php if(file_exists(ROOT . $checkFile)){ ?>
		<img src="<?php echo $checkFile ?>" id="levelplan" />
		<?php } else { ?>
		You need to upload a Level Plan
		<?php } ?>
		<div id="floorplanview"></div>
	</div>
	
	<div class="clear"></div>
	
	</form>
</div>



<?php Template::loadCSS('/admin/application/views/apartments/css/levelplanedit.css');?>

<script>

$(document).ready(function(){

	var queryString = 'project=<?php echo $apartment['module_project_id_pk']  ?>';
		queryString += 	'&building=<?php echo $apartment['module_apartment_building_id_pk'] ?>';
		queryString += 	'&level=<?php echo $apartment['module_apartment_level_id_pk'] ?>';
		queryString += SID_STRING;
	//alert('/admin/application/views/apartments/ajax/get-all-floorplans.php?' + queryString);
	floorplansMarkers = new $.UAFloorplans('#floorplanview', {
	
	 	 dataSource : '/admin/application/views/apartments/ajax/get-all-floorplans.php?' + queryString,
	 	 viewDetail : '/admin/apartments/viewfloorplan/',
	 	 toolTipClass : 'tooltip',
	 	imageMapWidth : 930,
	 	imageMapHeight:	690,
	 	imageMapId : '#levelplanmap',
	 	drawContainer: 'floorplanview'
	 
	 	
	 		
	});

});

 	
	
</script>

<?php Template::loadJavascript('/public/javascript/raphael-min.js');?>
<?php Template::loadCSS('/public/javascript/poshytip/tip-darkgray/tip-darkgray.css');?>
<?php Template::loadJavascript('/public/javascript/poshytip/jquery.poshytip.min.js');?>

<?php Template::loadCSS('/admin/application/views/apartments/css/ua-floorplans.css');?>
<?php Template::loadJavascript('/admin/application/views/apartments/javascript/ua-floorplans.js');?>

<?php echo sanitiseOutput($apartment['imagemap']) . "\r\n"; ?>