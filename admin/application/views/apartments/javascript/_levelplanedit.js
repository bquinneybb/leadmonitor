$(document).ready(function(){

		checkDraggable();
		markerAddMenu();
	});
	
	function markerAddMenu(){
	
		$('.status').bind('click', function(){
			
			openMenu($(this));
			
		});
	
		
		$('.status').each(function(){
			addMenu($(this).attr('id'));
		});
	}


	function checkDraggable(){

		if($('#levelplan').length > 0){
		
		if(allowedDrag){

			$('.status').draggable({
				containment: "#container"
					,stop: function() {
						updateMarker($(this).attr('id'), parseInt($(this).css('top'), 10), parseInt($(this).css('left'), 10));
				
			}
			
			});

		}

		}
	}

	function updateMarker(uid, top, left){

		if(left > mapOffset){

			$('#' + uid).data('status', 'placed');
			resortMarkers();
			
			queryString = '';
			queryString += 'project=' + projectId;
			queryString += '&uid=' + uid;
			queryString += '&top=' + top;
			queryString += '&left=' + (left - mapOffset);
			queryString += SID_STRING;
			//alert('/admin/application/views/apartments/ajax/levelplanmarkerupdate.php?' + queryString);
			$.post('/admin/application/views/apartments/ajax/levelplanmarkerupdate.php?' + queryString);
			
			// save it
			
			
		} else {
	
			// snap it back
			$('#' + id).data('status', 'unplaced');
			resortMarkers();
		}
	}

	function resortMarkers(){
		var i=0;
		$('.status').each(function(){
			// reset the position in the queue if it's not placed
			if($(this).data('status') == 'unplaced'){
				
				var yOffset = statusStartY + (i * statusHeight) + (i * statusGap);
				$(this).css('left', '0px').css('top', yOffset + 'px');
				i++;
				
			}

			
			
		});

	//	alert(i);
	}
	
	function addMenu(uid){
		
		var html = '';
		html += '<div class="markerMenu" id="markerMenu_' + uid + '">';
		html += '<strong>Change Unit Status</strong>'
		html += '</div>';
		
		$('#' + uid).append(html);
	}
	
	function openMenu(clickedObject){
		var uid = clickedObject.attr('id');
		$('#markerMenu_' + uid).css('display', 'block');
		
	}
	