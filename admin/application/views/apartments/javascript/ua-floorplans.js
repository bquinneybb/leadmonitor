/* LAST UPDATED 2011/10/17 */
(function($) {

   $.UAFloorplans = function(el, options) {

      var defaults = {
         propertyName: 'value',
         onSomeEvent: function() {}
      }

      var plugin = this;

      plugin.settings = {
    		  
    		
    		  
      }

      var init = function() {
         plugin.settings = $.extend({}, defaults, options);
         
         /* CORE SETTINGS */
         
         imageMapWidth = plugin.settings.imageMapWidth;
         imageMapHeight = plugin.settings.imageMapHeight;
         imageMapId = plugin.settings.imageMapId;
         
        
         dataSource = plugin.settings.dataSource;
         viewDetail = plugin.settings.viewDetail;
         externalContainer = $(el);
         internalContainerClass = '#uafloorplans';
         internalContainer = $('#uafloorplans');
         drawContainer = plugin.settings.drawContainer;
         statusClass = '#uafloorplaneditor .status';
         toolTipClass = plugin.settings.toolTipClass;
         
         lastMarkerClicked = 0;
         
         /* SETTING SHARED WITH ADMIN FUNCTIONS */
       
     
         //el = plugin.settings.el;
        // alert(el);
         // code goes here
         getFloorplanData();
      
        
       
      }

      /* GET WHAT WE NEED */
      var getFloorplanData = function() {
    	  
    	  $.get(dataSource, function(data){
    		 
    		  if(data){
    			  
    			 // we have data so we can draw them
    			  setInitialStyles(data);
    			  
    			 
    			  
    			  
    		  } else {
    			  
    			  alert('Could not load floorplans');
    		  }
    		  
    	  });
    	 
      }
      /* SET UP THE INITIAL  STRUCTURE */
      
     
      var setInitialStyles = function(data){
    	
    	  parseFloorplanData(data);
    	  
    	 // var markerHTML = drawFloorplanMarkers(data);
    	
    	 // alert(markerHTML[0]);
    	  
    	  // add to the main window
    	  externalContainer.css('position', 'relative').html('<div id="uafloorplans" style="z-index:5"></div>');
    	  internalContainer = $('#uafloorplans');
    	  internalContainer.css('position', 'absolute');
    	  
    	  createCanvas();
    	  convertImageMapToSVG();
    	  $('svg').css('z-index', 10);
    	 
    	   $('path').each(function(){
    			  
    			  var id = $(this).attr('id');
    			  var idParts = id.split('_');
    			$(this).attr('title','Unit&nbsp;' + idParts[1]).poshytip({
				className: 'tip-darkgray',
				followCursor : true,
				bgImageFrameSize: 9
			});
    			  
    	  });
      }
      
      /* DRAW AND STYLE THE MARKERS */
      
      var createCanvas = function(){
    	
    	 floorplanCanvas = Raphael(drawContainer,  imageMapWidth, imageMapHeight);
    	// floorplanCanvas = Raphael('floorplans', 0,0, imageMapWidth, imageMapHeight);
    	
      }	
      
      var convertImageMapToSVG = function(){
    	

    	
    	  $(imageMapId + ' area').each(function(){
    		  
    		
    		  
    		  var coords =  $(this).attr('coords');
    		  var id = $(this).attr('href');
    		
	    		  if(markerArray[id] != undefined){
	    		 
	    		  switch($(this).attr('shape')){
	    		  
	    		 case 'poly' :
	    		 
	    			  drawPolygon(coords, id);
	    		  break;
	    		  
	    		  default :
	    			  
	    			  drawRect(coords, id);
	    			  
	    			  break;
	    		  }
	    		  
	    	  }
    	  });
    	  
    	 
    	 

      }	
      
      var drawPolygon = function(coords, id){
    	  
    
    	  
    	  var cordPairs = coords.split(',');
    	  var delimiter = ' ';
		  var svgString = 'M';
		  
    		  for(var i=0;i<cordPairs.length/2;i++){
    			  
    			  svgString += cordPairs[i*2] + delimiter + cordPairs[(i*2) + 1] + ' ';
    			  svgString += 'L';
    			    			  
    		  }
    		  
    		  svgString += cordPairs[0] + delimiter + cordPairs[1] + ' ';
    		  var shape = floorplanCanvas.path(svgString);
    		  setMarkerAttributes(shape, id);
    		 
      }
      
      var drawRect = function(coords, id){
    	  
    	  var cordPairs = coords.split(',');
    	  var delimiter = ',';
    	  
    	
    	  var shape = floorplanCanvas.rect(cordPairs[0], cordPairs[1], cordPairs[2], cordPairs[3]);
    	 
    	  setMarkerAttributes(shape, id);
		  
      }
      
      var setMarkerAttributes = function(shape, id){
    	
    	  
    	  var fillColour = fillMarker(id);
		  
		  shape.attr("fill", fillColour);
		  shape.attr("fill-opacity", 0.5);
		  shape.node.id = 'uamarker_' + id;
		
		
		  if(markerArray[id] != 'sold'){
		  
			  shape.node.onclick = bindMarkerClick;
		  	shape.node.onmouseover = bindMarkerMouseOver;
		  	shape.node.onmouseout = bindMarkerMouseLeave;
		  }
		  	
		  
      }
      
      var fillMarker = function(id){
    	  
    	 var status = markerArray[id];
    
    	 switch(status) {
    
    	 	
    	 case 'sold' : 
    		 
    		 return 'red';
    		 
    		 break;
    		 
    		 case 'available' : 
    		 
    			 return 'green';
    		 
    		 break;
    		 
    		 default :
    			 
    			 return 'yellow';
    		 
    		 break;
    		 
    	 }
    	  
      }
      
      var bindMarkerClick = function(){
    	 
    	var floorplanId =  $(this).attr('id').split('_');
  	  
    		//$.fancybox({href:viewDetail + '?floorplan=' + floorplanId[1], type: 'iframe'});
			$.fancybox({href:'/posi/FP_Example.png'});
    	 

      }
      
     
      var bindMarkerMouseOver = function(){
    	  var floorplanId =  $(this).attr('id').split('_');
    	  //if(markerArray[floorplanId] != 'sold'){
    		  
    		  $('body').css('cursor', 'pointer');
    	//  }
      }
     
      var bindMarkerMouseLeave = function(){
    	
    		  
    		  $('body').css('cursor', 'default');
    	  
      }
      
      var parseFloorplanData = function(data){
    	  
    	  var jsonData = jQuery.parseJSON(data);
    	  var l=jsonData.length;
    	  
    	   markerArray = new Array();
    	  
    	 // need to add coords check
    	  for(i=0;i<l;i++){
    	
    		 if(jsonData[i].status != ''){
    		  
    		  var id = jsonData[i].id;
    		  var status = jsonData[i].status;
    		  markerArray[id] = status;
    		
    		 // jsonArray.id = id;  
    		 
    		 }
    		 
    	  }
    
    	  
      }
      
      
      var drawFloorplanMarkers = function(data){
    	  //* DEPRECATED */
    	  var  html = '';
    	
     	  var jsonData = jQuery.parseJSON(data);
    	  var l=jsonData.length;
    	 // need to add coords check
    	  for(i=0;i<l;i++){
    		 
    		  var id = jsonData[i].id;
    		  var status = jsonData[i].status;
    		  var coords = jsonData[i].coords.split(',');
    		  
    		  var html = '';
    		  
    		
    	  
    	  }
    	 return html;
    	 //return activeHTML;
    	  
      }
      var bindStatusMarkers = function(){
    	  
    	  $(statusClass).each(function(){
    		  
    		  // add the tooltip jquery function
    		  $(this).tipTip();
    		 
    		  if($(this).data('placement') == 'unplaced'){
    			  
    			  setCursorAction($(this)); 
    		  }
    		  
    		  if($(this).data('placement') == 'placed'){
    		  
    			  $(this).bind('click', function(){
	  		  			
		  			  lastMarkerClicked = $(this).data('id');
		  			  
	  		  		});
    			  
    			  $(this).draggable();
    			  
		    	  if($(this).data('status') != 'sold'){
		    			
		    		setCursorAction($(this)); 
		  		  	setRolloverAnimations($(this));
		  		  	// open the window if we're not in editing mode
		  		  	
		  		  
		  		  	
			  		  	$(this).bind('click', function(){
			  		  		
			  		  	  $.fancybox({href:viewDetail + '?floorplan=' + $(this).data('id'), type: 'iframe'});
			  		  	
			  		  	});
		  		  	
		  		  
		  		  	
		  		  
		  	  	}
		    	  
    	  	}
    	  
    	  });
      }
      var fixStatusText = function(){
    	  
    	  var statusHeight = $(statusClass).height();
    	  $(statusClass + ' .statusText').each(function(){
    		 
    		  $(this).css('padding-top', (statusHeight / 2) - ($(this).height()/2));
    		  
    	  });
    	 
      }
      
      var setRolloverAnimations = function(object){
    	  
    	 object.bind('mouseenter', function(){
    		 
    		 object.addClass('hover');
    	 
    	 });
    	 
    	 object.bind('mouseleave', function(){
    		 
    		 object.removeClass('hover');
    	 
    	 });
    	  
      }
      
      /* MISC HELPER FUNCITONS */
      var setCursorAction = function(object){
    	    
      	
    	  
    	  object.bind('mouseenter', function(){
    			
    		  $('body').css('cursor', 'pointer');
    	  });
    	  
    	  
    	  object.bind('mouseleave', function(){
    		  
    		  $('body').css('cursor', 'default');
    	  
    	  });
	  
	  
	  
  }
      
      
      
      
      /* PUBLIC FUNCTIONS */
      plugin.destroyMarkers = function() {
          // code goes here
    	  
    	  floorplanCanvas.clear();
    	  $('#' + drawContainer).html(); 
    	  $('svg').css('opacity', 0).remove();
    	  
       }


      init();

   }

})(jQuery);