<?php if(isset($this->variables['projectId'])){ ?>	
<?php
$projectId = $this->variables['projectId'];
$apartmentsInfo =  new ModuleProjectDeveloperApartments();

$development = $this->variables['apartments']; 
//print_r($apartments);
$project = $this->variables['project']; 
?>
<!--  view the buildings in this project -->
<h1>Buildings in  <?php echo sanitiseOutput($project['project_name']); ?></h1>
<table class="formTable" id="currentCampaigns" width="100%">
	<thead>
		<tr>
			<th width="10%">Building</th>
		
		</tr>

	</thead>
	<tbody>
	<?php if($development){ ?>
	<?php foreach($development as $building){ ?>
	<?php //print_r($building['levels']);die();?>
	<tr>
	<td><?php echo sanitiseOutput($building['building_name'])?></td>
	</tr>
	<?php } ?>
	<?php } ?>
	</tbody>
	</table>
<?php //print_r($development); ?>
<!--  view the buildings in this project -->
<!--  view apartments and level is this project -->
<h1>Levels and Apartments for <?php echo sanitiseOutput($project['project_name']); ?></h1>

<table class="formTable" id="currentCampaigns" width="100%">
	<thead>
		<tr>
			<th width="10%">Buidling</th>
			<th width="10%">Level</th>
			<th width="20%">Apartment</th>
			<th width="40%">Status</th>
			<th width="20%">Level Plan</th>
		</tr>

	</thead>

	<tbody>
	<?php if($development){ ?>
	<?php foreach($development as $building){ ?>
	<?php //print_r($building['levels']);die();?>
	<tr>
	<td colspan="5"><strong><?php echo sanitiseOutput($building['building_name'])?></strong></td>
	</tr>
	<?php //print_r(($building['levels'])); ?>
	<?php foreach($building['levels'] as $level){?>
	<tr>
			<td>&nbsp;</td>
			<td colspan="3"><a href="/admin/apartments/leveledit/<?php echo $level['apartments'][0]['module_apartment_level_id_pk'] ?>"/><?php echo sanitiseOutput($level['level_name']) ?></a></td>
			<td>
			<?php 
			$checkFile = ROOT . '/fileserver/apartment/' . $level['apartments'][0]['module_project_id_pk'] . '/' . $level['apartments'][0]['module_apartment_level_id_pk'] . '/levelplan.png';
		
			//	echo $checkFile;
			$linkText =  file_exists($checkFile) ? 'View Level Plan' : 'No Plan Uploaded';
			$linkURL=  '/admin/apartments/levelplanview/' .  $projectId . '/' .  $level['apartments'][0]['module_apartment_building_id_pk'] .'/' . $level['apartments'][0]['module_apartment_level_id_pk'];
			if(file_exists($checkFile)){ $templateItems->createIframeModalWindowLink($linkText, $linkURL , 700, 1100); } else { echo $linkText; }
			?>
			</td>
		</tr>
		
		<?php if($level['apartments']){ ?>
		<?php foreach($level['apartments'] as $apartment){ ?>
		<?php $apartmentStatus = isset($apartmentsInfo->aparmentStatus[$apartment['status']][0]) ? $apartmentsInfo->aparmentStatus[$apartment['status']][0] : '&nbsp;';?>
		<?php $linkURL = '/admin/apartments/apartmentedit/' . $apartment['module_project_id_pk']  . '/' . $apartment['module_apartment_unit_id_pk']; ?>
		<tr>
			<td></td>
			<td></td>
			<td><?php if(file_exists($checkFile)){ $templateItems->createIframeModalWindowLink($apartment['apartment_name'], $linkURL , 700, 1100); } else { echo '&nbsp;'; }?></td>
			<td><?php if(file_exists($checkFile)){ $templateItems->createIframeModalWindowLink($apartmentStatus, $linkURL , 700, 1100); } else { echo '&nbsp;'; }?></td>
			
			<td></td>
		</tr>
		
		
		<?php } ?>
		<?php } ?>
		
		<?php } ?>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>
<!--  /view apartments and level is this project -->
<?php } else { ?>
<!--  select project first -->
<?php $projects = $this->variables['projects']; ?>
<?php //print_r($projects); ?>
<h1>You must select a Project first</h1>
<h2>Select a Project to view the Apartments in</h2>
<table class="formTable" id="currentCampaigns" width="100%">
	<thead>
		<tr>
			<th width="20%">Project Name</th>

		</tr>

	</thead>

	<tbody>
	<?php if($projects){ ?>
	<?php foreach($projects as $project){ ?>
	<?php $editLink = '/admin/apartments/dashboard/' . $project['module_project_id_pk'] ?>
		<tr>
			<td><a href="<?php echo $editLink ?>"><?php echo sanitiseOutput($project['project_name']) ?></a></td>

		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>
<!--  /select project first -->
<?php } ?>