<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$projectId = isset($_POST['project']) ? $_POST['project'] : null;
$buildingId = isset($_POST['building']) ? $_POST['building'] : null;
$levelId = isset($_POST['level']) ? $_POST['level'] : null;



if($projectId == null || $buildingId == null || $levelId == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();

$level = new ModuleProjectDeveloperApartments();
$apartments = $level->findAllBuildingsAndApartments($projectId, $buildingId, $levelId);
$floorplans = array();
$i = 0;
foreach($apartments as $apartment){
	
	$floorplans[$i]['id'] = $apartment['module_apartment_unit_id_pk'];
	$floorplans[$i]['status'] = $apartment['status'];
	$i++;
	
}
//$floorplans[0]['id'] = $apartment->{0}['module_apartment_unit_id_pk'];
//$floorplans[0]['status'] = $apartment->{0}['status'];

echo json_encode($floorplans);