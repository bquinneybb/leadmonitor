<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$projectId = isset($_POST['project']) ? $_POST['project'] : null;
$apartmentId = isset($_POST['apartment']) ? $_POST['apartment'] : null;


if($projectId == null || $apartmentId == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();

$level = new ModuleProjectDeveloperApartments();
$apartment = $level->findAllBuildingsAndApartments($projectId, '', '', $apartmentId, true);
$floorplans = array();


$floorplans[0]['id'] = $apartment->{0}['module_apartment_unit_id_pk'];
$floorplans[0]['status'] = $apartment->{0}['status'];

echo json_encode($floorplans);