<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$projectId = isset($_POST['project']) ? $_POST['project'] : null;
$uid = isset($_POST['uid']) ? $_POST['uid'] : null;
$top = isset($_POST['top']) ? $_POST['top'] : null;
$left = isset($_POST['left']) ? $_POST['left'] : 0;


if($projectId == null || $uid == null || $top == null || $left == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();

$apartments = new ModuleApartments();
$apartments->levelPlanMarkerUpdate($projectId, $uid, $top, $left);