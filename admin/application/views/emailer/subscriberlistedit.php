<?php 
$subscribers = $this->variables['subscribers'];
$listId = $this->variables['listId'];
$totalAdded = $this->variables['totalAdded'];
$listName = $this->variables['listName'];
//print_r($subscribers);
?>

<form id="adminform" name="adminform" method="post" action="/admin/emailer/subscriberlistupdate/<?php echo $listId ?>">
	<?php $templateItems->drawUpdateNotice(); ?>
	<?php if($totalAdded == 0 && $totalAdded != null){ ?>
		<span class="errorMessage">Not all subscribers were added - some may have been duplicates or had requested permanent removal from the mailing list/s</span>
		<?php } ?>
		
	<?php if($totalAdded >= 1 && $totalAdded != null){ ?>
		<span class="errorMessage">You added <?php echo $totalAdded ?> subscribers</span>
		<?php } ?>	
<div class="formActions">

<div class="formAction"><a href="javascript:checkUnsubscribeBeforeSubmit();">Update Subscriber Status</a></div>
<?php 
	$templateItems->returnOnly = true;
	$link = $templateItems->createIframeModalWindowLink('Add New Subscribers', '/admin/emailer/subscriberpasteaddress/' . $listId, 400, 300);
	$templateItems->returnOnly = false;
?>
<div class="formAction"><?php echo $link; ?></div>
</div>
<!--  subscribers -->
<?php $templateItems->drawFromTableHeader('Edit Subcriber List : ' . $listName, 'user', 'subscriberLists')?>
<table class="formTable" id="subscriberLists" width="100%">
	<thead>
		<tr>
			
			<th width="10%"><a onclick="toggleSubscriptions();">Select All</a>
				<?php $templateItems->drawHidden('subscriber_toggle', 0); ?>
				</th>
			<th width="20%">Subscriber Name</th>
			<th>Subscriber Email</th>
			<th>Subscriber Filters</th>
		</tr>

	</thead>

	<tbody>
	<?php if($subscribers){ ?>
	<?php foreach($subscribers as $subscriber){ ?>
	<?php $editLink = '/admin/newsletter/subscriberedit/' . $subscriber['module_email_list_member_id_pk'] ?>
		<tr>
			<td><?php $templateItems->drawCheckbox('db_subscribed_' . $subscriber['module_email_list_member_id_pk'],  $subscriber['module_email_list_member_id_pk'], $subscriber['module_email_list_member_id_pk']); ?></td>
			<td><?php echo sanitiseOutput($subscriber['lastname'] . ', ' . $subscriber['firstname']) ?></td>
			<td><?php echo sanitiseOutput($subscriber['email_address']) ?></td>
			<td><a href="/admin/emailer/subscriberassigntags/<?php echo $subscriber['module_email_list_member_id_pk'] ?>" >Edit Subsriber Filters</a></td>
		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>
</form>
<!--  /subscribers -->
<?php  Template::loadJavascript('/admin/application/views/emailer/javascript/subscriber-list-edit.js'); ?>