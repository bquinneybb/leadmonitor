<?php
$subscribersIds = array(1);


$listBuilder = new ModuleLeadsListBuilder();
//print_r($listBuilder);
?>
<?php
if($subscribersIds){ 
	foreach($subscribersIds as $subscribersId){
	//print_r($subscriber);
		$subscribers = $listBuilder->findAllSubscriberTags($subscribersId);
	//	print_r($subscribers);
?>
<form id="adminform" name="adminform" method="post" action="/admin/emailer/subscriberupdatetags/">
	<div class="formActions">
<!--
<div class="formAction"><a href="javascript:document.adminform.submit();">Update Tags</a></div>
-->
</div>
	
	
<?php $templateItems->drawFromTableHeader('Edit Subcriber Filters : ', 'user', 'subscriberLists')?>
<table class="formTable" id="subscriberLists" width="100%">
	<thead>
		<tr>
			
			
			<th width="20%">Subscriber Name</th>
			<th>Subscriber Email</th>
			<th>Subscriber Filters</th>
		</tr>

	</thead>

	<tbody>

<?php

$totalSubscribers = 0;
	 if($subscribers){
		foreach($subscribers as $subscriber){
		
	//	print_r($subscribers);die();
		?>
	
<tr>
	
	<input type="hidden" class="subscriber_id" value="<?php echo $subscriber['module_email_list_member_id_pk']; ?>" />
	
	<td><?php echo sanitiseOutput($subscriber['lastname'] . ', ' . $subscriber['firstname']) ?></td>
	<td><?php echo sanitiseOutput($subscriber['email_address']) ?></td>
	<td>
		<!-- tags -->
		<?php if($subscriber['module_leads_list_categories_id_pk']){
	
	
	foreach($subscriber['module_leads_list_categories_id_pk'] as $tagId){
	//	echo $tagId;
		$tagData= $listBuilder->convertTagIdToString($tagId);
	//	print_r($tagData);
		$dbId = $tagData['tag_type'] == 'suburb' ? $tagData['tag_db_id'] : '';
		
?>

<div id="tagCloud">

<div class="tagCloud" data-tagType="<?php echo $tagData['tag_type']; ?>" data-tagId="<?php echo $tagData['tag_id']; ?>" data-dbId="<?php echo $dbId ?>" title="<?php echo sanitiseOutput($tagData['tag_label']); ?>"><?php echo sanitiseOutput($tagData['tag_string']); ?></div>
	</div>
<?php		
		
	}
	$totalSubscribers++;
} // end if tags

?>
<!-- /tags -->
	</td>			
</tr>
	<?php
	}
} // end if subscribers
?>


	</tbody>
</table>

<?php	
	}
} // end subscribers ?>
<input name="totalSubscribers" value="<?php echo $totalSubscribers; ?>" type="hidden" />
</form>

<!-- suburb selector -->
<div style="display:none">




<div id="suburbSelector">
	<?php
$suburbList = $listBuilder->findAllSuburbs('vic');

?>
	<div class="suburbSelectorHeader">Not Selected</div><div class="suburbSelectorHeader">Selected</div>
	<div class="clear"></div>
	<select name="suburb_select_b" id="suburb_select_b" size="10" class="suburbSelectorContent" multiple="multiple" onchange="select_hop_new('b',0 ,'suburb_select_a', 'suburb_select_b', 'suburbs');">
		
		
	<?php if($suburbList){
		//print_r($suburbList);
		foreach($suburbList as $suburb){
			
			?>
		
		<option value="<?php echo $suburb['data_postcode_id_pk'] ?>"><?php echo sanitiseOutput($suburb['suburb'] . ' : ' . $suburb['state']); ?></option>	
			<?php
		}
	}
	
	?>
		
	</select>
	
	<select name="suburb_select_a" id="suburb_select_a" size="10" class="suburbSelectorContent" multiple="multiple" onchange="select_hop_new('a',0 ,'suburb_select_a', 'suburb_select_b', 'suburbs');">
		
		
		</select>
	
	<div class="clear"></div>
		<div id="suburbSelectorFinished"><a href="javascript:closeSuburbEditor()">FINISHED</a></div>
	
	
</div>
</div>
<!-- /suburb selector -->




<script src="/admin/application/views/emailer/javascript/subscriber-assign-tags.js"></script>
