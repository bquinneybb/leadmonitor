<?php 
$subscriberLists = $this->variables['subscriberLists'];
?>
<?php $templateItems->drawFromTableHeader('Subcriber Lists', 'eblast', 'subscriberLists')?>
<form id="adminform" name="adminform" method="post">
<!--  campaigns -->
<table class="formTable" id="subscriberLists" width="100%">
	<thead>
		<tr>
			
			<th width="20%">List Name</th>
			<th width="20%">Total Subscribers</th>
			
		</tr>

	</thead>

	<tbody>
	<?php if($subscriberLists){ ?>
	<?php foreach($subscriberLists as $subscriberList){ ?>
	<?php $editLink = '/admin/emailer/subscriberlistedit/' . $subscriberList['module_email_list_id_pk'] ?>
		<tr>
			<td><a href="<?php echo $editLink; ?>"><?php echo sanitiseOutput($subscriberList['email_list_label']) ?></a></td>
			<td><a href="/admin/projects/dashboard/<?php echo sanitiseOutput($subscriberList['email_list_label']) ?>"><?php echo sanitiseOutput($subscriberList['subscribers']) ?></a></td>
		
		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>