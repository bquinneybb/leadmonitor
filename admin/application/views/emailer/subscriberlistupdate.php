<?php
$subscribers = $this->variables['subscribers'];
$listId = $this->variables['listId'];
$action = isset($_POST['confirm']) ? $_POST['confirm'] : null;


?>
<?php if($action == 'true'){ ?>
	<?php $totalRemoved = $this->variables['totalRemoved']; ?>
	<h1>You removed <?php echo $totalRemoved; ?> subscribers from your list</h1>
	
	
	<?php } else { ?>
		

	<form id="adminform" name="adminform" method="post"  action="/admin/emailer/subscriberlistupdate/<?php echo $listId ?>">

<div class="formActions">

<div class="formAction"><a href="javascript:document.adminform.submit()">Confirm Deletion of Subscribers</a></div>


</div>


	<h1>Please confirm you want to remove the following people from the list</h1>

		<?php $templateItems->drawHidden('confirm', 'true'); ?>
	
	<table class="formTable" id="subscriberLists" width="100%">
	<thead>
		<tr>
			
			<th width="10%"><a onclick="toggleSubscriptions();">Select All</a>
				<?php $templateItems->drawHidden('subscriber_toggle', 0); ?>
				</th>
			<th width="20%">Subscriber Name</th>
			<th>Subscriber Email</th>
		</tr>

	</thead>

	<tbody>
	
	<?php if($subscribers){ ?>
		
		
		
		
		<?php foreach($subscribers as $subscriber){ ?>
			
			<?php if(!isset($_POST['db_subscribed_' . $subscriber['module_email_list_member_id_pk']])){ ?>
				
				
				<tr>
			<td><?php $templateItems->drawCheckbox('db_subscribed_' . $subscriber['module_email_list_member_id_pk'],  $subscriber['module_email_list_member_id_pk'], $subscriber['module_email_list_member_id_pk']); ?></td>
			<td><?php echo sanitiseOutput($subscriber['lastname'] . ', ' . $subscriber['firstname']) ?></td>
			<td><?php echo sanitiseOutput($subscriber['email_address']) ?></td>
		
		</tr>
				
			
			<?php } ?>
			
		<?php } ?>
	
	<?php } ?>
	
	</tbody>
	
	</table>

<?php } ?>
</form>