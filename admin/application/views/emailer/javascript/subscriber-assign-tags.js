subscribers = new Array();
$(document).ready(function(){
	
	bindTags();
	showInitiallySelectedSuburbs();
	setSubscriberIDs();
});
function bindTags(){
	
		$('.tagCloud').each(function(){
		
		$(this).bind('click', function(){
			
			editTag($(this));
			
		});
		
		
	});
	
	
}


function editTag($tag){
	
	//alert($tag.data('tagtype'));
	switch($tag.data('tagtype')){
		
		case  'suburb' :
		
			showSuburbEditor($tag);
		
		break;
		
		
		default :
			
			showStringEditor($tag);
		
		break;
		
	}
}

function setSubscriberIDs(){
	
	$('.subscriber_id').each(function(){
		
		subscribers.push($(this).val());
	});
	
	
}

function showStringEditor($tag){
	//alert($tag.data('tagid'));
	var tagToEdit = $("div").find("[data-tagid='" + $tag.data('tagid') +"']");
	//alert(tagToEdit.data('tagid'));
	var html = '<input id="textTagEdit" class="textTagEdit" value="' + tagToEdit.html() + '" style="width:' + tagToEdit.width() + 'px" />'
	tagToEdit.html(html);
	tagToEdit.unbind('click');
	
	$('#textTagEdit').bind('blur', function(){
		
		tagToEdit.html($('#textTagEdit').val());
		
		saveTagChanges($tag);
		
		// save it
		bindTags();
		
	}).focus();
	// unbind it
	
}

function showInitiallySelectedSuburbs(){
	var existingSuburbs = new Array();
	
	
	
	
	//var element = document.getElementById('suburb_select_a[]');
	//element.options[1].selected = false;
	 //element.options[1].selected = true;
	
	$('.tagCloud').each(function(){
		
		if($(this).data('tagtype') == 'suburb'){
			
				existingSuburbs.push(parseInt($(this).data('dbid'), 10));
			
		}
	});
	//alert(existingSuburbs);
	thisSwapper = $('#suburb_select_b');
		var i = 0;
 	 $('#suburb_select_b option').each(function(){
 	 	
//console.log(jQuery.inArray($(this).val(), existingSuburbs) + "<br />");
   			if(jQuery.inArray(parseInt($(this).val(), 10), existingSuburbs) != -1){
   				//alert(i);
   				//alert($('#suburb_select_b option:eq('+ i + ')').val());
   				$('#suburb_select_b option:eq('+ i + ')').attr('selected', 'selected');
   			//	thisSwapper.options[i].selected = true;
   			}
   			
   			i++;
		});
  
	
	select_hop_new('b',0 ,'suburb_select_a', 'suburb_select_b', 'suburbs');
	addSuburbTagToFilters();
}

function addSuburbTagToFilters(){
	var i = 0;

	// add any missing ones
	$('#suburb_select_a option').each(function(){
	
		if( $('div[data-tagtype="suburb"][data-dbid="' + $(this).val() + '"]').length == 0){
			
			var html = '<div class="tagCloud" data-tagtype="suburb" data-tagid="1" data-dbid="' + $(this).val() + '" title="Suburb">' + $(this).text() + '</div>';
			//alert(html);
			$('#tagCloud').append(html);
		}
		
		
	});
	
	// remove any extra ones
		$('#suburb_select_b option').each(function(){
	
		if( $('div[data-tagtype="suburb"][data-dbid="' + $(this).val() + '"]').length == 1){
			
			
			 $('div[data-tagtype="suburb"][data-dbid="' + $(this).val() + '"]').remove();
		}
		
		
	});
	
}

function saveSuburbChanges(){
	
	
}


function saveTagChanges($tag){
	
	
}


function showSuburbEditor($tag){
	
		
		$.fancybox({'href' : '#suburbSelector', 'type': 'inline', 'width': 500, 'height' : 300});
	
	
}

function closeSuburbEditor(){
	$.fancybox.close();
	
}

function select_hop(box, whole, leftName, rightName, masterName) {
	masterString = "";
	master = document.getElementById(masterName);
	
	
	if (box == 'a') {
		var a = document.getElementById(leftName);
		var b = document.getElementById(rightName);
	} else if (box == 'b') {
		var a = document.getElementById(rightName);
		var b = document.getElementById(leftName);
	}
	for (i = a.options.length -1; i >= 0; i--) {
		
		
		
		if (whole == 1 || a.options[i].selected) {
			//alert(a.options[i].text + ': ' + a.options[i].value);
			
			
			b.options[b.options.length] = new Option(a.options[i].text, a.options[i].value, true, false);
			
			a.remove(i);
		}
	}
	
	realB = document.getElementById(rightName);	
	for (i = 0; i < realB.options.length; i++) {
			
		
			masterString += realB.options[i].value + ";";
			
	}
	
	
	master.value = masterString;
	
	a.blur();
}

function select_hop_new(box, whole, leftName, rightName, masterName) {
	//UBA 27/5/10 - new select hop function, hopefully resolves the front-end value mismatch bug while making things more efficient
	var a = $('#' + ((box == 'a') ? leftName : rightName).replace('[]', '\\[\\]'));
	var b = $('#' + ((box == 'b') ? leftName : rightName).replace('[]', '\\[\\]'));
	//if whole box, select all
	if (whole == 1) {
		$(a).find('option').attr('selected', true);
	}
	//move options
	$(a).find(':selected').each(function() {
		$(b).append('<option value="' + $(this).val() + '">' + $(this).text() + '</option>');
		
		$(this).remove();
	});
	//sort
	var suburbs = $(b).children().get();
	suburbs.sort(function(a, b) {
	   return ($(a).text() < $(b).text()) ? -1 : (($(a).text() > $(b).text()) ? 1 : 0);
	});
	$.each(suburbs, function(index, itm) {
		$(b).append(itm);
	});
	//update master
	var master = $('#' + masterName);
	$(master).val('');
	$('#' + rightName).find('option').each(function() {
		$(master).val($(master).val() + $(this).val() + ';');
	});
	/*var mylist = $('#myUL');
	var listitems = mylist.children('li').get();
	listitems.sort(function(a, b) {
	   var compA = $(a).text().toUpperCase();
	   var compB = $(b).text().toUpperCase();
	   return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
	})
	$.each(listitems, function(idx, itm) { mylist.append(itm); });*/
	
	addSuburbTagToFilters();
}

