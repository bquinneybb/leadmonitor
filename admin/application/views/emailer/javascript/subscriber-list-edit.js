$(document).ready(function(){
	
	$('#adminform').bind('submit', function(){
		
		
		checkUnsubscribeBeforeSubmit();
		return false;
	});

	

})

function toggleSubscriptions(){
		
	
		if($('#subscriber_toggle').val() == 0){
			
			$('#subscriberLists input:checkbox').each(function(){
				
				$(this).attr('checked',false);
				
			});
				
				
			$('#subscriber_toggle').val(1);
		
		} else {
			
			$('#subscriberLists input:checkbox').each(function(){
				
				$(this).attr('checked',true);
				
			});
			
			$('#subscriber_toggle').val(0);
		}
		
	}
	
function checkUnsubscribeBeforeSubmit(){
	
	var totalUnchecked = 0;
	$('#subscriberLists input:checkbox').each(function(){
		
			
			if(!$(this).attr('checked')){
				
				totalUnchecked++;
				
			}
	});
	
	if(totalUnchecked > 0){
		var question = confirm('Are you sure you wish to unsubscribe ' + totalUnchecked + ' people?');
		if(question){
			
			document.adminform.submit();
			
		} else {
			
			
		}
		
	} else {
		
		document.adminform.submit();
	}
}
