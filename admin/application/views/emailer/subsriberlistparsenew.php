<?php

$listId = $this->variables['listId'];

// split up the pastes email address into a list
$newAddress = isset($_POST['newaddresses']) ? $_POST['newaddresses'] : null;

if($newAddress == null){
	
	// go somewhere with an error
	die('Need some addresses in here');
	
}

$addressList = array();
// remove the extra whitespace
$newAddress = str_replace(', ', ',', $newAddress);
$hasNewline = false;
// do we have newline and return
if(strstr($newAddress, "\r\n")){
	
	$newAddress = explode("\r\n", $newAddress);
	$hasNewline = true;
	
} elseif(strstr($newAddress, "\r")){
	
	$newAddress = explode("\r", $newAddress);
	$hasNewline = true;
} else {
	
	$newAddress = explode("\n", $newAddress);
	$hasNewline = true;
}

foreach($newAddress as $address){
	
	
	$addressList[] = explode(',', $address);
}

?>
<form id="adminform" name="adminform" method="post" action="/admin/emailer/subscriberlistaddnew/<?php echo $listId; ?>">
<h1>Please confirm addition of <span class="subscriberCount"><?php echo count($addressList); ?></span> new subscribers</h1>
<?php $templateItems->drawSubmit('Add <span class="subscriberCount">' .  count($addressList) . '</span> Addresses to list'); ?>
<div class="clear"></div>

<ul>
	<?php 
	$i = 0;
	foreach($addressList as $address){ ?>
		
		<?php if(!isset($address[0]) || !isset($address[1]) || !isset($address[2])){ ?>
			
			<li><span class="error">This address is not valid : <?php echo implode('|', $address); ?></span></li>
			
		<?php } else { ?>
		
			<li><?php echo $address[0] ?> <?php echo $address[1] ?> : <?php echo $address[2] ?></li>
			<input type="hidden" name="address_<?php echo $i ?>" value="<?php echo implode('|', $address); ?>" />
			<?php	$i++; ?>
		<?php } ?>

<?php	} ?>
	
</ul>

<input type="hidden" name="total" id="total" value="<?php echo $i; ?>" />
</form>
<script src="/admin/application/views/emailer/javascript/subscriber-list-parse-new.js"></script>
