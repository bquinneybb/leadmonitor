var totalDataFields = parseInt($('#email_form_data_total').val(), 10);
var totalRecipientFields = parseInt($('#email_form_recipient_total').val(), 10);

$(document).ready(function(){
	
	checkDomainSubmitted();
	checkDetailEdit();
	
	$('#db_campaign_type').bind('change', function(){
		
		checkDetailEdit();
		
	});
});


function checkDetailEdit(){
	
	var urls = $('#campaignediturls').val();
	urls = urls.split('|');
	
	
	var campaignType = parseInt($('#db_campaign_type').val(), 10);
	//alert(urls[4]);
	if(urls[campaignType] != ''){
		
		$('#campaigndetails').html('<a href="/admin' + urls[campaignType] + '">EDIT</a>');
	} else {
		
		$('#campaigndetails').html('Has no editable components');
	}
	//campaigndetails
}




function addNewDataField(){
	queryString = 'offset=' + totalDataFields;
	queryString += SID_STRING;
	//alert('/admin/application/views/campaigns/ajax/add-form-data-dropdown.php?' + queryString);
	$.post('/admin/application/views/campaigns/ajax/add-form-data-dropdown.php?' + queryString, function(data){
		
		if(data){
			
		//	alert(data);
			totalDataFields++;
			 $('#form_data_fields').append(data);
			 $('#email_form_data_total').val(totalDataFields);
			
		
		}
	});
	
	
}

function addNewCustomDataField(){
	queryString = 'offset=' + totalDataFields;
	queryString += SID_STRING;
	//alert('/admin/application/views/campaigns/ajax/add-form-data-dropdown.php?' + queryString);
	/*
	$.post('/admin/application/views/campaigns/ajax/add-form-data-dropdown.php?' + queryString, function(data){


		
		
		if(data){
			
		//	alert(data);
			totalDataFields++;
			 $('#form_data_fields').append(data);
			 $('#email_form_data_total').val(totalDataFields);
			
		
		}
	});
	*/



	var newfield 	=	'';
	newfield 	+=	'<div class="formLeft" id="form_data_left_' + totalDataFields + '">Captured Field</div>';
	newfield 	+=	'<div class="formRight" id="form_data_right_' + totalDataFields + '">';
	newfield 	+=	'<input type="text" id="form_data_custom_identifier_' + totalDataFields + '" name="form_data_custom_identifier_' + totalDataFields + '" value="" class=" text-field " placeholder="field name" >&nbsp;';
	newfield 	+=	'<input type="text" id="form_data_custom_label_' + totalDataFields + '" name="form_data_custom_label_' + totalDataFields + '" value="" class=" text-field " placeholder="Label" >&nbsp;';
	newfield 	+=	'<input type="text" id="form_data_custom_type_' + totalDataFields + '" name="form_data_custom_type_' + totalDataFields + '" value="text" class=" text-field " placeholder="field type" >&nbsp; : ';
	newfield 	+=	'<a href="javascript:deleteDataField(' + totalDataFields + ')">DELETE</a></div>';
	newfield 	+=	'<div class="clear"></div>';

	totalDataFields++;

	$('#form_data_fields').append(newfield);
	$('#email_form_data_total').val(totalDataFields);
	
}

function deleteDataField(id){

	$('#form_data_' + id).val('');
	$('#form_data_custom_identifier_' + id).val('');
	$('#form_data_custom_label_' + id).val('');
	$('#form_data_custom_type_' + id).val('');
	$('#form_data_left_' + id).css('display', 'none');
	$('#form_data_right_' + id).css('display', 'none');
	
}
function deleteEmailField(id){

	$('#form_recipient_email_' + id).val('');
	$('#form_recipient_name_' + id).val('');

	$('#form_email_left_' + id).css('display', 'none');
	$('#form_email_right_' + id).css('display', 'none');
	
	$('#form_name_left_' + id).css('display', 'none');
	$('#form_name_right_' + id).css('display', 'none');
}


function addNewRecipientField(){
	
	queryString = 'offset=' + totalRecipientFields;
	queryString += SID_STRING;
	//alert('/admin/application/views/campaigns/ajax/add-form-data-dropdown.php?' + queryString);
	$.post('/admin/application/views/campaigns/ajax/add-form-recipient-field.php?' + queryString, function(data){
		
		if(data){
			
		//	alert(data);
			totalRecipientFields++;
			 $('#form_recipient_fields').append(data);
			 $('#email_form_recipient_total').val(totalRecipientFields);
			
		
		}
	});
	
}

function checkDomainSubmitted(){
	
	
	var url = $('#db_website').val();
	
	queryString = 'url=' + url;
	queryString += SID_STRING;
	
//	alert('/admin/application/views/campaigns/ajax/domain-check-searchengine.php?' + queryString);
	$.post('/admin/application/views/campaigns/ajax/domain-check-searchengine.php?' + queryString, function(data){
		
		if(data){
			
			$('#searchenginestatus').html(data);
			
		
		} 
	});
}