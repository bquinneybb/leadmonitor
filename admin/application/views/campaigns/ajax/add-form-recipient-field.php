<?php
include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();

$currentOffset = isset($_POST['offset']) ? (int)$_POST['offset'] : 0;
$formData = new ModuleCampaignsForm();
$dropdown = $formData->drawBlankRecipientField($currentOffset);
echo ($dropdown);