<?php 
$analytics = $this->variables['analytics'];
//print_r($analytics);
?>
<?php if($analytics){ ?>
	<?php foreach($analytics as $key=>$data){ ?>
	<h1>Total Visits for: <?php echo date('d M Y', strtotime($key)); ?> : <?php echo $data['totalVisits']; ?></h1>
	<h2>Total Time on Site : <?php echo sec2hms ($data['totalTime'], true)?></h2>
	<h2>Average Time on Site : <?php echo sec2hms ($data['averageTime'], true)?></h2>
	<?php } ?>
	<hr />
	<h1>Visitor Country</h1>
		<?php foreach($data['country'] as $country=>$cities){ ?>
		<h2><?php echo sanitiseOutput($country); ?></h2>
			<ul>
			<?php foreach($cities as $city=>$totalCity){ ?>
			<li><?php echo sanitiseOutput($city); ?> (<?php echo $totalCity ?>)</li>
			<?php } ?>
			</ul>
		<?php } ?>
		
	<h1>Referring Site</h1>	
		<ul>
		<?php foreach($data['referralPath'] as $referrer=>$totalReferrer){ ?>
		
		<li><?php echo sanitiseOutput($referrer); ?> (<?php echo $totalReferrer ?>)</li>
		
		<?php } ?>
		</ul>
		
			<h1>Pages Viewed</h1>	
		<ul>
		<?php foreach($data['pagePath'] as $pagesViewed=>$totalPagesViewed){ ?>
		
		<li><?php echo sanitiseOutput($pagesViewed); ?> (<?php echo $totalPagesViewed ?>)</li>
		
		<?php } ?>
		</ul>
<?php } ?>