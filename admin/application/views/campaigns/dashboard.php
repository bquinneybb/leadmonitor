
<?php
$projectId = isset($this->variables['projectId']) ? $this->variables['projectId'] : false;
?>

<?php
$campaigns = $this->variables['campaigns'];

//print_r($campaigns);
?>
<h1>Current Campaigns <?php if($projectId){ echo sanitiseOutput(' for the Project ' . $campaigns->{0}['project_name']); } ?></h1>
<form id="adminform" name="adminform" method="post">
<!--  campaigns -->
<table class="formTable" id="currentCampaigns" width="100%">
	<thead>
		<tr>
			<th width="20%">Campaign Name<br/>Campaign String</th>
			<th width="20%">Project Name</th>
			
			<th width="20%">Client Name</th>
			<th width="20%">View Leads</th>
			
			<th width="20%">View Analytics</th>
		</tr>
	</thead>

	<tbody>
	<?php if($campaigns){ ?>
	<?php foreach($campaigns as $campaign){ //print_r($campaign); die; ?>
	<?php $editLink = '/admin/campaigns/campaignedit/' . $campaign['module_campaign_id_pk'] ?>
		<tr>
			<?php $campaignName = $campaign['campaign_name'] == '' ? '[un-named campaign]' : $campaign['campaign_name']; ?>
			<td><a href="<?php echo $editLink ?>"><?php echo sanitiseOutput($campaignName) ?></a></td>
			<td><a href="/admin/projects/projectedit/<?php echo $campaign['module_project_id_pk'] ?>"><?php echo sanitiseOutput($campaign['project_name']) ?></a></td>
		
			<td><?php echo sanitiseOutput($campaign['client_name']); ?>
			<td><a href="/admin/leads/dashboard/<?php echo $campaign['module_project_id_pk'] ?>">View Leads</a></td>
		
			<td><a href="/admin/campaigns/campaignviewanalytics/<?php echo $campaign['module_campaign_id_pk'] ?>">View Analytics</a></td>

		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>
<!--  /campaigns -->


</form>
