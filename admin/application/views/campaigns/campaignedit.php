<?php
//include(ROOT . '/admin/application/shared/tinymce.php'); 

$autoResponder = $this->variables['autoresponder'];
if($campaign = $this->variables['campaign']){

$campaigns = new ModuleCampaigns();	
$campaignForms  = new ModuleCampaignsForm();

$campaignForm = $campaignForms->findCampaignForm($campaign['module_campaign_form_id_pk'], $campaign['client_id_pk']);

$formList = $templateItems->createList('module_campaign_form', 'module_campaign_form_id_pk', 'module_campaign_form_id_pk', '', '`module_campaign_form_id_pk`', "`client_id_pk` = '". (int)$campaign['client_id_pk'] . "'");


	?>
<h1>Edit Campaign : <?php echo sanitiseOutput($campaign['campaign_name']); ?><?php $templateItems->drawUpdateNotice(); ?></h1>

<h2>Site has been submitted to Search Engine : <span id="searchenginestatus"></span></h2>

<form id="adminform" name="adminform" method="post" action="/admin/campaigns/campaignsave/<?php echo $campaign['module_campaign_id_pk'] ?>">

	<?php $templateItems->drawSubmit('Save')?><br />
	<br /><br /><br />
	<?php echo $templateItems->drawFormLeft('Export Leads'); ?>
	<?php $leadsLink = '<a href="http://leadmonitor.com.au/_crontasks/download-lead-data.php?campaignid=' .  $campaign['module_campaign_id_pk'] . '" target="_blank">DOWNLOAD</a>'; ?>
	<?php echo $templateItems->drawFormRight($leadsLink); ?>
		
	<?php echo $templateItems->drawFormLeft('View Leads'); ?>
	<?php $viewleadsLink = '<a href="http://leadmonitor.com.au/_crontasks/view-lead-data.php?campaignid=' .  $campaign['module_campaign_id_pk'] . '" target="_blank">VIEW</a>'; ?>
	<?php echo $templateItems->drawFormRight($viewleadsLink); ?>

	<?php echo $templateItems->drawFormLeft('Email Leads'); ?>
	<?php $emailleadsLink = '<a href="http://leadmonitor.com.au/_crontasks/email-lead-data.php?campaignid=' .  $campaign['module_campaign_id_pk'] . '" target="_blank">EMAIL</a>'; ?>
	<?php echo $templateItems->drawFormRight($emailleadsLink); ?>

	<hr />
	<h2>Automated Leads Email Details</h2>

	<?php 
		$campaignTypes = array(
			"off" => "Off",
			"weekly" => "Weekly",
			"monthly" => "Monthly"
		);
	?>
	<?php $templateItems->drawFormList('db_lead_frequency', $campaignTypes, $campaign['lead_frequency'],  false, 'Email Frequency'); ?>
	<?php $templateItems->drawFormText('db_lead_email',$campaign['lead_email'], 'Email Address'); ?>

	<hr />
	<h2>Campaign Information</h2>
	
	<?php $templateItems->drawFormCheckbox('db_active', 1, $campaign['active'], 'Active'); ?>
	
	<?php //$clientList = $templateItems->createList('client', 'client_id_pk', 'client_name', false, '`client_name`', "`group_id_pk` = '" . $_SESSION['user']['group_id_pk'] . "'" )?>
	<?php //$templateItems->drawFormList('db_client_id_pk', $clientList, $campaign['client_id_pk'],  false, 'Client'); ?>
	<?php $templateItems->drawFormString($campaign['client_name'], 'Client Name'); ?>
	
	<?php $templateItems->drawHidden('db_client_id_pk',$campaign['client_id_pk']); ?>
	<?php $templateItems->drawFormText('db_campaign_name', $campaign['campaign_name'], 'Campaign Name'); ?>
	
	
	<?php echo $templateItems->drawFormLeft('Campaign Banner'); ?>
	<?php if (file_exists($_SERVER['DOCUMENT_ROOT'].'/fileserver/banners/'.$campaign['module_campaign_form_id_pk'].'.png')) { $banner =  '<img src="/fileserver/banners/'.$campaign['module_campaign_form_id_pk'].'.png" />'; } else { $banner = 'No file uploaded'; }	?>
	<?php echo $templateItems->drawFormRight($banner); ?>
	
	<?php 
		$campaignTypes = array();
		$campaignEditURL = array();
		$i = 0;
		foreach( $campaigns->campaignTypes as $label=>$edit){
			
			$campaignTypes[$i] = $label;
			$editVal = $edit != '' ? $edit . $campaign['module_campaign_id_pk']: '';
 			$campaignEditURL[$i] = $editVal;
			
			
			$i++;	
		}
		
	?>

	<?php $templateItems->drawFormList('db_campaign_type', $campaignTypes, $campaign['campaign_type'],  false, 'Campaign Type'); ?>
	
	
	<?php $templateItems->drawFormString('<div id="campaigndetails"></div><input type="hidden" id="campaignediturls" value="' . implode($campaignEditURL, '|') . '">', 'Edit Campaign Details'); ?>
	
	<?php $webAddress = $campaign['website'] == '' ? 'http://' : $campaign['website'] ?>
	<?php $templateItems->drawFormText('db_website', $webAddress, 'Website Address'); ?>
	<?php $returnAddress = $campaign['return_url'] == '' ? 'http://' : $campaign['return_url']; ?>
	<?php $templateItems->drawFormText('db_return_url', $returnAddress, 'Thank You Page'); ?>
	
	<?php $templateItems->drawFormText('db_analytics_id', $campaign['analytics_id'], 'Google Analytics Site ID'); ?>
	<?php $templateItems->drawFormText('db_report_id', $campaign['report_id'], 'Google Analytics Report ID'); ?>
	<?php //$templateItems->drawFormTextarea('db_adwords_tracking_code', $campaign['adwords_tracking_code'], 'Google Adwords Tracking Code (if different from Project)'); ?>	
	<?php $templateItems->drawFormText('campaign_string', encrypt($campaign['module_campaign_id_pk']), 'Campaign Secret String'); ?>
	<?php $templateItems->drawFormText('form_string', encrypt($campaign['module_campaign_form_id_pk']), 'Form Secret String'); 

	//die(decrypt('vaIdbktefy2Im36jAiTQJvYIUJLN4my0086ycu/Q9ig='));

	?>
	<hr />
	<h2>Form Information</h2>
	<span class="error">Isn't adding new form fields yet...</span><br />
	<?php if($campaignForm){ ?>
		
	
	<?php $templateItems->drawHidden('db_module_campaign_form_id_pk',$campaign['module_campaign_form_id_pk']); ?>
			
			<?php foreach(json_decode($campaignForm['form_data'], true) as $key=>$value){ ?>
		
		<?php switch($key){
			
			
				
			case 'recipient' :
				echo '<div id="form_recipient_fields">';
				echo '<h3>Recipients</h3>';
				
				$campaignForms->drawRecipientFields($value);
				echo '</div>';
				echo '<a href="javascript:addNewRecipientField();">Add New Recipient Field</a><br />';
				
				
				break;
				
				case 'headers' :
				echo '<h3>Email Headers</h3>';
				$campaignForms->drawHeaderFields($value);
				
				break;
				
				case 'data' :
					echo '<h3>Captured Fields</h3>';
				echo '<div id="form_data_fields">';
				$campaignForms->drawDataFields($value);
				echo '</div>';
				echo '<a href="javascript:addNewDataField();">Add New Standard Data Capture Field</a><br /><br />';
				echo '<a href="javascript:addNewCustomDataField();">Add New CUSTOM Data Capture Field</a><br /><br />';
				
				break;
				
			default :
				
				
				break;
			
		}
		?>
		
			
		<?php } ?>
		
		
	<?php } ?>
	
	<hr />
	
	<h2>AutoResponder</h2>
	
	<?php $templateItems->drawFormText('auto_subject', $autoResponder['subject'], 'Subject'); ?>
	<?php $templateItems->drawFormText('auto_from_email', $autoResponder['from_email'], 'From Email'); ?>
	<?php $templateItems->drawFormText('auto_from_name', $autoResponder['from_name'], 'From Name'); ?>
	<?php $templateItems->drawFormTextArea('auto_html', $autoResponder['html'], 'Message'); ?>
	<?php //echo $autoResponder['html']; ?>
	
	
	<?php $templateItems->drawSubmit('Save')?>
	<br/><br/>

	<style>
		#auto_html {
			
			height: 500px;
		}
	</style>				
<br />

</form>

<?php }  else { ?>

<h1>Error</h1>
<h2>The campaign you selected either doesn't exist or doesn't belong to you.</h2>
<?php } ?>
<script src="/admin/application/views/campaigns/javascript/campaign-edit.js"></script>