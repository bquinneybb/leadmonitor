<?php
include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$campaignId = isset($_POST['campaignId']) ? $_POST['campaignId'] : null;
$captions = isset($_POST['captions']) ? json_decode(stripcslashes($_POST['captions']), true) : null;
$urls = isset($_POST['urls']) ? json_decode(stripcslashes($_POST['urls']), true) : null;
$sources = isset($_POST['sources']) ? json_decode(stripcslashes($_POST['sources']), true) : null;
$images = isset($_POST['imageGallery']) ? $_POST['imageGallery'] : null;

//print_r($sources);
if($campaignId == null || $images == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();

//print_r($_POST);

// put everything back in the correct order

$campaign = ModuleFacebook::findAdminFacebookCampaignByCampaignId($campaignId);
$imageGallery = json_decode($campaign['image_gallery'], true);

$updatedImageGallery = array();
$i = 0;
foreach($images as $image){
	
	$imageKey = str_replace('gallery_', '', $image);
	
	$updatedImageGallery[$imageKey]['image'] = $sources[$imageKey];
	$updatedImageGallery[$imageKey]['caption'] = $captions[$imageKey];
	$updatedImageGallery[$imageKey]['url'] = $urls[$imageKey];
	
	$i++;
}

asort($updatedImageGallery);
$imageGallery['images'] = $updatedImageGallery;
//print_r($imageGallery);
$query = "UPDATE `module_campaign_facebook` SET `image_gallery` = ? WHERE `module_campaign_id_pk` = ?";
$parameters = array(json_encode($imageGallery), $campaignId);
$dataTypes = 'si';
Database::dbQuery($query, $dataTypes, $parameters);
?>