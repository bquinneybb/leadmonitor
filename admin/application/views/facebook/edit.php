<?php
if($campaign = $this->variables['campaign']){
	
?>

<form id="adminform" name="adminform" method="post" action="/admin/facebook/save/<?php echo $campaign['module_campaign_id_pk'] ?>">

<?php $templateItems->drawFormTextarea('db_html', $campaign['html'], 'HTML'); ?>
<?php $templateItems->drawFormTextarea('db_form_data', $campaign['form_data'], 'Form'); ?>
<?php } else { ?>
	
	<h1>There is no Facebook campaign for this item</h1>
	<?php } ?>

<hr />
<h1>Image Gallery</h1>
<?php  $gallery = json_decode($campaign['image_gallery'], true); ?>
	
	<table id="imageGallery">

<?php if($gallery['images']){ ?>
	<?php $i = 0; ?>
	<?php foreach($gallery['images'] as $image){ ?>
	
	<tr id="gallery_<?php echo $i ?>">
		<td><img src="/fileserver/facebook/<?php echo $campaign['module_campaign_id_pk'] ?>/gallery/<?php echo $image['image'] ?>" class="imageGallery" data-source="<?php echo $image['image'] ?>" /></td>
		<td><?php $templateItems->drawText('db_gallerycaption_' . $i,  $image['caption']); ?></td>
		<td><?php $templateItems->drawText('db_galleryurl_' . $i,  $image['url']); ?></td>
	</tr>
	<?php $i++; ?>
	<?php } ?>
<?php } ?>

	</table>
	<?php $templateItems->drawSubmit(); ?>

</form>
<script>
	
	var campaignId = '<?php echo $campaign['module_campaign_id_pk'] ?>';
	var totalGalleryItems = <?php echo $i ?>;
	
	
	
</script>

<script type="text/javascript" src="/min/?f=/public/javascript/tinymce/jscripts/tiny_mce/jquery.tinymce.js,/admin/application/views/facebook/javascript/edit.js"></script>
