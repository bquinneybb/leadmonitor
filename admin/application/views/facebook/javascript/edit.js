$(function() {
	
	/* BIND THE FORM TO UPDATE THE GALLERY FIELDS */

	/* GALLERY */
	// Initialise the second table specifying a dragClass and an onDrop function that will display an alert
	$("#imageGallery").tableDnD({
		onDragClass : "myDragClass",

		onDrop : function(table, row) {

			var updateURL = gatherReorderData();
		//	alert(updateURL);

			$.post(updateURL);

		}
	});

	/* TEXT AREAS */

	$('#db_html').css({
		'height' : '400px',
		'width' : '520px'
	}).addClass('tinymce');
	$('#db_form_data').css({
		'height' : '400px',
		'width' : '520px'
	}).addClass('tinymce');

	$('textarea.tinymce').tinymce({
		// Location of TinyMCE script
		script_url : '/public/javascript/tinymce/jscripts/tiny_mce/tiny_mce.js',

		// General options
		theme : "advanced",
		plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "/min/?f=public/modules/facebook/css/core.css,/fileserver/facebook/" + campaignId + "/css/core.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

	});
});


function gatherReorderData() {

	var sources = new Array();
	var captions = new Array();
	var urls = new Array();

	for(var i = 0; i < totalGalleryItems; i++) {

		captions.push($('#db_gallerycaption_' + i).val());
		urls.push($('#db_galleryurl_' + i).val());
		sources.push($('.imageGallery').eq(i).data('source'));
	}

	var serialized =  $.tableDnD.serialize();
	
	

	var updateURL = "/admin/application/views/facebook/ajax/reorder-gallery.php?campaignId=" + campaignId + "&" + serialized + '&captions=' + JSON.stringify(captions) + '&urls=' + JSON.stringify(urls) + '&sources=' + JSON.stringify(sources) + SID_STRING;
	//	alert(updateURL);

	return updateURL;
}