<?php
$campaigns = $this->variables['campaigns'];
$clientId = isset($this->variables['clientId']) ? true : false;
?>
<h1>Current Facebook Campaigns <?php if($clientId){ echo sanitiseOutput(' for the ' . $campaigns->{0}['client_name']); } ?></h1>
<form id="adminform" name="adminform" method="post">
<!--  campaigns -->
<table class="formTable" id="currentCampaigns" width="100%">
	<thead>
		<tr>
			<th width="20%">Campaign Name</th>
			<th width="20%">Client Name</th>
			
		
		</tr>

	</thead>

	<tbody>
	<?php if($campaigns){ ?>
	<?php foreach($campaigns as $campaign){ ?>
	<?php $editLink = '/admin/facebook/edit/' . $campaign['module_campaign_id_pk'] ?>
		<tr>
			<?php $campaignName = $campaign['campaign_name'] == '' ? '[un-named campaign]' : $campaign['campaign_name']; ?>
			<td><a href="<?php echo $editLink ?>"><?php echo sanitiseOutput($campaignName) ?></a></td>
			
		
			<td><?php echo sanitiseOutput($campaign['client_name']); ?>
			

		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>
<!--  /campaigns -->

</form>
