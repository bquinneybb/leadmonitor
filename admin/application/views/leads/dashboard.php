<?php

$newLeads = $this->variables['leads'];
//print_r($newLeads);
?>
<form id="adminform" name="adminform" method="post">
<?php $templateItems->drawFromTableHeader('New Leads', 'leads', 'newLeads')?>

<table class="formTable" id="newLeads" name="newLeads" width="100%">
	<thead>
		<tr>
			<th width="20%">Name</th>
			<th width="15%">Email</th>
			<th width="20%">Date</th>
			<?php if(!SINGLE_CLIENT_MODE){ ?>
			<th width="25%">Project</th>
			<th width="25%">Campaign</th>
			<?php } ?>
			
			
		</tr>

	</thead>

	<tbody>
	<?php if($newLeads){ ?>
	<?php foreach($newLeads as $lead){ ?>
	<?php $editLink = '/admin/leads/leadedit/' . $lead['module_campaign_lead_id_pk'] ?>
		<tr>
			<td><a href="<?php echo $editLink ?>"><?php echo sanitiseOutput($lead['lead_name']) ?></a></td>
			<td><a href="<?php echo $editLink ?>"><?php echo sanitiseOutput($lead['lead_email']) ?></a></td>
			<td><a href="<?php echo $editLink ?>"><?php echo sanitiseOutput($templateItems->dateToString($lead['date_created'])) ?></a></td>
			<?php if(!SINGLE_CLIENT_MODE){ ?>
			<td><a href="/admin/projects/projectedit/<?php echo $lead['module_project_id_pk'] ?>"><?php echo sanitiseOutput($lead['project_name']) ?></a></td>
			<td><a href="/admin/campaigns/campaignedit/<?php echo $lead['module_campaign_id_pk'] ?>"><?php echo sanitiseOutput($lead['campaign_name']) ?></a></td>
			<?php } ?>
			
			
		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>

</form>