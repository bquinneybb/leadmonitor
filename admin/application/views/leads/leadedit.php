<h1>THIS WILL NEED DIFFERENT FIELDS DEPENDING ON WHETHER IT'S A DEVELOPMENT OR COMMERCIAL</h1>
<?php
if($lead = $this->variables['lead']){ 
$leads = new ModuleCampaignsLeads();
//print_r($lead);
//print_r($lead);

//$investorTypes = $templateItems->setToArray('module_lead', 'investor_type');

//print_r($leads);
?>
<h1>Campaign Name : <?php echo sanitiseOutput($lead['campaign_name']); ?><?php $templateItems->drawUpdateNotice(); ?></h1>

<form id="adminform" name="adminform" method="post" action="/admin/leads/leadsave/<?php echo $lead['module_campaign_lead_id_pk'] ?>">
	<h2>Campaign Information</h2>
	
	<?php $templateItems->drawFormCheckbox('db_active', 1, $lead['active'], 'Active'); ?>
	<?php
	foreach($lead['data'] as $data){
		
		switch( $data['form_stucture'][2]){
			
case 'checkbox':
	$templateItems->drawFormCheckbox('lead_'. $data['form_stucture'][0], 'Yes', $data['form_result'], $data['form_stucture'][1]);
	
	//$templateItems->drawFormText('lead_'. $data['form_stucture'][0], $data['form_result'], $data['form_stucture'][1]);
	
break;

default:
	
	$templateItems->drawFormText('lead_'. $data['form_stucture'][0], $data['form_result'], $data['form_stucture'][1]);
	
	break;
			
		}
		
			
			}
	
	?>
	

	
	<?php $templateItems->drawFormList('db_leadsource', $leads->leadSource, $lead['leadsource'], true, 'Lead Source')?>
	<?php //$templateItems->drawFormList('db_investor_type', $leads->investorTypes, $lead['investor_type'], true, 'Investor Type')?>
	
	<?php $templateItems->drawFormCheckbox('db_followed_up', 1, $lead['followed_up'], 'Followed Up')?>
	<?php $templateItems->drawFormString(ModuleCampaignsLeads::convertRefererToKnownSite($lead['referer_url']), 'Referring Source')?>
	
	
	
		<?php $templateItems->drawFormTextArea('db_my_comments', $lead['my_comments'], 'My notes'); ?>

	

	<!--  apartment data -->	
	<br /><br />
	
	<?php if($lead['project_type'] == 'apartment'){ ?>
	<?php 
			$allApartments = $this->variables['allApartments'];
			$leadApartments = $this->variables['leadApartments'];
			$leadApartmentsClean = array();
			if($leadApartments){
				foreach($leadApartments as $leadApartment){
					
					$leadApartmentsClean[] = $leadApartment['module_apartment_unit_id_pk'];
					
				}
			}
	
	?>
	
	<?php $templateItems->drawFormLeft('Interested in these apartments'); ?>
	<?php 
	$html = <<<EOD
	<div style="float:left;margin-right: 20px">
	<strong>All Apartments</strong><br />
<select name="apts_a" id="apts_a" class="" multiple="multiple" onchange="select_hop('a', 0,'apts_a', 'apts_b', 'apts_interested');" style="width:200px;height:400px;">
	
EOD;


//print_r($allApartments);
if($allApartments){
	foreach($allApartments as $allApartment){	
		
		if(!in_array($allApartment['module_apartment_unit_id_pk'], $leadApartmentsClean)){
			
			$html .= '<option value="' . $allApartment['module_apartment_unit_id_pk'] . '">' 
					. sanitiseOutput($allApartment['building_name'] . ':' . $allApartment['level_name'] . ':' . $allApartment['apartment_name']) 
					. '</option>'; 
		}
	}
}
$html .= '</select>';
$html .= '</div>';

	$html .= <<<EOD
	<div style="float:left;margin-right: 20px;">
		<strong>Interested</strong><br />
<select name="apts_b" id="apts_b" class="" multiple="multiple" onchange="select_hop('b', 0,'apts_a', 'apts_b', 'apts_interested');"  style="width:200px;height:400px;">

EOD;
if($allApartments){
	foreach($allApartments as $allApartment){	
		
		if(in_array($allApartment['module_apartment_unit_id_pk'], $leadApartmentsClean)){
			
			$html .= '<option value="' . $allApartment['module_apartment_unit_id_pk'] . '">' 
					. sanitiseOutput($allApartment['building_name'] . ':' . $allApartment['level_name'] . ':' . $allApartment['apartment_name']) 
					. '</option>'; 
		}
	}
}
$html .= '</select>';
$html .= '</div>';


	?>
	<?php echo $templateItems->drawFormLeft('Interested in these apartments'); ?>
	<?php echo $templateItems->drawFormRight($html); ?>
	<?php $templateItems->drawHidden('apts_interested', ''); ?>		
	
	<?php } // end if apartment project ?>
	<!--  /apartment data -->
	
	<?php $templateItems->drawSubmit('Save')?>

</form>
<?php Template::loadJavascript('/admin/application/views/leads/javascript/leadedit.js'); ?>
<?php }  else { ?>

<h1>Error</h1>
<h2>The lead you selected either doesn't exist or doesn't belong to you.</h2>
<?php } ?>