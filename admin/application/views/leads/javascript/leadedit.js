function select_hop(box, whole, leftName, rightName, masterName) {
	
	var a = $('#' + ((box == 'a') ? leftName : rightName).replace('[]', '\\[\\]'));
	var b = $('#' + ((box == 'b') ? leftName : rightName).replace('[]', '\\[\\]'));
	//if whole box, select all
	if (whole == 1) {
		$(a).find('option').attr('selected', true);
	}
	//move options
	$(a).find(':selected').each(function() {
		$(b).append('<option value="' + $(this).val() + '">' + $(this).text() + '</option>');
		
		$(this).remove();
	});
	//sort
	var suburbs = $(b).children().get();
	suburbs.sort(function(a, b) {
	   return ($(a).val() < $(b).val()) ? -1 : (($(a).val() > $(b).val()) ? 1 : 0);
	});
	$.each(suburbs, function(index, itm) {
		$(b).append(itm);
	});
	//update master
	var master = $('#' + masterName);
	$(master).val('');
	$('#' + rightName).find('option').each(function() {
		$(master).val($(master).val() + $(this).val() + ';');
	});
	
}
