<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


	<meta name="keywords" content="<?php if(isset($contentItem->meta_keywords)){ echo sanitiseOutput($contentItem->meta_keywords); } ?>">
	<meta name="description" content="<?php if(isset($contentItem->meta_description)){ echo sanitiseOutput($contentItem->meta_description); } ?>">

<title><?php  if(isset($contentItem->pageTitle)){ echo $contentItem->pageTitle; } ?></title>
<?php
// some applications don't need all this code e.g Urban Motion players, so supress this big load
if(!isset($this->variables['supressDefaultCodebase']) || $this->variables['supressDefaultCodebase'] == false){

	
	
	
	
/*
	Template::loadCSS('/public/javascript/jquery-ui/css/sunny/jquery-ui-1.8.16.custom.css');
	//Template::loadCSS('/public/javascript/colorbox/colorbox.css');
	Template::loadCSS('/public/javascript/fancybox/jquery.fancybox-1.3.4.css');
	Template::loadCSS('/public/javascript/scrollpane/jScrollPane.css');
	Template::loadCSS('/public/css/boilerplate.css');
	
*/
// but always load jquery as pretty much everything uses it
//Template::loadJavascript('/public/concatenated/js_concat_head.js');
/*
Template::loadJavascript('/public/javascript/modernizr-2.0.6.min.js');
Template::loadJavascript('/public/javascript/jquery/jquery.min.js');
Template::loadJavascript('/public/javascript/jquery-ui/js/jquery-ui-min.js');
//Template::loadJavascript('/public/javascript/jquery.tmpl.js');
Template::loadJavascript('/public/javascript/less-1.1.3.min.js');
Template::loadJavascript('/public/javascript/jquery.easing.1.3.js');
*/
//jquery.fancybox-1.3.4.pack.js
?>
<link rel="stylesheet" type="text/css" href="/min/?f=
/public/javascript/jquery-ui/css/sunny/jquery-ui-1.8.16.custom.css
,/public/javascript/fancybox/jquery.fancybox.css
,/public/javascript/scrollpane/jScrollPane.css
,/public/css/boilerplate.css">
<link rel="stylesheet/less" type="text/css" href="/public/css/core.less">
<link rel="stylesheet/less" type="text/css" href="/public/css/modules.less">
<script src="/min/?f=public/javascript/modernizr-2.0.6.min.js
,public/javascript/jquery/jquery.min.js
,public/javascript/jquery-ui/js/jquery-ui-min.js
,public/javascript/jquery.tmpl.js
,public/javascript/less-1.1.3.min.js
,public/javascript/jquery.easing.1.3.js
,public/javascript/fancybox/jquery.fancybox.pack.js"></script>
<?php } ?>

<script type="text/javascript">
/* Set up some variables used site wide */
SITE_PATH = '<?php echo SITE_PATH ?>';
SID_STRING = <?php $templateItems->drawSidString(); ?>;
</script>
<?php

	// load predefined assets
		//echo 'c = ' . $controller;die();
		if(file_exists(ROOT . '/admin/application/views/' . $this->controller .'/css/core.css')){
			
			Template::loadCSS('/admin/application/views/' . $this->controller . '/css/core.css');
		}
?>		
</head>
<body>
<div id="dump"></div>