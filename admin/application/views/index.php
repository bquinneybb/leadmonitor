<?php
if(!in_array($this->model, $this->noPermissionRedirect)){



	User::checkUserLoggedIn('admin');
}

if(!isset($_SESSION['user'])){
	
	$_SESSION['user'] = array();
}

if(isset( $_SESSION['user']['logged_in'])){

	// if they are, do they have permission to view this page
	if(!in_array($this->model, $this->noPermissionRedirect)){
		// grab their credentials
		$thisUser = new User();
		$thisUser->getUserSimple($_SESSION['user']['user_id_pk'] ,$_SESSION['user']['group_id_pk']);
		$thisUser->checkPagePermissions($thisUser, 'read', $this->action, $this->permissions);
	}
}

// set some variables that we'll use on every page
$templateItems = new TemplateItems();
$_SESSION['system']['current_controller'] = $this->_controller;



if(isset($_SESSION['user']['user_id_pk'])){
	$thisUser = new User();
	$thisUser->getUserSimple($_SESSION['user']['user_id_pk'] ,$_SESSION['user']['group_id_pk'], false);
} else {
	$thisUser = new User();
	$thisUser->getUserSimple(1,1, false);

}


?>