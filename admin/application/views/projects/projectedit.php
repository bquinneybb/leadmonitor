<?php
if($project = $this->variables['project']){ ?>
<h1>Edit Project : <?php echo sanitiseOutput($project['project_name']); ?><?php $templateItems->drawUpdateNotice(); ?></h1>

<form id="adminform" name="adminform" method="post" action="/admin/projects/projectsave/<?php echo $project['module_project_id_pk'] ?>">
	<h2>Project Information</h2>
	
	<?php $templateItems->drawFormCheckbox('db_active', 1, $project['active'], 'Active'); ?>
	
	<?php $clientList = $templateItems->createList('client', 'client_id_pk', 'client_name', false, '`client_name`', "`group_id_pk` = '" . $_SESSION['user']['group_id_pk'] . "'" )?>
	<?php $templateItems->drawFormList('db_client_id_pk', $clientList, $project['client_id_pk'], false, 'Client'); ?>
	<!--  save the current client id in case it changes at save -->
	<?php $templateItems->drawHidden('db_client_id_pk_existing', $project['client_id_pk'])?>
	<?php $templateItems->drawFormText('db_project_name', $project['project_name'], 'Project Name'); ?>
	
	
	
	<?php 
	$projectTypes = array();
	$projectTypesUnkeyed = $templateItems->setToArray('module_project', 'project_type');
		foreach($projectTypesUnkeyed as $projectType){
			$projectTypes[$projectType] = ucwords($projectType);
		}
	?>
	<?php $templateItems->drawFormList('db_project_type', $projectTypes, $project['project_type'], false, 'Project Type'); ?>
	
	<?php $templateItems->drawSubmit('Save')?>
	
	</form>
	
	<hr />
	<form id="createcampaign" name="createcampaign" action="/admin/campaigns/campaigncreate/<?php echo $project['module_project_id_pk'] ?>/<?php echo $project['client_id_pk'] ?>" method="post">
	<?php $templateItems->drawHidden('db_client_id_pk_existing_b', $project['client_id_pk']); // saves the initial client code so we can check it if it's been updated upon save ?>
	+ <a href="javascript:document.createcampaign.submit()">Create New Campaign</a>
	</form>
	<hr />
	<h1>Campaigns in this Project</h1>

<table class="formTable" id="currentCampaigns" width="100%">
	<thead>
		<tr>
			<th width="20%">Campaign Name</th>
			
			<th width="20%">View Leads</th>
			
			<th width="20%">View Analytics</th>
		</tr>

	</thead>

	<tbody>
	<?php if($this->variables['campaigns']){ ?>
	<?php foreach($this->variables['campaigns'] as $campaign){ ?>
	<?php $editLink = '/admin/campaigns/campaignedit/' . $campaign['module_campaign_id_pk'] ?>
		<tr>
			<td><a href="<?php echo $editLink ?>"><?php echo sanitiseOutput($campaign['campaign_name']) ?></a></td>
			
			<td><a href="/admin/leads/dashboard/<?php echo $campaign['module_campaign_id_pk'] ?>">View Leads</a></td>
		
			<td><a href="/admin/campaigns/campaignviewanalytics/<?php echo $campaign['module_campaign_id_pk'] ?>">View Analytics</a></td>

		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>


<?php }  else { ?>

<h1>Error</h1>
<h2>The project you selected either doesn't exist or doesn't belong to you.</h2>
<?php } ?>