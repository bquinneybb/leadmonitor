<?php
$projects = $this->variables['projects'];

//echo encrypt(4);
//print_r($projects);

?>
<form id="adminform" name="adminform" method="post">
<?php if(isset($this->variables['clientProjectList']) || isset($this->variables['clientInformation']['client_name'])) { ?>
<?php 
if(isset($this->variables['clientProjectList'])){
	$clientProjectList = $this->variables['clientProjectList'];
	$clientName = $clientProjectList->{0}['client_name'];
	$clientId =  $clientProjectList->{0}['client_id_pk'];
	
} else {
	$clientName = $this->variables['clientInformation']['client_name'];
	$clientId = $this->variables['clientInformation']['client_id_pk'];
}
//print_r($clientProjectList);
?>
<!--  client campaigns -->
<div class="formActions">
	<div class="formAction"><a href="/admin/projects/projectcreate/<?php echo $clientId ?>">Create New Project for <?php echo sanitiseOutput($clientName); ?></a></div>

</div>
<?php $templateItems->drawFromTableHeader('Project List for ' . $clientName, 'projects', 'currentProjectsClient')?>




<?php if(isset($clientProjectList)){ ?>
<table class="formTable" id="currentProjectsClient" width="100%">
	<thead>
		<tr>
			
			<th width="20%">Project Name</th>
			<th width="20%">Client Name</th>
			<th width="20%">Campaigns</th>
			<th>Assets</th>
		</tr>

	</thead>

	<tbody>
	
	<?php foreach($clientProjectList as $project){ ?>
	
		<tr>
			<td><a href="/admin/projects/projectedit/<?php echo $project['module_project_id_pk'] ?>"><?php echo sanitiseOutput($project['project_name']) ?></a></td>
			<td><a href="/admin/client/clientedit/<?php echo $project['client_id_pk'] ?>"><?php echo sanitiseOutput($project['client_name']) ?></a></td>
			<td><a href="/admin/campaigns/dashboard/<?php echo $project['module_project_id_pk'] ?>">View Campaigns</a></td>
			<td>
			<?php if($project['project_type'] == 'apartment'){ ?>
			<a a href="/admin/apartments/dashboard/<?php echo $project['module_project_id_pk'] ?>">Apartments</a>
			<?php } ?></td>

		</tr>
		<?php } ?>
		
	</tbody>

</table>
<?php }  else { ?>
<h2>No project created for this client yet.</h2>
<?php } ?>
<?php } // end if specific client variables ?>
<!-- /client campaigns -->

<!--  all campaigns -->
<?php $templateItems->drawFromTableHeader('All Client Projects', 'projects', 'currentProjectsAll')?>

<table class="formTable" id="currentProjectsAll" width="100%">
	<thead>
		<tr>
			
			<th width="20%">Project Name</th>
			<th width="20%">Client Name</th>
			<th width="20%">Campaigns</th>
			<th>Buildings</th>
		</tr>

	</thead>

	<tbody>
	<?php if($projects){ ?>
	<?php foreach($projects as $project){ ?>
	
		<tr>
			<td><a href="/admin/projects/projectedit/<?php echo $project['module_project_id_pk'] ?>"><?php echo sanitiseOutput($project['project_name']) ?></a></td>
			<td><a href="/admin/client/clientedit/<?php echo $project['client_id_pk'] ?>"><?php echo sanitiseOutput($project['client_name']) ?></a></td>
			<td><a href="/admin/campaigns/dashboard/<?php echo $project['module_project_id_pk'] ?>">View Campaigns</a></td>
			<td>
			<?php if($project['project_type'] == 'apartment'){ ?>
			<a a href="/admin/apartments/dashboard/<?php echo $project['module_project_id_pk'] ?>">View</a>
			<?php } ?></td>

		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>

</form>