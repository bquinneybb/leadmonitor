<?php 
//print_r($_POST);
$search = $this->variables['search'];
$url = isset($_POST['external_url']) ? $_POST['external_url'] : '';
if($search && $url != ''){
	
	
	$scraper = new Scraper();
	$data = $scraper->getURLContents($url,'', false);
	$scraper->cleanScrapedText($data);
	//print_r($scraper);
	
	
}
$campaigns = $this->variables['campaigns'];
?>


<form id="adminform" name="adminform" method="post" action="/admin/tools/keywordanalyse/search/">
<h1>Select a site to Analyse</h1>
<?php $templateItems->drawFormText('external_url', $url, 'Page to Analyse'); ?>


<br />

	<?php $templateItems->drawSubmit('ANALYSE')?>
</form>

<?php if($search && $url != ''){ ?>
<?php $keywords = array(); ?>
<h2>Top 20 Unique Keywords - <a class="saveResults">Save Results</a></h2>
<?php $i = 0; ?>
<?php foreach($scraper->keywordCounts as $key=>$value){ ?>
<?php if($i < 20){ ?>
<?php $keywords[$key] = $value; ?>
<?php echo $templateItems->drawFormLeft($key); ?>
<?php echo $templateItems->drawFormRight($value); ?>
<?php } ?>
<?php $i++; ?>
<?php } ?>

	
	<script>
	var keywords = '<?php echo json_encode($keywords); ?>';
	var url = '<?php echo sanitiseOutput($url); ?>';
	var queryString = '';
	function saveKeywords(){

		
		queryString += 'save_keywords=' + keywords;
		queryString += '&save_external_url=' + url;
		queryString += '&save_campaign=' + $('#save_campaign').val();
		queryString += '&save_notes=' + $('#save_notes').val();
		queryString += SID_STRING;

		//alert('/admin/application/views/tools/ajax/keywordsave.php?' + queryString);
		$.post('/admin/application/views/tools/ajax/keywordsave.php?' + queryString, function(data){

			if(data){

				$('#savedSearch').css('display', 'block');
				$('#saveSearch').css('display', 'none');
			} 

		});
		
	}
	
	</script>
	<div id="saveSearch">
	<h1>Save Your Search</h1>
	<?php 
$campaignSelect = array();
if($campaigns){
	
	foreach($campaigns as $campaign){

		$campaignSelect[$campaign['module_campaign_id_pk']] = $campaign['name']; 
	}
}
?>	
<?php $templateItems->drawFormList('save_campaign', $campaignSelect, '', true, 'Select a Campaign'); ?>	
<?php $templateItems->drawFormTextArea('save_notes', '', 'Notes')?>		
	<a href="javascript:saveKeywords()">SAVE SEARCH</a>
	</div>
	
	<div id="savedSearch" style="display: none">SEARCH HAS BEEN SAVED</div>
<?php } ?>

