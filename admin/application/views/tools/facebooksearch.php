<?php
$searchTerm = $this->variables['searchTerm'];

if($searchTerm != ''){
	
	$facebook = new ScraperFacebook();

//	$companies = $facebook->findCompany($searchTerm);
//	echo $companies;
	
	$searchResults = $facebook->findPostBySubject($searchTerm);
	$searchResults = json_decode($searchResults, true);
	//echo '||||' . $searchResults . '||||';
	//print_r($searchResults);
}

?>
<form id="adminform" name="adminform" method="post" action="/admin/tools/facebooksearch/">
<h1>Search for Post on Facebook</h1>
<?php $templateItems->drawFormText('searchterm', $searchTerm, 'Phrase to Search For'); ?>


<br />

	<?php $templateItems->drawSubmit('SEARCH')?>
</form>
<?php if($searchTerm != '' && count($searchResults) > 0){ ?>
	<?php foreach($searchResults['data'] as $searchResult){ ?>
	<?php //print_r($searchResult);?>
	<h1>Posted By : <?php echo sanitiseOutput($searchResult['from']['name']) ?> on <?php echo $templateItems->dateToString($searchResult['updated_time'])?>:
	 <a href="http://www.facebook.com/people/<?php echo sanitiseOutput($searchResult['from']['name']) ?>/<?php echo sanitiseOutput($searchResult['from']['id']) ?>" target="_blank">View Poster</a>
	 </h1>
	<?php if(isset($searchResults['properties'])){ ?>
	<h2><?php echo $searchResults['properties']['text'] ?></h2>
	<?php } ?>
	<?php if(isset($searchResult['message'])){ ?>
	<p><?php echo replace_urls_callback(sanitiseOutput($searchResult['message']), 'linkify');  ?></p>
	<?php } ?>
	<?php if(isset($searchResult['picture']) && $searchResult['type'] != 'video'){ ?>
	<table>
		<tr>
		<td width="150">
			<a href="<?php echo $searchResult['link'] ?>" target="_blank"><img src="<?php echo $searchResult['picture'] ?>"  /></a>
		</td>	
			<td>
			<?php if(isset($searchResult['description'])){ ?>
			<strong>Description:</strong> <?php echo replace_urls_callback(sanitiseOutput($searchResult['description']), 'linkify');  ?>
			<?php } ?>
			</td>
		</tr>
	</table>
	<?php } ?>
	<?php if(isset($searchResult['type']) && $searchResult['type'] == 'video'){ ?>
	<table>
		<tr>
		<td width="150">
		<?php if(isset($searchResult['picture'])){ ?>
			<a href="<?php echo $searchResult['link'] ?>" target="_blank"><img src="<?php echo $searchResult['picture'] ?>" /></a>
			<?php } ?>
		</td>	
			<td>
			<?php if(isset($searchResult['description'])){ ?>
			<strong>Description:</strong> <?php echo replace_urls_callback(sanitiseOutput($searchResult['message']), 'linkify');  ?>
			<?php } ?>
			</td>
		</tr>
	</table>
	<?php } ?>
	<hr />
	<?php } ?>
<?php } ?>