<?php
$action = $this->variables['action'];
$domainName = $this->variables['domainName'];

if($action == 'confirm'){

$domain = new ModuleDomain();
$result = $domain->purchaseDomain($domainName);

//print_r($result);
$purchased = false;
if($result[1]){
	
	$purchased = true;
	$domain->saveDomainNamePurchase($domainName);
}
	
?>
<?php if($purchased){ ?>
<h1>Success</h1>
<h2>You have purchased the domain : <?php echo sanitiseOutput($domainName); ?></h2>
<p>You will receive your login / password details and domain registration certificate once the registration has been processed by the Domain Registrar.</p>
<?php } else { ?>
<h1>There was a problem</h1>
<h2>There was a problem with your order.</h2>
<p>Error message : <?php echo sanitiseOutput($result[0]->value)?></p>
<?php } ?>
<?php
// show the form
} else {

$companyList = $this->variables['companyList'];
$client = new Client();
?>
<h1>Register New Domain</h1>
<strong>Please note that a $50 charge will be made for the domain registration. Domain registration is for a period of 2 years.</strong>
<form name="adminform" id="adminform" method="post" action="/admin/tools/domainpurchase/confirm/" onsubmit="confirmRegistration();return false">
<h2>Use existing details</h2>
<?php $templateItems->drawFormList('select_company', $companyList, '', true, 'Select Company')?>
<hr />

<?php $templateItems->drawFormText('db_reg_domain_name',$domainName, 'Domain Name')?>
<?php $templateItems->drawFormText('db_reg_organisation','', 'Company Name'); ?>
<?php $templateItems->drawFormList('db_reg_companytype', $client->companyTypes, '', true, 'Company Type'); ?>
<?php $templateItems->drawFormList('db_reg_eligibilitytype', $client->eligibilityTypes, '', true, 'Eligibility Id Type'); ?>
<?php $templateItems->drawFormText('db_reg_eligibilitynumber', '', 'Eligibility Id Number'); ?>

<?php $templateItems->drawFormText('db_reg_address1','', 'Address 1'); ?>
<?php $templateItems->drawFormText('db_reg_address2','', 'Address 2'); ?>
<?php $templateItems->drawFormText('db_reg_suburb','', 'Suburb'); ?>
<?php $templateItems->drawFormText('db_reg_postcode','', 'Postcode'); ?>
<?php $templateItems->drawFormList('db_reg_state', $client->states, '', true, 'State'); ?>
<hr />
<?php $templateItems->drawFormText('db_reg_firstname', '', 'Firstname'); ?>
<?php $templateItems->drawFormText('db_reg_lastname', '', 'Surname'); ?>
<?php $templateItems->drawFormText('db_reg_phone', '', 'Phone'); ?>
<?php $templateItems->drawFormText('db_reg_email', '', 'Email'); ?>
<?php $templateItems->drawSubmit('PURCHASE DOMAIN'); ?>
</form>
<?php } // end if form ?>
<script src="/admin/application/views/tools/javascript/seo-tools.js"></script>