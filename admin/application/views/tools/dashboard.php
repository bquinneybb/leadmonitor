<?php 

$unconfirmedSearches = $this->variables['unconfirmedSearches'];
$domainSaves = $this->variables['domainSaves'];

Template::loadCSS('/admin/application/views/tools/css/core.css');

?>
<div id="checkDomainAvailability">
<h1>Check Domain Availability</h1>
	<form id="domainsearch" name="domainsearch" action="javascript:checkDomainAvailability();">
		
		<input name="domainname" id="domainname" type="text" value="urbanangles.com" class="text-input" style="width:200px"/>
		<a href="javascript:checkDomainAvailability();">CHECK DOMAIN AVAILABILITY</a>
	</form>
	
	
	<div id="domainResults"></div>
</div>
<hr />
<div id="findSitesToSpider">
<h1>Search for Sites By Keyword</h1>
	<form id="spidersearch" name="spidersearch" action="javascript:spidersite();">
		
		<input name="ix" id="ix" type="text" value="melbourne apartment for sale" class="text-input" style="width:200px"/>
		<a href="javascript:spidersite();">DO A SEARCH</a>
	</form>
	
	
	<div id="searchEngineResults"></div>
</div>


<h1>Select a Tool</h1>
<form id="adminform" name="adminform" method="post">
* to add - > GET https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=http%3A%2F%2Fdev2.urbanangles.com&pp=1&key={YOUR_API_KEY}


<table class="formTable" id="currentCampaigns" width="100%">
	<thead>
		<tr>
			<th width="20%">Tool Name</th>

		</tr>

	</thead>

	<tbody>
	<tr>
			<td><a href="/admin/tools/siteanalyse/">Site Ranking Analysis</a></td>

		</tr>
	<tr>
			<td><a href="/admin/tools/keywordanalyse/">Competitor Keyword Analysis</a></td>

		</tr>
		
		<tr>
			<td><a href="/admin/tools/facebooksearch/">Facebook Search</a></td>

		</tr>
		
		<tr>
			<td><a href="/admin/tools/sitesearchsubmit/">Search Engine Submit</a></td>

		</tr>
		
		<tr>
			<td><a href="/admin/tools/knowledgebase/">Knowledge Base [ to write ]</a></td>

		</tr>
	
	</tbody>

</table>
<h1>Saved Domain Searches</h1>



<table class="formTable" id="domainSaves" width="100%">
	<thead>
		<tr>
		
			<th width="20%">Domain Name</th>
			<th width="20%">Availability</th>
			<th width="20%">Action</th>
		</tr>

	</thead>

	<tbody>
	<?php if($domainSaves){ ?>
	<?php foreach($domainSaves as $domain){ ?>
	<?php $domainStatus = $domain['status'] == 1 ? 'Available' : 'Not Available'; ?>
	<tr>
		
			<td><?php echo sanitiseOutput($domain['domain_name']); ?></td>
			<td><?php echo sanitiseOutput($domainStatus); ?></td>
			<td><?php if($domain['status']){ ?><a href="/admin/tools/domainpurchase/form/<?php echo sanitiseOutput($domain['domain_name']) ?>">Register Domain</a><?php } ?></td>
		</tr>
	<?php } ?>
	<?php } ?>
	</tbody>

</table>
<h1>Un-confirmed Previous Searches</h1>



<table class="formTable" id="currentCampaigns" width="100%">
	<thead>
		<tr>
		
			<th width="20%">Searched URL</th>
<th width="20%">View</th>
		</tr>

	</thead>

	<tbody>
	<?php if($unconfirmedSearches){ ?>
	<?php foreach($unconfirmedSearches as $unconfirmedSearch){ ?>
	<tr>
		
			<td><a href="/admin/tools/keywordview/<?php echo $unconfirmedSearch['module_keyword_save_id_pk'] ?>"><?php echo sanitiseOutput($unconfirmedSearch['website_url']) ?></a></td>
			<td><a href="/admin/tools/keywordview/<?php echo $unconfirmedSearch['module_keyword_save_id_pk'] ?>">View</a></td>
		</tr>
	<?php } ?>
	<?php } ?>
	</tbody>

</table>

<!-- <script src="/admin/application/views/ajax/webwombatsearch.php -->
<script src="/admin/application/views/tools/javascript/seo-tools.js"></script>