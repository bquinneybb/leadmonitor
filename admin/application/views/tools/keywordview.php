<?php 
$searchId = $this->variables['searchId'];
$keywords = $this->variables['keywords'];

if($keywords){ ?>
<?php
	//print_r($keywords);
	$campaigns = $this->variables['campaigns'];
	$campaignSelect = array();
	if($campaigns){
		
		foreach($campaigns as $campaign){
	
			$campaignSelect[$campaign['module_campaign_id_pk']] = $campaign['campaign_name']; 
		}
	}
	$keywordData = array();
	foreach($keywords as $keyword){
		
		$keywordData['module_campaign_id_pk']  = $keyword['module_campaign_id_pk'];
		$keywordData['website_url']  = $keyword['website_url'];
		$keywordData['notes']  = $keyword['notes'];
		$keywordData['active']  = $keyword['active'];
	}
	
	?>
	<form id="adminform" name="adminform" method="post" action="/admin/tools/keywordsave/<?php echo $searchId ?>">
		<h2>Campaign Information</h2>
		<?php $templateItems->drawFormString($keywordData['website_url'], 'URL Analysed'); ?>
		
		<?php $templateItems->drawFormCheckbox('db_active', 1, $keywordData['active'], 'Keep this search'); ?>
	
		
		<?php $templateItems->drawFormList('db_module_campaign_id_pk', $campaignSelect, $keywordData['module_campaign_id_pk'] , true, 'Select a Campaign'); ?>	
	<?php $templateItems->drawFormTextArea('db_notes', $keywordData['notes'] , 'Notes')?>		
		
		<?php $templateItems->drawSubmit('Save / Update')?>
	
	</form>
	
	<?php if($keywords){  ?>
		<?php foreach($keywords as $keywords){ ?>
		
		<?php echo $templateItems->drawFormLeft($keywords['keyword']); ?>
		<?php echo $templateItems->drawFormRight($keywords['count']); ?>
		<?php } ?>
	<?php } ?>
	
	
	<?php } else { ?>
<h1>Error</h1>
<h2>This keyword search either doesn't exist or doesn't belong to you.</h2>	
	
	<?php } ?>