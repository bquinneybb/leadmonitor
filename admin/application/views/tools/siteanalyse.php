<?php
$query = isset($_POST['q']) ? $_POST['q'] : null;
//print_r($_POST);
if($query != null){
	$_SESSION['seo'] = array();
//echo $query;die();
	$scraper = new ScraperSEOMoz();
	$resultsSEOMoz = $scraper->checkDomainStatistics($query);
	$_SESSION['seo']['seoMoz'] = $resultsSEOMoz;
	
	$scraper = new ScraperYahoo();
	$resultsYahoo = $scraper->findSiteLinksAsTSV($query);
	$_SESSION['seo']['seoYahoo'] = $resultsYahoo;
	
//	print_r($_SESSION);
}

?>
<h1>Analyse a Site's Ranking</h1>
	<form id="sitesearch" name="sitesearch" action="/admin/tools/siteanalyse" method="post">
		
		<input name="q" id="q" type="text" value="<?php echo sanitiseOutput($query); ?>" class="text-input" style="width:200px"/>
		<?php $templateItems->drawSubmit('GO', 'sitesearch'); ?>
	</form>
	
	<?php if($query != null){ ?>
	<a href="javascript:savePageAnalysisSearchResults()">SAVE RESULTS</a><span id="saved"></span>
	
	<?php } ?>
	
<?php if(isset($resultsSEOMoz)){ ?>	
<h1>Domain Analysis (Source : SEO Moz)</h1>

	<table class="formTable" id="seomoz" width="100%">
	<thead>
		<tr>
			<th width="20%">Data Type</th>
<th width="20%">Result</th>
		</tr>

	</thead>

	<tbody>
	

	<tr>
		<td title="A score out of 100-points representing the likelihood for arbitrary content to rank on this domain">Domain Authority</td>
		<td><?php echo $resultsSEOMoz['domainAuthority']?> / 100</td>	
	</tr>
	
	<tr>	
		<td title="A score out of 100-points representing the likelihood for arbitrary content to rank on this page">Page Authority</td>
		<td><?php echo $resultsSEOMoz['pageAuthority']?> / 100</td>	
	</tr>	
	
	<tr>
		<td title="The number of 'juice-passing' links (internal or external) to the url">External Links passing rank</td>
		<td><?php echo $resultsSEOMoz['externalLinksJuice']?></td>	
	</tr>	
	
	<tr>
		<td title="The number of links (internal or external) to the url">All external links</td>
		<td><?php echo $resultsSEOMoz['externalLinksAny']?></td>	
	</tr>	
	
	<tr>
		<td title="The mozRank of the url - out of 10">Mozilla Rank</td>
		<td><?php echo $resultsSEOMoz['mozRank']?> / 10</td>	

	</tr>	
	
	</tbody>

</table>
<?php } ?>
<?php if(isset($resultsYahoo)){ ?>
<h1>Inbound Links from (Source :Yahoo)</h1>
<h2>Total Inbound Links : <?php echo count($resultsYahoo); ?></h2>
<table class="formTable" id="yahoo" width="100%">
	<thead>
		<tr>
			<th width="20%">Page Title</th>
<th width="">URL</th>
<th>Action</th>
		</tr>

	</thead>

	<tbody>
	<?php $i = 0; ?>
<?php foreach($resultsYahoo as $result){ ?>
<tr id="result_<?php echo $i ?>" href="<?php echo $result['url']; ?>">
	<td><?php echo sanitiseOutput($result['title'])?></td>
	<td><a href="<?php echo sanitiseOutput($result['url'])?>" target="_blank" ><?php echo sanitiseOutput($result['url'])?></a></td>
	<td>
						<a href="javascript:analysePage('<?php echo $i ?>')" title="Analyse Keywords">?</a>
						
						  <span class="savedAnalysis" id="analyse_<?php echo $i ?>"></span>
						</td>
</tr>
<?php $i++; ?>
<?php } ?>	
	</tbody>
	
	</table>

<?php } ?>
<?php Template::loadJavascript('/admin/application/views/tools/javascript/seo-tools.js'); ?>