/* SEARCH AND SAVE A SITE */
function spidersite(){
	
	var searchTerm = $('#ix').val();
	
	var queryString = '';
	queryString += 'searchTerm=' + searchTerm;
	queryString += SID_STRING;
	//alert('/admin/application/views/tools/ajax/spidersite.php?' + queryString);
	$.post('/admin/application/views/tools/ajax/spidersite.php?' + queryString, function(data){
			
		if(data){
			
			showSearchEngineResults(data);
		}
	
	});
}

function showSearchEngineResults(data){
	
	$('#searchEngineResults').html(data);
	
}

function analysePage(i){
	
	$('#analyse_' + i).html('SAVING...');
	
	var url = $('#result_' + i).attr('href');
	var queryString = '';
	queryString += 'url=' + url;
	queryString += SID_STRING;
//	alert('/admin/application/views/tools/ajax/keywordsgetandsavetemp.php?' + queryString);
	$.post('/admin/application/views/tools/ajax/keywordsgetandsavetemp.php?' + queryString, function(data){
		
		if(data){
			
			setSavedPageAnalysisSearch(i);
		}
		
	});
	
}


function setSavedPageAnalysisSearch(i){
	
	$('#analyse_' + i).html('SAVED');
}

function blockdomain(i, domain){
	
	$('.scrape_' + i).css('display', 'none');
	$(".formTable tr:odd").addClass("alt");
	var queryString = '';
	queryString += 'domain=' + domain;
	queryString += SID_STRING;
	
	$.post('/admin/application/views/tools/ajax/blockdomain.php?' + queryString, function(data){
		
		if(data){
			
			
		}
	
	});
	
}

/* ANALYSE A PARTICULAR SITE */
function savePageAnalysisSearchResults(){
	
	var queryString = '';
	
	queryString += SID_STRING;
	//alert('/admin/application/views/tools/ajax/siteanalysissave.php?' + queryString);
	$.post('/admin/application/views/tools/ajax/siteanalysissave.php?' + queryString, function(data){
			
		if(data){
			
			$('#saved').html('SAVED');
			
		}
	
	});
}

/* CHECK DOMAIN AVAILABILITY */

function checkDomainAvailability(){
	$('#domainResults').html('');
	var domainName = $('#domainname').val();
	
	if(validate_domain(domainName)){
		
		var queryString = '';
		
		queryString += SID_STRING;
		queryString += '&domainName=' + domainName;
		//alert('/admin/application/views/tools/ajax/checkdomainavailable.php?' + queryString);
		$.post('/admin/application/views/tools/ajax/checkdomainavailable.php?' + queryString, function(data){
				
			if(data){
				
				var results =jQuery.parseJSON(data);
			//	alert(results);
				if(results[1] == true){
					
					createDomainSearchOptions(results);
				} else {
					alert('ERROR: ' + results[0]);
				}
				//alert(data);
			} 
		
		
	});
	}	
}

function createDomainSearchOptions(results){
	
	var html = '<ul>';
	if(results[0] == 'AVAILABLE'){
	
		html += '<li><a href="/admin/tools/domainpurchase/form/' +  $('#domainname').val() + '">Purchase Domain</a></li>';
	
	}
	html += '<li><a href="javascript:saveDomainSearch(\'' + results[0] + '\')">Save this search</a><div id="domainSaveStatus"></div></li>';
	
	html += '</ul>';
	$('#domainResults').html(html);
}

function saveDomainSearch(status){
	
	var domainName = $('#domainname').val();
	queryString = '';
	queryString += SID_STRING;
	queryString += '&domainName=' + domainName;
	queryString += '&status=' + status;
//	alert('/admin/application/views/tools/ajax/savedomainsearch.php?' + queryString);
	$.post('/admin/application/views/tools/ajax/savedomainsearch.php?' + queryString, function(data){
		
			$('#domainSaveStatus').html('SAVED');
	});
}

function validate_domain(domainDomain){
	   var txt_domain = domainDomain;
	   var domain_array = txt_domain.split('.');

	  var domain = domain_array[0];
	//This is reguler expresion for domain validation
	  var reg = /^([A-Za-z0-9])+[A-Za-z0-9-]+([A-Za-z0-9])$/;

	if(domain == ''){
	   alert("Please enter the domain name"); 
	 
	     return false; 
	} 

	if(reg.test(domain) == false){
	   alert("Invalid character in domain. Only letters, numbers or hyphens are allowed.");
	 
	   return false;
	}
	 // alert("OK This is valid domain");
	return true;
	}
/* REGISTER A DOMAIN */
$(document).ready(function(){
	
	if($('#select_company').length > 0){
			
		$('#select_company').bind('change', function(){
			
			getCompanyData();
		});
		
	}
});

function getCompanyData(){
	
	var companyId = $('#select_company').val();
	//alert(companyId);
	if(companyId > 0){
		
		queryString = '';
		queryString += SID_STRING;
		queryString += '&companyId=' + companyId;
		
		$.post('/admin/application/views/tools/ajax/loadcompanydata.php?' + queryString, function(data){
			
			if(data){
				
				popuplateCompanyData(data);
			} else {
				
				alert('An error occured');
			}
		});
	}
	

}

function popuplateCompanyData(data){
	
	
	results = jQuery.parseJSON(data);
	$('#db_reg_organisation').val(results.company_name);
	$('#db_reg_companytype').val(results.companytype);
	$('#db_reg_eligibilitytype').val(results.eligibilitytype);
	$('#db_reg_eligibilitynumber').val(results.eligibilitynumber);
	
	
	$('#db_reg_address1').val(results.address1);
	$('#db_reg_address2').val(results.address2);
	$('#db_reg_suburb').val(results.suburb);
	$('#db_reg_postcode').val(results.postcode);
	$('#db_reg_state').val(results.state);
	
	
	$('#db_reg_firstname').val(results.contact_first_name);
	$('#db_reg_lastname').val(results.contact_last_name);
	$('#db_reg_phone').val(results.phone);
	$('#db_reg_email').val(results.email);
	
	
}

function confirmRegistration(){
	
	var answer = confirm("Confirm purchase of : "  + $('#db_reg_domain_name').val());
	if (answer){
		
		document.adminform.submit();
	}
	else{
		
		alert("Order Cancelled");
	}
	
}