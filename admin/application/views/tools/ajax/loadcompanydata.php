<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$companyId = isset($_POST['companyId']) ? $_POST['companyId'] : null;


if($companyId == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();
$company = new Company();
if($result = $company->findCompanyInformation($companyId, true)){
	
	$result = json_encode($result);
	
	
	// trim the [] off for javascript
	//$result = substr($result, 1);
	//$result = substr($result,0, -1);
	echo($result);

} else {

	echo 0;
}