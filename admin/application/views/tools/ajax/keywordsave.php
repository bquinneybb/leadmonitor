<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$searchTerms = isset($_POST['save_keywords']) ? $_POST['save_keywords'] : null;
$externalURL = isset($_POST['save_external_url']) ? $_POST['save_external_url'] : null;
$campaignId = isset($_POST['save_campaign']) ? $_POST['save_campaign'] : 0;
$notes = isset($_POST['save_notes']) ? $_POST['save_notes'] : 0;

if($searchTerms == null || $externalURL == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();

$keywords = new  SEOTools();
$result = $keywords->keywordsSaveSearch($searchTerms, $externalURL, $campaignId, $notes);
echo($result);