<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$searchTerm = isset($_POST['searchTerm']) ? $_POST['searchTerm'] : null;


if($searchTerm == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();
$spider = new ScraperBing();
$result = $spider->webSearch($searchTerm);
//$spider = new ScraperWebWombat();
//$result = $spider->scrapeSearch($searchTerm);
echo($result);