<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

$url = isset($_POST['url']) ? $_POST['url'] : null;


if($url == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();

$scraper = new Scraper();
$data = $scraper->getURLContents($url,'');
$scraper->cleanScrapedText($data);

//print_r( $scraper);

$keywords = new  SEOTools();


$searchTerms = array();
$i = 0;
foreach($scraper->keywordCounts as $key=>$value){
	
	if($i<20){
		
		$searchTerms[$key] = $value;
		
	}
	
	$i++;
}
//print_r($searchTerms);die();
$keywords->keywordsSaveSearch(json_encode($searchTerms), $url, '', '', 0);

echo(1);
?>