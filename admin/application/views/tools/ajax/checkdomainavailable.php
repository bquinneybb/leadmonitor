<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$domainName = isset($_POST['domainName']) ? $_POST['domainName'] : null;


if($domainName == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();
$domainCheck = new ModuleDomain();
$result = $domainCheck->checkDomainAvailability($domainName);

echo(json_encode($result));