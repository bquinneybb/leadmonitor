<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$domainName = isset($_POST['domainName']) ? $_POST['domainName'] : null;
$status = isset($_POST['status']) ? $_POST['status'] : null;

if($domainName == null || $status == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();
$domainCheck = new ModuleDomain();
$result = $domainCheck->saveDomainSearch($domainName, $status);

echo(json_encode($result));