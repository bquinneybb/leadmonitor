<?php

include ('../../../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$domain = isset($_POST['domain']) ? $_POST['domain'] : null;


if($domain == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();

$blocker = new  ScraperSEO();
$blocker->addDomainToBlockedList($domain);
?>
