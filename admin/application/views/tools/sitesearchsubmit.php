<?php 

$action = isset($_POST['action']) ? $_POST['action'] : null;

if($action != null){

	$url = isset($_POST['domainsubmit']) ? $_POST['domainsubmit'] : null;
	$email = isset($_POST['clientemail']) ? $_POST['clientemail'] : null;
	
	if($url != null & $email != null && $url != 'http://'){
	
		$submit = new SEOSearchSubmission();
		$result = $submit->submitToSearchEngines($url, $email);

	} else {
		
		$error = 'You must fill in both fields to submit the domain';
		
	}
}
?>
<h1>Submit Domain to Search Engines</h1>
<?php if(isset($error)){ ?>
<span class="error"><?php echo sanitiseOutput($error) ?></span>
<?php } ?>	

<?php if(isset($result)){ ?>
<?php echo sanitiseOutput($result); ?>
<hr />
<?php } ?>
	
	<form id="domainsubmit" name="domainsubmit" action="/admin/tools/sitesearchsubmit/" method="post">
		
<?php $templateItems->drawFormText('domainsubmit', isset($_POST['domainsubmit']) ? $_POST['domainsubmit'] : 'http://', 'Domain URL')?>		
<?php $templateItems->drawFormText('clientemail', isset($_POST['clientemail']) ? $_POST['clientemail'] : '', 'Client Email')?>		
<?php $templateItems->drawHidden('action', 'submit'); ?>		
		
		<?php $templateItems->drawSubmit('Submit to Search Engines'); ?>

	</form>
	