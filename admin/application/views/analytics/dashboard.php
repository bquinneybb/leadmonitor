<?php
$analytics = $this->variables['analytics'];

//print_r($this->variables);
?>
<h1>Analytics Reports</h1>
<form id="adminform" name="adminform" method="post">
<!--  analytics reports -->
<table class="formTable" id="currentCampaigns" width="100%">
	<thead>
		<tr>
			<th width="20%">Website Name</th>
			<th width="20%">Schedule</th>
			
					</tr>

	</thead>

	<tbody>
	<?php if($analytics){ ?>
	<?php foreach($analytics as $analytic){ ?>
	<?php $editLink = '/admin/analytics/analyticsedit/' . $analytic['module_analytics_id_pk']; ?>
		<tr>
			<?php $schedule = $analytic['schedule'] == 'week' ? 'Weekly' : 'Monthly'; ?>
			<td><a href="<?php echo $editLink ?>"><?php echo sanitiseOutput($analytic['website_name']) ?></a></td>
			<td><a href="<?php echo $editLink ?>"><?php echo sanitiseOutput($schedule) ?></a></td>

		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>
<!--  /analytics reports -->


</form>
