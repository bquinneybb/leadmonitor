<?php
//include(ROOT . '/admin/application/shared/tinymce.php'); 


if($analytics = $this->variables['analytics']){



	?>
<h1>Edit Analytics Report : <?php echo sanitiseOutput($analytics['website_name']); ?><?php $templateItems->drawUpdateNotice(); ?></h1>



<form id="adminform" name="adminform" method="post" action="/admin/analytics/analyticssave/<?php echo $analytics['module_analytics_id_pk'] ?>">

	<?php $templateItems->drawSubmit('Save')?><br />
	<br /><br /><br />
	
	<h2>Report Information</h2>
	
	<?php $templateItems->drawFormCheckbox('db_active', 1, $analytics['active'], 'Active'); ?>
	
	<?php $templateItems->drawFormText('db_website_name', $analytics['website_name'], 'Website Name'); ?>
	<?php $templateItems->drawFormText('db_google_id', $analytics['google_id'], 'Google Account <br /><small>(Admin / View / View Settings / View ID)</small>'); ?>
	<?php $templateItems->drawFormTextarea('db_recipients', $analytics['recipients'], 'Recipients [JSON encoded]<br /><small>(Use Double Quotes)<br> i.e. [["Test","tech@barkingbird.com.au"]]</small>'); ?>
	
	<?php
		$list = array('week'=>'Weekly', 'month'=>'Monthly');
	?>
	<?php $templateItems->drawFormList('db_schedule', $list, $analytics['schedule'],  false, 'Frequency'); ?>
</form>

<?php }  else { ?>

<h1>Error</h1>
<h2>The report you selected either doesn't exist or doesn't belong to you.</h2>
<?php } ?>
