<?php 
$clientList = $this->variables['clientList'];
?>
<?php $templateItems->drawFromTableHeader('Client List', 'companies', 'clients')?>
<form id="adminform" name="adminform" method="post">
<!--  campaigns -->
<table class="formTable" id="clients" width="100%">
	<thead>
		<tr>
			
			<th width="20%">Client Name</th>
			<th width="20%">Existing Projects</th>
			<th width="20%">New Project</th>
		</tr>

	</thead>

	<tbody>
	<?php if($clientList){ ?>
	<?php foreach($clientList as $client){ ?>
	<?php $editLink = '/admin/client/clientedit/' . $client['client_id_pk'] ?>
		<tr>
			<td><a href="<?php echo $editLink; ?>"><?php echo sanitiseOutput($client['client_name']) ?></a></td>
			<td><a href="/admin/projects/dashboard/<?php echo sanitiseOutput($client['client_id_pk']) ?>">Existing Projects</a></td>
			<td><a href="/admin/projects/projectcreate/<?php echo sanitiseOutput($client['client_id_pk']) ?>">New Project</a></td>

		</tr>
		<?php } ?>
		<?php } ?>
	</tbody>

</table>