<?php if($clientInformation = $this->variables['client']){ ?>
<?php 
$client = new Client();
?>
<?php $templateItems->drawFromTableHeader('Edit Client : ' . $clientInformation['client_name'], 'companies', '')?>

<?php $templateItems->drawUpdateNotice(); ?>

<form id="adminform" name="adminform" method="post" action="/admin/client/clientsave/<?php echo $clientInformation['client_id_pk'] ?>">
<div class="formActions">
<?php $templateItems->drawSubmit('Save'); ?>
<div class="formAction"><a href="/admin/projects/dashboard/<?php echo $clientInformation['client_id_pk'] ?>">View Projects</a></div>
<div class="formAction"><a href="/admin/campaigns/dashboard/<?php echo $clientInformation['client_id_pk'] ?>">View Campaigns</a></div>
</div>

	<?php if($_SESSION['user']['group_id_pk'] === 1){ ?>

	<!-- allow the client to be moved to a new super group -->
	
	<?php
	
		$masterClientList = $templateItems->createList('group', 'group_id_pk', 'group_name');
		$templateItems->drawFormList('db_group_id_pk_new', $masterClientList, $clientInformation['group_id_pk'], false, 'Change Group Owner');
	
	?>

	<!-- allow the client to be moved to a new super group -->
	
	<?php } ?>
	<?php $templateItems->drawHidden('db_group_id_pk', $clientInformation['group_id_pk']); ?>
	
	<?php $templateItems->drawFormCheckbox('db_active', 1, $clientInformation['active'], 'Active'); ?>
	<?php $templateItems->drawFormText('db_client_name', $clientInformation['client_name'], 'Client Name'); ?>
	<?php $templateItems->drawFormList('db_companytype', $client->companyTypes, $clientInformation['companytype'], true, 'Company Type'); ?>
	<?php $templateItems->drawFormList('db_eligibilitytype', $client->eligibilityTypes, $clientInformation['eligibilitytype'], true, 'Eligibility Type'); ?>
	<?php $templateItems->drawFormText('db_eligibilitynumber', $clientInformation['eligibilitynumber'], 'Eligibility Number'); ?>
	<hr />
	<?php $templateItems->drawFormText('db_contact_first_name', $clientInformation['contact_first_name'], 'Contact Fistname'); ?>
	<?php $templateItems->drawFormText('db_contact_last_name', $clientInformation['contact_last_name'], 'Contact Surname'); ?>
	<?php $templateItems->drawFormText('db_phone', $clientInformation['phone'], 'Phone'); ?>
	<?php $templateItems->drawFormText('db_email', $clientInformation['email'], 'Email'); ?>
	
	<hr />
	<?php $templateItems->drawFormText('db_address1', $clientInformation['address1'], 'Address 1'); ?>
	<?php $templateItems->drawFormText('db_address2', $clientInformation['address2'], 'Address 2'); ?>
	<?php $templateItems->drawFormText('db_suburb', $clientInformation['suburb'], 'Suburb'); ?>
	<?php $templateItems->drawFormText('db_postcode', $clientInformation['postcode'], 'Postcode'); ?>
	<?php $templateItems->drawFormList('db_state', $client->states, $clientInformation['state'], true, 'State'); ?>
	


</form>

<?php }  else { ?>

<h1>Error</h1>
<h2>The project you selected either doesn't exist or doesn't belong to you.</h2>
<?php } ?>