<script type="text/javascript">


sid = <?php $templateItems->drawSidString(); ?>;
bitString = '<?php echo encrypt($this->variables['user_bit_string']); ?>';
function addMainMenuListOption(){

	queryString = SITE_PATH + '/admin/helpers/configuration/add-to-menu-main-list.php?';
	
	queryString += 'bitString=' + bitString;
    queryString += sid;
    
 alert(queryString);
 	$.post(queryString, function(data) {
			
			// get a return value so we can append it to the DOM
			drawMenuMainNode(data, '', '', '');
	
	});

	
}

function drawMenuMainNode(nodeId, label, url, controller){

		var reorderField = 'menu_main';

		nodeHTML = '<tr  id="' + nodeId + '">';
		nodeHTML += '<td><div class="reorder"></div></td>';

		nodeHTML += '<td><label>Menu Label&nbsp;<input type="text" id="' + reorderField + '[' + nodeId + ']" name="' + reorderField + '[' + nodeId + ']" rel="' + nodeId + '" ';
		nodeHTML += 'value="' + label + '" class="text-field" /></label></td>';

		nodeHTML += '<td><label>URL&nbsp;<input type="text" id="' + reorderField + '[' + nodeId + ']" name="' + reorderField + '[' + nodeId + ']" rel="' + nodeId + '" ';
		nodeHTML += 'value="' + url + '" class="text-field" /></label></td>';

		nodeHTML += '<td><label>Controller Name&nbsp;<input type="text" id="' + reorderField + '[' + nodeId + ']" name="' + reorderField + '[' + nodeId + ']" rel="' + nodeId + '" ';
		nodeHTML += 'value="' + controller + '" class="text-field" /></label></td>';
		
		nodeHTML += '<td><a href="#" onclick="return false" class="delete" rel="' + nodeId + '"><img src="' + SITE_PATH + '/public/template-core/icons/cross.png" /></a></td>';
		nodeHTML += '</tr>';
//		alert(nodeHTML);
		$('#reorderable').append(nodeHTML);
	
	
}

</script>


<?php 
$menuItems = isset($this->variables['menuItems']) ? $this->variables['menuItems'] : null; 

?>
<h1>Edit Main Menu Items for : <?php echo $owner['group_name']; //print_r($owner)?></h1>
<table id="reorderable">

<?php
if($menuItems){
foreach($menuItems as $menuItem){
	
?>
<script type="text/javascript">
createNode(<?php echo $menuItem[0] ?>, '<?php echo $menuItem[0] ?>', '<?php echo $menuItem[1] ?>', '<?php echo $menuItem[2] ?>');
</script>
<?php 	
}
	
}?>

</table>
<?php $templateItems->ZebraStripeTable('#reorderable', 'alt'); ?>