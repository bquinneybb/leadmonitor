$(document).ready(function() {

	

    $('#bannerColorpicker').farbtastic('#config_banner_background_colour');
    //$('#colorpicker').css('display', 'none');

    $('#showBannerColorPicker').colorbox({'href': '#bannerColorpicker', 'inline': true, 'innerHeight': '200px', 'innerWidth': '200px', 'transition': 'none'});
    
   $('#emailColorpicker').farbtastic('#config_email_background_colour');
    //$('#colorpicker').css('display', 'none');

   $('#showEmailColorPicker').colorbox({'href': '#emailColorpicker', 'inline': true, 'innerHeight': '200px', 'innerWidth': '200px', 'transition': 'none'});

    $('#file_upload_main_banner').uploadify({

        'uploader'  : SITE_PATH + '/public/javascript/jquery.uploadify/uploadify.swf',

        'script'    : SITE_PATH + '/admin/helpers/configuration/uploadbanners.php?',
        
        'scriptData' : {'sid': SID_JSON, 'uploadType' : 'main_banner'},
        
        'fileExt'        : '*.jpg;*.gif;*.png',
        'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
              
        
        'cancelImg' : SITE_PATH + '/public/javascript/jquery.uploadify/cancel.png',

        'folder'    : '/tmp',

        'auto'      : true,

        'multi'       : false,
      
        'onAllComplete' : function(event,data) {
    	  	
      //       $('#uploadcomplete').html(data.filesUploaded + ' files uploaded successfully!');
            document.adminform.submit();
        //	alert(data.errors);
            },
        
            'onError'     : function (event,ID,fileObj,errorObj) {
            
                  alert(errorObj.type + ' Error: ' + errorObj.info);
           
                },
                'onComplete' : function(event, ID, fileObj, response, data) {
            //   	alert(response);
              //  	$('#uploadcomplete').append(response);
                }
                

      });
    
    $('#file_upload_email_banner').uploadify({

        'uploader'  : SITE_PATH + '/public/javascript/jquery.uploadify/uploadify.swf',

        'script'    : SITE_PATH + '/admin/helpers/configuration/uploadbanners.php?',
        
        'scriptData' : {'sid': SID_JSON, 'uploadType' : 'email_banner'},
        
        'fileExt'        : '*.jpg;*.gif;*.png',
        'fileDesc'       : 'Image Files (.JPG, .GIF, .PNG)',
              
        
        'cancelImg' : SITE_PATH + '/public/javascript/jquery.uploadify/cancel.png',

        'folder'    : '/tmp',

        'auto'      : true,

        'multi'       : false,
      
        'onAllComplete' : function(event,data) {
    	  	
      //       $('#uploadcomplete').html(data.filesUploaded + ' files uploaded successfully!');
            document.adminform.submit();
        //	alert(data.errors);
            },
        
            'onError'     : function (event,ID,fileObj,errorObj) {
            
                  alert(errorObj.type + ' Error: ' + errorObj.info);
           
                },
                'onComplete' : function(event, ID, fileObj, response, data) {
            //   	alert(response);
              //  	$('#uploadcomplete').append(response);
                }
                

      });

    
  });

function copyField(field, value){
	
	$('#' + field).val(value);
	$('#' + field).focus();
}
