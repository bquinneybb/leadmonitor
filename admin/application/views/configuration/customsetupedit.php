<form id="adminform" name="adminform" method="post" action="<?php SITE_PATH ?>/admin/configuration/customsetupsave/">
<?php $configuration = $this->variables['configuration']; ?>
<h1>Custom Setup for SuperAdmin only</h1>
<h2>Clients</h2>
<?php $templateItems->drawFormCheckbox('config_user_allowphoto', 1, isset($configuration['user_allowphoto']) ? 1 : 0, 'Allow Client Photo'); ?>

<?php $templateItems->drawSubmit('Save'); ?>
</form>