<?php 
//echo phpinfo();

//$image = new  Imagick(ROOT . '/fileserver/client-logo/site-owner/main_banner_temp.jpg');
 ?>
<form id="adminform" name="adminform" method="post" action="<?php SITE_PATH ?>/admin/configuration/ownersetupsave/">
<?php $configuration = $this->variables['configuration']; ?>

<h1>Company Colours</h1>

<?php $templateItems->drawFormText('config_primary_email_contact', isset($configuration['primary_email_contact']) && $configuration['primary_email_contact'] != '' ? $configuration['primary_email_contact'] : '', 'Primary Email Contact'); ?>

<!--  main banner -->
<?php echo $templateItems->drawFormLeft('Banner Logo');?>
<?php $logoPath = ROOT .'/fileserver/client-logo/site-owner/main_banner.png';
if(file_exists($logoPath)){
	
	
	$suggestedBackground = ManipulateImage::suggestBestBackgroundColour(ROOT .  '/fileserver/client-logo/site-owner/main_banner.png');
	
	$logoPath = SITE_PATH . '/fileserver/client-logo/site-owner/main_banner.png?id=' . md5(microtime());
	
	$picture = new Imagick(ROOT . '/fileserver/client-logo/site-owner/main_banner.png');
	$imageSize = $picture->getImageGeometry();
		
	$scalePreview = $imageSize['width'] > 300 ? 300 : $imageSize['width'];
	$previewLink = '<img src="' . $logoPath . '" width="' . $scalePreview . '" />';
	
	$templateItems->returnOnly = true;
	$imageLink = $templateItems->createImageModalLink($previewLink, $logoPath);
	$templateItems->returnOnly = false;
	
//	$imageLink .= '<br />';
	
	
} else {
	
$imageLink = 'No logo uploaded';
$suggestedBackground = '#';
} 	


?>
<?php echo $templateItems->drawFormRight($imageLink . '<br /><input id="file_upload_main_banner" name="file_upload_main_banner" />');?>

 
<?php echo $templateItems->drawFormLeft('Banner Background Colour');?>
<?php $bannerColour = isset($configuration['banner_background_colour']) && $configuration['banner_background_colour'] != '' ? $configuration['banner_background_colour'] : '#FFFFFF'; 
$templateItems->returnOnly = true;
$bannerField = $templateItems->drawText('config_banner_background_colour', $bannerColour);
$templateItems->returnOnly = false;
?>
<?php echo $templateItems->drawFormRight($bannerField . '<div style="float:right"><a href="#" id="showBannerColorPicker">Show Colour Picker</a></div><br />Suggested Background Color: ' . $suggestedBackground . ' : <a href="javascript:copyField(\'config_banner_background_colour\', \'' . $suggestedBackground . '\')">Copy</a><br />'); ?>

<div class="clear"></div>

<!--  main banner -->
 
 <!--  email banner -->
<?php echo $templateItems->drawFormLeft('Email Logo');?>
<?php $logoPath = ROOT .'/fileserver/client-logo/site-owner/email_banner.png';
if(file_exists($logoPath)){
	
	
	$suggestedBackground = ManipulateImage::suggestBestBackgroundColour(ROOT . '/fileserver/client-logo/site-owner/email_banner.png');

	$logoPath = SITE_PATH . '/fileserver/client-logo/site-owner/email_banner.png?id=' . md5(microtime());
	
	$picture = new Imagick(ROOT . '/fileserver/client-logo/site-owner/email_banner.png');
	$imageSize = $picture->getImageGeometry();
		
	$scalePreview = $imageSize['width'] > 300 ? 300 : $imageSize['width'];
	$previewLink = '<img src="' . $logoPath . '" width="' . $scalePreview . '" />';
	
	$templateItems->returnOnly = true;
	$imageLink = $templateItems->createImageModalLink($previewLink, $logoPath);
	$templateItems->returnOnly = false;
	
//	$imageLink .= '<br />';
	
	
} else {
	
$imageLink = 'No logo uploaded';
$suggestedBackground = '#';
} 	


?>
<?php echo $templateItems->drawFormRight($imageLink . '<br /><input id="file_upload_email_banner" name="file_upload_email_banner" />');?>

 
<?php echo $templateItems->drawFormLeft('Email Background Colour');?>
<?php $bannerColour = isset($configuration['email_background_colour']) && $configuration['email_background_colour'] != '' ? $configuration['email_background_colour'] : '#FFFFFF'; 
$templateItems->returnOnly = true;
$bannerField = $templateItems->drawText('config_email_background_colour', $bannerColour);
$templateItems->returnOnly = false;
?>
<?php echo $templateItems->drawFormRight($bannerField . '<div style="float:right"><a href="#" id="showEmailColorPicker">Show Colour Picker</a></div><br />Suggested Background Color: ' . $suggestedBackground . ' : <a href="javascript:copyField(\'config_email_background_colour\', \'' . $suggestedBackground . '\')">Copy</a><br />'); ?>

<div class="clear"></div>

<!--  email banner -->
<?php $templateItems->drawSubmit('Save'); ?>
<div id="uploadcomplete"></div>

</form>

        
<?php Template::loadJavascript('/public/javascript/farbtastic/farbtastic.js'); ?>
<?php Template::loadCSS('/public/javascript/farbtastic/farbtastic.css'); ?>
<script type="text/javascript">
SID_JSON = '<?php echo encrypt(session_id()) ?>';
</script>

<div style="display:none"><div id="bannerColorpicker" name="bannerColorpicker" style="margin-left: auto;margin-right: auto"></div></div>
<div style="display:none"><div id="emailColorpicker" name="emailColorpicker" style="margin-left: auto;margin-right: auto"></div></div>
<?php  Template::loadCSS('/public/javascript/jquery.uploadify/uploadify.css'); ?>
<?php  Template::loadJavascript('/public/javascript/jquery.uploadify/swfobject.js'); ?>
<?php  Template::loadJavascript('/public/javascript/jquery.uploadify/jquery.uploadify.min.js'); ?>
<?php  Template::loadJavascript('/admin/application/views/configuration/javascript/ownersetup.js'); ?>
