<?php
class ClientController extends Controller {


	public function dashboard($queryString, $parameters){

		$this->set('activeLabel', 'Client List');

		$modelName = $this->_model;
		$model = new $this->$modelName;

		$clientList = $model->findAllClients();
		$this->set('clientList', $clientList);
	}

	public function clientcreate(){
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		$client = $model->clientCreate();
	}
	
	public function clientedit($queryString, $parameters){

		$this->set('activeLabel', 'Edit');

		$modelName = $this->_model;
		$model = new $this->$modelName;

		$client = $model->findClientInformation($parameters[0]);
		$this->set('client', $client);
	}
	
	public function clientSave($queryString, $parameters){
		
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$client = $model->clientSave($parameters[0]);
		
	}

}
?>