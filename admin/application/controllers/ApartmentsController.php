<?php
class ApartmentsController extends Controller {

	function dashboard($queryString, $parameters){

		$this->set('activeLabel', 'Development Overview');

		$modelName = $this->_model;
		$model = new $this->$modelName;

		// did they select a campaign to edit ?
		if($parameters[0] == ''){
				
			$projects = $model->findActiveProjects();
			$this->set('projects', $projects);
				
		} else {
			//print_r($parameters);
			$this->set('projectId', $parameters[0]);
			
			$project = $model->projectEdit($parameters[0]);
			$this->set('project', $project);
			
			$apartments = $model->findAllApartmentsAndSortByBuilding($parameters[0], $parameters[1]);
			$this->set('apartments', $apartments);
			
		}


	}
	
	function levelplanView($queryString, $parameters){
		
		$this->set('showTemplate', false);
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$this->set('projectId', $parameters[0]);
		
		$apartments = $model->levelPlanFindData($parameters[0], $parameters[1], $parameters[1]);
		$this->set('apartments', $apartments);
		
	}
	
function apartmentEdit($queryString, $parameters){
		
		$this->set('showTemplate', false);
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$this->set('projectId', $parameters[0]);
		
		$apartments = $model->apartmentFindData($parameters[0], $parameters[1]);
		$this->set('apartments', $apartments);
		
	}
	
	function apartmentSaveStatus($queryString, $parameters){
		
			$modelName = $this->_model;
			$model = new $this->$modelName;
			$apartments = $model->apartmentSaveStatus($parameters[0], $parameters[1]);
		
	}

}