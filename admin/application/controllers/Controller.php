<?php
class Controller {
	
	protected $_model;
	protected $_modelName;
	protected $_controller;
	protected $_action;
	protected $_template;
	
	
	protected $noPermissionRedirect = array('ErrorModel', 'LoginModel');
	public $hideMenu = array('login');
	
	function __construct($model, $controller, $action) {

	
	//echo $model . '|' . $controller . '|' . $action ;die();
	
		$this->_controller = $controller;
		$this->_action = $action;
		$this->_model = $model;
		$this->_modelName = $model;
		$this->model = $model;
		//echo $model;
		
		if($model != 'Model'){

			$model .= 'Model';
			$this->_model = $model;
		}
		//print_r($this);
	
		
		$this->$model = new $model;
		$this->_template = new Template($controller,$action, 'admin');
		$this->_template->model = $model;
		$this->_template->action = $action;
		
		// not sure we're using this one....
			$this->set('hideNavigation', false);
		
		 if(in_array($this->_controller, $this->hideMenu)){
		 	
		 	$this->set('hideNavigation', true);
		 }
		
		 
		 // set some a variable so we can supress the menus etc
		$this->set('showTemplate', true);
		
		$groupId = isset($_SESSION['user']['group_id_pk']) ? $_SESSION['user']['group_id_pk'] : 1;
		
		$customConfiguration = Configuration::getConfigurationForEditing('custom_setup', $groupId);
		$this->set('customConfiguration', $customConfiguration);
		$this->_template->noPermissionRedirect = $this->noPermissionRedirect;
		
		
		
		
		
		if(!in_array($model, $this->noPermissionRedirect) && is_callable(array($this->$model, 'setPermissions'))){
		
			
			$permissions = $this->$model->setPermissions();
			
			
		} else {
			
				$thisModel = new Model();
				$permissions = $thisModel->setPermissions();
			
		}
		
		$this->_template->permissions = $permissions;
		
		$this->_template->mode = 'admin';
		
		
		$this->set('breadCrumbs', array());
		
	
		
		
	}
	
	function set($name,$value) {
		$this->_template->set($name,$value);
		
	}

	function __destruct() {
			
		$this->_template->render();
	}


	
}
?>