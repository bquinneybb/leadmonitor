<?php
class ProjectsController extends Controller {

	function dashboard($queryString, $parameters){

		$this->set('activeLabel', 'Project Dashboard');

		$modelName = $this->_model;
		$model = new $this->$modelName;

		if($parameters[0] != ''){
				
			if($clientProjectList = $model->findAllProjectsByClient($parameters[0])){
				//print_r($clientProjects);
				$this->set('clientProjectList', $clientProjectList);
					
			} else {

				$clientInformation = $model->findClientInformation($parameters[0]);
				$this->set('clientInformation', $clientInformation);

			}

		}

		$projects = $model->findAllProjects();
		
		$this->set('projects', $projects);
	}


	function projectCreate($queryString, $parameters){


		$modelName = $this->_model;
		$model = new $this->$modelName;

		$project = $model->projectCreate($parameters[0]);

	}

	function projectEdit($queryString, $parameters){

		$this->set('activeLabel', 'Edit Project');

		$modelName = $this->_model;
		$model = new $this->$modelName;

		$project = $model->projectEdit($parameters[0]);
		$this->set('project', $project);

		$campaigns = $model->findCampaignsInProject($parameters[0]);
		$this->set('campaigns', $campaigns);
	}

	function projectSave($queryString, $parameters){

		$modelName = $this->_model;
		$model = new $this->$modelName;

		$project = $model->projectSave($parameters[0]);

	}
}