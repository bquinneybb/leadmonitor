<?php
class ConfigurationController extends Controller {

	function ownersetupedit(){
		
		$this->set('activeLabel', 'Site Configuration');
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$configuration = $model->findOwnerConfiguration();
		$this->set('configuration', $configuration);
		
	}
	
function ownersetupsave(){
		
		
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$configuration = $model->saveOwnerConfiguration();
		
		
	}
	
	
	
	function menumainedit($queryString, $parameters){
		
		$this->set('activeLabel', 'Edit Main Menu Items');
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$menuItems = $model->findMainMenuItems($parameters);
		$this->set('menuItems', $menuItems);
		
		$owner = $model->findOwnerDetails($parameters);
		$this->set('owner', $owner);
		
		$this->set('user_bit_string', $parameters[0] . '|' . $parameters[1] . '|' . $parameters[2]);
	}
	
	function customsetupedit(){
		
		$this->set('activeLabel', 'Custom Configuration');
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$configuration = $model->findCustomConfiguration();
		$this->set('configuration', $configuration);
		
	}
	
	function customsetupsave(){
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$configuration = $model->saveCustomConfiguration();
		$this->set('configuration', $configuration);
	}
	
}