<?php
class AnalyticsController extends Controller {

	function dashboard($queryString, $parameters){

		$this->set('activeLabel', 'Analytics Reports');

		$modelName = $this->_model;
		$model = new $this->$modelName;

		$analytics = $model->findAllAnalyticsAccounts();
	//	print_r($analytics);
		$this->set('analytics', $analytics);
		//print_r($this);
		
	}

	
	function analyticsedit($queryString, $parameters){

		$this->set('activeLabel', 'Edit an Analytics Reports');

		$modelName = $this->_model;
		$model = new $this->$modelName;

		$analytics = $model->findSingleAnalyticsAccount($parameters[0]);
	//	print_r($analytics);
		$this->set('analytics', $analytics);
		//print_r($this);
		
	}

	function analyticssave($queryString, $parameters){
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		$analytics = $model->saveAnalytics($parameters[0]);
		
	}
	
	function analyticscreate(){
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		$analytics = $model->createNewAnalyticsEntry();
		
	}
}	
	
?>