<?php
class CampaignsController extends Controller {

	function dashboard($queryString, $parameters){

		$this->set('activeLabel', 'Campaigns Overview');

		$modelName = $this->_model;
		$model = new $this->$modelName;

		$campaigns = $model->findActiveCampaigns($parameters[0]);
		$this->set('campaigns', $campaigns);
		
		if($parameters[0] != ''){
			
			$this->set('projectId', $parameters[0]);
		}
		
	
	//	$leads = $model->campaignGetNewLeads($parameters[0], '', true);
	//	$this->set('leads', $leads);

	}
	
	function campaignCreate($queryString, $parameters){
		
		if($parameters[0] == '' || $parameters[1] == ''){
			
			header('location: /admin/campaigns/errorcreate/');
			die();
		}
		
		if($parameters[1] != $_POST['db_client_id_pk_existing_b']){
			
			$campaign = $model->campaignSave($parameters[1]);
			$parameters[1] = (int)$_POST['db_client_id_pk_existing_b'];
		}
		
		
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		// expects project / client
		$model->campaignCreateNew($parameters[0], $parameters[1]);
	}
	
	function campaignEdit($queryString, $parameters){

		$this->set('activeLabel', 'Campaign Details');

		$modelName = $this->_model;
		$model = new $this->$modelName;

		$campaign = $model->campaignEdit($parameters[0]);
		$this->set('campaign', $campaign);
		
		$autoResponder = $model->campaignAutoResponder($parameters[0]);
		$this->set('autoresponder', $autoResponder);
		


	}
	
	function campaignSave($queryString, $parameters){

		$modelName = $this->_model;
		$model = new $this->$modelName;
	
		$model->campaignSave($parameters[0]);
		if($foo = $model->formSave($parameters[0])){
			
			header('location: /admin/campaigns/campaignedit/' . (int)$parameters[0] . '&update');
			die();
		} 
		
	//	$this->set('campaign', $campaign);
		
		


	}
	
	function campaignViewAnalytics($queryString, $parameters){

		$this->set('activeLabel', 'Campaign Analytics');
//		print_r($parameters); die;

		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$analytics = (count($parameters) > 1)? $model->campaignViewAnalytics($parameters[0], $parameters[1], $parameters[2]) : FALSE;
		$this->set('analytics', $analytics);
		
	}
	
	function errorCreate($queryString, $parameters){
		
		$this->set('activeLabel', 'Error Creating Campaign');
	}
}