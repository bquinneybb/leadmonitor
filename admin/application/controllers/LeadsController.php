<?php
class LeadsController extends Controller {
	

	public function dashboard($queryString, $parameters){

		$this->set('activeLabel', 'Leads Overview');

		$modelName = $this->_model;
		$model = new $this->$modelName;

	
		$leads = $model->campaignGetNewLeads($parameters[0], $parameters[1], true);
		$this->set('leads', $leads);

	}
	
	
	public function leadEdit($queryString, $parameters){
		
		$this->set('activeLabel', 'Edit Lead');

		$modelName = $this->_model;
		$model = new $this->$modelName;

	
		$lead = $model->leadEdit($parameters[0]);
		$this->set('lead', $lead);
		//print_r($lead);
		
		$allApartments = $model->apartmentsFindAll($lead['module_project_id_pk']);
		$this->set('allApartments', $allApartments);
		
		$leadApartments = $model->leadFindInterestedApartments($parameters[0]);
		$this->set('leadApartments', $leadApartments);
		
	}
	
	public function leadSave($queryString, $parameters){
		
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		$lead = $model->leadSave($parameters[0]);
	}
}