<?php
class ToolsController extends Controller {
	
	public function dashboard($queryString, $parameters){
		
		$this->set('activeLabel', 'Available Tools');

		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$unconfirmedSearches = $model->findUnconfirmedSearches();
		$this->set('unconfirmedSearches', $unconfirmedSearches);
		
		$domainSaves = $model->findPreviouslySavedDomains();
		$this->set('domainSaves', $domainSaves);
	}
	
	
	public function keywordAnalyse($queryString, $parameters){
		
		$this->set('activeLabel', 'Competitor Keyword Analysis');

		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$search = $parameters[0] != '' ? true : false;
		
		$this->set('search', $search);
		
		
		$campaigns = $model->findActiveCampaigns();
		$this->set('campaigns', $campaigns);
	
	}
	
	public function keywordView($queryString, $parameters){
		
		$this->set('activeLabel', 'Competitor Keyword Analysis');

		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$this->set('searchId', $parameters[0]);
		
		$keywords = $model->findSavedKeywords($parameters[0]);
		$this->set('keywords', $keywords);
		
		$campaigns = $model->findActiveCampaigns();
		$this->set('campaigns', $campaigns);
		
	}
	
	public function keywordSave($queryString, $parameters){
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$model->keywordSave($parameters[0]);
		
	}
	
	public function facebooksearch($queryString, $parameters){
		
		$this->set('activeLabel', 'Facebook Search');

		$modelName = $this->_model;
		$model = new $this->$modelName;
	//	print_r($queryString);
	$searchterm = isset($_POST['searchterm']) ? $_POST['searchterm'] : '';
		$this->set('searchTerm',$searchterm);
		
		
		
	}
	
public function siteanalyse($queryString, $parameters){
		
		$this->set('activeLabel', 'Site Ranking Analysis');

		$modelName = $this->_model;
		$model = new $this->$modelName;
	//	print_r($queryString);
	$searchterm = isset($_POST['q']) ? $_POST['q'] : '';
		$this->set('searchTerm',$searchterm);
		
		
		
	}
	
	public function domainpurchase($queryString, $parameters){
		
			$this->set('activeLabel', 'Register a new Domain Name');

		$modelName = $this->_model;
		$model = new $this->$modelName;
		//print_r($parameters);
		$action = isset($parameters[0]) ? $parameters[0] : 'form';
		$this->set('action',$action);
	//	print_r($_POST);
		$domainName = $parameters[1] != '' ? $parameters[1] : $_POST['db_reg_domain_name'];
		$this->set('domainName',$domainName);
		
		$companyList = $model->findCompanyList();
		$this->set('companyList', $companyList);
	}
	
	
	public function siteSearchSubmit($queryString, $parameters){
		
		$this->set('activeLabel', 'Submit Site to Search Engine');
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		
		
	}
	
	public function competitorMypropertyaddress($queryString, $parameters){
		
		$this->set('activeLabel', 'Analyse http://mypropertyaddress.com.au/individual-property-websites/');
	}
	
	public function googleAdwordsScrape($queryString, $parameters){
		
	}
}

