<?php

class FacebookController extends Controller {
	
	public function dashboard($queryString, $parameters) {

		$modelName = $this -> _model;
		$model = new $this->$modelName;

		$campaigns = $model -> findFacebookCampaigns();
		$this -> set('campaigns', $campaigns);
		
		if($parameters[0] != ''){
			
			$this->set('clientId', $parameters[0]);
		}
	}
	
	public function edit($queryString, $parameters) {
		
			
		$modelName = $this -> _model;
		$model = new $this->$modelName;

		$campaign = $model -> editFacebookCampaign($parameters[0]);
		$this -> set('campaign', $campaign);
		
	}
}

?>
