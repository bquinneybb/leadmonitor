<?php

class EmailerController extends Controller {

	public function dashboard($queryString, $parameters) {

		$modelName = $this -> _model;
		$model = new $this->$modelName;

		$subscriberLists = $model -> findActiveEmailLists();
		$this -> set('subscriberLists', $subscriberLists);
	}

	/* LIST MANAGEMENT */

	public function subscriberListEdit($queryString, $parameters) {

		//print_r($parameters);

		$modelName = $this -> _model;
		$model = new $this->$modelName;

		$subscribers = $model -> findSubscribersInList($parameters[0]);
		$this -> set('subscribers', $subscribers);

		$this -> set('listId', $parameters[0]);
		$this -> set('totalAdded', $parameters[1] != '' ? $parameters[1] : null);
		
		$listName = $model->findListName($parameters[0]);
		$this->set('listName', $listName);
	}

	public function subscriberPasteAddress($queryString, $parameters) {

		$this -> set('hideNavigation', true);
		$this -> set('listId', $parameters[0]);
	}

	public function subsriberListParseNew($queryString, $parameters) {

		$this -> set('hideNavigation', true);
		$this -> set('listId', $parameters[0]);

	}

	public function subscriberListAddNew($queryString, $parameters) {

		$this -> set('hideNavigation', true);

		$modelName = $this -> _model;
		$model = new $this->$modelName;

		$model -> addBulkSubscribersToList($parameters[0]);
	}

	public function subscriberListUpdate($queryString, $parameters) {

		$modelName = $this -> _model;
		$model = new $this->$modelName;

		$subscribers = $model -> findSubscribersInList($parameters[0]);
		$this -> set('subscribers', $subscribers);

		$this -> set('listId', $parameters[0]);
		$this -> set('totalAdded', $parameters[1] != '' ? $parameters[1] : null);
		
		
		$listName = $model->findListName($parameters[0]);
		$this->set('listName', $listName);
		
		$totalRemoved = $model->confirmBulkSubscriberDeletion($parameters[0]);
		
		$this->set('totalRemoved', $totalRemoved);
		
	}
	
	/* TAG EDITING */
	public function subscriberAssignTags($queryString, $parameters){
		
		$modelName = $this -> _model;
		$model = new $this->$modelName;
	}
	
	public function subscriberUpdateTags($queryString, $parameters){
		
			$modelName = $this -> _model;
			$model = new $this->$modelName;
			
			$model->subscriberUpdateTags();
	}

	/* SEND THE EMAILS */

	public function sendChooseEmailer($queryString, $parameters) {

		$modelName = $this -> _model;
		$model = new $this->$modelName;

		$subscribers = $model -> findSubscribersInList($parameters[0]);
		$this -> set('subscribers', $subscribers);
		$this -> set('listId', $parameters[0]);
	}

}
?>