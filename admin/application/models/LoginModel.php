<?php
class LoginModel extends Model {
	
	function attemptLogin($queryString){
		
		User::attemptLogin($queryString, true);
	}
	
	function performLogout(){
		
		User::performLogout(true);
	}
	
	
}