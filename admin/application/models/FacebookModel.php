<?php

class FacebookModel extends Model {
	
	public function findFacebookCampaigns($clientId = ''){
		
		$campaigns = ModuleFacebook::findAdminFacebookCampaigns($clientId);
		return $campaigns;
	}
	
	public function editFacebookCampaign($campaignId){
		
		$campaign = ModuleFacebook::findAdminFacebookCampaignByCampaignId($campaignId);
		
		return $campaign;
		
	}
	
	public function saveFacebookCampaign($campaignId){
		
		ModuleFacebook::updateAdminFacebookCampaignByCampaignId($campaignId);
	}
}

?>