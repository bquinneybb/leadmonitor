<?php 
class ClientModel extends Model {
	
	public function findAllClients(){
		
		$clients = new Client();
		$clientList = $clients->findAllClients();
		
		return $clientList;
		
	}
	
	public function clientCreate(){
		
		$client = new Client();
		$client->createNewClient();
	}
	
	public function findClientInformation($clientId){
		
		
		$client = new Client();
		$clientInformation = $client->findClientInformation($clientId);
		
		return $clientInformation;
	}

	
	public function clientSave($clientId){
		
		$client = new Client();
		$client->saveClientInformation($clientId);
	}
}


?>