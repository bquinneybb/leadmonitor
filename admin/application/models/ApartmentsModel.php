<?php
class ApartmentsModel extends Model {

	public function findActiveProjects(){

		$projects = new ModuleProjects();
		$activeProjects = $projects->findAllProjects();

		return $activeProjects;

	}

	public function projectEdit($projectId){

		$projects = new ModuleProjects();
		$project = $projects->projectEdit($projectId);

		return $project;

	}

	public function findAllApartmentsAndSortByBuilding($projectId){

		$apartment = new ModuleProjectDeveloperApartments();
		$apartments = $apartment->findAllApartmentsAndSortByBuilding($projectId);

		return $apartments;
	}

	public function levelPlanFindData($projectId, $buildingId, $levelId){
		
		$level = new ModuleProjectDeveloperApartments();
		$levelData = $level->findAllBuildingsAndApartments($projectId, $buildingId, $levelId, '', true);
		return $levelData;
		
	}
	
	public function apartmentFindData($projectId, $apartmentId){
		
		$apartment = new ModuleProjectDeveloperApartments();
		$apartmentData = $apartment->findAllBuildingsAndApartments($projectId, '', '', $apartmentId, true);
	
		return $apartmentData;
		
	}
	
	public function apartmentSaveStatus($projectId, $apartmentId){
		
		$apartment = new ModuleProjectDeveloperApartments();
		if($apartment->apartmentSaveStatus($projectId, $apartmentId)){
		
			header('location: /admin/apartments/apartmentedit/' . $projectId . '/' . $apartmentId);
			
		} else {
			
			header('location: /admin/apartments/apartmentedit/' . $projectId . '/' . $apartmentId .  '&error=ownership');
		}
		
		die();
		
	}
}