<?php
class ProjectsModel extends Model {

	public function findAllProjects(){

		$projects = new ModuleProjects();
		$projectList = $projects->findAllProjects();

		return $projectList;

	}

	public function findAllProjectsByClient($clientId){

		$projects = new ModuleProjects();
		// we still need their information even if the projects are blank
		$clientProjectList = $projects->findAllProjects($clientId);

			return $clientProjectList;
		
		

	}

	public function exportProjects(){

		$projects = new ModuleProjects();
		$projectList = $projects->findAllProjects();
		if($projectList)
		{
			foreach ($projectList as $key => $project) 
			{
				echo "<pre>".print_r($project, TRUE).'</pre>'; die;
			}
		}
		return $projectList;

	}

	public function findClientInformation($clientId){
		
		
		$client = new Client();
		$clientInformation = $client->findClientInformation($clientId);
		
		return $clientInformation;
	}
	
	public function projectCreate($clientId){


		if($clientId != ''){

			$projects = new ModuleProjects();
			$projects->projectCreate($clientId);
		}
	}

	public function projectEdit($projectId){

		$projects = new ModuleProjects();
		$project = $projects->projectEdit($projectId);

		return $project;
	}

	public function projectSave($projectId){

		ModuleProjects::projectSave($projectId);
	}

	public function findCampaignsInProject($projectId){

		$campaign = new ModuleCampaigns();
		$campaigns = $campaign->findActiveCampaigns($projectId);
		return $campaigns;
	}


}