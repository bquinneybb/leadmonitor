<?php
class ToolsModel extends Model {

	public function findActiveCampaigns(){

		$campaigns = new ModuleCampaigns();
		$activeCampaigns = $campaigns->findActiveCampaigns();

		return $activeCampaigns;

	}

	public function findUnconfirmedSearches(){
		
		$searches = SEOTools::findPreviousSavedSearches(true);
		return $searches;
	}
	
	public function findSavedKeywords($searchId){
		
		$keywords = SEOTools::findPreviousSavedSearchById($searchId);
		return $keywords;
		
	}
	
	public function keywordSave($searchId){
		
		SEOTools::keywordsUpdateSearch($searchId);
		header('location:/admin/tools/keywordview/' . (int)$searchId);
	}
	
	public function findPreviouslySavedDomains(){
		
		$domains = new ModuleDomain();
		$domainSaves = $domains->findPreviouslySavedDomains();
		
		return $domainSaves;
	}
	
	public function findAllClients(){
		
		$clients = new Clients();
		$clientList = $clients->findAllClients(true);
		return $clientList;
	}
}