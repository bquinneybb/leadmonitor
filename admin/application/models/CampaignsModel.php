<?php
class CampaignsModel extends Model {

	public function findActiveCampaigns($projectId = ''){

		$campaigns = new ModuleCampaigns();
		$activeCampaigns = $campaigns->findActiveCampaigns($projectId);

		return $activeCampaigns;

	}
	
	public function campaignCreateNew($projectId, $clientId){
		
		$campaigns = new ModuleCampaigns();
		$campaigns->campaignCreate($clientId, $projectId);
	}

	public function campaignEdit($campaignId){

		$campaigns = new ModuleCampaigns();
		$campaign = $campaigns->campaignEdit($campaignId);
		
		
		
		return $campaign;

	}

	public function campaignAutoResponder($campaignId){
		
		$autoresponders = new ModuleCampaigns();
		$autoresponder = $autoresponders->findCampaignAutoresponder($campaignId);
		
		
		
		return $autoresponder;

	}
	public function campaignSave($campaignId){

		$campaigns = new ModuleCampaigns();
		
		$campaigns->saveAutoresponder($campaignId);
		
		$campaign = $campaigns->campaignSave($campaignId);
		
		

	//	return $campaign;

	}
	
	public function formSave($campaignId){
		
		$form = new ModuleCampaignsForm();
		return $form->saveFormFields($campaignId);
	}

	public function campaignGetNewLeads($projectId = '', $campaignId = '', $onlyNew){

			$campaigns = new ModuleCampaignsLeads();
			$leads = $campaigns->findAllLeads($projectId, $campaignId, $onlyNew);
			return $leads;

	}
	
	public function campaignViewAnalytics($campaignId, $dateStart = '', $dateEnd = ''){
		
		$analytics = new ModuleAnalytics();
		$data = $analytics->generateVisitorReport($campaignId, $dateStart, $dateEnd );
		return $data;
	}
}