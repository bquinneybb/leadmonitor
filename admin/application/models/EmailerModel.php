<?php

class EmailerModel extends Model {
	
	public function findActiveEmailLists(){
		
		
		$lists = ModuleEmailer::findAllEmailLists($_SESSION['user']['group_id_pk']);
		
		return $lists;
	}
	
	public function findListName($listId){
		
		$listName = ModuleEmailer::findListName($listId, $_SESSION['user']['group_id_pk']);
		return $listName;
	}
	
	public function findSubscribersInList($listId){
		
		$subscribers = ModuleEmailer::findSubscribersInList($listId, $_SESSION['user']['group_id_pk']);
		return $subscribers;
	}
	
	public function addBulkSubscribersToList($listId){
		
		//we're expecting the list in a pipe delimited fashion
		$totalSubscribers = isset($_POST['total']) ? $_POST['total'] : 0;
		$totalAdded = ModuleEmailer::addBulkSubscribersToList($totalSubscribers, $listId);
		
		$status = $totalAdded;
		if($totalAdded != $totalSubscribers){
			
			$status = 0;
		}
		echo '<script>parent.document.location="/admin/emailer/subscriberlistedit/' . $listId . '/' . $status . '&updated";</script>';
	}
	
	
	public function confirmBulkSubscriberDeletion($listId){
		
		if(isset($_POST['confirm'])){
		
			$totalRemoved = ModuleEmailer::removeBullkSubscribersFromList($listId, $_SESSION['user']['group_id_pk']);
			return $totalRemoved;	
		}
	}
	
	
	public function subscriberUpdateTags(){
		
		ModuleEmailer::subscriberUpdateTags();
	}
}
?>