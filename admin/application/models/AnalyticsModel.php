<?php
class AnalyticsModel extends Model {

	public function findAllAnalyticsAccounts(){

		$analytics = new ModuleAnalytics();
		$allAnalytics = $analytics->findClientsToSendAnalyticsTo(false);
	//	print_r($allAnalytics);
		return $allAnalytics;

	}
	
	public function findSingleAnalyticsAccount($accountId){
		
		$analytics = new ModuleAnalytics();
		$analyticsAccount = $analytics->findSingleReport($accountId);
	
		return $analyticsAccount;


		
	}
	
	public function saveAnalytics($accountId){
		
		$analytics = new ModuleAnalytics();
		$analyticsAccount = $analytics->saveAnalyticsSettings($accountId);
		
	}
	
	function createNewAnalyticsEntry(){
		
		$analytics = new ModuleAnalytics();
		$analyticsAccount = $analytics->createNewAnalyticsSettings();

	}
	
}
?>