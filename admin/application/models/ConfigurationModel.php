<?php
class ConfigurationModel extends Model {

	public function findOwnerConfiguration(){

		$configuration = Configuration::getConfigurationForEditing('owner_setup', $_SESSION['user']['group_id_pk']);
		return $configuration;
	}

	public function saveOwnerConfiguration(){

		Configuration::updateConfigurationDetails('owner_setup', $_SESSION['user']['group_id_pk']);
		header('location: ' . SITE_PATH . '/admin/configuration/ownersetupedit/');
	}

	public function findCustomConfiguration(){

		$configuration = Configuration::getConfigurationForEditing('custom_setup', $_SESSION['user']['group_id_pk']);
		return $configuration;
	}

	public function saveCustomConfiguration(){

		Configuration::updateConfigurationDetails('custom_setup', $_SESSION['user']['group_id_pk']);
		header('location: ' . SITE_PATH . '/admin/configuration/customsetupedit/');
	}

	public function findOwnerDetails($parameters){


		$owner = User::getUserForEditing($parameters[0], $parameters[1], $_SESSION['user']['user_id_pk']);
		//print_r($owner);
		return $owner;
	}

	public function findMainMenuItems($parameters){


		$configuration = Configuration::getConfigurationForEditing('menu_main', $parameters[0], $parameters[1], $parameters[2]);
		return $configuration;
	}

}