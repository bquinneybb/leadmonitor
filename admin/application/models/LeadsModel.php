<?php
class LeadsModel extends Model {

	public function campaignGetNewLeads($projectId = '', $campaignId = '', $onlyNew){

		$campaigns = new ModuleCampaignsLeads();
		$leads = $campaigns->findAllLeads($projectId, $campaignId, $onlyNew);
		return $leads;

	}
	
	public function leadEdit($leadId){
		
		$leads = new ModuleCampaignsLeads();
		$lead = $leads->leadEdit($leadId);
		
		return $lead;
	}
	
	public function apartmentsFindAll($projectId){
		$apartments = new ModuleProjectDeveloperApartments();
		$apartmentsInProject = $apartments->findAllApartmentsAndSortByBuilding($projectId);
		return $apartmentsInProject;
	}
	
	public function leadFindInterestedApartments($leadId){
		
		$apartments = ModuleCampaignsLeads::leadFindInterestedApartments($leadId);
		return $apartments;
	}
	
	
	public function leadSave($leadId){
		
		$leads = new ModuleCampaignsLeads();
		$lead = $leads->leadSave($leadId);
	}

}