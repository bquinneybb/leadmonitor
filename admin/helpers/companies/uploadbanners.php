<?php 

include ('../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$bannerType = isset($_POST['uploadType']) ? $_POST['uploadType'] : null;
$companyId = isset($_POST['companyId']) ? $_POST['companyId'] : null;
if($bannerType == null || $companyId == null){
	
	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();
$banner = new ClientCompany();
$success = $banner->updateClientBanner($bannerType, $companyId);
echo $success;

?>