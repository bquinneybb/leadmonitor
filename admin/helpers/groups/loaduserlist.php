<?php 

include ('../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$groupId = isset($_POST['groupId']) ? $_POST['groupId'] : null;

if($groupId == null){

	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();



if($userList = Group::getAllUsersInGroup($groupId, true, true)){

echo json_encode($userList);
} else {
	
	echo 0;
}

?>