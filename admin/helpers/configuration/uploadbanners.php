<?php 

include ('../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$bannerType = isset($_POST['uploadType']) ? $_POST['uploadType'] : null;

if($bannerType == null){
	
	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();
$configuration = new Configuration();
$success = $configuration->updateConfigurationOwnerBanner($bannerType);
echo $success;

?>