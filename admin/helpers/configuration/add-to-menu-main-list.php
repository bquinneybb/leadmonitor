<?php
include ('../../../_config/config.php');


// we want to decrypt the table and column names when we get here
$_POST = array_merge($_GET, $_POST);
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$bitString = isset($_POST['bitString']) ? decrypt($_POST['bitString'], true) : null;

$fieldValues = isset($_POST['menu_main']) ? $_POST['menu_main'] : null;

// explode the bitString so we obtain the correct set of data to add to the database
$bitMask = explode('|', $bitString);
$groupParentId = $bitMask[0];
$groupId = $bitMask[1];
$userId = $bitMask[2];


session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();



if($bitString == null || $groupParentId == null || $groupId == null || $userId == null){

	die();
}

// now check they're high enough level
$checkUser = new User();
$requiredLevel = $checkUser->convertRoleToLevel('superadmin');

if($allowed = $checkUser->checkEditPermissionsAgainstLevel($_SESSION['user']['user_id_pk'], $requiredLevel)){

	$configurationItems = array();

	// does the configuration already exist
	$configuration = new Configuration;
	
	

	$existingConfiguration = $configuration->getConfigurationForEditing('menu_main', $groupParentId, $groupId, $userId);

	if(!$existingConfiguration){

		$configurationItems = array('Label', 'URL', 'Contoller');
		
		$query = "INSERT INTO `configuration`
			VALUES('', ?, ?, ?, 'menu_main', ?, 1)";

		$types = 'iiis';
		$parameters = array($groupParentId, $groupId, $userId, json_encode($configurationItems));

		$insertId = Database::dbInsert($query, $types, $parameters);
		echo 'i = ' . $insertId;
		die();
	
	} else {
		
		
		print_r( $existingConfiguration);
		
	}


}
?>
