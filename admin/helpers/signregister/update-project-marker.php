<?php 
include ('../../../_config/config.php');



$_POST = array_merge($_GET, $_POST);
$projectId = isset($_POST['id']) ? $_POST['id'] : null;
$cords = isset($_POST['cords']) ? $_POST['cords'] : '0,0';
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();

// check ownership
if(!$project = ModuleSignRegister::findSignProjectById($projectId)){
	
	die();
	
}
$project = new ModuleSignRegister;
$checkDistance = $project->updateProjectMarkerPosition( $projectId, $cords);
echo $checkDistance;
?>