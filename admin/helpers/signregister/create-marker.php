<?php 

include ('../../../_config/config.php');



$_POST = array_merge($_GET, $_POST);
$id = isset($_POST['db_module_signregister_id_pk']) ? $_POST['db_module_signregister_id_pk'] : null;
$clientId = isset($_POST['db_module_client_id_pk']) ? $_POST['db_module_client_id_pk'] : null;
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();


// check ownership
if(!$project = ModuleSignRegister::findSignProjectById($id) || $clientId == null){
	
	die();
	
}
ModuleSignRegisterAssets::createMarker($project, $clientId);
?>