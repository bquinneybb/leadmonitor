<?php 
include ('../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$projectId = isset($_POST['id']) ? $_POST['id'] : null;
$uid = isset($_POST['uid']) ? $_POST['uid'] : null;
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();

// check it's a valid project
if(!$project = ModuleSignRegister::findSignProjectById($projectId)){

	die();
	
}
//print_r($_SESSION);die();
// check the directory exists
ModuleSignRegisterAssets::uploadMapToProject($projectId, $uid);
?>