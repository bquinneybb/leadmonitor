<?php 
include ('../../../_config/config.php');



$_POST = array_merge($_GET, $_POST);
$id = isset($_POST['id']) ? $_POST['id'] : null;
$marker = isset($_POST['marker']) ? $_POST['marker'] : null;
$cords = isset($_POST['cords']) ? $_POST['cords'] : '0,0';
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();

// check ownership
if(!$project = ModuleSignRegister::findSignProjectById($id)){
	
	die();
	
}
ModuleSignRegisterAssets::updateMarkerPosition($cords, $marker);
?>