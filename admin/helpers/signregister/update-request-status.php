<?php 
include ('../../../_config/config.php');



$_POST = array_merge($_GET, $_POST);
$projectId = isset($_POST['project']) ? $_POST['project'] : null;
$signId = isset($_POST['id']) ? $_POST['id'] : null;
$commentId = isset($_POST['commentId']) ? $_POST['commentId'] : null;
$comment = isset($_POST['comment']) ? $_POST['comment'] : null;
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;


session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();

// check ownership
if(!$project = ModuleSignRegister::findSignProjectById($projectId) || $signId == null){
	
	die();
	
}

// update this ticket
	ModuleSignRegisterRequests::updateSignStatusRequest($comment, $projectId,  $signId, $commentId);



?>