<?php 
include ('../../../_config/config.php');
include ('../../application/shared.php');


$_POST = array_merge($_GET, $_POST);
$id = isset($_POST['id']) ? $_POST['id'] : null;
$uid = isset($_POST['uid']) ? $_POST['uid'] : null;
$signid = isset($_POST['signid']) ? $_POST['signid'] : null;
$face =  isset($_POST['face']) ? $_POST['face'] : 0;
$imageToCrop = isset($_POST['imageid']) ? $_POST['imageid'] : null;
$previousDropbox = isset($_POST['dropboxState']) ? $_POST['dropboxState'] : null;
	
$session = new SessionManager();
session_start();
checkAccessAllowed();

// check ownership
if(!$project = ModuleSignRegister::findSignProjectById($id) || $signid == null || $uid == null){

	die();
	
}
if(ModuleSignRegisterAssets::cropSignImage($id, $uid, $signid, $face, $imageToCrop, $previousDropbox)){

?>
<script type="text/javascript">
parent.saveForm();
</script>	
	<?php 
}
?>
