<?php 
include ('../../../_config/config.php');
include ('../../application/shared.php');


$_POST = array_merge($_GET, $_POST);
$projectId = isset($_POST['id']) ? $_POST['id'] : null;
$id = isset($_POST['signid']) ? $_POST['signid'] : null;
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$field =  isset($_POST['field']) ? $_POST['field'] : null;
$value =  isset($_POST['value']) ? $_POST['value'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();

// check ownership
if(!$project = ModuleSignRegister::findSignProjectById($projectId) || $id == null){
	
	die();
	
}


ModuleSignRegisterAssets::updateSignSummary($field, $value, $id);
?>