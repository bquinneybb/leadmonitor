<?php 
include ('../../../_config/config.php');
include ('../../application/shared.php');


$_POST = array_merge($_GET, $_POST);
$id = isset($_POST['id']) ? $_POST['id'] : null;
$marker = isset($_POST['marker']) ? $_POST['marker'] : null;
$top = isset($_POST['top']) ? $_POST['top'] : null;
$left = isset($_POST['left']) ? $_POST['left'] : null;
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();

// check ownership
if(!$project = ModuleSignRegister::findSignProjectById($id)){
	
	die();
	
}
ModuleSignRegisterAssets::updateMarkerPosition($left, $top, $marker);
?>