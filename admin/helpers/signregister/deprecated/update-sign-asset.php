<?php
include ('../../../_config/config.php');
include ('../../application/shared.php');


$_POST = array_merge($_GET, $_POST);
$projectId = isset($_POST['id']) ? $_POST['id'] : null;
$uid = isset($_POST['uid']) ? $_POST['uid'] : null;
$signid = isset($_POST['signid']) ? $_POST['signid'] : null;
$face =  isset($_POST['face']) ? $_POST['face'] : 0;
$imageToCrop = isset($_POST['cropImage']) ? $_POST['cropImage'] : 0;
$error = isset($_POST['error']) ? $_POST['error'] : 0;
$update = isset($_POST['update']) ? $_POST['update'] : null;


$session = new SessionManager();
session_start();
checkAccessAllowed();

// check ownership
if(!$project = ModuleSignRegister::findSignProjectById($projectId) || $signid == null || $uid == null){

	die();

}
$project = ModuleSignRegister::findSignProjectById($projectId);

$templateItems = new TemplateItems();
$templateItems->returnOnly = true;
$signAssets = new ModuleSignRegisterAssets();
$assets = $signAssets->findSignItemAssets($uid, $signid, $project['module_client_id_pk']);
//print_r($assets);

$signImagePath = '/fileserver/module-signregister/' . $uid . '/signs/' . (int)$signid . '/';

Template::loadCSS('/public/css/core.css.php');
Template::loadCSS('/public/css/modal.css.php');
Template::loadCSS('/public/modules/signregister/css/core.css.php');
Template::loadCSS('/public/modules/signregister/css/admin.css.php');
Template::loadJavascript('/public/javascript/jquery/jquery.min.js');
Template::loadJavascript('/public/javascript/jcrop/js/jquery.Jcrop.min.js');
Template::loadCSS('/public/javascript/jcrop/css/jquery.Jcrop.css');
$faceAsset = 'back';
if($face == 1){

	$faceAsset = 'front';
}

$link = null;
//echo 'a = ' . $assets[$signid][$faceAsset][0];
if(isset($assets[$signid][$faceAsset][0])){

	$link = $signImagePath . $assets[$signid][$faceAsset][0];

}

?>
<?php $templateItems->drawScreen(); ?>

<div class="modal"><?php if($error != null){ ?> <span class="error">There
was a problem cropping your image. Your dropbox has changed between
selecting the image and cropping it.</span> <?php } ?>

 <?php 
$dropbox = new Dropbox();

$libraryImages = $dropbox->scanDropbox($_SESSION['user']['user_id_pk'], 'signregister', $projectId);
//print_r($libraryImages);
$libraryImagesAssets = array();
if($libraryImages){
	$c=0;
	foreach($libraryImages as $libraryImage){

		list($width, $height, $type, $attr) = getimagesize(ROOT . $libraryImage);
		$libraryImagesAssets[$c]['url'] = $libraryImage;
		$libraryImagesAssets[$c]['height'] = $height;
		$libraryImagesAssets[$c]['width'] = $width;
		$c++;
	}
	$libraryImagesAssetsRaw = $libraryImagesAssets;
	$libraryImagesAssets = json_encode($libraryImagesAssets);
	//	print_r($libraryImagesAssetsRaw);
	//print_r(stripslashes($libraryImagesAssetsRaw));
//	print_r(count($libraryImagesAssetsRaw));
} else {
	
	$libraryImagesAssets = '';
}

?>

<div id="updateAssetEditImage">
<h1>Select Crop Area</h1>
<?php if($libraryImages){ ?>

<form method="post" enctype="multipart/form-data" id="cropform"
	name="cropform" action="crop-sign-asset.php">

<table>
	<tr>
		<td><img src="<?php echo $libraryImages[$imageToCrop] ?>" id="cropbox" />
		<td style="padding-left: 20px" valign="top"><!--  existing image -->

		<div id="updateAssetExistingImage">
		<h2>Last Crop / Sign Image</h2>
		<?php if($link != null && file_exists(ROOT . $link)){ ?> <img
			src="../../../_utils/phpThumb/phpThumb.php?src=<?php echo $link ?>&w=200" />
			<?php } else { ?> <img
			src="<?php echo SITE_PATH ?>public/plugins/module-signregister/images/assets/no-image-loaded.jpg" />
			<?php } ?></div>

		<!--  end existing image --></td>
	
	</tr>
</table>


<input name="action" type="hidden" id="action" value="crop" /> <input
	type="hidden" id="x" name="x" /> <input type="hidden" id="y" name="y" />
<input type="hidden" id="w" name="w" /> <input type="hidden" id="h"
	name="h" /> <input type="hidden" id="imageid" name="imageid"
	value="<?php echo $imageToCrop ?>" /> <?php foreach($_POST as $key=>$value){ ?>
			<?php if($key != 'cropImage'){ ?> <input type="hidden"
	value="<?php echo $value ?>" name="<?php echo $key ?>"
	id="<?php echo $key ?>" /> <?php } ?> <?php } ?> <input type="hidden"
	id="dropboxState" name="dropboxState"
	value="<?php echo md5(serialize($libraryImages)); ?>" /> <br />
			<?php $templateItems->drawIcon('crop'); ?> <a href="#"
	onclick="cropImage();return false" class="bold">Crop Image</a></form>
	</div>
<div class="clear"></div>


<hr />

<!--  image library -->
<h1>Image Library</h1>


<form id="changeImageForm" name="changeImageForm" method="post"><?php foreach($_POST as $key=>$value){ ?>
			<?php if($key != 'cropImage'){ ?> <input type="hidden"
	value="<?php echo $value ?>" name="<?php echo $key ?>"
	id="<?php echo $key ?>" /> <?php } ?> <?php } ?> <input type="hidden"
	value="<?php echo $imageToCrop ?>" name="cropImage" id="cropImage" /></form>
<div id="imageLibrary"><?php if($libraryImages){
	$c=0;
	foreach($libraryImages as $libraryImage){

		?>
<div class="imageLibraryThumbnail"><!-- 
<a href="#" onclick="updateCropImage(<?php echo $c ?>);return false"><img src="<?php echo UTIL_PHPTHUMB ?>?src=<?php echo $libraryImage ?>&w=100" /></a>
 --> <a href="#"
	onclick="updateCropImage(<?php echo $c ?>);return false"><img
	src="<?php echo $libraryImage ?>" width="100" /></a></div>
		<?php

		$c++;
	}

}?> <!--  end image library --></div>
</div>
<script language="Javascript">
libraryAssets = '<?php echo stripslashes($libraryImagesAssets) ?>';
$imageToCrop = <?php echo $imageToCrop ?>;
$previewHeight =  <?php echo $signAssets->signPreviewHeight ?>;
$previewWidth = <?php echo $signAssets->signPreviewWidth ?>;
</script>
<?php Template::loadJavascript('/admin/application/views/signregister/javascript/cropsignimage.js'); ?>
		<?php } else { ?>
<h1>You don't have images uploaded yet.</h1>
			<?php } ?>