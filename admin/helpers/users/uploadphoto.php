<?php 

include ('../../../_config/config.php');

$_POST = array_merge($_GET, $_POST);

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;
$userId = isset($_POST['userId']) ? $_POST['userId'] : null;

if($userId == null){
	
	echo '0';
	die();
}

session_id(decrypt($sid, true));
$session = new SessionManager();

session_start();

checkAccessAllowed();
$user = new User();
$success = $user->updateUserPhoto($userId);
echo $success;

?>