<?php
include ('../../../_config/config.php');
include ('../../application/shared.php');


$_POST = array_merge($_GET, $_POST);
$username = isset($_POST['username']) ? $_POST['username'] : null;
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();


$result = User::checkIfUserAlreadyExists($username);
echo $result;
?>