<?php
include ('../../../_config/config.php');

// we want to decrypt the table and column names when we get here
$_POST = array_merge($_GET, $_POST);
$table = isset($_POST['table']) ? decrypt($_POST['table']) : null;
$where = isset($_POST['where']) ? decrypt($_POST['where']) : null;
$fieldId = isset($_POST['fieldId']) ? $_POST['fieldId'] : null;
$fieldValue = isset($_POST['fieldValue']) ? $_POST['fieldValue'] : null;

$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();



if($table == null || $where == null || !in_array($table, $tableWhitelist) || !in_array($where, $columnWhitelist)){

	die();
}


	$query = "UPDATE `" . ($table) . "` 
			SET `list_value` = ?
			WHERE `". $where . "` = ?
			AND `group_parent_id_pk` = ?
			AND `group_id_pk` = ?";

	$types = 'siii';
	$parameters = array($fieldValue, $fieldId, $_SESSION['user']['group_parent_id_pk'], $_SESSION['user']['group_id_pk']);

	Database::dbQuery($query, $types, $parameters);