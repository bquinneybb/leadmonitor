<?php
include ('../../../_config/config.php');


// we want to decrypt the table and column names when we get here
$_POST = array_merge($_GET, $_POST);
$table = isset($_POST['table']) ? decrypt($_POST['table']) : null;
$where = isset($_POST['where']) ? decrypt($_POST['where']) : null;
$sid = isset($_POST['sid']) ? $_POST['sid'] : null;

session_id(decrypt($sid, true));
$session = new SessionManager();
session_start();
checkAccessAllowed();



if($table == null || $where == null || !in_array($table, $tableWhitelist) || !in_array($where, $columnWhitelist)){

	die();
}



if($_POST['reorderable'][0] == ''){
	
	unset($_POST['reorderable'][0]);
	
}
$counter = 0;
foreach($_POST['reorderable'] as $order){


	$query = "UPDATE `" . ($table) . "` 
			SET `ordering` = '" . (int)$counter . "'
			WHERE `". $where . "` = ?
			AND `group_parent_id_pk` = ?
			AND `group_id_pk` = ?
			";

	$types = 'iii';
	$parameters = array($order, $_SESSION['user']['group_parent_id_pk'], $_SESSION['user']['group_id_pk']);

	Database::dbInsert($query, $types, $parameters);
	$counter++;


}