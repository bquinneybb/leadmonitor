<?php class Console {

	private $connection = NULL;
	
public function __construct($location, $wsdl, $username, $password, $proxyHost = NULL, $proxyPort = NULL) {
if(is_null($proxyHost) || is_null($proxyPort)) $connection = new SoapClient($wsdl, array('login' => $username, 'password' => $password));
else $connection = new SoapClient($wsdl, array('login' => $username, 'password' => $password, 'proxy_host' => $proxyHost, 'proxy_port' => $proxyPort));
$connection->__setLocation($location); $this->connection = $connection; return $this->connection;
}

public function screen($var) { print '<pre>'; print_r($var);
print '</pre>'; return $this->connection;
}
public function srv() { return $this->connection;
}
}
?>