<?php class Client extends Console {
	// stores the client data private $data = array(); // stores the result of the last method private $response = NULL;
	/** * Sets data into the client data array. This will be later referenced by * other methods in this object to extract data required. * * @param str $key - The data parameter * @param str $value - The data value * @return $this - The current (self) object */
	public function set($key, $value) { $this->data[$key] = $value;
	return $this;
	}
	/** * dels data in the client data array. * * @param str $key - The data parameter * @return $this - The current (self) object */
	public function del($key) { unset($this->data[$key]);
	return $this;
	}
	/** * Gets data from the client data array. * * @param str $key - The data parameter * @return str $value - The data value */
	private function get($key) { return $this->data[$key];
	}
	/** * Returns the result / server response from the last method
	 * * @return $this->response */
	public function response() { return $this->response;
	}
	/**
	 * Check domain name availability * * @return $this - The current (self) object */
	public function domainLookup() { $this->response = $this->srv()->domainLookup(array('domain' => $this->get('domain'))); return $this;
	}
	/** * Perform a whois lookup on a domain name * * @return $this - The current (self) object */
	public function domainInfo() { $this->response = $this->srv()->domainInfo(array('domain' => $this->get('domain'))); return $this;
	}
	/** * Perform a whois lookup on a domain, with authcode / domain password * if the authcode / domain password is correct, full whois data is provided regardless * if the domain belongs to the reseller account. * * @return $this - The current (self) object */
	public function domainInfoWithPassword() {
		$this->response = $this->srv()->domainInfoWithPassword(array('domain' => $this->get('domain'), 'password' => $this->get('password')));
		return $this;
	}
	/** * Request renewal of domain name * * @return $this - The current (self) object */
	public function renewDomain() {
		$this->response = $this->srv()->renewDomain(array('domain' => $this->get('domain'), 'period' => $this->get('period')));
		return $this;
	}
	/**
	 /** * Look up the do
	 * main authcode / password * * @return $this - The current (self) object */

	public function domainAuthcode() {

		$this->response = $this->srv()->domainAuthcode(array('domain' => $this->get('domain'))); return $this;

	}
	/**
	 * * Generate new domain authcode / password * * @return $this - The current (self) object */
	public function domainGenerateAuthcode() { $this->response = $this->srv()->domainGenerateAuthcode(array('domain' => $this->get('domain'), 'authcode' => $this->get('authcode')));

	return $this;
	}
	/** * Check available pre-paid balance in reseller account * * @return $this - The current (self) object */
	public function resellerBalance() { $this->response = $this->srv()->resellerBalance(); return $this;
	}
	/** * Check the status of an order * * @return $this - The current (self) object */
	public function orderStatus() { $this->response = $this->srv()->orderStatus(array('orderId' => $this->get('orderId'))); return $this;
	}
	/** * Update the name server on an existing domain * * @return $this - The current (self) object */
	public function updateDomainNS() {
		$this->response = $this->srv()->updateDomainNS(array('domain' => $this->get('domain'), 'nameServers' => $this->get('nameServers')));
		return $this;
	}
	/** * Request the inbound transfer of an existing domain name from another Registrar to Netregistry * * @return $this - The current (self) object */
	public function transferDomain() {
		$this->response = $this->srv()->transferDomain(array('domain' => $this->get('domain'), 'contactDetails' => $this->get('contactDetails'), 'authcode' => $this->get('authcode'), 'period' => $this->get('period')));
		return $this;
	}
	/**
	 * * Request the registration of a domain name * * @return $this - The current (self) object */
	public function registerDomain() { $this->response = $this->srv()->registerDomain(array('domain' => $this->get('domain'),'contactDetails' => $this->get('contactDetails'), 'eligibility' => $this->get('eligibility'), 'period' => $this->get('period'), 'nameServers' => $this->get('nameServers')));
	return $this;
	}
}
?>