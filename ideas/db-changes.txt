// add a field so we can see what it's called - shouldn't affect any inserts
// add a field in case we want to use different email templates for the same campaign
ALTER TABLE  `module_campaign_lead_autoresponder` ADD  `autoresponder_title` VARCHAR( 255 ) NOT NULL
ALTER TABLE  `module_campaign_lead_autoresponder` ADD  `module_campaign_lead_autoresponder_id_pk` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST

// change of email table - not in use yet anyway

ALTER TABLE  `module_email` DROP  `email_label` ,
DROP  `email_body` ,
DROP  `use_custom_script` ,
DROP  `complete` ,
DROP  `history` ,
DROP  `active` ;


// swap the email_sent table for a modified copy of auto responder one
DROP TABLE  `module_email_sent`

// added leadsource
private function addTackingPixel($leadId) {
