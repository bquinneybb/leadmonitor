<?php
class ScraperLookle extends Scraper {

	function scrapeSearch($searchTerm){

		$results = $this->getLooklePage($searchTerm);
		$formatted = new ScraperSEO();
		$html = $formatted->returnFormatedResults($results);
		return $html;
	}

	public function getLooklePage($query){

		$blockedDomains = ScraperSEO::findBlockedDomains();
		
		$query .= $blockedDomains;
		
		$url = 'http://www.lookle.com/search/index.php?page=search/web&search=' . urlencode($query) . '&type=web&fl=0';
		
		
		
		//	echo $url;die();
		$contents = $this->getURLContents($url, '', false);
		//$contents = file_get_contents(ROOT . '/fileserver/index.php.html');
		//echo $results;die();
		$html = new simple_html_dom();


		//echo $contents;die();
		$html->load($contents);
		//$html->load_file();

		$scrape = array();
		$c = 0;
		$i = 0;
		foreach($html->find('.resultlink') as $result){

			if($i%2 == 0){
				$scrape[$c]['url'] = $result->href;
				$c++;
			}
			$i++;
		}
		
		$c = 0;
		foreach($html->find('.resulttitle') as $result){

			$scrape[$c]['headline'] = strip_tags($result->plaintext);
			$c++;
				
		}
		$c = 0;
		foreach($html->find('.resultdescription') as $result){
			
			$scrape[$c]['description'] = strip_tags($result->plaintext);
			$c++;

		}
		//print_r($scrape);
		return($scrape);
	}


}