<?php

class SessionManager {

	/* THIS ARTICLE HAS A GOOD BIT OF CODE ON AUTO LOGGING OUT PEOPLE
	 * WHICH COULD BE USEFUL FOR THE 'TABLE LOCKING' FUNCITON
	 * http://www.talkphp.com/mysql-databases/1020-5-useful-mysql-functions-control-flows.html
	 * UPDATE
	 `$this->db->table['users']`
	 SET
	 `$this->db->col['user_session']` = ''
	 WHERE
	 DATE_SUB(NOW(), INTERVAL 15 MINUTE) > `$this->db->col['user_last_action']`;
	 */

	var $life_time = 300;

	function SessionManager() {

		// Read the maxlifetime setting from PHP
		$this->life_time = get_cfg_var('session.gc_maxlifetime');

		// Register this object as the session handler
		session_set_save_handler(
		array( &$this, 'open' ),
		array( &$this, 'close' ),
		array( &$this, 'read' ),
		array( &$this, 'write'),
		array( &$this, 'destroy'),
		array( &$this, 'gc' )
		);

	}

	function open( $save_path, $session_name ) {

		global $sess_save_path;

		$sess_save_path = $save_path;

		// Don't need to do anything. Just return TRUE.

		return true;

	}

	function close() {

		return true;

	}

	function read( $id ) {

		//$db = new Database();

		// Set empty result
		$data = '';

		// Fetch session data from the selected database
		$sql = "SELECT `session_data` FROM `sessions` WHERE `session_id` = ? AND `expires` > ?";

		$time = time();

		// $DBH = Database::dbConnect(NULL);
		// $newid = mysql_real_escape_string($id);
		$newid = Database::escape_value($id);

		$parameters = array($id, $time);
		//print_r($parameters);die();
			
		if($rs = Database::dbRow($sql, $parameters)){

			$data = $rs['session_data'];

		} else {

			$log = new Logging();
			$log->logVisitor();
		}
		//print_r($data);
		return $data;

	}

	function write( $id, $data ) {
			
		$db = new Database();
		// Build query

		//	$link = new Database;
		$newid = $id;
		$newdata = $data;
		$time = time() + $this->life_time;
			
		$dataTypes = 'ssissi';
		$parameters = array($newid, $newdata, $time, $newid, $newdata, $time);
			
		//$sql = "REPLACE INTO `sessions` (`session_id`,`session_data`,`expires`) VALUES(?, ?, ?)";
			
		$sql= "INSERT INTO sessions (`session_id`,`session_data`,`expires`) VALUES (?, ?, ?)
  			ON DUPLICATE KEY UPDATE `session_id` = ?, `session_data` = ?, `expires` = ?";
		Database::dbQuery($sql, $dataTypes, $parameters);
//		$rs = $db->dbQuery($sql, $dataTypes, $parameters);
			
		return TRUE;

	}

	function destroy( $id ) {

		//$db = new Database();
			
		// Build query
		$newid = $id;
		$dataTypes = 'i';
		$parameters = array($newid);
			
		$sql = "DELETE FROM `sessions` WHERE `session_id` = ?";
			
		Database::dbQuery($sql, $dataTypes, $parameters);
			
		return TRUE;

	}

	function gc() {

		//$db = new Database();
		// Garbage Collection

		// Build DELETE query.  Delete all records who have  passed the expiration time
		$sql = 'DELETE FROM `sessions` WHERE `expires` <  UNIX_TIMESTAMP();';
		$dataTypes = '';
		$parameters = array();

		Database::dbQuery($sql, $dataTypes, $parameters);

		// Always return TRUE
		return true;

	}

}

?>