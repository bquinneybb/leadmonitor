<?php
class Menu  {
	
	/**
	 * The menu class is design to serve up an individual menu for each client as defined in their configuration
	 * It will look up the model in the database to see what sub menu items are available
	 */
	
	
	function __construct(){
		
		$this->drawSubmenu();
		
	}
	
	private function drawSubmenu(){
		
		$html = '';
		
		$menu['model'][] = array('foo', 'foo');
		$menu['model'][] = array('bar', 'bar');
		
		$html .= '<ul class="subMenu">' . "\r\n";
		
		foreach($menu['model'] as $subMenuItem){
			
			$html .= '<li"><a href="' . $subMenuItem[1] . '">' . $subMenuItem[0] . '</a></li>' . "\r\n";
			
		}
		
		$html .= '</ul>' . "\r\n";
		
		echo $html;
		
	}
}