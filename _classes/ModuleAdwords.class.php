<?php
$path = dirname(__FILE__) . '/../_utils/GoogleAdwords';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
require_once 'Google/Api/Ads/AdWords/Lib/AdWordsUser.php';
require_once 'Google/Api/Ads/Common/Util/MapUtils.php';
require_once 'Google/Api/Ads/Common/Util/MediaUtils.php';

class ModuleAdwords {



	function getAllActiveCampaigns(){

		try {
			// Get AdWordsUser from credentials in "../auth.ini"
			// relative to the AdWordsUser.php file's directory.
			$user = new AdWordsUser();

			// Log SOAP XML request and response.
			$user->LogDefaults();

			// Get the CampaignService.
			$campaignService = $user->GetCampaignService('v201101');

			// Create selector.
			$selector = new Selector();
			$selector->fields = array('Id', 'Name');
			$selector->ordering = array(new OrderBy('Name', 'ASCENDING'));

			// Get all campaigns.
			$page = $campaignService->get($selector);

			// Display campaigns.
			if (isset($page->entries)) {
				foreach ($page->entries as $campaign) {
					print 'Campaign with name "' . $campaign->name . '" and id "'
					. $campaign->id . "\" was found.\n";
				}
			} else {
				print "No campaigns were found.\n";
			}
		} catch (Exception $e) {
			print $e->getMessage();
		}

	}

	function getAllAdGroups($campaignId){


		try {
			// Get AdWordsUser from credentials in "../auth.ini"
			// relative to the AdWordsUser.php file's directory.
			$user = new AdWordsUser();

			// Log SOAP XML request and response.
			$user->LogDefaults();

			// Get the AdGroupService.
			$adGroupService = $user->GetAdGroupService('v200909');

			$campaignId = (float) $campaignId;

			// Create selector.
			$selector = new AdGroupSelector();
			$selector->campaignId = $campaignId;

			// Get all ad groups.
			$page = $adGroupService->get($selector);
			//print_r($page);
			// Display ad groups.
			if (isset($page->entries)) {
				foreach ($page->entries as $adGroup) {

					print 'Ad group with name "' . $adGroup->name . '"and id "'
					. $adGroup->id . "\" was found.\n";
				}
			} else {
				print "No ad groups were found.\n";
			}
		} catch (Exception $e) {
			print $e->getMessage();
		}

	}

	function getAllAdsForAdgroup($adGroupId){

		try {
			// Get AdWordsUser from credentials in "../auth.ini"
			// relative to the AdWordsUser.php file's directory.
			$user = new AdWordsUser();

			// Log SOAP XML request and response.
			$user->LogDefaults();

			// Get the AdGroupAdService.
			$adGroupAdService = $user->GetAdGroupAdService('v200909');

			$adGroupId = (float) $adGroupId;

			// Create selector.
			$selector = new AdGroupAdSelector();
			$selector->adGroupIds = array($adGroupId);

			// Get all ads.
			$page = $adGroupAdService->get($selector);
			if (isset($page->entries)) {

				return $page->entries;
			}
			/*
			 // Display ads.
			 if (isset($page->entries)) {
			 foreach ($page->entries as $adGroupAd) {
			 //	print_r($adGroupAd);
			 print 'Ad with id "' . $adGroupAd->ad->id . '" and type "'
			 . $adGroupAd->ad->AdType . "\" was found.\n";
			 }
			 } else {
			 print "No ads were found.\n";
			 }
			 */
		} catch (Exception $e) {
			print $e->getMessage();
		}

	}

	function getAllAdParamters($adGroupId){

		try {
			// Get AdWordsUser from credentials in "../auth.ini"
			// relative to the AdWordsUser.php file's directory.
			$user = new AdWordsUser();

			// Log SOAP XML request and response.
			$user->LogDefaults();

			// Get the AdParamService.
			$adParamService = $user->GetAdParamService('v200909');

			$adGroupId = (float) $adGroupId;

			// Create selector.
			$selector = new AdParamSelector();
			$selector->adGroupIds = array($adGroupId);

			// Get all ad parameters.
			$page = $adParamService->get($selector);

			// Display ad parameters.
			if (isset($page->entries)) {
				foreach ($page->entries as $adParam) {
					print 'Ad parameter with ad group id "' . $adParam->adGroupId
					. '", criterion id "' . $adParam->criterionId
					. '", insertion text "' . $adParam->insertionText
					. '", and parameter index "' . $adParam->paramIndex
					. "\" was found.\n";
				}
			} else {
				print "No ad parameters were found.\n";
			}
		} catch (Exception $e) {
			print $e->getMessage();
		}

	}

	function getRelatedKeywords($keywords, $startIndex = 0, $perPage = 10){

		try {
			// Get AdWordsUser from credentials in "../auth.ini"
			// relative to the AdWordsUser.php file's directory.
			$user = new AdWordsUser();

			// Log SOAP XML request and response.
			$user->LogDefaults();

			// Get the TargetingIdeaService.
			$targetingIdeaService = $user->GetTargetingIdeaService('v201101');

			// Create seed keyword.
			$keyword = new Keyword();
			$keyword->text = $keywords;
			$keyword->matchType = 'BROAD';

			// Create selector.
			$selector = new TargetingIdeaSelector();
			$selector->requestType = 'IDEAS';
			$selector->ideaType = 'KEYWORD';
			$selector->requestedAttributeTypes = array('CRITERION', 'AVERAGE_TARGETED_MONTHLY_SEARCHES');

			// Set selector paging (required for targeting idea service).
			$paging = new Paging();
			$paging->startIndex = $startIndex;
			$paging->numberResults = $perPage;
			$selector->paging = $paging;

			// Create related to keyword search parameter.
			$relatedToKeywordSearchParameter = new RelatedToKeywordSearchParameter();
			$relatedToKeywordSearchParameter->keywords = array($keyword);

			// Create keyword match type search parameter to ensure unique results.
			$keywordMatchTypeSearchParameter = new KeywordMatchTypeSearchParameter();
			$keywordMatchTypeSearchParameter->keywordMatchTypes = array('BROAD');

			$selector->searchParameters =
			array($relatedToKeywordSearchParameter, $keywordMatchTypeSearchParameter);

			// Get related keywords.
			$page = $targetingIdeaService->get($selector);

			// Display related keywords.
			if (isset($page->entries)) {

				//	return $page;

				$i = 0;
				$results = array();

				foreach ($page->entries as $targetingIdea) {
					$data = MapUtils::GetMap($targetingIdea->data);
					//	print_r($data);
					/*

					$averageMonthlySearches =
					isset($data['AVERAGE_TARGETED_MONTHLY_SEARCHES']->value)
					? $data['AVERAGE_TARGETED_MONTHLY_SEARCHES']->value : 0;
					printf("Keyword with text '%s', match type '%s', and average monthly "
					. "search volume '%s' was found.<br />\n", $keyword->text,
					$keyword->matchType, $averageMonthlySearches) ;
					*/

					$keyword = $data['CRITERION']->value;
					$results[$i]['keywords'] = $keyword->text;
					$results[$i]['search_type'] = $keyword->matchType;
					$results[$i]['total_search'] = isset($data['AVERAGE_TARGETED_MONTHLY_SEARCHES']->value) ? $data['AVERAGE_TARGETED_MONTHLY_SEARCHES']->value : 0;

					$results[$i]['cpc'] = self::findPPC($keyword->text);

					//	print_r($traffic);
					$i++;
				}

				return $results;
			} else {
				//	print "No related keywords were found.\n";
				return false;
			}
		} catch (Exception $e) {
			print $e->getMessage();
		}
	}

	function findPPC($searchKeywords){
		//	echo $searchKeywords;die();
		$user = new AdWordsUser();

		// Log SOAP XML request and response.
		$user->LogDefaults();
		$trafficEstimatorService = $user->GetTrafficEstimatorService('v201008');
		$keywords = array();
		//foreach($searchKeywords as $searchKeyword){

		$keywords[] = new Keyword($searchKeywords, 'BROAD');
		$keywords[] = new Keyword($searchKeywords, 'PHRASE');
		$keywords[] = new Keyword($searchKeywords, 'EXACT');

		//	}

		// Create a keyword estimate request for each keyword.
		$keywordEstimateRequests = array();
		foreach ($keywords as $keyword) {
			$keywordEstimateRequest = new KeywordEstimateRequest();
			$keywordEstimateRequest->keyword = $keyword;
			$keywordEstimateRequests[] = $keywordEstimateRequest;
		}
		// Create ad group estimate requests.
		$adGroupEstimateRequest = new AdGroupEstimateRequest();
		$adGroupEstimateRequest->keywordEstimateRequests = $keywordEstimateRequests;
		$adGroupEstimateRequest->maxCpc = new Money(1000000);
		$adGroupEstimateRequests = array($adGroupEstimateRequest);

		// Create campaign estimate requests.
		$campaignEstimateRequest = new CampaignEstimateRequest();
		$campaignEstimateRequest->adGroupEstimateRequests = $adGroupEstimateRequests;
		$campaignEstimateRequest->targets = array(new CountryTarget('AU'), new LanguageTarget('en'));
		$campaignEstimateRequests = array($campaignEstimateRequest);

		// Create selector.
		$selector = new TrafficEstimatorSelector();
		$selector->campaignEstimateRequests = $campaignEstimateRequests;

		// Get traffic estimates.
		$result = $trafficEstimatorService->get($selector);
		// Display traffic estimates.
		if (isset($result)) {
			$keywordEstimates = $result->campaignEstimates[0]->adGroupEstimates[0]->keywordEstimates;

			$cpc = array();


			for ($i = 0; $i < sizeof($keywordEstimates); $i++) {
				$keyword = $keywordEstimateRequests[$i]->keyword;
				$keywordEstimate = $keywordEstimates[$i];

				// Find the mean of the min and max values.
				$meanAverageCpc = ($keywordEstimate->min->averageCpc->microAmount + $keywordEstimate->max->averageCpc->microAmount) / 2;
				$meanAveragePosition = ($keywordEstimate->min->averagePosition + $keywordEstimate->max->averagePosition) / 2;
				$meanClicks = ($keywordEstimate->min->clicks + $keywordEstimate->max->clicks) / 2;
				$meanTotalCost = ($keywordEstimate->min->totalCost->microAmount + $keywordEstimate->max->totalCost->microAmount) / 2;

				$cpc['average_cpc'] = $meanAverageCpc;
				$cpc['average_position'] = $meanAveragePosition;
				$cpc['average_clicks'] = $meanClicks;
				$cpc['total_cost'] = $meanTotalCost;
			}
			//print_r($cpc);die();
			return $cpc;

		}

		return false;
	}

	#########################
	### ADD OBJECT

	public function addNewCampaign($campaignName){


		try {
			// Get AdWordsUser from credentials in "../auth.ini"
			// relative to the AdWordsUser.php file's directory.
			$user = new AdWordsUser();

			// Log SOAP XML request and response.
			$user->LogDefaults();

			// Get the CampaignService.
			$campaignService = $user->GetCampaignService('v200909');

			// Create campaign.
			$campaign = new Campaign();
			$campaign->name = $campaignName;
			$campaign->status = 'PAUSED';
			$campaign->biddingStrategy = new ManualCPC();

			$budget = new Budget();
			$budget->period = 'DAILY';
			$budget->amount = new Money((float) 50000000);
			$budget->deliveryMethod = 'STANDARD';
			$campaign->budget = $budget;

			// Create operations.
			$operation = new CampaignOperation();
			$operation->operand = $campaign;
			$operation->operator = 'ADD';

			$operations = array($operation);

			// Add campaign.
			$result = $campaignService->mutate($operations);

			// Display campaigns.
			if (isset($result->value)) {
				foreach ($result->value as $campaign) {
					print 'Campaign with name "' . $campaign->name . '" and id "'
					. $campaign->id . "\" was added.\n";
				}
			} else {
				print "No campaigns were added.\n";
			}
		} catch (Exception $e) {
			print $e->getMessage();
		}

	}

	public function createNewAd($addGroupId, $headline, $desc1, $desc2, $displayURL, $clickURL){

		try {
			// Get AdWordsUser from credentials in "../auth.ini"
			// relative to the AdWordsUser.php file's directory.
			$user = new AdWordsUser();

			// Log SOAP XML request and response.
			$user->LogDefaults();

			// Get the AdGroupAdService.
			$adGroupAdService = $user->GetAdGroupAdService('v200909');

			$adGroupId = (float) $addGroupId;

			// Create text ad.
			$textAd = new TextAd();
			$textAd->headline = $headline;
			$textAd->description1 = $desc1;
			$textAd->description2 = $desc2;
			// needs to strip http://
			$textAd->displayUrl = $displayURL;
			$textAd->url = $clickURL;

			// Create ad group ad.
			$textAdGroupAd = new AdGroupAd();
			$textAdGroupAd->adGroupId = $adGroupId;
			$textAdGroupAd->ad = $textAd;

			// Create image ad.
			$imageAd = new ImageAd();
			$imageAd->name = 'Cruise to mars image ad #' . time();
			$imageAd->displayUrl = 'www.example.com';
			$imageAd->url = 'http://www.example.com';

			// Create image.
			$image = new Image();
			$image->data = MediaUtils::GetBase64Data('http://goo.gl/HJM3L');
			$imageAd->image = $image;

			// Create ad group ad.
			$imageAdGroupAd = new AdGroupAd();
			$imageAdGroupAd->adGroupId = $adGroupId;
			$imageAdGroupAd->ad = $imageAd;

			// Create operations.
			$textAdGroupAdOperation = new AdGroupAdOperation();
			$textAdGroupAdOperation->operand = $textAdGroupAd;
			$textAdGroupAdOperation->operator = 'ADD';

			$imageAdGroupAdOperation = new AdGroupAdOperation();
			$imageAdGroupAdOperation->operand = $imageAdGroupAd;
			$imageAdGroupAdOperation->operator = 'ADD';

			$operations = array($textAdGroupAdOperation, $imageAdGroupAdOperation);

			// Add ads.
			$result = $adGroupAdService->mutate($operations);

			// Display ads.
			if (isset($result->value)) {
				foreach ($result->value as $adGroupAd) {
					print 'Ad with id "' . $adGroupAd->ad->id . '" and type "'
					. $adGroupAd->ad->AdType . "\" was added.\n";
				}
			} else {
				print "No ads were added.\n";
			}
		} catch (Exception $e) {
			print $e->getMessage();
		}
	}
}