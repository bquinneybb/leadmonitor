<?php
class TemplateItems {

	public $returnOnly = false;
	public $classTextBox = 'text-field';
	function __contruct(){


	}




	private function outputHTML($html){

		if($this->returnOnly){

			return $html;

		} else {

			echo $html;

		}

	}


	##################################
	#### PUBLIC INTERFACE

	public function hide_email($email){

		$character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
		$key = str_shuffle($character_set); $cipher_text = ''; $id = 'e'.rand(1,999999999);
		for ($i=0;$i<strlen($email);$i+=1) $cipher_text.= $key[strpos($character_set,$email[$i])];
		$script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";';
		$script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
		$script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"';
		$script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")";
		$script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';
		$html = '<span id="'.$id.'">[javascript protected email address]</span>'.$script;

		return $this->outputHTML($html);

	}

	##################################
	#### GENRAL FORM HELPERS INTERFACE

	private function setRequiredText(){

		return '<span class="required">*</span>';
	}

	private function setItemPricing($id, $formCodeData){

		//print_r($formCodeData);
		$html = "\r\n";
		$html .= '<div id="price_' . $id . '" class="price">';

		if($formCodeData['item_price_percentage'] != 0){

			$html .= '+' . ($formCodeData['item_price_percentage'] * 100) . "%";

		} else {


			$html .= '$' . number_format( $formCodeData['item_price'], 2);
		}

		$html .= '</div>' . "\r\n";


		$html .= '<script type="text/javascript">';

		$html .= "$('#" . $id . "').data('price', '" . $formCodeData['item_price'] . "').data('price_percent', '" . $formCodeData['item_price_percentage'] . "');";

		$html .= '</script>' . "\r\n";

		return $html;

	}

	public function createFormConfiguration($config){

		$configuration = array();
		$configurationItems = array();
		$config = explode('|', $config);

		foreach($config as $configurationItem){

			$configurationItems[] = explode(':', $configurationItem);
			//$configurationItems[$configuration[0]] = $configuration[1];
		}

		$value = '';
		$required = '';

		foreach($configurationItems as $configurationItem){

			switch($configurationItem[0]){

				case 'session' :

					$configuration['value'] = self::createConfigurationValue($configurationItem[1]);
					$configuration['savesession'] = 1;
					break;

				case 'validation' :

					$configuration['validation'] = self::createConfigurationValidation($configurationItem[1]);

					break;

				case 'required' :

					$configuration['required'] = true;

					break;

				case 'min' :

					$configuration['min'] = $configurationItem[1];

					break;

				case 'max' :

					$configuration['max'] = $configurationItem[1];

					break;

				case 'step' :

					$configuration['step'] = $configurationItem[1];

					break;

			}
		}


		return $configuration;


	}

	private function createConfigurationValue($identifier){

		$value = isset($_SESSION['orderform'][$identifier]) ? $_SESSION['orderform'][$identifier] : '';
		return $value;
	}

	private function createConfigurationValidation($switches){

		$value = 'validate[' . $switches . ']';

		return $value;
	}

	public function drawFormLeft($label, $float = false, $id=''){

		$html = '<div class="formLeft';

		if($float !== ''){

			$html .= ' ' . $float;
		}

		

		$html .= '"';
		
		if($id != ''){
			
			$html .= ' id="' . $id . '"';
		}
		
		$html .= '>';
		$html .= sanitiseOutput($label);
		$html .= '</div>' . "\r\n";


		return $html;
	}

	public function drawFormRight($fieldData, $style = '', $id=''){

		$html = '<div class="formRight"';
		
		if($id != ''){
			
			$html .= ' id="' . $id . '"';
		}
		
		$html .= '>';
		
		if($style != ''){
			
			$html .= '<' . $style . '>';
			
				$html .= $fieldData;
				
			$html .= '</' . $style . '>';
		
		} else {
			
			$html .= $fieldData;
			
		}
		$html .= '</div>' . "\r\n";
		$html .= '<div class="clear"></div>' . "\r\n";

		return $html;
	}

	private function drawOrderFormLeft($id, $label, $config = '', $showRequired = false){

		$html = '<span class="orderLeft">'. sanitiseOutput($label);

		if($showRequired){

			$html .= self::setRequiredText();
		}
		$html .= '</span>';

		return $this->outputHTML($html);

	}

	private function drawOrderFormRight($id, $description){

		$html = '';
		$html = '<span class="orderRight">';

		$html  = '<span class="orderDescription">' ;
		$html .= '<label for="' . $id . '">';
		$html .= sanitiseOutput($description) . '</span>';
		$html .= '</label>';
		$html .= '</span>';


		return $this->outputHTML($html);
	}

	public function drawSubmit($text, $form = ''){

		$formId = $form != '' ? $form : 'adminform';
		
		$html = '<div class="formAction"><a href="javascript:document.' . $formId . '.submit()">' .  sanitiseOutput($text) . '</a></div>';
		//$html = '<input type="submit" value="' . sanitiseOutput($text) . '" class="submit corners" />';
		return $this->outputHTML($html);
	}
	


public function drawFormString($value, $label){



		$html = $this->drawFormLeft($label);
		
		$html .= $this->drawFormRight($value);


		return $this->outputHTML($html);

	}
	
	public function drawFormText($id, $value, $label){

		$print = $this->returnOnly;
		$value = $value == ' ' ? '' : $value;
		$html = $this->drawFormLeft($label);
		$this->returnOnly = true;
		$fieldData = $this->drawText($id, $value);
		$this->returnOnly = $print;
		$html .= $this->drawFormRight($fieldData);


		return $this->outputHTML($html);

	}
	
public function drawFormTextArea($id, $value, $label){

	
		$print = $this->returnOnly;	
	
		$html = $this->drawFormLeft($label);
		$this->returnOnly = true;
		$fieldData = $this->drawTextArea($id, $value);
		$this->returnOnly = $print;
		$html .= $this->drawFormRight($fieldData);


		return $this->outputHTML($html);

	}

	public function drawPrintedText($value, $label, $script = ''){



		$html = $this->drawFormLeft($label);
		$fieldData = sanitiseOutput($value);

		if($script != ''){

			$fieldData .= $script;
		}

		$html .= $this->drawFormRight($fieldData);


		return $this->outputHTML($html);

	}

	public function drawFormTextPassword($id, $value, $label){

		
		$print = $this->returnOnly;	
	
		$html = $this->drawFormLeft($label);
		$this->returnOnly = true;
		$fieldData = $this->drawTextPassword($id, $value);
		$this->returnOnly = $print;
		$html .= $this->drawFormRight($fieldData);
		


		return $this->outputHTML($html);

	}

	public function drawFormTextReorderList($id, $value, $label){


		$print = $this->returnOnly;	
		
		$html = '<div class="reorder"></div>';
		$this->returnOnly = true;
		$html .= $this->drawFormLeft($label, 'left');
		
		$fieldData = $this->drawText($id, $value);
		$this->returnOnly = $print;
		$html .= $this->drawFormRight($fieldData);

		
		return $this->outputHTML($html);

	}



	public function drawFormList($id, $values, $checkAgainst = '', $startWithBlank = true, $label){

		$print = $this->returnOnly;	

		$html = $this->drawFormLeft($label);
		$this->returnOnly = true;
		$fieldData = $this->drawList($id, $values, $checkAgainst, $startWithBlank);
		$this->returnOnly = $print;
		$html .= $this->drawFormRight($fieldData);


		return $this->outputHTML($html);

	}

	public function drawFormCheckbox($id, $value, $checkAgainst, $label){

		$print = $this->returnOnly;	
		
		$html = $this->drawFormLeft($label);
		$this->returnOnly = true;
		$fieldData = $this->drawCheckbox($id, $value, $checkAgainst);
		$this->returnOnly = $print;
		$html .= $this->drawFormRight($fieldData);


		return $this->outputHTML($html);

	}

	public function drawFormRadio($id, $value, $checkAgainst, $label){

		$print = $this->returnOnly;	
		
		$html = $this->drawFormLeft($label);
		$this->returnOnly = true;
		$fieldData = $this->drawRadio($id, $value, $checkAgainst);
		
		$this->returnOnly = $print;
		$html .= $this->drawFormRight($fieldData);


		return $this->outputHTML($html);

	}


	public function checkOrderFormId($id){

		if(substr($id, 0, 3) != 'db_'){

			$id  = 'db_' . $id;

		}

		return $id;
	}

	public function drawOrderFormRadio($id, $value, $checkAgainst, $formCodeData, $name, $config){

		$id = self::checkOrderFormId($id);
		$name = self::checkOrderFormId($name);

		$configuration = self::createFormConfiguration($config);

		$label = isset($formCodeData['item_label']) ? $formCodeData['item_label_web'] : '';
		$description = isset($formCodeData['item_description_web']) ? $formCodeData['item_description_web'] : '';

		$value = isset($configuration['value']) ? $configuration['value'] : $value;
		$validation = isset($configuration['validation']) ? $configuration['validation'] : '';



		$html = $this->drawOrderFormLeft($id, $label, $config);
		$html .= $this->drawRadio($id, $value, $checkAgainst, $name, $validation);
		$html .= $this->drawOrderFormRight($id, $description);

		if(isset($formCodeData['item_price'])){

			$html .= self::setItemPricing($id, $formCodeData);
		}

		$html .= '<div class="clear"></div>';

		return $this->outputHTML($html);

	}

	public function drawOrderFormCheckbox($id, $value, $checkAgainst, $formCodeData, $name, $config, $editable = false){
		//print_r($formCodeData);
		$id = self::checkOrderFormId($id);
		$configuration = self::createFormConfiguration($config);

		$label = isset($formCodeData['item_label']) ? $formCodeData['item_label_web'] : '';
		$description = isset($formCodeData['item_description_web']) ? $formCodeData['item_description_web'] : '';

		$value = isset($configuration['value']) ? $configuration['value'] : $value;
		$validation = isset($configuration['validation']) ? $configuration['validation'] : '';



		$html = $this->drawOrderFormLeft($id, $label, $config);
		/*
		 $html .= $this->drawCheckbox($id, $value, $checkAgainst, $name, $validation);
		 */
		if($editable){

		//	$html .= '<div class="formhide" id="input_' . $id . '">';
		}

		 $html .= $this->drawCheckbox($id, $value, $checkAgainst, $name, $validation);

		if($editable){
		//	$html .= '</div>';
			//	$html .= '</div>';
			
		//	$html .= '<div class="formedit" id="edit_' . $id . '"><a href="javascript:editForm(\'' . $id. '\',\'' . $formCodeData['item_label_web'] . ' ' . $formCodeData['item_description_web'] . '\')">' . $formCodeData['item_label_web'] . ' ' . $formCodeData['item_description_web']. '</a></div>';

		}
		$html .= $this->drawOrderFormRight($id, $description);


		if(isset($formCodeData['item_price'])){

			$html .= self::setItemPricing($id, $formCodeData);
		}
		$html .= '<div class="clear"></div>';

		return $this->outputHTML($html);

	}

	public function drawOrderFormList($id, $value, $checkAgainst, $formCodeData, $name, $config){

		$id = self::checkOrderFormId($id);
		$configuration = self::createFormConfiguration($config);

		$label = isset($formCodeData['item_label']) ? $formCodeData['item_label_web'] : '';
		$description = isset($formCodeData['item_description_web']) ? $formCodeData['item_description_web'] : '';

		$value = isset($configuration['value']) ? $configuration['value'] : $value;
		$validation = isset($configuration['validation']) ? $configuration['validation'] : '';

		$minValue = isset($configuration['min']) ? $configuration['min'] : 0;
		$maxValue = isset($configuration['max']) ? $configuration['max'] :10;
		$stepValue = isset($configuration['step']) ? $configuration['step'] : 1;
		//print_r($configuration);
		$values = self::createNumberList($minValue, $maxValue, $stepValue);
		//print_r($values);
		$html = $this->drawOrderFormLeft($id, $label, $config);
		$html .= $this->drawList($id, $values, $checkAgainst, false, '', $validation);
		$html .= $this->drawOrderFormRight($id, $description);

		if(isset($formCodeData['item_price'])){

			$html .= self::setItemPricing($id, $formCodeData);
		}
		$html .= '<div class="clear"></div>';

		return $this->outputHTML($html);

	}

	public function drawOrderFormText($id, $label, $config, $editable = false, $forceValue = ''){

		$id = self::checkOrderFormId($id);

		$configuration = self::createFormConfiguration($config);

		$value = isset($configuration['value']) ? $configuration['value'] : '';
		$validation = isset($configuration['validation']) ? $configuration['validation'] : '';
		$showRequired = isset($configuration['required']) ? $configuration['required'] : false;

		$html = $this->drawOrderFormLeft($id, $label, $config, $showRequired);


		if($editable){

			$html .= '<div class="formhide" id="input_' . $id . '">';
		}

		$html .= $this->drawText($id, $value, $validation);

		if($editable){
			$html .= '</div>';
			//	$html .= '</div>';
			if($forceValue == ''){
					
				$forceValue = '[blank]';
			}
			$html .= '<div class="formedit" id="edit_' . $id . '"><a href="javascript:editForm(\'' . $id. '\',\'\')">' . $forceValue. '</a></div>';

		}


		//'Office', 'db_office', 'session:db_office|required:true|validate:mixed'
		$html .= '<div class="clear"></div>';
		return $this->outputHTML($html);

	}

	public function drawOrderFormTextArea($id, $label, $config){

		$id = self::checkOrderFormId($id);

		$configuration = self::createFormConfiguration($config);
		$value = isset($configuration['value']) ? $configuration['value'] : '';
		$validation = isset($configuration['validation']) ? $configuration['validation'] : '';
		$showRequired = isset($configuration['required']) ? $configuration['required'] : false;

		$html = $this->drawOrderFormLeft($id, $label, $config, $showRequired);
		$html .= $this->drawTextArea($id, $value, $validation);

		//'Office', 'db_office', 'session:db_office|required:true|validate:mixed'
		$html .= '<div class="clear"></div>';
		return $this->outputHTML($html);

	}

	public function drawText($id, $value, $validation = ''){

		$html = '';
		$html = '<input type="text"
			id="' . $id . '" name="' . $id . '"
			value="' . sanitiseOutput($value) . '"
			class="' . $validation . ' ' . $this->classTextBox . ' '  . '"'
			
			. '/>';

			return $this->outputHTML($html);

	}

	public function drawTextArea($id, $value, $validation = ''){

		$html = '';
		$html = '<textarea
			id="' . $id . '" name="' . $id . '"
		
			class="' . $validation . ' ' . $this->classTextBox . ' '  . '"'
			
			. '/>';
			$html .= sanitiseOutput($value);
			$html .= '</textarea>';
			return $this->outputHTML($html);

	}

	public function drawHidden($id, $value){

		$html = '';
		$html = '<input type="hidden"
			id="' . $id . '" name="' . $id . '"
			value="' . sanitiseOutput($value) . '"
			
			 />';

		return $this->outputHTML($html);

	}

	public function drawTextPassword($id, $value){

		$html = '';
		$html = '<input type="password"
			id="' . $id . '" name="' . $id . '"
			value="' . sanitiseOutput($value) . '"
			class="' . $this->classTextBox . '"
			/>';

		return $this->outputHTML($html);

	}

	public function drawList($id, $values, $checkAgainst = '', $startWithBlank = true, $name = '', $validation = ''){


		//print_r($values);


		$html = '<select ';
		$html .= 'id="' . $id . '" name="' . $id .'" ';
		$html .= ' class="' . $validation .  'formSelect text-input"';
		$html .='>'. "\r\n";

		if($startWithBlank){

			$html .= '<option value="-1"></option>';
		}
			
		if($values){
			//print_r($values);die();
			foreach($values as $key=>$value){

				$html .= '<option value="' . sanitiseOutput($key) . '"';
				//print_r($value);
				//	echo $key . ':' . $checkAgainst . '<br />';
				if($key == $checkAgainst ){

					$html .= ' selected="selected"';

				}

				$html .= ' >' . sanitiseOutput($value) . '</option>';

			}

		}



		$html .= '</select>';

		return $this->outputHTML($html);

	}

	public function drawCheckbox($id, $value, $checkAgainst = '', $name = '', $validation = ''){

		//echo 'v = ' . $checkAgainst;
		$name = $name != '' ? $name : $id;
		$html = '<input type="checkbox" class="' . $validation . ' checkbox-input" value="' . sanitiseOutput($value) . '" ';
		$html .= 'id="' . $id . '" name="' . $name .'" ';

		if($checkAgainst == ''){

			//	$checkAgainst = 0;
		}

		if($value == $checkAgainst){
			//		echo $value .'=='. $checkAgainst . '<br />';
			$html .= 'checked="checked"';
		}

		$html .= '/>'. "\r\n";
		return $this->outputHTML($html);
	}

	public function drawRadio($id, $value, $checkAgainst = '', $name = '', $validation = ''){

		//echo 'v = ' . $checkAgainst;
		if($name == ''){

			$name = $id;
		}
		$html = '<input type="radio" class="' . $validation . ' radio-input" value="' . sanitiseOutput($value) . '" ';
		$html .= 'id="' . $id . '" name="' . $name .'" ';

		if($checkAgainst == ''){

			//	$checkAgainst = 0;
		}

		if($value == $checkAgainst){
			//		echo $value .'=='. $checkAgainst . '<br />';
			$html .= 'checked="checked"';
		}

		$html .= '/>'. "\r\n";
		return $this->outputHTML($html);
	}

	public function dateToString($date){

		if(is_numeric($date)){

			$html = date('D d M Y', $date);

		} else {

			$html = date('D d M Y', strtotime($date));
		}



		return $this->outputHTML($html);

	}

	private function createNumberList($min, $max, $step){

		$values = array();
		//echo $max;
		for($i=$min;$i<$max+1;$i++){

			$listVal = $i * $step;
			//echo $step;
			$values[$listVal] = $listVal;

		}

		return $values;
	}

	function createList($table, $column1, $column2, $initialBlankValue = '', $orderBy = '', $where = ' active = 1'){

		$db = new Database();

		$parameters = array();

		$orderByClause = $orderBy;
		if($orderBy != ''){

			$orderByClause = "  ORDER BY " . $orderBy;
		}
		$query = "SELECT `" . $column1 . "` AS COL1 ";

		if(is_array($column2)){
			$i = 2;
			foreach($column2 as $column){

				$query .= ", `" . $column . "` AS COL" . $i;
				$i++;
			}

		} else {

			$query .= ", `" . $column2 . "` AS COL2 ";
		}

		$query .= " FROM `". $table . "` WHERE " . $where . " " . $orderByClause;
		//echo $query;
		$list = array();

		if($initialBlankValue != ''){

			$list[] = array($initialBlankValue, '');
		}

		if($listItems = $db->dbResults($query, $parameters)){

			foreach($listItems as $listItem){

				if(is_array($column2)){

					$column2String = '';
					$i = 2;
					foreach($column2 as $column){
						$columnNumber = 'COL' . $i;



						$column2String .= $listItem[$columnNumber] . ' ';
						$i++;
					}
					$column2String = substr($column2String, 0, -1);
					$list[$listItem['COL1']] = $column2String;

				} else {
					$list[$listItem['COL1']] = $listItem['COL2'];
					//$list[] = array($listItem[COL1], $listItem[COL2]);

				}


			}


		}
		//	print_r($list);
		return $list;

		//echo $query;
	}

	function setToArray($table, $column){
			


		$setData = Database::dbRow("DESCRIBE `" . Database::escape_value($table) . "` `" . Database::escape_value($column) . "`", array());
			
		$setData = substr($setData['Type'],5, -2 );


		$values = explode('\',\'', $setData);
			
		return $values;
			
	}



	###############################
	### JAVASCRIPT ITEMS

	public function wrapJsScript($script, $jqueryOnReady = false){

		$html = '<script type="text/javascript">' . "\r\n";
			
		if($jqueryOnReady){

			$html .= '$(document).ready(function() {' . "\r\n";
			$html .= $script . "\r\n";
			$html .= '});' . "\r\n";

		} else {

			$html .= $script . "\r\n";


		}
		$html .= '</script>' . "\r\n";
		return $this->outputHTML($html);
	}

	public function drawTimePickerJavascript($id, $minToday = '+0', $dateFormat = 'dd/mm/yy'){


		$dateFormatString = 'dateFormat:\'' . $dateFormat . '\'';

		$minDateString  = ',minDate: \'+0 days\'';
			


		$html = '$("#' . $id . '").datepicker({' . $dateFormatString . $minDateString .'});' . "\r\n";

		return $this->outputHTML($html);
	}

	public function drawDateTimePickerJavascript($id, $minToday = '+0', $dateFormat = 'dd/mm/yy'	){


		$dateFormatString = 'dateFormat:\'' . $dateFormat . '\'';

		$minDateString  = ',minDate: \'+0 days\'';
			



		$html = '$("#' . $id . '").datetimepicker({' . $dateFormatString . $minDateString . ',';

		$html .= 'ampm: true,
		stepMinute: 30,
		hourMin: 8,
		hourMax: 20';

		$html .= '});' . "\r\n";

		return $this->outputHTML($html);
	}

	public function drawTimePickerValue($dateStamp, $showSeconds = false, $formatSeconds = false){
		//echo 'd = ' . $dateStamp;

		if($dateStamp == DATE_NULL || $dateStamp == ''){

			$timeStamp ='';

		} else {

			$timeStamp = explode('-', substr($dateStamp, 0, 10));
			$timeStamp = $timeStamp[2] . '/' . $timeStamp[1] . '/' . $timeStamp[0];


			if($showSeconds){
					
				$seconds = substr($dateStamp, 10);

				if($formatSeconds){

					$seconds = date('h:i a', strtotime($seconds));
				}
					
				$timeStamp .= ' ' . $seconds;
			}
		}


		//echo $timeStamp;
		return $this->outputHTML($timeStamp);
	}

	public function timePickerToDB($dateStamp, $addSeconds = false){

		if($dateStamp == DATE_NULL || $dateStamp == ''){

			$dateStamp ='';

		} else {
			$dateStamp = str_replace('/', '-', $dateStamp);
			$dateStamp = explode('-', substr($dateStamp, 0, 10));
			$dateStamp = $dateStamp[2] . '-' . $dateStamp[1] . '-' . $dateStamp[0];

		}

		if($addSeconds){

			$dateStamp .= ' 00:00:00';
		}

		return $this->outputHTML($dateStamp);
	}

	public function dateTimePickerToDB($dateStamp){

		$timeStamp = self::timePickerToDB($dateStamp, false);
		$timeStamp .= ' ' . date('H:i:s', strtotime(substr($dateStamp, 10)));

		return $this->outputHTML($timeStamp);


	}

	public function timestampToNiceDate($timestamp, $showSeconds = false){
		//echo $timestamp;die();
		if(!is_int($timestamp)){

			$timestamp = strtotime($timestamp);
		}

		if($showSeconds){

			$dateStamp = date('d/m/y h:i a', $timestamp);

		} else {

			$dateStamp = date('d/m/y', $timestamp);
		}
		return $this->outputHTML($dateStamp);
	}

	public function createImageModalLink($smallImageLink, $largeImage){
		/*
		$html = '<a href="" onclick="javascript:$.fn.fancybox({href:\'' .  $largeImage . '\'});return false">';
		$html .= $smallImageLink;
		$html .= '</a>';
		*/
		$html = '<a href="' . $largeImage . '" class="modalimage">' . $smallImageLink . '</a>';
		return $html;
	}

	public function createIframeModalWindowLink($linkText, $href, $height = '', $width = '', $escape=false){

		$html = '';
		if($escape){

			$html .= "<a href=\"#\" onclick=\"";
			$html .= self::createIframeModalWindowJavascript($href, $height, $width, $escape);
			$html .= "\">";

		} else {

			$html .= '<a href="#" onclick="';
			$html .= self::createIframeModalWindowJavascript($href, $height, $width, $escape);
			$html .= '">';

		}
		$html .= sanitiseOutput($linkText) . '</a>' . "\r\n";

		return $this->outputHTML($html);

	}

	public function createIframeModalWindowJavascript($href, $height = '', $width = '', $escape=false){



		$html = '';
		if($escape){

			$heightString = '';
			$widthString = '';

			if($height != ''){

				$heightString = "\'height\':\'" . $height . "\',";
			}

			if($width != ''){

				$widthString = "\'width\':\'" . $width . "\',";
			}

			//$html .= "$.fn.colorbox({". $heightString . $widthString . " iframe:true, href:\'" . $href . "\', transition: 'none' });return false\"";
			$html .= "$.fancybox({". $heightString . $widthString . " \'type\':\'iframe\', href:\'" . $href . "\' });return false\"";


		} else {

			$heightString = '';
			$widthString = '';

			if($height != ''){

				$heightString = '\'height\':\'' . $height . '\',';
			}

			if($width != ''){

				$widthString = '\'width\':\'' . $width . '\',';
			}

			//$html .= '$.fn.colorbox({' . $heightString . $widthString . ' iframe:true, href:\'' . $href . '\', transition: \'none\'});return false';
			$html .= '$.fancybox({' . $heightString . $widthString . ' \'type\': \'iframe\', \'href\':\'' . $href . '\'});return false';


		}


		return $html;

	}

	public function createInlineModalWindow($id, $messsage, $showLoadingBar = false){

		$html = '';
		$html .= '<div style="display:none">' . "\r\n";
		$html .= '<div class="modal"  id="' . $id . '">' . "\r\n";
		$html .= $messsage;
			
		if($showLoadingBar){

			$html .= '<hr /><h1>Processing</h1><img src="' .SITE_PATH . '/public/template-core/interface/loading-icon-white.gif" />' . "\r\n";

		}
		$html .= '</div>'. "\r\n";
		$html .= '</div>' . "\r\n";
			
		return $this->outputHTML($html);

	}

	public function showInlineModalWindow($id, $height = '100%', $width = '100%'){

		$html = '';
		$html .= '$.fn.fancybox({type:\'inline\', href:#\'' . id . '\', height:\'' . $height . '\', width:\'' . $width . '\'});' . "\r\n";


		TemplateItems::wrapJsScript($html, true);
	}

	public function closeInlineModalWindow($divHideId = ''){

		$html = '';
		$html .= '$.fn.fancybox.close();';

		if($divHideId != ''){

			$html .= '$("#' . $divHideId . '").css("display", "none");';
		}


		TemplateItems::wrapJsScript($html, true);

	}

	public function closeIframeModalWindow(){

		$html = '';
		$html .= 'parent.$.fn.fancybox.close()' . "\r\n";


		TemplateItems::wrapJsScript($html, true);

	}

	public function drawModalCloseLink($refreshParent = false){

		$html = '';
		$html .= self::drawIcon('cross');

		$link = 'javascript:parent.closePopup();';
		if($refreshParent){

			$link = 'javascript:parent.history.go(0)';
		}

		$html .= '<a href="' . $link . '">Close Window</a>';
		echo $html;
	}

	public function ZebraStripeTable($item, $class){

		$html = '';

		$html .= "\$('" . $item . " tr:odd').addClass('" . $class . "');";
		$this->wrapJsScript($html, true);
	}

	public function setCorners(){
		echo '<span style="color:#F00">setCorners : DEPRECATED</span>';
		die();
		$html = <<<EOD
			settings = {
	          tl: { radius: 3 },
	          tr: { radius: 3 },
	          bl: { radius: 3 },
	          br: { radius: 3 },
	          antiAlias: true,
	          autoPad: true
	      }
		
	$('.corners').corner(settings);
EOD;
		return $this->outputHTML($html);


	}

	public function drawTextEditorSimple($id){

		$sitePath = SITE_PATH;

		$html = <<<EOD
		
		$('#{$id}').htmlbox({ 
        toolbars:[ 
            ["bold","italic","underline","ul"] 
        ]
        , skin:"blue" 
        ,idir: "{$sitePath}/public/javascript/htmlbox/images/"
    }); 

EOD;
		return $this->outputHTML($html);

	}
	#########################
	### Images
	public function drawIcon($name, $moduleName = '', $title = ''){

		if($moduleName == ''){

			$html = '<img src="' . SITE_PATH . '/public/template-core/icons/' . $name . '.png" title="' . $title . '" />' . "\r\n";

		} else {

			$html = '<img src="' . SITE_PATH . '/public/modules/' . $moduleName . '/icons/' . $name . '.png" title="' . $title . '" />' . "\r\n";
		}

		if($title != '' && ($title === true || $title === false)){
			
			echo 'DEPRECATED FUNCTION CALLING drawIcon()';
		}
/*
		if($return){

			return $html;

		} else {

			echo $html;
		}
		*/
		return $this->outputHTML($html);
	}

	function drawStatusImage($status){

		if($status == 0){

			$html = self::drawIcon('cross', '', true);

		} elseif($status == 1) {
			$html = self::drawIcon('tick', '', true);

		}

		return $this->outputHTML($html);

	}

	function drawThumbPath(){

		echo SITE_PATH . '/_utils/PhpThumb/phpThumb.php?src=';
	}

	############################
	## Messages
	public function drawUpdateNotice($message = ''){

		if($message == ''){

			$message = 'Updated';
		}
		//print_r($_POST);
		$html = '<span class="formsave">' . $message . '</span>';
		if(isset($_POST['update'])){

			return $this->outputHTML($html);

		}
	}

	############################
	## Ajax
	public function drawSidString(){

		$html = "'&sid=" . encrypt(session_id())  . "';\r\n";
		return $this->outputHTML($html);

	}


	############################
	## Interface

	function drawContentLeftOpen(){

		$html = '<div class="contentleft">';

		echo $html;

	}

	function drawContentLeftClose(){

		$html = '</div>';
		echo $html;

	}

	public function drawModuleMenuTop($label, $icon){

		$label = sanitiseOutput($label);

		$html = <<<EOD
		
	<div id="sectionHeader">
		<div class="sectionHeaderIcon"><img src="/public/template-core/section-icons/{$icon}.png" />{$label}</div>
	</div>
	
	<div id="sectionActions">
			
		<div id="sectionActionsInner">
			
			
    	
EOD;

		echo $html;

	}

	public function drawModuleMenuLink($text, $url, $icon){

//		$html = '<li><a href="'  . $url . '">' . sanitiseOutput($text) . '</a></li>';
		$text = sanitiseOutput($text);
		
		$html = <<<EOD

		<div class="sectionActionsInner">
				<div class="icon"><img src="/public/template-core/section-icons/{$icon}.png" /></div>
				<div class="sectionActionText"><a href="{$url}">{$text}</a></div>
			</div>
				
EOD;
		echo $html;
	}

	public function drawModuleMenuBottom(){

		$html = <<<EOD
		
		  </div>
		
	</div>
		 
EOD;
		echo $html;
	}


	public function drawSubmenuTop(){

		

		$html = <<<EOD
		

  		<div class="formActions">
    	
EOD;

		echo $html;

	}

	public function drawSubmenuBottom(){

		$html = <<<EOD
		
		    	</div>
		
		<div class="clear"></div>
	
	<hr />
		 
EOD;
		echo $html;
	}
	


	public function drawSubmenuLink($text, $url = '', $javascript = '', $showIcon = ''){

		$html = '<div class="formAction">';


		if($url == ''){

			$html .= '<a href="#" onclick="' . $javascript . ';return false">' .  sanitiseOutput($text) . '</a>';

		} else {

			$html .= '<a href="' . $url . '">' .  sanitiseOutput($text) . '</a>';

		}



		$html .= '</div>' . "\r\n";

		echo $html;
	}

	public function drawSubmenuLinkModal($text, $url, $height = 600, $width = 600, $escape = false, $icon = ''){

		$this->returnOnly = true;

		$html = '<li>';
		$html .= $this->createIframeModalWindowLink($text, $url, $height, $width, $escape);
		$html .= '</li>' . "\r\n";

		echo $html;


	}

	public function drawScreen(){

		$html =  '<div id="screen">';
		$html .= '<div id="loadingMessage">';
		$html .= '<img src="' . SITE_PATH . '/public/template-core/interface/loading-icon-black.gif" />';
		$html .= '<h1>LOADING PAGE ITEMS</h1>';
		$html .= '</div>';
		$html .=  '</div>';

		return $html;


	}
	
	public function drawFromTableHeader($text, $icon, $searchId = ''){
		
		$text = sanitiseOutput($text);
		
		$html = <<<EOD
		
		
		<div class="tableHeader">
		<img src="/public/template-core/menu-icons/{$icon}.png" />{$text}
EOD;
		if($searchId != ''){
		
			$html .= '<div class="clearFilter" id="filterClear_' . $searchId . '">CLEAR</div><input type="text" size="30" maxlength="30" value="" id="filterBox_' . $searchId . '" class="quickFilter rounded-corners" >';
			
				$html .= <<<EOD
			
			<script type="text/javascript">

  jQuery(document).ready(function() {
    $("#$searchId")
      .tablesorter({debug: false, widgets: ['zebra'], sortList: [[0,0]]})
      .tablesorterFilter({filterContainer: $("#filterBox_$searchId"),
                          filterClearContainer: $("#filterClear_$searchId"),
                          filterColumns: [0],
                          filterCaseSensitive: false});
  });
</script>
						
EOD;
		}
		$html .= '</div>';

echo $html;
	}
	
	############################
	## Users and Groups
	public function drawGroupHeaderLogo(){

		$groupId = isset($_SESSION['user']['group_id_pk']) ? $_SESSION['user']['group_id_pk'] : 1;
		$configuration = isset($_SESSION['user']['group_configuration']) ? $_SESSION['user']['group_configuration'] : null;

		$clientLogoPath = '/fileserver/client-logo/core/' . (int)$groupId . '.png';
		$clientLogo = SITE_PATH  . '/public/posi/briner-logo.png';

		if(file_exists(ROOT . $clientLogoPath)){

			$clientLogo = SITE_PATH .  $clientLogoPath;
		}

		$backgroundColour = isset($configuration['template']['bannerbackground']) ? $configuration['template']['bannerbackground'] : '000000';

		$html = '<div id="clientLogo" style="background-color:#' . $backgroundColour . ';"><img src="' . $clientLogo . '" /></div>';

		echo $html;
	}


	public function drawJavaUploader($successPage,  $uploadDirectory, $allowDirectories = false){


	}


	public function _drawJavaUploader($whichCredentials, $successPage, $uploadDirectory, $allowDirectories = false){

		global $ftpCredentials;

		$foo = <<<EOD
<param name="userName" value="devupload">
<param name="password" value="myp@ss">
<param name="host" value="117.58.255.146">

<param name="userName" value="{$credentials[0]}">
<param name="password" value="{$credentials[1]}">
<param name="host" value="{$credentials[2]}">
<param name="key" value="{$credentials[3]}">
EOD;

		$credentials = $ftpCredentials[$whichCredentials];
		//	print_r($ftpCredentials);
		$successPage = $sitePath . $successPage;
		$html = <<<EOD
		
<applet code="com.javaatwork.myftpuploader.MyFTPUploader.class" name="MyFTPUploader" width="400"  height="250" 
 archive="{$sitePath}/public/java/ftp-uploader/myftpuploader-standard-signed-v13.jar, {$sitePath}/public/java/ftp-uploader/labels.jar" id="MyFTPUploader">


<param name="userName" value="devway">
<param name="password" value="fReeD3vS1t#">
<param name="host" value="117.58.255.146">


<param name="debug" value="true">

<param name="successURL" value="{$successPage}" />
<param name="uploadDirectories" value="{$allowDirectories}" />
<param name="nameConflict" value="makeUnique" />
<param name="automaticLogin" value="true" />
<param name="rejectFileFilter" value="exe, bat, php, pl, asp, ini, shtml, cfm" />
<param name="uploadDirectory" value="{$uploadDirectory}" />
<param name="transferType" value="automatic" />
<param name="asciiFileExtensions" value="txt, html, htm, xml" />
</applet>
		
EOD;

		echo $html;

	}

	function drawQuicktimeEmbed($link, $linkText){

		$html = '<a class="media" href="' . SITE_PATH . '/' . $link . '">' . sanitiseOutput($linkText) . '</a>';
		return $this->outputHTML($html);
	}

	function drawActionLink($text, $link = '', $javascript = '', $icon = ''){

		$html = '<div class="actionLink">';

		if($icon != ''){

			$html .= self::drawIcon($icon, '', true);
		}

		if($javascript != ''){

			$html .= '<a href="#" onclick="' . $javascript . '">';

			$html .=  sanitiseOutput($text);

			$html .= '</a>';


		} else {

			$html .= '<a href="' . $link . '">';

			$html .=  sanitiseOutput($text);

			$html .= '</a>';

		}

		$html .= '</div>' . "\r\n";

		return $this->outputHTML($html);
	}

	function drawGoogleMapJS(){

		echo '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>';
	}

	function drawFlashUploadJS(){
		
		Template::loadCSS('/public/javascript/jquery.uploadify/uploadify.css');
		Template::loadJavascript('/public/javascript/jquery.uploadify/swfobject.js');
		Template::loadJavascript('/public/javascript/jquery.uploadify/jquery.uploadify.min.js');
		
	}
	
	function drawBreadcrumbs($crumbs){
		
		$html = '<div id="breadcrumbs"><div id="breadcrumbsInner">';
			
			$html .= '<a href="/admin/dashboard/"><strong>Home</strong></a> > ';
		
			$totalCrumbs = count($crumbs);
			$i = 0;
			foreach($crumbs as $key=>$value){
				
				$html .= '<a href="/admin/' . $key . '"><strong>' . $value . '</strong></a>';
				
				if($i<$totalCrumbs-1){
					
					$html .= ' > ';
				}
				
				$i++;
			}
		
		$html .= '</div></div>';
		
		echo sanitiseOutput($html);
	}
}

