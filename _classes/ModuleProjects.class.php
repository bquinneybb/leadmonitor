<?php
class ModuleProjects {

	public function findAllProjects($clientId = ''){


		$query = "SELECT 
		PR.module_project_id_pk, PR.project_name, PR.project_type
		, CL.client_id_pk, CL.client_name
		FROM `module_project` PR, `client` CL
		WHERE PR.group_id_pk = ?
		AND PR.active = 1
		AND PR.client_id_pk = CL.client_id_pk
		";
		
		if($clientId != ''){
			
			$query .= " AND PR.client_id_pk = ?";
		}
		
		$query .= " ORDER BY PR.project_name";
		//echo $query;
		$parameters = array($_SESSION['user']['group_id_pk']);
		
		if($clientId != ''){
			
			$parameters[] = $clientId;
		}

		if($projects = Database::dbResults($query, $parameters)){
				
				
			return $projects;
		}

		return false;

	}
	
	public function projectCreate($clientId){
		
		$schema = new Schema();
		$table = 'module_project';
		$postVars = array();
		$postVars['db_client_id_pk'] = (int)$clientId;
		$postVars['db_group_id_pk'] = (int) $_SESSION['user']['group_id_pk'];
		$postVars['db_active'] = 1;
		$returnURL= '/admin/projects/projectedit/{id}';
		$schema->createNewDatabaseEntry($table, $postVars, $returnURL);
	}

	public function projectEdit($projectId){

		$query = "SELECT * FROM `module_project` WHERE `module_project_id_pk` = ?";
		$parameters = array($projectId);

		if($project = Database::dbRow($query, $parameters)){
				
				
			return $project;
		}

		return false;

	}

	public function projectSave($projectId){

		

		
		if($_POST['db_client_id_pk_existing'] != $_POST['db_client_id_pk']){
			// any campaigns that use this client also need to be updated
			$query = "UPDATE `module_campaign` SET `client_id_pk` = ? WHERE `client_id_pk` = ? AND `group_id_pk` = ?";
			$dataTypes = 'iii';
			$parameters = array($_POST['db_client_id_pk_existing'], $_POST['db_client_id_pk'], $_SESSION['user']['group_id_pk']);
			Database::dbQuery($query, $dataTypes, $parameters);
			
			// update forms
			$query = "UPDATE `module_campaign_form` SET `client_id_pk` = ? WHERE `client_id_pk` = ?  AND `group_id_pk` = ?";
			$dataTypes = 'iii';
			$parameters = array($_POST['db_client_id_pk_existing'], $_POST['db_client_id_pk'], $_SESSION['user']['group_id_pk']);
			Database::dbQuery($query, $dataTypes, $parameters);
			
		}
		// update the main project
		$schema = new Schema();
		$table = 'module_project';
		$where = " `module_project_id_pk` = '" . (int)$projectId . "'";

		$returnURL = '/admin/projects/projectedit/' . (int)$projectId . '&update';
		//	print_r($schema);
		$schema->updateDatabaseEntry($table, $where, array(), $returnURL);

	}
}