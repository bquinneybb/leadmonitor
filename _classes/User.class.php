<?php
class User  {

	public $userRoles;
	protected $minimumUserEditLevel = 50;

	public $userPhotoMaxWidth = 300;
	public $userPhotoMaxHeight = 300;


	function __construct(){

		$this->userRoles = array();

		$this->userRoles['superadmin'] = array('999','Super Admin');
		$this->userRoles['admin'] = array('100', 'Administration');

		// client roles
		$this->userRoles['client'] = array('20', 'Client');
		$this->userRoles['visitor'] = array('0', 'Visitor');

		self::defineCustomUserGroups(CURRENT_CLIENT);

	}

	public function attemptLogin($queryString, $admin = false){

		
		$loginPath = '';

		if($admin){

			$loginPath = '/admin';

		}

		$username = isset($_POST['db_email']) ? $_POST['db_email'] : null;
		$password = isset($_POST['db_password']) ? $_POST['db_password'] : null;

		if($username == null || $password == null){

			header('location: ' . SITE_PATH . $loginPath . '/login/login/error=blank');
			die();
		}

		$query = "SELECT
		G.group_id_pk
		,U.user_id_pk
		FROM `group` G, `user` U
		WHERE 
		U.email = ?
		AND U.password = ?
		AND U.active = 1
		AND U.group_id_pk = G.group_id_pk
	
		";

		$parameters = array($username, $password);

		//echo $query;print_r($parameters);die();
		//Database::spe($query, $parameters);
		if($user = Database::dbRow($query, $parameters)){
			
			$_SESSION['user']['group_id_pk'] = $user['group_id_pk'];
			User::getUserSimple($user['user_id_pk'], $user['group_id_pk']);

			header('location: ' . SITE_PATH . $loginPath . '/home/dashboard/');
			die();
			//	print_r($user);

		} else {

			header('location: ' . SITE_PATH . $loginPath . '/login/login/error=noexist');
			die();

		}



	}

	public function performLogout($admin = false){

		$loginPath = '';

		if($admin){

			$loginPath = '/admin';

		}


		unset($_SESSION['user']);
		header('location: ' . SITE_PATH . $loginPath  . '/login/login/');
	}

	function defineCustomUserGroups($groupName){

		switch($groupName){


			
					
			default :
				// administrative roles
				$this->userRoles['manager'] = array('90', 'Account Manager');

				$this->userRoles['advertising'] = array('60', 'Advertising');
				

				break;
		}

	}

	private function convertRoleToHighestLevel($roles){

		// find the highest level role a user has
		if($roles){
			$userRoles = array();
			foreach($roles as $roleLevel){
					
				$userRoles[] = self::convertRoleToLevel($roleLevel);
			}

			rsort($userRoles);
			//print_r($userRoles);
			if(isset($userRoles[0])){
					
				return $userRoles[0];
					
			} else {
					
				return false;
			}

		}
			
	}

	public function convertRoleToLevel($role){

		// instantiate a new copy as we not already have done so
		$roles = new User();
		foreach($roles->userRoles as $key=>$value){

			if($role == $key){

				return $value[0];
			}


		}

	}

	public function createNewUser($groupId = ''){

		$table = 'user';
		$postVars = array();
		$postVars['db_email'] = '';


	
		$postVars['db_password'] = Helpers::createPassword();
		$postVars['db_created_by_user_id_pk'] = $_SESSION['user']['user_id_pk'];
		$postVars['db_active'] = 0;
		$returnURL = SITE_PATH . '/admin/users/useredit/' .$groupId. '/{id}/';

		$schema = new Schema();
		$schema->createNewDatabaseEntry($table, $postVars, $returnURL);
	}

	private function returnGetUserQuery(){

		// shared between queries

		$query = "SELECT G.group_name
	
			,G.group_id_pk
			,G.group_configuration
			,U.user_id_pk
			,U.password
			,U.firstname
			,U.lastname
			,U.jobtitle
			,U.email
			
			,U.telephone
			,U.address1
			,U.address2
			,U.state
			,U.postcode
			,U.active
			FROM
			`user` U, `group` G WHERE 
			U.user_id_pk = ?
		
			AND U.group_id_pk = G.group_id_pk
			AND G.group_id_pk = ?
			
			";

		//echo $query;die();
		return $query;
	}

	public function getUserSimple($userId, $groupId, $setSession = true){


		$query = self::returnGetUserQuery();
		//echo $query;

		$parameters = array($userId, $groupId);

		//	echo $query;print_r($parameters);die();
		$memcacheKey = 'user_simple_user_id_pk_' . $userId;

		if($user = Database::dbRow($query, $parameters)){
			//print_r($user);die();
			//$thisUser = new User();
			self::assignUserVariables($userId, $groupId, $user);

			if($setSession){
				self::setUserSessionData($user);
			}
			//print_r($this);die();
		}

	}

	private function assignUserVariables($userId, $groupId, $user){
		//print_r($user);
		$this->userId = $userId;
		$this->groupId = $groupId;
	
		
		$this->userFirstname = $user['firstname'];
		$this->userLastname = $user['lastname'];
		$this->userFullName = $this->userFirstname . ' ' . $this->userLastname;
		$this->userEmail = $user['email'];
		$this->userPassword = $user['password'];
		$this->userTelephone = $user['telephone'];
		$this->userAdddress1 = $user['address1'];
		$this->userAdddress2 = $user['address1'];
		$this->userState = $user['state'];
		$this->userPostcode = $user['postcode'];
			
		$userRoles = self::getUserRoles($userId);

		$this->userAssignedRoles = $userRoles;
		$this->highestRole = self::convertRoleToHighestLevel($userRoles);
		//	print_r($this);die();
		//$this->group_configuration = json_decode($user['group_configuration'], true);

	}

	public function getUserForEditing($userId, $editorId){

		// check that they're actually allowed to edit this user

		$allowed = true;

		if($userId != $editorId){

			$allowed = self::checkEditPermissions($userId, $editorId);



		}

		if(!$allowed){

			return false;
		}

		$query = self::returnGetUserQuery();
		$parameters = array($userId,$_SESSION['user']['group_id_pk'] );
		
		if($user = Database::dbRow($query, $parameters, true)){
			//	print_r($user);
			//	$userRoles = array();
			self::assignUserVariables($userId, $editorId, $user);

			if($userRoles = self::getUserRoles($userId)){

				$user['userAssignedRoles'] = $userRoles;
					
			} else {

				$user['userAssignedRoles'] = array();
			}
			//print_r($user);
			return $user;
		}

		return false;

	}

	public function updateUser($groupId, $userId, $editorId){

		//$this->memDelete('user_simple_user_id_pk_' . $userId);
		$error = '';
		$updated = true;
		//are we allowed to be editing the user
		if(!$checkPermissions = self::getUserForEditing($userId, $editorId)){


			$updated = false;
			$error = 'permissions';

		}

		//	print_r($checkPermissions);die();
		// find out if they're changing their username
		if($_POST['db_email'] != $checkPermissions['email']){

			$checkUsername = self::checkIfUserAlreadyExists($_POST['db_email']);

			//echo 'no change =' . $checkUsername;die();
			//echo $checkUsername;die();
			// if that exists, don't let them change it
			if($checkUsername){

				$_POST['db_email'] = $checkPermissions['email'];
				$error = 'exists';
			}

		}


		if($error == ''){

			// update main information
			$table = 'user';
			$where = "`user_id_pk` = '". (int)$userId . "'";

			//	AND `module_client_id_pk` = '" . (int)$groupId . "'";
			$schema= new Schema();
			$schema->updateDatabaseEntry($table, $where);

			// update permissions
			if(!$checkUpdateRoles = self::updateUserRoles($userId, $editorId)){

				$updated = false;
				$error = 'roles';
			}

		}

		if($error != ''){

			header('location: /admin/users/useredit/'. (int)$groupId .'/'. (int)$userId . '&error=' . $error);
			die();
		}


		return true;

	}

	public function checkEditPermissionsAgainstLevel($userId, $minimumLevel){

		//echo $userId;

		$userRoles = self::getUserRoles($userId);
		$highestUserRole = self::convertRoleToHighestLevel($userRoles);
		//echo $minimumLevel;

		if($highestUserRole >= $minimumLevel){

			return true;
		}

		return false;
	}

	private function checkEditPermissions($userId, $editorId){

		// set a minimum user level for editing - in this case 50 which is admin and above
		// find the editors level first
		$user = new User();
		//print_r($user->minimumUserEditLevel);
		$permissions = self::checkEditPermissionsAgainstLevel($editorId,$user->minimumUserEditLevel);
		//echo $permissions;
		return $permissions;
	}


	private function getUserRoles($userId){

		$query = "SELECT `user_role`
		FROM
		`user_role`
		WHERE `user_id_pk` = ?";

		$parameters = array($userId);
		$memcacheKey = 'user_roles_' . $userId;

		//echo $query;print_r($parameters);
		// turn into a clean array
		$roles = array();
		if($userRoles = Database::dbResults($query, $parameters, true)){

			//print_r($userRoles);die();
			foreach($userRoles as $userRole){

				$roles[] = $userRole['user_role'];
			}
			//print_r($roles);


			return $roles;

		}
		return false;
			

	}

	public function getUseRolesMultiple($userIds, $role = ''){

		$query = "SELECT `user_role`, `user_id_pk`
		FROM
		`user_role`
		WHERE `user_id_pk` IN(?)";
		$parameters = array($userIds);

		if($role != ''){
				
			$query .= " AND `user_role` = ?";
			$parameters[] = $role;
				
		}



		//echo $query;print_r($parameters);
		// turn into a clean array
		$roles = array();
		if($userRoles = Database::dbResults($query, $parameters, true)){

			//print_r($userRoles);die();
			foreach($userRoles as $userRole){

				$roles[$userRole['user_id_pk']]['roles'][] = $userRole['user_role'];
				
			}
			//print_r($roles);
			return $roles;

		}
		return false;


	}

	public function getAllUsersWithRole($role){
		
		$query = "SELECT  UR.user_id_pk, U.firstname
		FROM
		`user_role` UR, `user` U
		WHERE `user_role`  = ? 
		AND UR.user_id_pk = U.user_id_pk
		ORDER BY U.firstname;
		";
		$parameters = array($role);

		


		//echo $query;print_r($parameters);
		// turn into a clean array
		$roles = array();
		if($userRoles = Database::dbResults($query, $parameters, true)){

			//print_r($userRoles);die();
			foreach($userRoles as $userRole){

			//	$roles[$userRole['user_id_pk']]['roles'][] = $userRole['user_role'];
				$roles[$userRole['user_id_pk']]['user_name'] = $userRole['firstname'];
				
			}
			//print_r($roles);
			return $roles;

		}
		return false;
		
	}



	function setUserSessionData($user){

		$_SESSION['user']['group_id_pk'] = $user['group_id_pk'];
		$_SESSION['user']['user_id_pk'] = $user['user_id_pk'];
		$_SESSION['user']['loggedin'] = true;
		$_SESSION['user']['group_configuration'] = json_decode($user['group_configuration'], true);

	}

	static function checkUserLoggedIn($area = ''){
		//print_r($_SESSION);die();
		//	echo SITE_PATH . $area . '/login/login/';die();
		// if they're not logged in at all, bounce them to the login page
		if(!isset($_SESSION['user']['loggedin']) || !$_SESSION['user']['loggedin']){

			if($area != ''){

				$area = '/' . $area;
			}

			header('location:' . SITE_PATH . $area . '/login/login/');
			die();


		}

	}

	function updateUserRoles($userId, $editorId){

		//	print_r($_POST);die();
		// check that the updater has at least some level higher than the person they're updating
		$user = new User();
		if(!$permissions = self::checkEditPermissionsAgainstLevel($editorId,$user->minimumUserEditLevel)){

			return false;
		}

		$memcacheKey = 'user_roles_' . $userId;

		// if so, delete all the current user roles and then re-add them in
		$query = "DELETE FROM `user_role` WHERE `user_id_pk` = ?";
		$dataTypes = 'i';
		$parameters = array($userId);

		Database::dbQuery($query, $dataTypes, $parameters);


		//Database::memDelete($memcacheKey);



		// rebuild it
		if(isset($_POST['db_user_roles'])){
			foreach($_POST['db_user_roles'] as $userRole){
				//echo "INSERT INTO `user_role` VALUES(" . $userId . ", " . $userRole . ")"  . '<br />';


				$query = "INSERT INTO `user_role` VALUES(?, ?)";
				$dataTypes = 'is';
				$parameters = array($userId, $userRole);
				//print_r($userRole);
				Database::dbQuery($query, $dataTypes, $parameters);
			}
		}

		return true;
	}

	function checkPagePermissions($thisUser, $method, $action, $permissions){



		// this needs to be broken up into little routines so we can check against more complex criteria
		// such as "is it locked to one group or user"

		$readPermission = false;

		// if they don't even have read permissions, bounce them out
		if(!$readPermissions = array_intersect($thisUser->userAssignedRoles, $permissions['read']['all'])){

			// now check if there's >role option
			foreach($permissions['read']['all'] as $checkRead){

				if(substr($checkRead, 0, 1) == '>'){

					// find the minium user level required
					//print_r($this);die();
					$minUserLevel = $this->userRoles[substr($checkRead, 1)][0];
					//	echo $minUserLevel;die();

				}
				//print_r($this->userRoles);
				// see if the user has at least this level
				foreach($this->userAssignedRoles as $userRoles){

					if(isset($this->userRoles[$userRoles]) && $this->userRoles[$userRoles][0] >= $this->minimumUserEditLevel){

						$readPermission = true;

					}
				}
			}

		} else {

			$readPermission = true;

		}

		if(!$readPermission){
			echo 'no privs';
			//	header('location:' . SITE_PATH . '/error/privileges/');
			//	die();
		}
		// write permission will only require those that don't fit into the read category as if you can't read it, you can't write it
	}

	public function checkIfUserAlreadyExists($username){

		$query = "SELECT `email` FROM `user` WHERE `email` = ?";
		$parameters = array($username);
		//Database::spe($query, $parameters, '');
		if(Database::dbRow($query, $parameters)){

			return 1;

		} else {

			return 0;

		}

	}

	########################
	#### HELPERS
}