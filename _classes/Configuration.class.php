<?php
class Configuration {

	/**
	 * KEEP A LIST OF CONFIGURATIONS WE'RE TRYING TO ACCESS
	 * template, client_type
	 *
	 */
	private $logoBannerMaxWidth = 900;
	private $logoBannerMaxHeight = 100;
	private $logoEmailMaxWidth = 600;
	private $logoEmailMaxHeight = 100;




	static public function getConfiguration($configurationType, $jsonDecode = true, $thisUser){
//print_r($thisUser);
		/**
		 * this will try and return a configuration file
		 * we want to use memcache for this as it's going to be accessed a lot
	 	*/
		
		
		$groupId = (int)$thisUser->groupId;
		$userId = (int)$thisUser->userId;

		
		$groupIdClause = '';
		$userIdClause = '';

		$query = "SELECT `configuration_rules` FROM `configuration` WHERE
			`configuration_type` = ?
			AND `active` = 1"; 



		/**
		 * try for a user first
		 */
		if($userId != ''){

			$userIdClause = " AND `user_id_pk` = ?";
			$memcacheKey = $configurationType . '_group_id_pk_' . $userId;

			$parameters = array($configurationType, $userId);

			if($configuration = Database::dbRow($query . $userIdClause, $parameters)){
					
				//return $configuration['configuration_rules'];
				return Database::returnResults($configuration['configuration_rules'], true);
			}
		}

		/**
		 * next try a group
		 */
		if($groupId != ''){

			$groupIDClause = " AND `group_id_pk` = ?";
			$memcacheKey = $configurationType . '_group_id_pk_' . $groupId;

			$parameters = array($configurationType, $groupId);

			if($configuration = Database::dbRow($query . $groupIDClause, $parameters)){
				//return $configuration['configuration_rules'];
				return Database::returnResults($configuration['configuration_rules'], true);
			}


		}

		

		return array();

	}

	static public function getConfigurationForEditing($configurationType,$groupId = 0, $userId = 0){



	
		$groupQuery = '';
		$userQuery = '';

			
		// we don't want any blank values in here
		if( $groupId == 0 && $userId == 0){

			return false;
		}


		$parameters = array($configurationType);
			
		
		if($groupId != 0){

			$groupQuery = " AND `group_id_pk` = ?";
			$parameters[] = $groupId;
		}
			
		if($userId != 0){

			$userQuery = " AND `user_id_pk` = ?";
			$parameters[] = $userId;
		}

		$query = "SELECT `configuration_rules` FROM `configuration` WHERE
			`configuration_type` = ?
			AND `active` = 1  $groupQuery $userQuery";

		//echo $query;
		//echo $query;die();
		//	print_r($parameters);
		//Database::showPreparedErrors($query, $parameters);
		if($configuration = Database::dbRow($query, $parameters)){

			return Database::returnResults($configuration['configuration_rules'], true);

		} else {
			//echo 'create';die();
			$schema = new Schema();
			$table = 'configuration';
			$postVars = array();
			$postVars['db_configuration_type'] = $configurationType;
			
			$postVars['db_group_id_pk'] = $groupId;
			$postVars['db_user_id_pk'] = $userId;
			$postVars['db_active'] = 1;
			$schema->createNewDatabaseEntry($table, $postVars);
			//	die();
			$results = self::getConfigurationForEditing($configurationType, $groupId, $userId);
			return Database::returnResults($configuration['configuration_rules'], true);
		}

		// if it doesn't exist, create it
			
		//return false;
	}


	public function updateConfigurationDetails($configurationType,$groupId = 0, $userId = 0){


		$configuration = '';
			
		foreach($_POST as $key=>$value){
			//echo substr($key, 0, 3) . '<br />';
			if(substr($key, 0, 7) == 'config_'){
					
				$configuration[substr($key, 7)] = $value;
			}
		}


		$query = "UPDATE `configuration` SET
		`configuration_rules` = ?
		WHERE 
		`configuration_type` = ?
		 AND `group_id_pk` = ?	AND `user_id_pk` = ?
		AND `active` = 1";

		$dataTypes = 'ssiii';
		//print_r($configuration);
		$parameters = array(json_encode($configuration), $configurationType, $groupId, $userId);
		Database::dbQuery($query, $dataTypes, $parameters);
		//	Database::spe($query, $parameters);
		//	die();
	}

	public function updateConfigurationOwnerBanner($bannerType = ''){
		//	echo $bannerType;
		// check if they got here by mistake
		// check we have the dirs and they're ready for writing

		//return print_r($_FILES);
		if(!isset($_FILES['Filedata']['tmp_name']) || $_FILES['Filedata']['tmp_name'] == '' || $bannerType == ''){
			//	return(print_r($_POST));
			//header('location: ' . SITE_PATH . '/admin/configuration/ownersetupedit/error/');
			//echo '0';
			//die();
			return '0';
		}

		// check we have the dirs and they're ready for writing
		FileUpload::checkForExistingUploadPath(ROOT . '/fileserver/client-logo/');
		FileUpload::checkForExistingUploadPath(ROOT . '/fileserver/client-logo/site-owner/');

		$info = pathinfo($_FILES['Filedata']['name']);
		$fileType =  $info['extension'];
		//	return $fileType;
		$upload = new FileUpload();
		$upload->uploadFieldId = 'Filedata';
		$upload->uploadType = 'image';
		$uploadDir =  ROOT . '/fileserver/client-logo/site-owner/';
		$upload->uploadDir = $uploadDir;
		$uploadName = $bannerType . '_temp.' . $fileType;
		$upload->uploadName = $uploadName;

		$success = $upload->uploadFile();

		if($success){
			// set session to stop the image caching in the header
			$_SESSION['user']['group_configuration']['template']['bannerbackgroundrefresh'] = true;
			
			// process it
			self::processConfigurationOwnerBanner($bannerType, $uploadName);
			@unlink($uploadDir . $uploadName);
			return '1';
			//echo '1';
		} else {
				
			return '0';
		}


	}

	private function processConfigurationOwnerBanner($bannerType, $uploadName){

		// turn the upload into something usable
		//$logoBannerMaxWidth

		$picture = new Imagick(ROOT . '/fileserver/client-logo/site-owner/' . $uploadName);
			
		

		$imageSize = $picture->getImageGeometry();
		$scale = findBestScale($this->logoBannerMaxWidth, $this->logoBannerMaxHeight , $imageSize['width'], $imageSize['height']);

			
		$picture->resizeImage($scale[0], $scale[1], Imagick::FILTER_LANCZOS,1, true);
		$picture->writeImage(ROOT .  '/fileserver/client-logo/site-owner/' . $bannerType . '.png');
		$picture->clear();
		$picture->destroy();


	}
}