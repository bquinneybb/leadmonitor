<?php
class CompetitorMyPropertyAddress {

	public function analyseNewAdditions(){

		$contents = self::getNewestDomains();
		$domainsSorted = self::splitNewestDomainsIntoComponents($contents);
		self::addNewClientsToDatabase($domainsSorted);
		self::addNewWebsitesToDatabase($domainsSorted);
		
	}

	private function getNewestDomains(){

		$url = "http://mypropertyaddress.com.au/individual-property-websites/200/0";
		$contents = file_get_contents($url);

		return $contents;

	}

	private function splitNewestDomainsIntoComponents($contents){

		$html = new simple_html_dom();
		$html->load($contents);

		$scrape = array();
		$c = 0;
		foreach($html->find('.adminTable tr td a') as $result){

			$scrape[$c] = $result->href;
			$c++;
		}
		//print_r($scrape);die();
		//rsort($scrape);

		$webSites['url'] = array();
		$webSites['client'] = array();
		for($i=0;$i<count($scrape);$i++){

			if($i%2 == 0){

				$webSites['url'][] = $scrape[$i];

			} else {

				$webSites['client'][] = $scrape[$i];
			}


		}



		//print_r($webSites);
		$websiteKeys = array();
		$i = 0;


		foreach($webSites['client'] as $client){

			$websiteKeys[$client][] = $webSites['url'][$i];
			$i++;
		}

		return $websiteKeys;
	}

	private function addNewClientsToDatabase($domainsSorted){

		foreach($domainsSorted as $client=>$value){

			$query = "SELECT `competitor_mypropertyaddress_client_pk` FROM `competitor_mypropertyaddress_client` WHERE `client_url` = ?";
			$parameters = array($client);

			if($clientId = Database::dbRow($query, $parameters)){

				// nothing
			} else {

				$query = "INSERT INTO `competitor_mypropertyaddress_client` VALUES ('', ?)";
				$dataTypes = 's';
				$parameters = array($client);
				Database::dbQuery($query, $dataTypes, $parameters);

			}


		}
	}

	private function addNewWebsitesToDatabase($domainsSorted){

		foreach($domainsSorted as $client=>$value){

			foreach($value as $domain){

				$query = "SELECT `competitor_mypropertyaddress_domain_id_pk`, `domain_url` , `date_created` FROM `competitor_mypropertyaddress_domain` WHERE `domain_url` = ?";
				$parameters = array($domain);

				if($domainId = Database::dbRow($query, $parameters)){

					// do nothing
				} else {
						
					// find the owner's id
					$query = "SELECT `competitor_mypropertyaddress_client_pk` FROM `competitor_mypropertyaddress_client` WHERE `client_url` = ?";
					$parameters = array($client);

					$clientId = Database::dbRow($query, $parameters);
					$query = "INSERT INTO `competitor_mypropertyaddress_domain` VALUES ('', ?, ?, '')";
					$dataTypes = 'is';
					$parameters = array($clientId['competitor_mypropertyaddress_client_pk'], $domain);
					//	Database::spe($query, $parameters);
					Database::dbQuery($query, $dataTypes, $parameters);
				}

			}
		}
	}
	
	private function findDomainsWithoutWhois(){
		
		$query = "SELECT `competitor_mypropertyaddress_domain_id_pk`, `domain_url` , `date_created` FROM `competitor_mypropertyaddress_domain` WHERE `date_created` = '	0000-00-00'";
		$parameters = array($domain);
		$whois = new SEOWhois();
		if($domains = Database::dbRow($query, $parameters)){
			
			foreach($domains as $domainId){
				
				$whoisDate = $whois->findWhoisInformation($$domainId['domain_url']);
				
				$query = "UPDATE `competitor_mypropertyaddress_domain` SET `date_created` = ? WHERE `competitor_mypropertyaddress_domain_id_pk` = ?";
				$dataTypes = 'si';
				$parameters = array( $whoisDate, $domainId['competitor_mypropertyaddress_domain_id_pk']);
			//	Database::spe($query, $parameters);
				Database::dbQuery($query, $dataTypes, $parameters);
				
			}
		}
	}
}