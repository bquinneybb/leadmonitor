<?php
class ScraperSEOMoz extends Scraper {
	
	private $host = 'lsapi.seomoz.com';
	private $accessId = 'member-58ed1fc3a3';
	private $secretKey = '8ba2ab25b8adda48f1e13409fb07b521';
	
	
	
	function checkDomainStatistics($url){
		
		$seoURL = 'http://' . $this->accessId . ':' . $this->secretKey . '@lsapi.seomoz.com/linkscape/url-metrics/' . $url;
		//echo $seoURL;
		if($results = Scraper::getURLContents($seoURL)){
			
			$resultDetails = self::convertResultToArray($results);
			//print_r($resultDetails);
			return $resultDetails;
		}
		
		
		
	}
	
	private function convertResultToArray($results){
		
		$resultDetails = array();
		$results = json_decode($results, true);
		
		
		$resultDetails['mozRank'] = $results['umrp'];
		$resultDetails['mozRankSubdomain'] = $results['fmrp'];
		$resultDetails['domainAuthority'] = $results['pda'];
		$resultDetails['externalLinksJuice'] = $results['ueid'];
		$resultDetails['externalLinksAny'] = $results['uid'];
		$resultDetails['pageAuthority'] = $results['upa'];
		return $resultDetails;
		//print_r($resultDetails);
	}
}