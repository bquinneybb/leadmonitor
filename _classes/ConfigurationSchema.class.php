<?php
class ConfigurationSchema extends Configuration {

	function returnConfigurationSchema($configurationType){

		switch($configurationType){

			// PHOTOGRAPHY
			case 'module_photography_image_resize':

				return '{"thumbnail":[100,100],"midsize":[200,200],"preview":[300,300]}';
				break;
					
			case 'module_photography_upload_images':

				return  '{"requiredFolder":"sRGB Internet JPGS","thumbnailSourceFolder":"sRGB Internet JPGS","ignoreContentCount":["files"]}';
				break;


				// NEW BUSINESS
			case 'module_new_business_status_types':

				return '{"1":"Red","2":"Green","3":"Blue"}';
				break;

			case 'module_new_business_category_types':

				return '{"r":"Renders","b":"Brochure","i":"Interactive \/ Virtual Tour","p":"Photography"}';
				break;


				// CLIENTS
			case 'client_types':
				return '["signregister","newbusiness"]';
				break;
		}



	}


	function fixMissingNodes(){


	}

}