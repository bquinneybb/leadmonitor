<?php
class ModuleCampaignsForm {

	public $dataFields = array();
	public $defaultHeaderFields = array();
	public $defaultForm = '';

	function __construct() {

		$this -> dataFields = array('name' => array('name', 'Name', 'text'), 'email' => array('email', 'Email', 'email'), 'phone' => array('phone', 'Phone', 'text'), 'message' => array('message', 'Message', 'textarea'));

		$this -> defaultHeaderFields = array('subject' => 'Subject', 'from' => 'Email Address From', 'fromname' => 'Email Address Form');

		$this -> defaultForm = '{"data":[["name","Name","text"],["email","Email","email"],["phone","Phone","text"],["message","Message","textarea"]]
				,"recipient":[["tech@barkingbird.com.au","Tech"]]
				,"headers":{"subject":"New Email from [somesite] contact form","from":"tech@barkingbird.com.au","fromname":"[somesite] contact form"}
				,"custom_action":[]}';

	}

	public function findCampaignForm($formId, $clientId) {

		$query = "SELECT  CF.form_data
		FROM `module_campaign_form` CF, `module_campaign` MC
		WHERE 
		MC.client_id_pk = CF.client_id_pk
		AND CF.module_campaign_form_id_pk = ?
		AND MC.module_campaign_form_id_pk = CF.module_campaign_form_id_pk
		AND CF.client_id_pk = ?
		AND MC.group_id_pk = ?";

		$parameters = array($formId, $clientId, $_SESSION['user']['group_id_pk']);

		//Database::spe($query, $parameters);
		if ($result = Database::dbRow($query, $parameters)) {

			return $result;
		}

		return false;
	}

	#####################
	######## Draw the fields for the CMS

	public function drawDataFields($fields) {

		$formValues = array();
		$formValues[''] = '';

		foreach ($this->dataFields as $key => $value) {

			$formValues[$key] = $value[1];

		}

		$html = '';
		$templateItems = new TemplateItems();
		$templateItems -> returnOnly = true;
		$i = 0;

		if ($fields) {
			foreach ($fields as $field) {

				if (isset($this -> dataFields[$field[0]])) {

					$html .= $templateItems -> drawFormLeft('Captured Field', '', 'form_data_left_' . $i);

					$dropDown = $templateItems -> drawList('form_data_' . $i, $formValues, $field[0], false);
					$html .= $templateItems -> drawFormRight($dropDown . ' : <a href="javascript:deleteDataField(' . $i . ')">DELETE</a>', $style = '', 'form_data_right_' . $i);
					//$html .= $templateItems->drawFormList('form_data_' . $i, $formValues, $field[0], true, 'Captured Field');
					$i++;
				}

			}

			
			foreach ($fields as $field) {

				if (!isset($this -> dataFields[$field[0]])) {
					$html .= $templateItems -> drawFormLeft('Captured Field', '', 'form_data_left_' . $i);
					$customField = '';
					$customField .= $templateItems -> drawText('form_data_custom_identifier_' . $i, $field[0]) . '&nbsp;';
					$customField .= $templateItems -> drawText('form_data_custom_label_' . $i, $field[1]) . '&nbsp;';
					$customField .= $templateItems -> drawText('form_data_custom_type_' . $i, $field[2]) . '&nbsp;';
					$html .= $templateItems -> drawFormRight($customField . ' : <a href="javascript:deleteDataField(' . $i . ')">DELETE</a>', $style = '', 'form_data_right_' . $i);
					$i++;
				}
			}

		} else {

			$html .= $templateItems -> drawFormLeft('Captured Field', '', 'form_data_left_' . $i);

			$dropDown = $templateItems -> drawList('form_data_' . $i, $formValues, '', true);
			$html .= $templateItems -> drawFormRight($dropDown . ' : <a href="javascript:deleteDataField(' . $i . ')">DELETE</a>', $style = '', 'form_data_right_' . $i);

			$i++;

		}

		$html .= $templateItems -> drawHidden('email_form_data_total', $i);

		echo $html;

	}

	public function drawBlankDropDown($currentOffset) {

		$formValues = array();
		$formValues[''] = '';
		foreach ($this->dataFields as $key => $value) {

			$formValues[$key] = ucwords($value[0]);
		}

		$html = '';
		$templateItems = new TemplateItems();
		$templateItems -> returnOnly = true;

		//$html .= $templateItems->drawFormList('form_data_' . $currentOffset ,$formValues, '-1', true, 'Captured Field');
		$html .= $templateItems -> drawFormLeft('Captured Field', '', 'form_data_left_' . $currentOffset);

		$dropDown = $templateItems -> drawList('form_data_' . $currentOffset, $formValues, '', false);
		$html .= $templateItems -> drawFormRight($dropDown . ' : <a href="javascript:deleteDataField(' . $currentOffset . ')">DELETE</a>', $style = '', 'form_data_right_' . $currentOffset);
		$currentOffset++;

		return $html;

	}

	public function drawRecipientFields($fields) {

		$html = '';
		$templateItems = new TemplateItems();
		$templateItems -> returnOnly = true;
		$i = 0;
		if ($fields) {
			foreach ($fields as $field) {

				$html .= $templateItems -> drawFormLeft('Recipient #' . ($i + 1) . ' Name', '', 'form_name_left_' . $i);
				$input = $templateItems -> drawText('form_recipient_name_' . $i, $field[1]);
				$html .= $templateItems -> drawFormRight($input, $style = '', 'form_name_right_' . $i);

				$html .= $templateItems -> drawFormLeft('Recipient #' . ($i + 1) . ' Email', '', 'form_email_left_' . $i);
				$input = $templateItems -> drawText('form_recipient_email_' . $i, $field[0]);

				if (count($fields) > 1) {

					$html .= $templateItems -> drawFormRight($input . ' : <a href="javascript:deleteEmailField(' . $i . ')">DELETE</a>', $style = '', 'form_email_right_' . $i);

				} else {

					$html .= $templateItems -> drawFormRight($input, $style = '', 'form_email_right_' . $i);

				}

				//	$html .= $templateItems->drawFormText('form_recipient_email_' . $i ,$field[0], 'Recipient #' . ($i+1)  . ' Email');
				//	$html .= $templateItems->drawFormText('form_recipient_name_' . $i, $field[1], 'Recipient #' . ($i+1) . ' Name');

				$i++;
			}

		} else {

			$html .= $templateItems -> drawFormText('form_recipient_name_' . $i, '', 'Recipient #' . ($i + 1) . ' Name');
			$html .= $templateItems -> drawFormText('form_recipient_email_' . $i, '', 'Recipient #' . ($i + 1) . ' Email');
			$i++;
		}

		$html .= $templateItems -> drawHidden('email_form_recipient_total', $i);

		echo $html;

	}

	public function drawBlankRecipientField($currentOffset) {

		$html = '';
		$templateItems = new TemplateItems();
		$templateItems -> returnOnly = true;

		
		$html .= $templateItems -> drawFormText('form_recipient_name_' . $currentOffset, '', 'Recipient #' . ($currentOffset + 1) . ' Name');
		$html .= $templateItems -> drawFormText('form_recipient_email_' . $currentOffset, '', 'Recipient #' . ($currentOffset + 1) . ' Email');

		return $html;
	}

	public function drawHeaderFields($fields) {

		$html = '';
		$templateItems = new TemplateItems();
		$templateItems -> returnOnly = true;
		$i = 0;
		foreach ($fields as $key => $value) {
			//print_r($field);
			$html .= $templateItems -> drawFormText('form_header_id_' . $i, $value, $this -> defaultHeaderFields[$key]);

			$i++;
		}

		//$html .= $templateItems->drawHidden('email_form_header_total', $i);

		echo $html;

	}

	public function saveFormFields() {

		$formId = isset($_POST['db_module_campaign_form_id_pk']) ? $_POST['db_module_campaign_form_id_pk'] : null;
		$clientId = isset($_POST['db_client_id_pk']) ? $_POST['db_client_id_pk'] : null;

		$form = array();
		$form['data'] = array();
		$form['recipient'] = array();
		$form['headers'] = array();

		$formName = isset($_POST['form_form_name']) ? $_POST['form_form_name'] : '';

		$totalDataFields = isset($_POST['email_form_data_total']) ? $_POST['email_form_data_total'] : 1;
		$totalRecipientFields = isset($_POST['email_form_recipient_total']) ? $_POST['email_form_recipient_total'] : 1;

		//print_r($_POST);
		// data fields
		for ($i = 0; $i < $totalDataFields; $i++) {

			$val = isset($_POST['form_data_' . $i]) ? $_POST['form_data_' . $i] : null;
			
			$customValIdentifier =  isset($_POST['form_data_custom_identifier_' . $i]) ? $_POST['form_data_custom_identifier_' . $i] : null;
			$customValLabel =  isset($_POST['form_data_custom_label_' . $i]) ? $_POST['form_data_custom_label_' . $i] : null;
			$customValType =  isset($_POST['form_data_custom_type_' . $i]) ? $_POST['form_data_custom_type_' . $i] : null;
			
			
			
			if ($val != null) {

				$form['data'][] = $this -> dataFields[$val];

			}
			
			if($customValIdentifier != null && $customValLabel != null && $customValType != null){
				
				$form['data'][] = array($customValIdentifier, $customValLabel, $customValType);
			}
			//echo $val . '<br />';
		}
		//recipients
		//	echo 'r = ' . $totalRecipientFields;
		//print_r($_POST);die();
		for ($i = 0; $i < $totalRecipientFields; $i++) {

			$recipientEmail = isset($_POST['form_recipient_email_' . $i]) ? $_POST['form_recipient_email_' . $i] : null;
			$recipientName = isset($_POST['form_recipient_name_' . $i]) ? $_POST['form_recipient_name_' . $i] : null;
			if ($recipientEmail != null && $recipientName != null) {

				$form['recipient'][] = array($recipientEmail, $recipientName);

			}
			//echo $val . '<br />';
		}

		// headers
		$i = 0;
		foreach ($this->defaultHeaderFields as $key => $value) {

			$val = isset($_POST['form_header_id_' . $i]) ? $_POST['form_header_id_' . $i] : null;
			$form['headers'][$key] = $val;

			$i++;
		}
		// check ownership
		if ($this -> findCampaignForm($formId, $clientId)) {
			//	echo 'updating';die();
			$query = "UPDATE `module_campaign_form` SET
			`form_data` = ?
			WHERE `module_campaign_form_id_pk` = ?";

			$dataTypes = 'si';
			$parameters = array(json_encode($form), $formId);
			//	Database::spe($query, $parameters);
			Database::dbQuery($query, $dataTypes, $parameters);
			//	Database::spe($query, $parameters);
			//	die();

			return true;
		}

		return false;
		//	die();
	}

}
