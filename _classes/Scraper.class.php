<?php
class Scraper {

	
	public $urls;
	public $keywordCounts;
	public $uniqueKeywords;
	
	public function getURLContents($url, $postVars = '', $proxy = false,  $forceCertainProxy = ''){

		$ch = curl_init($url);
	//	print_r($forceCertainProxy);die();
	//	curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	
	//	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 60);

		// always allow cookies
			curl_setopt($ch, CURLOPT_COOKIEFILE, '/cookies.txt');
			curl_setopt($ch, CURLOPT_COOKIEJAR, '/cookies.txt');
			
			// there are probs retreiving facebook data
			if(strstr($url, 'https') && strstr($url, 'facebook')){
				
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			}
		if($proxy){

			$proxyAddress  = self::findProxy();
			//print_r($proxyAddress);die();
				//curl_setopt($ch, CURLOPT_PROXY, $proxyAddress[0] . ':' . $proxyAddress[1]);
				curl_setopt($ch, CURLOPT_PROXY, $proxyAddress[0]);
				curl_setopt($ch, CURLOPT_PROXYPORT, $proxyAddress[1]);
				curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
			//	curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);

			//curl_setopt($ch, CURLOPT_PROXYUSERPWD, 'user:password');
		}
	
		// to bypass web wombat we need to send specific post vars depending on the ip we use
		if($forceCertainProxy != ''){
		//	echo $forceCertainProxy[0] . ':' . $forceCertainProxy[1];die();
			curl_setopt($ch, CURLOPT_PROXY, $forceCertainProxy[0]);
			curl_setopt($ch, CURLOPT_PROXYPORT, $forceCertainProxy[1]);
			curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
		//	curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
		}
		
		$userAgent = self::setUserAgent();
		curl_setopt($ch,CURLOPT_USERAGENT,$userAgent);
			
		$referer = self::setReferrer();
		curl_setopt($ch,CURLOPT_REFERER,$referer);
	
		// add any post vars
		$fields_string = '';
		if($postVars != ''){
			
			foreach($postVars as $key=>$value){ 
				
				$fields_string .= $key.'='.$value.'&';

			}
			
				rtrim($fields_string,'&');
				
				curl_setopt($ch,CURLOPT_POST,count($postVars));
				curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		}
			
	//	echo $fields_string;die();
	//	$data =  self::curl_exec_follow($ch);
		
		$data = curl_exec($ch);
		//echo curl_error($ch);
		//print_r(curl_getinfo($ch));
	
		curl_close($ch);

		
		//echo $data;die();
		return $data;

	}

	public function curl_exec_follow(/*resource*/ $ch, /*int*/ &$maxredirect = null) { 
    $mr = $maxredirect === null ? 5 : intval($maxredirect); 
    if (ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')) { 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $mr > 0); 
        curl_setopt($ch, CURLOPT_MAXREDIRS, $mr); 
    } else { 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false); 
        if ($mr > 0) { 
            $newurl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); 

            $rch = curl_copy_handle($ch); 
            curl_setopt($rch, CURLOPT_HEADER, true); 
            curl_setopt($rch, CURLOPT_NOBODY, true); 
            curl_setopt($rch, CURLOPT_FORBID_REUSE, false); 
            curl_setopt($rch, CURLOPT_RETURNTRANSFER, true); 
            do { 
                curl_setopt($rch, CURLOPT_URL, $newurl); 
                $header = curl_exec($rch); 
                if (curl_errno($rch)) { 
                    $code = 0; 
                } else { 
                    $code = curl_getinfo($rch, CURLINFO_HTTP_CODE); 
                    if ($code == 301 || $code == 302) { 
                        preg_match('/Location:(.*?)\n/', $header, $matches); 
                        $newurl = trim(array_pop($matches)); 
                    } else { 
                        $code = 0; 
                    } 
                } 
            } while ($code && --$mr); 
            curl_close($rch); 
            if (!$mr) { 
                if ($maxredirect === null) { 
                    trigger_error('Too many redirects. When following redirects, libcurl hit the maximum amount.', E_USER_WARNING); 
                } else { 
                    $maxredirect = 0; 
                } 
                return false; 
            } 
            curl_setopt($ch, CURLOPT_URL, $newurl); 
        } 
    } 

    return curl_exec($ch); 
} 

	public function findProxy(){

		/*
		$proxies = array(

		array('212.95.52.165', '3128')
		);

		$proxy = array_rand($proxies);
		return array($proxies[$proxy][0],  $proxies[$proxy][1]);
		*/
		$proxy = ScraperProxies::selectRandomProxy();
		return $proxy;
	}

	function setUserAgent(){

		$userAgents = array(
		'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (FM Scene 4.6.1)'
		,'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3 (.NET CLR 3.5.30729) (Prevx 3.0.5)'
		,'AnzwersCrawl/2.0 (anzwerscrawl@anzwers.com.au;Engine)'
		
		);

		$userAgent = array_rand($userAgents);
		return $userAgents[$userAgent];

	}

	function setReferrer(){
		return '';
		//return 'http://www.google.com?q=';
	}

	function checkProxyValid($proxy){


	}

	function cleanScrapedText($html){

		$urls = self::extract_html_urls($html);



		$text = self::strip_html_tags($html);
		$text = html_entity_decode( $text, ENT_QUOTES, "utf-8" );
		$text = self::strip_punctuation( $text);
		$text = self::strip_numbers($text);

		$text = mb_strtolower( $text, "utf-8" );

		// split to words
		mb_regex_encoding( "utf-8" );
		$words = mb_split( ' +', $text );
		// find the root stem


		foreach ( $words as $key => $word )
		//$words[$key] = PorterStemmer::Stem( $word, true );
		$words[$key] = $word;

		$stopText  = file_get_contents( ROOT . '/_utils/stopwords.txt' );
		$stopWords = mb_split( '[ \n]+', mb_strtolower( $stopText, 'utf-8' ) );
		foreach ( $stopWords as $key => $word )
		//$stopWords[$key] = PorterStemmer::Stem( $word, true );
		
		$words = array_diff( $words, $stopWords );
		
		
		$keywordCounts = array_count_values( $words );
		arsort( $keywordCounts, SORT_NUMERIC );
		
		$keywordCounts = self::removeSmallWords($keywordCounts);
		
		$uniqueKeywords = array_keys( $keywordCounts );
		//print_r($keywordCounts);
		// remove small words
		$this->urls = $urls;
		$this->keywordCounts = $keywordCounts;
		$this->uniqueKeywords = $uniqueKeywords;
		//print_r($this);
		//return array($urls, $keywords);
		//return;
	}
	
	private function removeSmallWords($array){
		//print_r($array);die();
		$keywords = array();
		foreach($array as $key=>$value){
		
			if(strlen($key) > 2){
				
				$keywords[$key] = $value;
				
			}
		}
		
		return $keywords;
	}

	private function extract_html_urls( $text ){

		$match_elements = array(
		// HTML
		array('element'=>'a',       'attribute'=>'href'),       // 2.0
		array('element'=>'a',       'attribute'=>'urn'),        // 2.0
		array('element'=>'base',    'attribute'=>'href'),       // 2.0
		array('element'=>'form',    'attribute'=>'action'),     // 2.0
		array('element'=>'img',     'attribute'=>'src'),        // 2.0
		array('element'=>'link',    'attribute'=>'href'),       // 2.0

		array('element'=>'applet',  'attribute'=>'code'),       // 3.2
		array('element'=>'applet',  'attribute'=>'codebase'),   // 3.2
		array('element'=>'area',    'attribute'=>'href'),       // 3.2
		array('element'=>'body',    'attribute'=>'background'), // 3.2
		array('element'=>'img',     'attribute'=>'usemap'),     // 3.2
		array('element'=>'input',   'attribute'=>'src'),        // 3.2

		array('element'=>'applet',  'attribute'=>'archive'),    // 4.01
		array('element'=>'applet',  'attribute'=>'object'),     // 4.01
		array('element'=>'blockquote','attribute'=>'cite'),     // 4.01
		array('element'=>'del',     'attribute'=>'cite'),       // 4.01
		array('element'=>'frame',   'attribute'=>'longdesc'),   // 4.01
		array('element'=>'frame',   'attribute'=>'src'),        // 4.01
		array('element'=>'head',    'attribute'=>'profile'),    // 4.01
		array('element'=>'iframe',  'attribute'=>'longdesc'),   // 4.01
		array('element'=>'iframe',  'attribute'=>'src'),        // 4.01
		array('element'=>'img',     'attribute'=>'longdesc'),   // 4.01
		array('element'=>'input',   'attribute'=>'usemap'),     // 4.01
		array('element'=>'ins',     'attribute'=>'cite'),       // 4.01
		array('element'=>'object',  'attribute'=>'archive'),    // 4.01
		array('element'=>'object',  'attribute'=>'classid'),    // 4.01
		array('element'=>'object',  'attribute'=>'codebase'),   // 4.01
		array('element'=>'object',  'attribute'=>'data'),       // 4.01
		array('element'=>'object',  'attribute'=>'usemap'),     // 4.01
		array('element'=>'q',       'attribute'=>'cite'),       // 4.01
		array('element'=>'script',  'attribute'=>'src'),        // 4.01

		array('element'=>'audio',   'attribute'=>'src'),        // 5.0
		array('element'=>'command', 'attribute'=>'icon'),       // 5.0
		array('element'=>'embed',   'attribute'=>'src'),        // 5.0
		array('element'=>'event-source','attribute'=>'src'),    // 5.0
		array('element'=>'html',    'attribute'=>'manifest'),   // 5.0
		array('element'=>'source',  'attribute'=>'src'),        // 5.0
		array('element'=>'video',   'attribute'=>'src'),        // 5.0
		array('element'=>'video',   'attribute'=>'poster'),     // 5.0

		array('element'=>'bgsound', 'attribute'=>'src'),        // Extension
		array('element'=>'body',    'attribute'=>'credits'),    // Extension
		array('element'=>'body',    'attribute'=>'instructions'),//Extension
		array('element'=>'body',    'attribute'=>'logo'),       // Extension
		array('element'=>'div',     'attribute'=>'href'),       // Extension
		array('element'=>'div',     'attribute'=>'src'),        // Extension
		array('element'=>'embed',   'attribute'=>'code'),       // Extension
		array('element'=>'embed',   'attribute'=>'pluginspage'),// Extension
		array('element'=>'html',    'attribute'=>'background'), // Extension
		array('element'=>'ilayer',  'attribute'=>'src'),        // Extension
		array('element'=>'img',     'attribute'=>'dynsrc'),     // Extension
		array('element'=>'img',     'attribute'=>'lowsrc'),     // Extension
		array('element'=>'input',   'attribute'=>'dynsrc'),     // Extension
		array('element'=>'input',   'attribute'=>'lowsrc'),     // Extension
		array('element'=>'table',   'attribute'=>'background'), // Extension
		array('element'=>'td',      'attribute'=>'background'), // Extension
		array('element'=>'th',      'attribute'=>'background'), // Extension
		array('element'=>'layer',   'attribute'=>'src'),        // Extension
		array('element'=>'xml',     'attribute'=>'src'),        // Extension

		array('element'=>'button',  'attribute'=>'action'),     // Forms 2.0
		array('element'=>'datalist','attribute'=>'data'),       // Forms 2.0
		array('element'=>'form',    'attribute'=>'data'),       // Forms 2.0
		array('element'=>'input',   'attribute'=>'action'),     // Forms 2.0
		array('element'=>'select',  'attribute'=>'data'),       // Forms 2.0

		// XHTML
		array('element'=>'html',    'attribute'=>'xmlns'),

		// WML
		array('element'=>'access',  'attribute'=>'path'),       // 1.3
		array('element'=>'card',    'attribute'=>'onenterforward'),// 1.3
		array('element'=>'card',    'attribute'=>'onenterbackward'),// 1.3
		array('element'=>'card',    'attribute'=>'ontimer'),    // 1.3
		array('element'=>'go',      'attribute'=>'href'),       // 1.3
		array('element'=>'option',  'attribute'=>'onpick'),     // 1.3
		array('element'=>'template','attribute'=>'onenterforward'),// 1.3
		array('element'=>'template','attribute'=>'onenterbackward'),// 1.3
		array('element'=>'template','attribute'=>'ontimer'),    // 1.3
		array('element'=>'wml',     'attribute'=>'xmlns'),      // 2.0
		);

		$match_metas = array(
        'content-base',
        'content-location',
        'referer',
        'location',
        'refresh',
		);

		// Extract all elements
		if ( !preg_match_all( '/<([a-z][^>]*)>/iu', $text, $matches ) )
		return array( );
		$elements = $matches[1];
		$value_pattern = '=(("([^"]*)")|([^\s]*))';

		// Match elements and attributes
		foreach ( $match_elements as $match_element )
		{
			$name = $match_element['element'];
			$attr = $match_element['attribute'];
			$pattern = '/^' . $name . '\s.*' . $attr . $value_pattern . '/iu';
			if ( $name == 'object' )
			$split_pattern = '/\s*/u';  // Space-separated URL list
			else if ( $name == 'archive' )
			$split_pattern = '/,\s*/u'; // Comma-separated URL list
			else
			unset( $split_pattern );    // Single URL
			foreach ( $elements as $element )
			{
				if ( !preg_match( $pattern, $element, $match ) )
				continue;
				$m = empty($match[3]) ? $match[4] : $match[3];
				if ( !isset( $split_pattern ) )
				$urls[$name][$attr][] = $m;
				else
				{
					$msplit = preg_split( $split_pattern, $m );
					foreach ( $msplit as $ms )
					$urls[$name][$attr][] = $ms;
				}
			}
		}

		// Match meta http-equiv elements
		foreach ( $match_metas as $match_meta )
		{
			$attr_pattern    = '/http-equiv="?' . $match_meta . '"?/iu';
			$content_pattern = '/content'  . $value_pattern . '/iu';
			$refresh_pattern = '/\d*;\s*(url=)?(.*)$/iu';
			foreach ( $elements as $element )
			{
				if ( !preg_match( '/^meta/iu', $element ) ||
				!preg_match( $attr_pattern, $element ) ||
				!preg_match( $content_pattern, $element, $match ) )
				continue;
				$m = empty($match[3]) ? $match[4] : $match[3];
				if ( $match_meta != 'refresh' )
				$urls['meta']['http-equiv'][] = $m;
				else if ( preg_match( $refresh_pattern, $m, $match ) )
				$urls['meta']['http-equiv'][] = $match[2];
			}
		}

		// Match style attributes
		$urls['style'] = array( );
		$style_pattern = '/style' . $value_pattern . '/iu';
		foreach ( $elements as $element )
		{
			if ( !preg_match( $style_pattern, $element, $match ) )
			continue;
			$m = empty($match[3]) ? $match[4] : $match[3];
			$style_urls = self::extract_css_urls( $m );
			if ( !empty( $style_urls ) )
			$urls['style'] = array_merge_recursive(
			$urls['style'], $style_urls );
		}

		// Match style bodies
		if ( preg_match_all( '/<style[^>]*>(.*?)<\/style>/siu', $text, $style_bodies ) )
		{
			foreach ( $style_bodies[1] as $style_body )
			{
				$style_urls = self::extract_css_urls( $style_body );
				if ( !empty( $style_urls ) )
				$urls['style'] = array_merge_recursive(
				$urls['style'], $style_urls );
			}
		}
		if ( empty($urls['style']) )
		unset( $urls['style'] );

		return $urls;
	}

	private function extract_css_urls( $text )
	{
		$urls = array( );

		$url_pattern     = '(([^\\\\\'", \(\)]*(\\\\.)?)+)';
		$urlfunc_pattern = 'url\(\s*[\'"]?' . $url_pattern . '[\'"]?\s*\)';
		$pattern         = '/(' .
         '(@import\s*[\'"]' . $url_pattern     . '[\'"])' .
        '|(@import\s*'      . $urlfunc_pattern . ')'      .
        '|('                . $urlfunc_pattern . ')'      .  ')/iu';
		if ( !preg_match_all( $pattern, $text, $matches ) )
		return $urls;

		// @import '...'
		// @import "..."
		foreach ( $matches[3] as $match )
		if ( !empty($match) )
		$urls['import'][] =
		preg_replace( '/\\\\(.)/u', '\\1', $match );

		// @import url(...)
		// @import url('...')
		// @import url("...")
		foreach ( $matches[7] as $match )
		if ( !empty($match) )
		$urls['import'][] =
		preg_replace( '/\\\\(.)/u', '\\1', $match );

		// url(...)
		// url('...')
		// url("...")
		foreach ( $matches[11] as $match )
		if ( !empty($match) )
		$urls['property'][] =
		preg_replace( '/\\\\(.)/u', '\\1', $match );

		return $urls;
	}

	private function strip_html_tags( $text ){
		$text = preg_replace(
		array(
		// Remove invisible content
            '@<head[^>]*?>.*?</head>@siu',
            '@<style[^>]*?>.*?</style>@siu',
            '@<script[^>]*?.*?</script>@siu',
            '@<object[^>]*?.*?</object>@siu',
            '@<embed[^>]*?.*?</embed>@siu',
            '@<applet[^>]*?.*?</applet>@siu',
            '@<noframes[^>]*?.*?</noframes>@siu',
            '@<noscript[^>]*?.*?</noscript>@siu',
            '@<noembed[^>]*?.*?</noembed>@siu',
		// Add line breaks before and after blocks
            '@</?((address)|(blockquote)|(center)|(del))@iu',
            '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
            '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
            '@</?((table)|(th)|(td)|(caption))@iu',
            '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
            '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
            '@</?((frameset)|(frame)|(iframe))@iu',
		),
		array(
            ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
            "\n\$0", "\n\$0",
		),
		$text );
		return strip_tags( $text );
	}

	private function strip_punctuation( $text ){
		$urlbrackets    = '\[\]\(\)';
		$urlspacebefore = ':;\'_\*%@&?!' . $urlbrackets;
		$urlspaceafter  = '\.,:;\'\-_\*@&\/\\\\\?!#' . $urlbrackets;
		$urlall         = '\.,:;\'\-_\*%@&\/\\\\\?!#' . $urlbrackets;

		$specialquotes  = '\'"\*<>';

		$fullstop       = '\x{002E}\x{FE52}\x{FF0E}';
		$comma          = '\x{002C}\x{FE50}\x{FF0C}';
		$arabsep        = '\x{066B}\x{066C}';
		$numseparators  = $fullstop . $comma . $arabsep;

		$numbersign     = '\x{0023}\x{FE5F}\x{FF03}';
		$percent        = '\x{066A}\x{0025}\x{066A}\x{FE6A}\x{FF05}\x{2030}\x{2031}';
		$prime          = '\x{2032}\x{2033}\x{2034}\x{2057}';
		$nummodifiers   = $numbersign . $percent . $prime;

		return preg_replace(
		array(
		// Remove separator, control, formatting, surrogate,
		// open/close quotes.
            '/[\p{Z}\p{Cc}\p{Cf}\p{Cs}\p{Pi}\p{Pf}]/u',
		// Remove other punctuation except special cases
            '/\p{Po}(?<![' . $specialquotes .
		$numseparators . $urlall . $nummodifiers . '])/u',
		// Remove non-URL open/close brackets, except URL brackets.
            '/[\p{Ps}\p{Pe}](?<![' . $urlbrackets . '])/u',
		// Remove special quotes, dashes, connectors, number
		// separators, and URL characters followed by a space
            '/[' . $specialquotes . $numseparators . $urlspaceafter .
                '\p{Pd}\p{Pc}]+((?= )|$)/u',
		// Remove special quotes, connectors, and URL characters
		// preceded by a space
            '/((?<= )|^)[' . $specialquotes . $urlspacebefore . '\p{Pc}]+/u',
		// Remove dashes preceded by a space, but not followed by a number
            '/((?<= )|^)\p{Pd}+(?![\p{N}\p{Sc}])/u',
		// Remove consecutive spaces
            '/ +/',
		),
        ' ',
		$text );
	}

	private function strip_numbers( $text ){
		$urlchars      = '\.,:;\'=+\-_\*%@&\/\\\\?!#~\[\]\(\)';
		$notdelim      = '\p{L}\p{M}\p{N}\p{Pc}\p{Pd}' . $urlchars;
		$predelim      = '((?<=[^' . $notdelim . '])|^)';
		$postdelim     = '((?=[^'  . $notdelim . '])|$)';

		$fullstop      = '\x{002E}\x{FE52}\x{FF0E}';
		$comma         = '\x{002C}\x{FE50}\x{FF0C}';
		$arabsep       = '\x{066B}\x{066C}';
		$numseparators = $fullstop . $comma . $arabsep;
		$plus          = '\+\x{FE62}\x{FF0B}\x{208A}\x{207A}';
		$minus         = '\x{2212}\x{208B}\x{207B}\p{Pd}';
		$slash         = '[\/\x{2044}]';
		$colon         = ':\x{FE55}\x{FF1A}\x{2236}';
		$units         = '%\x{FF05}\x{FE64}\x{2030}\x{2031}';
		$units        .= '\x{00B0}\x{2103}\x{2109}\x{23CD}';
		$units        .= '\x{32CC}-\x{32CE}';
		$units        .= '\x{3300}-\x{3357}';
		$units        .= '\x{3371}-\x{33DF}';
		$units        .= '\x{33FF}';
		$percents      = '%\x{FE64}\x{FF05}\x{2030}\x{2031}';
		$ampm          = '([aApP][mM])';

		$digits        = '[\p{N}' . $numseparators . ']+';
		$sign          = '[' . $plus . $minus . ']?';
		$exponent      = '([eE]' . $sign . $digits . ')?';
		$prenum        = $sign . '[\p{Sc}#]?' . $sign;
		$postnum       = '([\p{Sc}' . $units . $percents . ']|' . $ampm . ')?';
		$number        = $prenum . $digits . $exponent . $postnum;
		$fraction      = $number . '(' . $slash . $number . ')?';
		$numpair       = $fraction . '([' . $minus . $colon . $fullstop . ']' .
		$fraction . ')*';

		return preg_replace(
		array(
		// Match delimited numbers
            '/' . $predelim . $numpair . $postdelim . '/u',
		// Match consecutive white space
            '/ +/u',
		),
        ' ',
		$text );
	}
}
