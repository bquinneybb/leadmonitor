<?php
class ModuleCampaignsLeads extends ModuleCampaigns {

	public $leadSource;
	public $investorTypes;

	function __construct() {

		$this -> createLeadSourceOptions();
		$this -> createInvestorTypes();
	}

	private function createLeadSourceOptions() {

		$this -> leadSource = array();
		$this -> leadSource['web'] = 'Web';
		$this -> leadSource['display'] = 'Display Centre';
		$this -> leadSource['phone'] = 'Phone';
		$this -> leadSource['email'] = 'Email';

	}

	private function createInvestorTypes() {

		$this -> investorTypes = array();
		$this -> investorTypes['owner'] = 'Owner / Occupier';
		$this -> investorTypes['investor'] = 'Investor';
	}

	public function leadEdit($leadId, $checkValidityOnly = false) {
		//, MCL.firstname, MCL.lastname, MCL.email,	, MCL.my_comments ,MCL.apartments_interested_in
		//, MCL.phone, MCL.postcode,MCL.investor_type
		$query = "SELECT
		MCL.module_campaign_lead_id_pk, MCL.my_comments
		 
		,MCL.module_campaign_id_pk, MCL.leadsource, MCL.referer_url
		,MCL.followed_up, MCL.active,  MCL.date_created, MCL.group_id_pk
	
		, MC.campaign_name, MC.module_campaign_form_id_pk
		, MP.module_project_id_pk, MP.project_name, MP.project_type, MP.client_id_pk
		FROM `module_campaign_lead` MCL, `module_campaign` MC, `module_project` MP
		WHERE
		MCL.module_campaign_lead_id_pk = ?
		AND MCL.group_id_pk = ?
		AND MCL.active = 1
		AND  MCL.module_campaign_id_pk = MC.module_campaign_id_pk
		AND MC.module_project_id_pk = MP.module_project_id_pk
		";

		$parameters = array($leadId, $_SESSION['user']['group_id_pk']);

		if ($lead = Database::dbRow($query, $parameters)) {

			if ($checkValidityOnly) {

				return $lead;
			}

			//print_r($lead);
			// turn it into an array so we can use the generic data function
			$lead = array($lead);
			$leadData = self::leadsFindFieldData($lead);
			//		print_r($leadData);
			// grab the fields this item uses

			// unkey for editing
			$lead = $leadData[$leadId];

			// get the form data strucutre out
			$dataStructure = array();
			$i = 0;
			$leadStructure = ModuleCampaignsForm::findCampaignForm($lead['module_campaign_form_id_pk'], $lead['client_id_pk']);
			$leadFormDataStucture = json_decode($leadStructure['form_data'], true);
			foreach ($leadFormDataStucture['data'] as $key => $leadFormData) {

				$dataStructure[$i]['form_stucture'] = $leadFormData;
				$dataStructure[$i]['form_result'] = $lead['lead_' . $leadFormData[0]];

				$i++;
			}
			//	print_r($dataStructure);
			//	print_r($leadData);
			$lead['data'] = $dataStructure;
			return $lead;
		}

		return false;

	}

	public function leadFindInterestedApartments($leadId) {

		$query = "SELECT LA.module_apartment_unit_id_pk, LA.date_created
		,MAU.apartment_name, MAU.status
		,MAL.level_name
		
		FROM `module_lead_apartment` LA, `module_apartment_unit` MAU, `module_apartment_level` MAL
		WHERE 
		LA.module_lead_id_pk = ?
		AND LA.module_apartment_unit_id_pk = MAU.module_apartment_unit_id_pk
		AND MAU.module_apartment_level_id_pk = MAL.module_apartment_level_id_pk
		
		AND MAU.active = 1 AND MAL.active = 1
		";
		//AND MAL.group_id_pk = ? // ,  $_SESSION['user']['group_id_pk']
		$parameters = array($leadId);

		if ($apartments = Database::dbResults($query, $parameters)) {
			//	print_r($apartments);die();
			return $apartments;
		}

		return false;

	}

	public function leadSave($leadId) {

		if ($checkLead = self::leadEdit($leadId, true)) {

			//update the interested apartments first
			$query = "DELETE FROM `module_lead_apartment` WHERE
			`module_lead_id_pk` = ? AND `group_id_pk` = ?";

			$dataTypes = 'ii';
			$parameters = array($leadId, $_SESSION['user']['group_id_pk']);
			Database::dbQuery($query, $dataTypes, $parameters);

			$apartments = isset($_POST['apts_interested']) ? $_POST['apts_interested'] : null;
			if ($apartments != null) {
				$apartments = explode(';', trim($apartments, ';'));
				$dataTypes = '';
				$parameters = array();
				//		print_r($apartments);die();
				$query = "INSERT INTO `module_lead_apartment` VALUES ";
				foreach ($apartments as $apartment) {

					$query .= "(?, ?, ?, NOW()),";

					$dataTypes .= 'iii';
					$parameters[] = $leadId;
					$parameters[] = $_SESSION['user']['group_id_pk'];
					$parameters[] = $apartment;

				}

				//print_r($parameters);
				$query = trim($query, ',');

				//Database::spe($query, $parameters);

				Database::dbQuery($query, $dataTypes, $parameters);
			}

			// now update any user info fields
			$query = "DELETE FROM `module_campaign_lead_data` WHERE
			`module_campaign_lead_id_pk` = ?";

			$dataTypes = 'i';
			$parameters = array($leadId);
			Database::dbQuery($query, $dataTypes, $parameters);

			// find the forms's fields and save them
			$formFields = self::findFormFields($checkLead['module_campaign_form_id_pk'], $checkLead['group_id_pk']);
			$defaultFields = json_decode($formFields, true);
			$postFields = $defaultFields['data'];

			$leadFields = array();

			foreach ($postFields as $postFields) {

				$leadFields[$postFields[0]] = isset($_POST['lead_' . $postFields[0]]) ? $_POST['lead_' . $postFields[0]] : '';
			}

			$dataTypes = '';
			$parameters = array();

			$query = "INSERT INTO `module_campaign_lead_data` VALUES ";
			foreach ($leadFields as $key => $value) {

				$query .= "(?, ?,?),";

				$dataTypes .= 'iss';
				$parameters[] = $leadId;
				$parameters[] = $key;
				$parameters[] = $value;

			}

			//print_r($parameters);
			$query = trim($query, ',');

			//Database::spe($query, $parameters);

			Database::dbQuery($query, $dataTypes, $parameters);

			//print_r($leadFields);die();

			$schema = new Schema();
			$table = 'module_campaign_lead';
			$where = "`module_campaign_lead_id_pk` = '" . (int)$leadId . "'";
			$returnURL = '/admin/leads/leadedit/' . (int)$leadId . '&update';

			$schema -> updateDatabaseEntry($table, $where, '', $returnURL);

		}

		return false;

	}

	public function findAllLeads($projectId = '', $campaignId = '', $onlyNew = true) {

		$query = "SELECT
			MCL.module_campaign_lead_id_pk, MCL.date_created, MCL.module_campaign_id_pk 
			, MC.campaign_name
			, MP.module_project_id_pk, MP.project_name
			
			FROM `module_project` MP , `module_campaign_lead` MCL , `module_campaign` MC
			 WHERE
			 MP.module_project_id_pk = MC.module_project_id_pk
		AND MCL.group_id_pk = ?
		AND MCL.active = 1
		";

		if ($projectId != '') {

			$query .= " AND MP.module_project_id_pk = ?";
		}

		/*
		 if($campaignId == ''){

		 $query .= " , MC.name ";
		 }

		 $query .= " FROM `module_campaign_lead` MCL";

		 if($campaignId == ''){

		 $query .= " , `module_campaign` MC";
		 }
		 module_projects
		 */

		if ($onlyNew) {

			$query .= " AND MCL.followed_up = 0";
		}

		if ($campaignId != '') {

			$query .= " AND  MCL.module_campaign_id_pk = ? ";

		} else {

			$query .= " AND  MCL.module_campaign_id_pk = MC.module_campaign_id_pk ";
		}

		$parameters = array($_SESSION['user']['group_id_pk']);

		if ($projectId != '') {

			$parameters[] = $projectId;
		}

		if ($campaignId != '') {

			$parameters[] = $campaignId;

		}

		if ($leads = Database::dbResults($query, $parameters)) {
			$results = self::leadsFindFieldData($leads);
			return $results;
			//return $leads;
		}

		return false;
	}

	private function leadsFindFieldData($leads) {
		$leadsKeyed = array();
		$parameters = array();
		$inString = '';

		foreach ($leads as $lead) {
			//	print_r($leads);die();
			$leadsKeyed[$lead['module_campaign_lead_id_pk']] = $lead;
			$parameters[] = $lead['module_campaign_lead_id_pk'];
			$inString .= '?,';

		}

		$inString = trim($inString, ',');
		$query = "SELECT `module_campaign_lead_id_pk`, `lead_key`, `lead_value` FROM `module_campaign_lead_data`
		WHERE `module_campaign_lead_id_pk` IN(" . $inString . ")";
		//Database::spe($query, $parameters);
		if ($results = Database::dbResults($query, $parameters)) {
			//print_r($results);die();
			foreach ($results as $result) {

				//if($result['lead_key'] == 'email'){
				//echo $result['module_campaign_lead_id_pk'] . '<br />';
				$leadsKeyed[$result['module_campaign_lead_id_pk']]['lead_' . $result['lead_key']] = $result['lead_value'];
				//	$leadsKeyed[19]['ff'] ='bar';
				//	}
				/*
				 // later on we need to write code for separate fields
				 if($result['lead_key'] == 'name'){

				 $leadsKeyed[$lead['module_campaign_lead_id_pk']]['name'] = $result['lead_value'];

				 }

				 if($result['lead_key'] == 'phone'){

				 $leadsKeyed[$lead['module_campaign_lead_id_pk']]['phone'] = $result['lead_value'];

				 }

				 */
			}
			//	print_r($leadsKeyed);	die();
			return $leadsKeyed;
			//
		}
		//echo $inString;
		return false;
	}

	#####################
	### WEB LEADS
	public function saveWebLead($campaignIdEncrypted, $formIdEncrypted, $testing = false, $additional_recipient = false, $email_subject = FALSE, $attachments = FALSE, $debug_mode = FALSE) 
	{

		$attachedImages 	= array();
		$clean_up 			= array();
		$campaign 			= (int)decrypt($campaignIdEncrypted);
		$formId 			= (int)decrypt($formIdEncrypted);

		// find out what we need about the campaign
		if ($campaignDetails = $this -> campaignFindIdFromPost($campaign)) 
		{
			if ($formFields = $this -> findFormFields($formId, $campaignDetails['group_id_pk'])) 
			{
				$defaultFields 	= json_decode($formFields, true);
				$postFields 	= $defaultFields['data'];
				$schema 		= new Schema();
				$table 			= 'module_campaign_lead';

				if ($testing == 'true') $table .= '_tmp';

				$postVars = array();

				$postVars['db_module_project_id_pk'] 	= (int)$campaignDetails['module_project_id_pk'];
				$postVars['db_module_campaign_id_pk'] 	= $campaign;
				$postVars['db_module_form_id_pk'] 		= $formId;
				$postVars['db_group_id_pk'] 			= (int)$campaignDetails['group_id_pk'];
				$postVars['db_active'] 					= 1;
				$postVars['db_followed_up'] 			= 0;
				$postVars['db_leadsource'] 				= 'web';
				$postVars['db_uid'] 					= isset($_POST['db_uid']) ? $_POST['db_uid'] : '';
				$postVars['db_referer_url'] 			= isset($_POST['db_referrer']) ? $_POST['db_referrer'] : '';
				$postVars['db_split_test'] 				= isset($_POST['db_split_test']) ? $_POST['db_split_test'] : '';
				
				$emailText = self::returnEmailHeader($campaign);

				// add in the header
				$emailText .= '[header]You have received a new email enquiry from your ' . stripslashes($campaignDetails['campaign_name'])  . '.[/header]' . "\r\n";
				$emailText .= 'Received on ' .  date('l jS \of F Y h:i:s A') . '<br /><br />' . "\r\n";	

				$newId 		= $schema->createNewDatabaseEntry($table, $postVars, '');
				// print_r($newId); die("end");

				$dataTypes 	= '';
				$parameters = array();

				$table 		= 'module_campaign_lead_data';

				if ($testing == 'true') $table .= '_tmp';

				$query = "INSERT INTO `" . $table . "`
				(`module_campaign_lead_id_pk`, `lead_key`, `lead_value`) VALUES";

				foreach ($postFields as $defaultField) 
				{
					$fieldValue = isset($_POST['db_' . $defaultField[0]]) ? stripslashes($_POST['db_' . $defaultField[0]]) : null;

					$fieldValue = str_replace("\r\n", "<br/>", $fieldValue);

					$query 			.= "(?, ?, ?),";
					$parameters[] 	= $newId;
					$parameters[] 	= $defaultField[0];
					$parameters[] 	= $fieldValue;
					$dataTypes 		.= 'iss';
					
					if($defaultField[2] == 'image')
					{
						if($_FILES['db_' . $defaultField[0]]['size'] > 0)
						{
							if($imageAttachment = self::checkImageValidity($defaultField[0]))
							{							
								$attachedImages[] = 	$_FILES['db_' . $defaultField[0]];
							}
						}
					} 
					else 
					{
						$emailText .= '[columnleft]' . $defaultField[1] . '[/columnleft]' . '[columnright]' . stripslashes($fieldValue) . '[/columnright]'. "<br />\r\n";
					}
				}
				
				// footer
				$emailText 	.= self::returnEmailFooter();
				$query 		= substr($query, 0, -1);
				//if (!$debug_mode) Database::dbQuery($query, $dataTypes, $parameters);
				Database::dbQuery($query, $dataTypes, $parameters);

				// send the email
				$emailer 	= new SendEmail();
				$emailText 	= $emailer->formatEmail($emailText);
				$log_behavior = FALSE;

				if (!$debug_mode) 
				{
					if(count($defaultFields['recipient']) > 0 || $emailer->emailEmailTo != '')
					{
						foreach ($defaultFields['recipient'] as $recipient) 
						{
							$emailer->AddAddress($recipient[0], $recipient[1]);
						}
					}
					else
					{
						//$emailer->AddAddress('tech@barkingbird.com.au', 'No Recipient');

						// Set flag to log date, campaign ID and email content
						$log_behavior = TRUE;
					}
				}
				

				//are we sending to other recipients? if so many or just one?				
				if (!$debug_mode && $additional_recipient)
				{
					if (is_array($additional_recipient)) 
					{
						foreach ($additional_recipient as $recipients_email) 
						{
							$emailer->AddAddress($recipients_email, $recipients_email);
						}
					}
					else
					{
						$emailer->AddAddress($additional_recipient, $additional_recipient);
					}					
				}
				if ($debug_mode)
				{
					$emailer->AddAddress('tech@barkingbird.com.au', 'tech@barkingbird.com.au');
				}

				// set all the default details for the email

				//subject
				if ($email_subject) 
				{
					$emailer->emailSubject 	= $email_subject;
				}
				else
				{
					$emailer->emailSubject 	= $defaultFields['headers']['subject'];
				}
				
				$emailer->fromName 		= $defaultFields['headers']['fromname'];

				// Array to store UA Corp client ids that will display a different reply to address
				// Urban Angles = 1
				// Urban Freeway = 2
				// UA Creative = 17
				// Barking Bird = 23
				$ua_corp_clients = array(1,2,17,23);
				if(in_array($campaignDetails['client_id_pk'], $ua_corp_clients))
				{
					$from_address = 'no-reply@leadmonitor.com.au';
				}
				else
				{
					$from_address 	= $_POST['db_email'];
				}

				$emailer->fromEmail 	= $from_address;
				$emailer->replyTo = $from_address;
				
				$emailer->emailBody 	= $emailText;
				
				//die($emailText);

				// add any attachment
				if(count($attachedImages) > 0)
				{
					foreach($attachedImages as $attachedImage)
					{
						$emailer->addAttachment($attachedImage['tmp_name'], $attachedImage['name']);
					}
				}
				// add file attachments from remote server
				if ($attachments) 
				{
					
					if (is_array($attachments)) 
					{
						foreach ($attachments as $key => $remote_file) 
						{
							$file_headers = @get_headers($remote_file);
							// var_dump($remote_file);
							// var_dump($file_headers);
							if($debug_mode) die(var_dump($file_headers));
							if (($file_headers[0] == 'HTTP/1.0 404 Not Found') || ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found')) continue;

							// use curl instead of file_get_contents
							$userAgent = 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0';

							$ch = curl_init();

							// set url
							curl_setopt($ch, CURLOPT_URL, $remote_file);

							// return the transfer as a string
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

							// set the user agent
							curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);

							// $output contains the output string
							$remote_data = curl_exec($ch); 

							//$remote_data = @file_get_contents($remote_file);

							if($remote_data)
							{
								$tmp_file 	= tempnam("tmp", 'atta');
								$file_name 	= end(explode('/', $remote_file));

								$saved_remote = file_put_contents($tmp_file, $remote_data);
								if ($saved_remote !== FALSE) 
								{
									$added = $emailer->addAttachment($tmp_file, $file_name);
								}
								$clean_up[] = $tmp_file;
							}

							// close curl resource to free up system resources
							curl_close($ch);
						}
						//die("end");
					}
				}

				// check for any rules that might override these details
				// only our form for now
				if ($formId == 28)
				{
					ModuleCampaignsLeadsRules::parseRules($emailer, $defaultFields);
				}

				if($log_behavior){

					$table = "log_email";
					$dataTypes = 'sssssi';
					$campaign_link = "http://www.leadmonitor.com.au/admin/campaigns/campaignedit/".$campaign;

					$query = "INSERT INTO `" . $table . "`
					(`sent_by`, `configuration`, `date_created`, `email_content`, `campaign_link`, `campaign_id`) VALUES (?, ?, ?, ?, ?, ?)";

					$params = array("Not Set","NA",date("Y-m-d H:i:s"),json_encode($emailer),$campaign_link,$campaign);

					Database::dbQuery($query, $dataTypes, $params);

					header('location:' . $campaignDetails['return_url']);
					die();
				}

				$sent = $emailer->sendEmailOut();

				if(!$sent && trim($emailer->ErrorInfo))
				{
					$table = "log_email";
					$dataTypes = 'sssssi';
					$campaign_link = "http://www.leadmonitor.com.au/admin/campaigns/campaignedit/".$campaign;

					$query = "INSERT INTO `" . $table . "`
					(`sent_by`, `configuration`, `date_created`, `email_content`, `campaign_link`, `campaign_id`) VALUES (?, ?, ?, ?, ?, ?)";

					$params = array("ADMIN: Not Sent","NA",date("Y-m-d H:i:s"),json_encode($emailer),$campaign_link,$campaign);

					Database::dbQuery($query, $dataTypes, $params);
				}

				if (count($clean_up) > 0) 
				{
					foreach ($clean_up as $key => $remove_file) 
					{
						unlink($remove_file);
					}
				}

				// do we need to send an auto responder
				$autoresponder = new ModuleCampaignsLeadsEmailSend();
				$autoresponder->checkForAutoResponder($campaign, $_POST, $newId);

				//print_r($campaignDetails);die();
				header('location:' . $campaignDetails['return_url']);
				die();
			}

		} else {
			
			
		}

	}

	public function saveWebLead_dev($campaignIdEncrypted, $formIdEncrypted, $testing = false, $additional_recipient = false, $email_subject = FALSE, $attachments = FALSE, $debug_mode = FALSE) {

		
		$attachedImages = array();
		$campaign = (int)decrypt($campaignIdEncrypted);
		$formId = (int)decrypt($formIdEncrypted);
		//	echo 'f = ' . $campaign;die();
		// find out what we need about the campaign
		if ($campaignDetails = $this -> campaignFindIdFromPost($campaign)) {
				//print_r($campaignDetails);die();
			if ($formFields = $this -> findFormFields($formId, $campaignDetails['group_id_pk'])) 
			{
				$defaultFields = json_decode($formFields, true);
				//print_r($defaultFields);
				$postFields = $defaultFields['data'];

				$schema = new Schema();

				$table = 'module_campaign_lead';

				if ($testing == 'true') {
					$table .= '_tmp';
				}

				$postVars = array();

				$postVars['db_module_project_id_pk'] = (int)$campaignDetails['module_project_id_pk'];
				$postVars['db_module_campaign_id_pk'] = $campaign;
				$postVars['db_module_form_id_pk'] = $formId;
				$postVars['db_group_id_pk'] = (int)$campaignDetails['group_id_pk'];
				$postVars['db_active'] = 1;
				$postVars['db_followed_up'] = 0;
				$postVars['db_leadsource'] = 'web';
				$postVars['db_uid'] = isset($_POST['db_uid']) ? $_POST['db_uid'] : '';
				$postVars['db_referer_url'] = isset($_POST['db_referrer']) ? $_POST['db_referrer'] : '';
				$postVars['db_split_test'] = isset($_POST['db_split_test']) ? $_POST['db_split_test'] : '';
							
				
				$emailText = self::returnEmailHeader($campaign);
				// add in the header
				//$emailText .= '[header]' . $defaultFields['headers']['subject'] . '[/header]' . "\r\n";
				$emailText .= '[header]You have received a new email enquiry from your ' . stripslashes($campaignDetails['campaign_name'])  . '.[/header]' . "\r\n";
				$emailText .= 'Received on ' .  date('l jS \of F Y h:i:s A') . '<br /><br />' . "\r\n";	

				$newId = $schema -> createNewDatabaseEntry($table, $postVars, '');
				$dataTypes = '';
				$parameters = array();

				$table = 'module_campaign_lead_data';

				if ($testing == 'true') {
					$table .= '_tmp';
				}

				$query = "INSERT INTO `" . $table . "`
				(`module_campaign_lead_id_pk`, `lead_key`, `lead_value`) VALUES";
				//print_r($postFields);die();
				foreach ($postFields as $defaultField) 
				{
					$fieldValue = isset($_POST['db_' . $defaultField[0]]) ? stripslashes($_POST['db_' . $defaultField[0]]) : null;
					//$postVars['db_' . $defaultField[0]] = $fieldValue;

					$query .= "(?, ?, ?),";
					$parameters[] = $newId;
					$parameters[] = $defaultField[0];
					$parameters[] = $fieldValue;
					$dataTypes .= 'iss';
					//print_r($_FILES);die();
					
					if($defaultField[2] == 'image'){
						if($_FILES['db_' . $defaultField[0]]['size'] > 0){
							if($imageAttachment = self::checkImageValidity($defaultField[0])){
							
								$attachedImages[] = 	$_FILES['db_' . $defaultField[0]];
							}
						}
						//echo $imageAttachment;die();
					} else {
					//	$emailText .= '<strong>' . $defaultField[1] . '</strong> : ' . $fieldValue . "<br />\r\n";
						$emailText .= '[columnleft]' . $defaultField[1] . '[/columnleft]' . '[columnright]' . stripslashes($fieldValue) . '[/columnright]'. "<br />\r\n";
					}
				}
				
				// footer
				$emailText .= self::returnEmailFooter();
				//$query = substr($query, 0, -1);
				//Database::dbQuery($query, $dataTypes, $parameters);

				// send the email
				$emailer 	= new SendEmail();
				$emailText 	= $emailer->formatEmail($emailText);

				$emailer->AddAddress('tech@barkingbird.com.au', 'tech@barkingbird.com.au');

				//subject
				if ($email_subject) 
				{
					$emailer->emailSubject 	= $email_subject;
				}
				else
				{
					$emailer->emailSubject 	= $defaultFields['headers']['subject'];
				}
				
				$emailer->fromName 		= $defaultFields['headers']['fromname'];
				$emailer->fromEmail 	= $_POST['db_email'];
				$emailer->emailBody 	= $emailText;
				
				// add any attachment
				if(count($attachedImages) > 0)
				{
					foreach($attachedImages as $attachedImage)
					{
						$emailer->addAttachment($attachedImage['tmp_name'], $attachedImage['name']);
					}
				}
				// file attachments from remote server
				if ($attachments) 
				{
					if (is_array($attachments)) 
					{
						foreach ($attachments as $key => $remote_file) 
						{
							$file_headers = @get_headers($remote_file);
							if (($file_headers[0] == 'HTTP/1.0 404 Not Found') || ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found')) continue;

							$remote_data = @file_get_contents($remote_file);
							if($remote_data)
							{
								$tmp_file 	= tempnam("tmp", 'atta');
								$file_name 	= end(explode('/', $remote_file));

								$saved_remote = file_put_contents($tmp_file, $remote_data);
								if ($saved_remote !== FALSE) 
								{
									$emailer->addAttachment($tmp_file, $file_name);
								}
								unlink($tmp_file);
							}
						}
					}
				}
				
				$emailer->replyTo = $_POST['db_email'];

				// check for any rules that might override these details
				// only our form for now
				if ($formId == 28) 
				{
					ModuleCampaignsLeadsRules::parseRules($emailer, $defaultFields);
				}
				//print_r($emailer);die();

				$emailer->sendEmailOut();

				// do we need to send an auto responder
				$autoresponder = new ModuleCampaignsLeadsEmailSend();
				$autoresponder->checkForAutoResponder($campaign, $_POST, $newId);

				//print_r($campaignDetails);die();
				//header('location:' . $campaignDetails['return_url']);
				die();
			}

		} else {
			
			
		}

	}
	
	
	
	public function returnEmailHeader($campaign){
	
	
		
		$emailText = '';
		$emailText .= '<table width="100%" border="0" align="center"><tr><td bgcolor="#EEEEEE">' . "\r\n";
		$emailText .= '<table width="600" align="center"  cellpadding="10" cellspacing="10"><tr><td bgcolor="#FFFFFF">' . "\r\n";

		$bannerPath = $_SERVER['DOCUMENT_ROOT'].'/fileserver/banners/'.$campaign.'.png';
				
			if (file_exists($bannerPath))
			{
				$emailText .= '<img src="http://www.leadmonitor.com.au/fileserver/banners/'.$campaign.'.png" />' . "\r\n";
			}
		
		
		return $emailText;

		
	}
	
	public function returnEmailFooter(){
		
		$emailText = '[footer]'. "\r\n";
				
			$emailText .= '<hr />' . "\r\n";
			$emailText .= '<table width="100%"><tr>' . "\r\n";
				
				$emailText .= '<td valign="top"><a href="http://www.barkingbird.com.au" style=""><img src="http://www.leadmonitor.com.au/fileserver/branding/barking-bird-logo.png" alt="Barking Bird" width="100" /></a><br /><br />' . "\r\n";
				$emailText .= '
					<span style="font-size:11px;font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;">
						<strong>Powered by Barking Bird</strong><br />
						 Ground Floor, 10 Grattan St, Prahran, VIC 3181, Australia<br />
						 <b>E</b> <a href="mailto:peck@barkingbird.com.au" style="color:#000000">peck@barkingbird.com.au</a>&nbsp;&nbsp;<b>P</b> 1300 660 177<br /><br />
						 
						 <b><a href="http://www.barkingbird.com.au" style="color:#000000">Visit Us Online</a></b>
					</span>
					</td>';
			
				
			
			$emailText .= '</tr></table>' . "\r\n";
		
		$emailText .= '[/footer]' . "\r\n";
		$emailText .= '</td></tr></table>' . "\r\n";
		$emailText .= '</td></tr></table>' . "\r\n";
		
		return $emailText;
	}
	
	private function checkImageValidity($field){
		$a = getimagesize($_FILES['db_' . $field]['tmp_name']);
		$image_type = $a[2];
		if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
    {
        return true;
    }
    return false;
		//print_r($a);
		//print_r($_FILES);die();	
	}

	static public function findFormFields($formId, $groupId) {

		$query = "SELECT `form_data` FROM `module_campaign_form` WHERE
		`module_campaign_form_id_pk` = ? AND `group_id_pk` = ?";

		$parameters = array($formId, $groupId);

		if ($result = Database::dbRow($query, $parameters)) {

			return $result['form_data'];
		}
	}

	private function hasLeadPreviouslyRegistered($newId, $emailAddress, $firstName, $lastName) {

		// see if someone has previously registered for any campaigns. Maybe they're interested in
		// this current one or didn't get responded to in a previous one

		$query = "SELECT `module_campaign_lead_id_pk` FROM `module_campaign_lead` WHERE
		`email` = ? OR (`firstname` = ? AND `lastname` = ?)";

		$parameters = array($emailAddress, $firstName, $lastName);

		if ($previousRegistrant = Database::dbResults($query, $parameters)) {

			// do some action to let admin know they' registered before
			// such as alert admin, or don't send more information
			// if it's the same campaign

		}

	}

	private function checkLeadBlacklist($emailAddress) {

		// find if the person enquiring is on a blacklist e.g they're from your competitor site
		$emailAddress = explode('@', $emailAddress);
		$domain = $emailAddress[1];

		$query = "SELECT `domain` FROM `module_campaign_lead_domain_blacklist` WHERE `domain` = ?";
		$parameters = array($domain);

		if ($checkDomain = Database::dbRow($query, $parameters)) {

			return true;
		}

		return false;
	}

	public function convertRefererToKnownSite($referer) {

		if ($referer != '') {
			$parse = parse_url($referer);
			if (isset($parse['host'])) {
				$finalReferer = $parse['host'];

				$knownSites = array( array('google.com', 'Google'));

				foreach ($knownSites as $knownSite) {
					//	echo 'k = ' . $knownSite[0];
					if (stristr($referer, $knownSite[0])) {

						$finalReferer = $knownSite[1];
					}

				}

				//	print_r($parse);die();
				return $finalReferer;

			} else {

				return '';
			}
		} else {
			return '';
		}
	}

	public function checkForPossibleSpam($campaignId, $referrer = FALSE)
	{
		if(!$referrer) return FALSE;
		$campaignDetails = $this -> campaignFindIdFromPost($campaignId);
		$info = parse_url($campaignDetails['return_url']);
		$hostCampaign = $info['host'];
		//echo $hostCampaign;
		$referrer = str_replace('www.', '', $referrer);
		$info = parse_url($referrer);
		$hostRefer = $info['host'];
		
		if($hostCampaign != $hostRefer){
			
			//echo 'failed';
			
			return true;
			
		} else {
			
			//echo 'passed';
			
			return false;
		}
		//echo $host;
		//print_r($campaignDetails);
		
	}
	
	public function logSpam($campaignId, $formId, $reason = ''){
		
		$query = "INSERT INTO `spamtrap` VALUES (
		'', ?, ?, ?, ?, ?, ?, ?, NOW()
		
		)";
		$dataTypes = 'sssssss';
		$parameters = array($campaignId, $formId, serialize($_POST), serialize($_SERVER), $_SERVER['HTTP_REFERER'], $_SERVER['REMOTE_ADDR'], $reason);
		//print_r($parameters);
		Database::dbQuery($query, $dataTypes, $parameters);
	}

}
