<?php
class ModuleProjectDeveloperApartments extends ModuleProjectDeveloperBuilding {
	
	public $aparmentStatus;

	function __construct(){
		
		$this->createApartmentStatusOptions();
		
	}
	
	
	private function createApartmentStatusOptions(){
		
		$this->aparmentStatus = array();
		$this->aparmentStatus['available'] = array('Available');
		$this->aparmentStatus['reserved'] = array('Reserved');
		$this->aparmentStatus['developerreservation'] = array('Developer Reservation');
		$this->aparmentStatus['deposit'] = array('Deposit');
		$this->aparmentStatus['sold'] = array('Sold');
		
	}
	
	public function findAllLevelsInApartment($projectId, $onlyActive = true){
		
		$query = "SELECT `module_apartment_level_id_pk`, `level_name`
		FROM `module_apartment` 
		WHERE 
		`module_project_id_pk` = ? AND `group_id_pk` = ?";
		
		if($onylActive){
			
			$query .= " AND `active` = 1";
		}
		
		$parameters = array($projectId,  $_SESSION['user']['group_id_pk']);

		if($levels = Database::dbResults($query, $parameters)){
				
			return $levels;
		}

		return false;
	}
	
	public function apartmentSaveStatus($projectId, $apartmentId){
		
		// check they own it
		if($checkOwnership = $this->findAllBuildingsAndApartments($projectId, '', '', $apartmentId, true)){
		
			$status = isset($_POST['db_status']) ? $_POST['db_status'] : 'sold';
			$dataTypes = 'si';
			$parameters = array($status, $apartmentId);
			$query = "UPDATE `module_apartment_unit` SET `status` = ? WHERE `module_apartment_unit_id_pk` = ?";
			
			Database::dbQuery($query, $dataTypes, $parameters);
			return true;
		} else {
			return false;
		}
		
	}
	
	
	public function findAllApartmentsAndSortByBuilding($projectId, $buildingId = ''){
		
		$sorted = array();
		
		if($buildings = $this->findAllBuildingsAndApartments($projectId, $buildingId)){
			
		//	print_r($buildings);die();
			
			foreach($buildings as $building){
				
				$sorted[$building['module_apartment_building_id_pk']]['building_name'] = $building['building_name'];
				
				$sorted[$building['module_apartment_building_id_pk']]['levels'][$building['module_apartment_level_id_pk']]['level_name'] = $building['level_name'];
				$sorted[$building['module_apartment_building_id_pk']]['levels'][$building['module_apartment_level_id_pk']]['apartments'][] = $building;
			}
			
		}
	//	print_r($sorted);die();
		return $sorted;
	}
	
	/* DEPRECATED AS WE'RE NOT USING COORDS, BUT IMAGE MAPS INSTEAD */
	/* BUT WE NEED TO WRITE SOMETHING TO UPDATE THE STATUS */
	public function levelPlanMarkerUpdate($projectId, $uid, $top, $left){
		
		$query = "SELECT AP.module_apartment_unit_id_pk FROM 
		`module_apartment_unit` AP, `module_apartment_level` AL, `module_project` P
		WHERE AP.uid = ?
		AND AP.module_apartment_level_id_pk = AL.module_apartment_level_id_pk
		AND AL.module_project_id_pk = P.module_project_id_pk
		AND P.group_id_pk = ?";
		
		$parameters = array($uid, $_SESSION['user']['group_id_pk']);
		
		if($checkOwner = Database::dbRow($query, $parameters)){
		
		$coords = '[' . $top . ',' . $left . ']';
		$query = "UPDATE `module_apartment_unit` SET `coords` = ?
		WHERE `module_apartment_unit_id_pk` = ?";
		$dataTypes = 'si';
		$parameters = array($coords, $checkOwner['module_apartment_unit_id_pk']);
		
		Database::dbQuery($query, $dataTypes, $parameters);
		
		}
	}

}