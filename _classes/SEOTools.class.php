<?php
class SEOTools {

	public function keywordsSaveSearch($searchTerms, $externalURL, $campaignId = '', $notes = '', $confirmed = 1){

		$searchTerms = json_decode(stripslashes($searchTerms), true);
		//	print_r( $searchTerms);
		$query = "INSERT INTO `module_keyword_save` VALUES(
		'', ?, ?, ?, ?,?, 1, NOW()		
		)";

		$parameters = array($_SESSION['user']['group_id_pk'], $campaignId, $externalURL, $notes, $confirmed);

		$dataTypes = 'iisss';

		if($newEntry = Database::dbInsert($query, $dataTypes, $parameters)){


			foreach($searchTerms as $key=>$value){

				$query = "INSERT INTO `module_keywords_keyword` VALUES(
				
				?, ?, ?, ?
				
				)";

				$parameters = array($newEntry, $_SESSION['user']['group_id_pk'], $key, $value);

				$dataTypes = 'iiss';
			 Database::dbInsert($query, $dataTypes, $parameters);
			}
		}

		return 1;

	}
	
	public function keywordsUpdateSearch($searchId){
	//	print_r($_POST);die();
		$active = isset($_POST['db_active']) ? $_POST['db_active'] : '';
		$campaignId = isset($_POST['db_module_campaign_id_pk']) ? $_POST['db_module_campaign_id_pk'] : '';
		$notes = isset($_POST['db_notes']) ? $_POST['db_notes'] : '';
		
		$query = "UPDATE `module_keyword_save` SET
		`confirmed` = 1
		,`module_campaign_id_pk` = ?
		,`notes` = ?
		,`active` = ?
		WHERE `group_id_pk` = ?
		AND `module_keyword_save_id_pk` = ?";
		
		$parameters = array($campaignId,$notes, $active, $_SESSION['user']['group_id_pk'], $searchId);
		$dataTypes = 'isiii';
		
		Database::dbQuery($query, $dataTypes, $parameters);
		
		
		
	}

	
	public function findPreviousSavedSearches($onlyUnconfirmed = false){


		$query = "SELECT `module_keyword_save_id_pk`, `website_url`
		FROM
		`module_keyword_save`
		WHERE `group_id_pk` = ?";

		$parameters = array($_SESSION['user']['group_id_pk']);

		if($onlyUnconfirmed){

			$query .= " AND `confirmed` != 1 ";

		}

		if($results = Database::dbResults($query, $parameters)){
				
			return $results;
		}

		return false;
	}

	public function findPreviousSavedSearchById($searchId){

		$query = "SELECT MCS.module_keyword_save_id_pk, MCS.module_campaign_id_pk, MCS.website_url, MCS.notes,
					MCS.confirmed, MCS.active
					, MCK.keyword, MCK.count
			
			FROM `module_keyword_save` MCS, `module_keywords_keyword` MCK
			
			WHERE MCK.module_keyword_save_id_pk = ?
			AND MCK.group_id_pk = ?
			AND MCK.module_keyword_save_id_pk = MCS.module_keyword_save_id_pk
		";
		$parameters = array($searchId, $_SESSION['user']['group_id_pk']);

		if($results = Database::dbResults($query, $parameters)){
				
			return $results;
		}

		return false;

	}
}