<?php
class ModuleLeadsListBuilder {
	
	public $tagLabels = array();
	
	public function __construct(){
		
		$this->tagLabels = array('suburb'=>'Suburb', 'tag'=>'Text');
		
	}
	
	public function subscriberUpdateTags(){
		
		print_r($_POST);
		
		$totalSubscribers = isset($_POST['totalSubscribers']) ? $_POST['totalSubscribers'] : 0;
		
		if($totalSubscribers > 0){
			
			
			
		}
	}
	
	
	public function findAllSubscriberTags($subscriberId){
		
		$query = "SELECT 
		ELM.module_email_list_member_id_pk, ELM.firstname, ELM.lastname, ELM.email_address
		,MLC.module_leads_list_categories_id_pk 
		FROM `module_email_list_member` ELM, `module_email_list_member_tags` MLC
		WHERE MLC.module_email_list_member_id_pk = ?
		AND ELM.module_email_list_member_id_pk = MLC.module_email_list_member_id_pk";
		
		$parameters = array($subscriberId);
		
		if($results = Database::dbResults($query, $parameters)){
			
			$cleanResults = self::flattenSubscriberAndTagList($subscriberId, $results);
			
			return $cleanResults;
		}
		
		
		return false;
	}
	
	private function flattenSubscriberAndTagList($subscriberId, $results){
		
		$subscriber = array();
		$subscriber[$results->{0}['module_email_list_member_id_pk']]['module_email_list_member_id_pk'] = $subscriberId;
		$subscriber[$results->{0}['module_email_list_member_id_pk']]['firstname'] = $results->{0}['firstname'];
		$subscriber[$results->{0}['module_email_list_member_id_pk']]['lastname'] = $results->{0}['lastname'];
		$subscriber[$results->{0}['module_email_list_member_id_pk']]['email_address'] = $results->{0}['email_address'];
		
		foreach($results as $result){
			
			
			$subscriber[$result['module_email_list_member_id_pk']]['module_leads_list_categories_id_pk'][] = $result['module_leads_list_categories_id_pk'];
		}
		
		return $subscriber;
	}
	
	public function addTagIdToSubscriber($subscriberId, $tagId){
		
		$query = "SELECT  `module_email_list_member_id_pk` FROM `module_email_list_member_tags` WHERE
		`module_email_list_member_id_pk` = ? AND `module_leads_list_categories_id_pk` = ?
		";
		
		$dataTypes = 'ii';
		
		$parameters = array($subscriberId, $tagId);
		
		// see if they're already tagged with this one
		if(!Database::dbRow($query, $parameters)){
			
			$query = "INSERT INTO `module_email_list_member_tags` VALUES(?, ?)";
			Database::dbQuery($query, $dataTypes, $parameters);
			return true;
		}	
		
		return false;	
	}
	
	
	public function convertTagIdToString($tagId){
		
		
		$query = "SELECT `tag`, `tag_type`, `foreign_id`  FROM
		`module_leads_list_categories` WHERE `module_leads_list_categories_id_pk` = ?";
		
		$parameters = array($tagId);
		
		if($tagData = Database::dbRow($query, $parameters)){
			
			switch($tagData['tag_type']){
				
				case 'suburb' :
					
					$tag = self::convertTagIdToSuburb($tagId, $tagData);
					
					break;
				
				
				default :
					
					$tag = self::convertTagIdToTag($tagId, $tagData);
					
					break;
				
			}
			
			return array('tag_id'=>$tagId, 'tag_type'=>$tagData['tag_type'], 'tag_string'=>$tag, 'tag_label'=>$this->tagLabels[$tagData['tag_type']], 'tag_db_id'=>$tagData['foreign_id']);
		}
		
		return false;
	}
	
	private function convertTagIdToTag($tagId, $tagData){
		
		$query = "SELECT `tag` FROM `module_leads_list_categories`
		WHERE `module_leads_list_categories_id_pk` = ?";
		
		$parameters = array($tagId);
		
		if($result = Database::dbRow($query, $parameters)){
			
			return $result['tag'];
		}
		
		return false;
	}
	
	
	private function convertTagIdToSuburb($tagId, $tagData){
		
		//$columnsSearch = array('suburb'=>'suburb', 'postcode'=>'postcode');
		
		$query = "SELECT `suburb` , `state` FROM `data_postcode` WHERE `data_postcode_id_pk` = ?";
		$parameters = array($tagData['foreign_id']);
		if($result = Database::dbRow($query, $parameters)){
			
			return $result['suburb'] . ' : ' . $result['state'];
		}
		
		return false;
	}
	
	
	public function findAllSuburbs($state = 'VIC'){
		
		
		$query = "SELECT `data_postcode_id_pk`, `suburb`, `state` FROM `data_postcode`
		WHERE `state` = ?
		ORDER BY `suburb`";
		
		$parameters = array($state);
		
		if($results = Database::dbResults($query, $parameters)){
			
			return $results;
		}
		
		return false;
	}
	
	
	
}

	

?>