<?php
class ModuleDashboard {

	private $availableModules;
	public $tasksToAction;

	

	public function findUserOutstandingTasks(){

		// somehow this has to figure out what the current user should see
		// each module will present it's results differently, so we rely on that module
		// to format the results -> but we want a consistent returned column count
		// no matter what i.e
		// Module | Task | Information | URL | Earliest Date
		
		self::defineModuleAndRoutinesToSearch();
		return $this->tasksToAction;
	}


	private function defineModuleAndRoutinesToSearch(){

		$this->availableModules = array();

		//sign register : signs to fix
		// job register : orders, schedules

		$this->availableModules['signregister'][] = array('ModuleSignRegisterRequests', 'findAllOutstandingSignChangeRequests', array(true));

		$this->availableModules['jobregister'][] = array('ModuleJobRegisterForms', 'findAllFormsToConfirm',  array(true));
		
		
		foreach($this->availableModules as $key=>$value){
			//print_r($key);
			
			foreach($this->availableModules[$key] as $task){
				
				// we might get an array back, so grab it first, then add it to the list
				
				if($tasks = call_user_func_array(array($task[0], $task[1]), array($task[2]))){
			//		print_r($tasks);
					foreach($tasks as $task){
						
						$this->tasksToAction[] = $task;
					}
					
				}
				
				
				
			}
			
			
		}
		//$data = array( array("firstname" => "Mary", "lastname" => "Johnson", "age" => 25), array("firstname" => "Amanda", "lastname" => "Miller", "age" => 18), array("firstname" => "James", "lastname" => "Brown", "age" => 31), array("firstname" => "Patricia", "lastname" => "Williams", "age" => 7), array("firstname" => "Michael", "lastname" => "Davis", "age" => 43), array("firstname" => "Sarah", "lastname" => "Miller", "age" => 24), array("firstname" => "Patrick", "lastname" => "Miller", "age" => 27) ); 

		//print_r($data);
		$this->tasksToAction = orderArrayByData($this->tasksToAction , 'date');
	//	print_r($this->tasksToAction);
	}

}