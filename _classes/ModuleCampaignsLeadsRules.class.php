<?php

class ModuleCampaignsLeadsRules extends  ModuleCampaigns {

	public function parseRules($emailer, $defaultFields) {
		//	print_r($defaultFields);die();

		if (isset($defaultFields['custom_action'])) {

			foreach ($defaultFields['custom_action'] as $key => $value) {

				//	print_r($key);die();
				switch($key) {

					case 'recipients' :
						self::setCustomRecipient($emailer, $value);

						break;
				}
			}

		}

	}

	private function setCustomRecipient($emailer, $rules) {

		// this rules looks for
		// field name, field value, override

		//print_r($rules);die();
		$recipientList = array();
		foreach ($rules as $rule) {
			//	print_r($rule);die();
			// did we find the field
			//echo ($_POST[$rule[0]]);die();
			if (isset($_POST[$rule[0]])) {

				foreach ($rule[1] as $recipient) {

					//print_r($recipient);die();
					if ($_POST[$rule[0]] == $recipient[0]) {
						//print_r($recipient);die();

						if (is_array($recipient[1])) {
								
							foreach ($recipient[1] as $address) {

								$recipientList[] = $address;
							}

						} else {

							$recipientList[] = $recipient[1];
						}
					}

				}

			}
		}
		// if we have some values, clear the original recipient and write these
		if (count($recipientList) > 0) {

			$emailer -> ClearAllRecipients();
		}
		foreach ($recipientList as $recipient) {

			$emailer -> addAddress($recipient, $recipient);
		}

		//print_r($emailer);die();
	}

}
?>