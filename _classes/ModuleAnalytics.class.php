<?php

class ModuleAnalytics {

	public $reportId;
	public $clientName;
	public $clientEmail;
	public $domainName;
	public $dateRange = array();

	public function findClientsToSendAnalyticsTo($runReport = true){
		
		/*$query = "SELECT MC.report_id, MC.website, MF.form_data FROM `module_campaign` MC, `module_campaign_form` MF
		WHERE MC.report_id != '' AND MC.module_campaign_form_id_pk = MF.module_campaign_form_id_pk";
		*/
		
		$query = "SELECT * FROM `module_analytics` WHERE `active` = 1 ORDER BY `website_name`";
		// die($query);
		$parameters = array();
		if($results = Database::dbResults($query, $parameters))
		{
			if($runReport)
			{
				return self::findTodayReport($results);	
			} 
			else 
			{
				return $results;	
			}
		}
		
		return false;
		
	}
	
	public function findSingleReport($reportId){
		
		$query = "SELECT * FROM `module_analytics` WHERE `active` = 1 AND `module_analytics_id_pk` = ?";
		$parameters = array($reportId);
		if($result = Database::dbRow($query, $parameters)){
			
			return $result;
		}
		return false;
	}
	
	public function saveAnalyticsSettings($reportId){
		
		$schema = new Schema();
		
		
		$table = 'module_analytics';
		$where = "`module_analytics_id_pk` = '" . (int)$reportId . "'";
			$returnURL = '/admin/analytics/analyticsedit/'  . (int)$reportId;
		
		$schema->updateDatabaseEntry($table, $where, '', $returnURL);
		
	}
	
	public function createNewAnalyticsSettings(){
		
		$schema = new Schema();
		$form = new ModuleCampaignsForm();
		
		$table = 'module_analytics';
		$postVars = array();
		$postVars['db_active'] = 1;
	
		$returnURL = '/admin/analytics/analyticsedit/{id}';
		
		$schema->createNewDatabaseEntry($table, $postVars, $returnURL);
	}

	
	private function findTodayReport($results){
		
		$reports = array();
		
		foreach($results as $result)
		{
			switch($result['schedule'])
			{	
				case 'week':
					// is it a Monday
					if(date('l', strtotime('today')) == 'Monday')
					{
						$reports[] = $result;						
					}				
				break;
				case 'month' :
					// is it the first of the month
					if(date('d', strtotime('today')) == '01')
					{
						$reports[] = $result;
					}				
				break;
				
			} 
			// testing function	
			/*		
			if($result['google_id'] == '1152945')
			{				
				$reports[] = $result;
			}
			*/
		
		}
		
		
		return $reports;
		
		
	}
	
	public function setDateRange($reportFrequency, $destination = 'google'){
		//2013-05-01', '2013-06-01'
		//$analyticsReport->setDates(date('Y-m-d', strtotime('-8 days')), date('Y-m-d', strtotime('-2 day')));
		$dates = array();
		switch($reportFrequency){
			
			
			case 'week':
			
				$dates[0] = date('Y-m-d', strtotime('-7 days'));
				$dates[1] = date('Y-m-d', strtotime('-1 day'));
				
			break;
			
			case 'month' :
				
				$dates[0] = date('Y-m-d',strtotime('first day of last month'));
				$dates[1] = date('Y-m-d',strtotime('last day of last month'));
			
			break;
		}
		
		return $dates;
		
	}
	
	public function sendReportViaEMail(){
		
		$name = explode(' ' , $this->clientName);
		$domainName = $this->domainName;
		$domainName = str_replace('http://', '', $domainName);
		
		$emailText = self::returnAnalyticsEmailFormat();
	//	$emailText .= ModuleCampaignsLeads::returnEmailHeader();
		
		
		
		//$emailText .= '[header]' . 'Your Weekly Google Analytics Report from Barking Bird' . '[/header]' . "\r\n";
		
		//$emailText .= '<p>Dear ' . $name[0] . ',<br />Please find attached your weekly Google Analytics Report for ' . $domainName . ' in PDF format.</p><br />' . "\r\n";
		
		
		//$emailText .= '<p style="font-size:11px;font-style: italic;">Did you know Barking Bird also runs Adwords campaigns which can get you onto the first page of Google?<br /> <a href="mailto:peck@barkingbird.com.au?subject=Tell me about Google Adwords" style="color:#000000">Ask us how...</a></p><br /><br />' . "\r\n";
		
		
		
		//$emailText .= '<br />' .  date('l jS \of F Y') . '<br />' . "\r\n";
	//	setDateRange($reportFrequency
		
		$emailText = str_replace('{name}', $name[0], $emailText);
		$emailText = str_replace('{domainName}', $domainName, $emailText);
	//	$emailText = str_replace('{dateFrom}',date('l jS \of F Y', strtotime('-8 days')) , $emailText);
	//	$emailText = str_replace('{dateTo}',date('l jS \of F Y', strtotime('-2 days')) , $emailText);
	
		$emailText = str_replace('{dateFrom}',date('l jS \of F Y', strtotime($this->dateRange[0])) , $emailText);
		$emailText = str_replace('{dateTo}',date('l jS \of F Y', strtotime($this->dateRange[1])) , $emailText);
		
		
		//$emailText .= ModuleCampaignsLeads::returnEmailFooter();
		
		$emailer = new SendEmail();
				
		$emailText = $emailer->formatEmail($emailText);
		$emailer->AddAddress($this->clientEmail, $this->clientName);		

		// set all the default details for the email
		$emailer->emailSubject 	= 'Your Google Analytics Report for ' . $domainName;
		$emailer->fromName 		= 'Barking Bird';
		$emailer->fromEmail 	= 'peck@barkingbird.com.au';
		$emailer->emailBody 	= $emailText;
		$emailer->replyTo 		= 'peck@barkingbird.com.au';
		
		// add the PDF
		$emailer->addAttachment( $_SERVER['DOCUMENT_ROOT'] .'/_crontasks/analytics/temp/' .$this->reportId . '.pdf', 'Analytics Report ' . date('Y-m-d', strtotime('now')) . '.pdf');
		
		$emailer->sendEmailOut();
		//@unlink($_SERVER['DOCUMENT_ROOT'] .'/_crontasks/analytics/temp/' .$this->reportId . '.pdf');
		//print_r(array($this->clientEmail, $this->clientName)); die;
		
	}

	private function  returnAnalyticsEmailFormat() {
		
$html = <<<EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: Arial, Helvetica, sans-serif; color: #000000;">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Your Message Subject or Title</title>
<!-- Targeting Windows Mobile -->
<!--[if IEMobile 7]>
	<style type="text/css">
	
	</style>
	<![endif]-->
<!-- ***********************************************
	****************************************************
	END MOBILE TARGETING
	****************************************************
	************************************************ -->
<!--[if gte mso 9]>
		<style>
		/* Target Outlook 2007 and 2010 */
		</style>
	<![endif]-->
  </head>
  <body style="font-family: Arial, Helvetica, sans-serif; color: #000000; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; padding: 0;"><style type="text/css">
h1 a:active { color: red !important; }
h2 a:active { color: red !important; }
h3 a:active { color: red !important; }
h4 a:active { color: red !important; }
h5 a:active { color: red !important; }
h6 a:active { color: red !important; }
h1 a:visited { color: purple !important; }
h2 a:visited { color: purple !important; }
h3 a:visited { color: purple !important; }
h4 a:visited { color: purple !important; }
h5 a:visited { color: purple !important; }
h6 a:visited { color: purple !important; }
&gt;</style> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tr style="font-family: Arial, Helvetica, sans-serif; color: #000000;"><td bgcolor="#eeeeee" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> 
    <table width="580" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tr style="font-family: Arial, Helvetica, sans-serif; color: #000000;"><td bgcolor="#FFFFFF" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tr style="font-family: Arial, Helvetica, sans-serif; color: #000000;"><td width="15" height="15" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> </td> 
        <td style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> </td> 
        <td width="15" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> </td> 
      </tr><tr style="font-family: Arial, Helvetica, sans-serif; color: #000000;"><td width="15" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> </td> 
        <td style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tr style="font-family: Arial, Helvetica, sans-serif; color: #000000;"><td bgcolor="#ffe23d" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"><span style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"><img src="http://www.leadmonitor.com.au/_crontasks/analytics/img/just-in.png" alt="Just In - Your Google Analytics Report" style="font-family: Arial, Helvetica, sans-serif; color: #000000; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block;" /></span></td> 
          </tr></table></td> 
        <td style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> </td> 
      </tr><tr style="font-family: Arial, Helvetica, sans-serif; color: #000000;"><td width="15" height="15" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> </td> 
        <td style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"><p style="font-family: Arial, Helvetica, sans-serif; color: #000000; margin: 1em 0;"><strong style="font-family: Arial, Helvetica, sans-serif; color: #000000;">Dear {name},</strong></p> 
          <p style="font-family: Arial, Helvetica, sans-serif; color: #000000; margin: 1em 0;">Please find attached your Google Analytics Report for your {domainName} website.</p> 
          <p style="font-family: Arial, Helvetica, sans-serif; color: #000000; margin: 1em 0;">This report is in PDF format and covers the period of {dateFrom} to {dateTo}.</p> 
          <p style="font-family: Arial, Helvetica, sans-serif; color: #000000; margin: 1em 0;">If you have any queries regarding this report, please do not hesitate to contact us.</p> 
          <p style="font-family: Arial, Helvetica, sans-serif; color: #000000; margin: 1em 0;">Regards,<br style="font-family: Arial, Helvetica, sans-serif; color: #000000;" /> 
            The Team at Barking Bird.<br style="font-family: Arial, Helvetica, sans-serif; color: #000000;" /></p></td> 
        <td style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> </td> 
      </tr><tr style="font-family: Arial, Helvetica, sans-serif; color: #000000;"><td height="15" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> </td> 
        <td style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> 
        <hr style="font-family: Arial, Helvetica, sans-serif; color: #000000;" /><table width="100%" cellpadding="0" cellspacing="0" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"><tr style="font-family: Arial, Helvetica, sans-serif; color: #000000;"><td valign="top" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"><a href="http://www.barkingbird.com.au" style="font-family: Arial, Helvetica, sans-serif; color: orange;"><img src="http://www.leadmonitor.com.au/fileserver/branding/barking-bird-logo.png" alt="Barking Bird" width="100" style="font-family: Arial, Helvetica, sans-serif; color: #000000; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; border: none;" /></a><br style="font-family: Arial, Helvetica, sans-serif; color: #000000;" /><span style="font-size: 11px; font-family: Arial, Helvetica, sans-serif; color: #000000;" trebuchet="" ms="" arial="" helvetica="" sans-serif=""> 
						<strong style="font-family: Arial, Helvetica, sans-serif; color: #000000;">Powered by Barking Bird</strong><br style="font-family: Arial, Helvetica, sans-serif; color: #000000;" /> 
						 Ground Floor, 10 Grattan St, Prahran, VIC 3181, Australia<br style="font-family: Arial, Helvetica, sans-serif; color: #000000;" /><b style="font-family: Arial, Helvetica, sans-serif; color: #000000;">E</b> <a href="mailto:peck@barkingbird.com.au" style="color: #000000; font-family: Arial, Helvetica, sans-serif;">peck@barkingbird.com.au</a>  <b style="font-family: Arial, Helvetica, sans-serif; color: #000000;">P</b> 1300 660 177<br style="font-family: Arial, Helvetica, sans-serif; color: #000000;" /><br style="font-family: Arial, Helvetica, sans-serif; color: #000000;" /><b style="font-family: Arial, Helvetica, sans-serif; color: #000000;"><a href="http://www.barkingbird.com.au" style="color: #000000; font-family: Arial, Helvetica, sans-serif;">Visit Us Online</a></b> 
					</span> 
					</td> 
                    </tr><tr style="font-family: Arial, Helvetica, sans-serif; color: #000000;">
                      <td valign="top" style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;">&nbsp;</td> 
          </tr></table></td> 
        <td style="font-family: Arial, Helvetica, sans-serif; color: #000000; border-collapse: collapse;"> </td> 
      </tr></table></td> 
  </tr></table></td> 
  </tr></table></body>
</html>
EOD;
		return $html;
		
	}
	
	

}

?>