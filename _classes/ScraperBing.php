<?php
class ScraperBing extends Scraper {
	
	private $appId = 'E0D9EC26D3A9F654EAF48C74E24BB6D5FC3805F2';
	public $serverString = 'http://api.bing.net/json.aspx?AppId=';
	
	
	private function constructBaseURL(){
		
		
		$url = $this->serverString . $this->appId . '&Version=2.2&Market=en-US';
		
		return $url;
	}
	
	public function webSearch($searchTerm){
		
		$url = $this->constructBaseURL();
		
		$blockedDomains = ScraperSEO::findBlockedDomains();
		
		$searchTerm .= $blockedDomains;
		
		$url .= '&Sources=web&Query=' . urlencode($searchTerm);
		
		//echo $url;
		$contents = $this->getURLContents($url, '', false);
		//echo $contents;
		$scrape = array();
		$c = 0;
		
		$results = json_decode($contents, true);
	
		if($results['SearchResponse']['Web']['Results']){
			
			foreach($results['SearchResponse']['Web']['Results'] as $result){
				
				$scrape[$c]['url'] = $result['Url'];
				$scrape[$c]['headline'] = $result['Title'];
				$scrape[$c]['description'] = $result['Description'];
				$c++;	
			}
		}
		$formatted = new ScraperSEO();
		$html = $formatted->returnFormatedResults($scrape);
		return $html;
		//return json_encode($scrape);
		//print_r($scrape);
	}
}