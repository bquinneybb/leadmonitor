<?php
class FileDelete {

	public $filePath;
	public $directoryPath;
	public $emptyOnly = false;

	public function deleteFile(){

		
		if($this->filePath == ''){
			
			return false;
		}
			
	if(!$check = $this->checkFileExists()){
			
			return false;
		}
	
		if(!$check =  $this->deleteFileFromDirectory()){
			
			return false;
		}
		
		return true;
		

	}
		
	
	private function deleteFileFromDirectory(){
		
		
		if(!unlink($this->filePath)){
			
			return false;
		}
		
		return true;
	}
	
	private function checkFileExists(){
		
		if(!file_exists($this->filePath)){
			
			return false;
		}
		
		return true;
	}

	public function deleteDirectory(){
		
		if($this->directoryPath == ''){
			
			return false;
		}
		
		if(!$check = $this->checkDirectoryIsNotTheRoot()){
			
			return false;
		}
		
	
		if(!$check == $this->checkDirectoryExists()){
			
			return false;
		}
		
		// all is good so delete it
		if($this->recursiveRemoveDirectory($this->directoryPath, $this->emptyOnly)){
			
			return true;
		}
		
		return false;

	}

	private function checkDirectoryIsNotTheRoot(){
			
		// we never want to actually delete the root path
			
			if($this->directoryPath == ROOT){
				
				return false;
			}
			
			return true;
			
	}

	
	private function checkDirectoryExists(){
		
		if(!is_dir($this->directoryPath)){
			
			return false;
		}
		
		return true;
		
	}

	private function recursiveRemoveDirectory($directory, $empty=FALSE){
		
	
	// ------------ lixlpixel recursive PHP functions -------------
	// recursive_remove_directory( directory to delete, empty )
	// expects path to directory and optional TRUE / FALSE to empty
	// of course PHP has to have the rights to delete the directory
	// you specify and all files and folders inside the directory
	// ------------------------------------------------------------

	// to use this function to totally remove a directory, write:
	// recursive_remove_directory('path/to/directory/to/delete');

	// to use this function to empty a directory, write:
	// recursive_remove_directory('path/to/full_directory',TRUE);
		
	
		// if the path has a slash at the end we remove it here
		if(substr($directory,-1) == '/')
		{
			$directory = substr($directory,0,-1);
		}

		// if the path is not valid or is not a directory ...
		if(!file_exists($directory) || !is_dir($directory))
		{
			// ... we return false and exit the function
			return FALSE;

			// ... if the path is not readable
		}elseif(!is_readable($directory))
		{
			// ... we return false and exit the function
			return FALSE;

			// ... else if the path is readable
		}else{

			// we open the directory
			$handle = opendir($directory);

			// and scan through the items inside
			while (FALSE !== ($item = readdir($handle)))
			{
				// if the filepointer is not the current directory
				// or the parent directory
				if($item != '.' && $item != '..')
				{
					// we build the new path to delete
					$path = $directory.'/'.$item;

					// if the new path is a directory
					if(is_dir($path))
					{
						// we call this function with the new path
						$this->recursiveRemoveDirectory($path);

						// if the new path is a file
					}else{
						// we remove the file
						unlink($path);
					}
				}
			}
			// close the directory
			closedir($handle);

			// if the option to empty is not set to true
			if($empty == FALSE)
			{
				// try to delete the now empty directory
				if(!rmdir($directory))
				{
					// return false if not possible
					return FALSE;
				}
			}
			// return success
			return TRUE;
		}
	}
}