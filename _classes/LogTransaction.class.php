<?php
class LogTransaction  {
	
/**
* Logs various Freeway transactions
*
* @author Nigel
* @version 1.0
* @since 1.0
* @access public
* @copyright Urban Angles
*
*/	
	
	public function logAction($module, $action, $itemName, $url){
		
		/**
		 * Writes an action to database for later showing
		 * Assumes user session
		 */
		
		$log = new Schema();
		$table = 'log_action';
		$postVars['db_user_id_pk'] = $_SESSION['user']['user_id_pk'];
		$postVars['db_module_identifier'] = $module;
		$postVars['db_action_identifier'] = $action;
		$postVars['db_item_identifier'] = $itemName;
		$postVars['db_action_url'] = $url;
		
		$log->createNewDatabaseEntry($table, $postVars, '');
	}
	
	public function logFileDownload($inputFile, $outputFile){
		
		/**
		 * Writes transaction to database
		 * Assumes user session
		 */
		
	}
	
	public function findActionHistory($limit = 10){
		
		$query = "SELECT `module_identifier`, `action_identifier`, `item_identifier`, `action_url`, `date_created`
		FROM `log_action`
		WHERE `user_id_pk` = ?
		ORDER BY `date_created` DESC
		LIMIT " . (int)$limit;
		
		$parameters = array($_SESSION['user']['user_id_pk']);
		
		$actionList = array();
		
		if($results = $this->dbResults($query, $parameters)){
			
			return $results;
		}
		
		return false;
	}
	
	public function translateActions($module, $action, $item_identifier){
		
		$outputString = '';
		
		$languageFile = ROOT . '/admin/language/log/' . $module . '.php';
		
		if(file_exists($languageFile)){
			
			include($languageFile);
		//	echo $action;
			$languageString = isset($language_string[$action][0]) ? $language_string[$action][0] : '';
			$outputString .=  $languageString;
			
			$outputString .= self::findModuleDetails($language_string[$action], $item_identifier);
		}

		
		
		echo $outputString;
	}
	
	private function findModuleDetails($language_string, $item_identifier){
		
		$item = ' : ';
		
	//	$foo = call_user_function($language_string[1] . '::' . $language_string[2]($item_identifier));
		
		$foo = new $language_string[1];
		
		if(is_array($item_identifier)){
			
			$bar = $foo->$language_string[2](implode("', '" . $item_identifier));
			
		} else {
			
			$bar = $foo->$language_string[2]($item_identifier);
		}
		
		
		
		foreach($language_string[3] as $string){
			
		
			$item .= $bar[$string];
		}
	//	print_r($bar);
		
		return $item;
	}
}