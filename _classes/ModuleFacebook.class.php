<?php

class ModuleFacebook {

	/* ADMIN ROUTINES */
	
	public function findAdminFacebookCampaigns($clientId){
		
		$query = "SELECT MCFB.module_campaign_facebook_id_pk
		,MC.campaign_name, MC.module_campaign_id_pk
		,CL.client_name
		
		FROM `module_campaign_facebook` MCFB,`module_campaign` MC, `client` CL
		
		WHERE MCFB.module_campaign_id_pk = MC.module_campaign_id_pk
		AND MC.client_id_pk = CL.client_id_pk
		  ";
		  
		 $parameters = array();
		 
		 if($clientId != ''){
		 	
			$parameters[] = $clientId;
			$query .= " AND MC.client_id_pk = ?";
			
		 } 
		 
		 if($campaigns = Database::dbResults($query, $parameters)){
		 	
			return $campaigns;
		 }
		 
		 return false;
	}
	
	public function findAdminFacebookCampaignByCampaignId($campaignId){
		
		$query = "SELECT * FROM `module_campaign_facebook` WHERE `module_campaign_id_pk` = ?";
		$parameters[] = $campaignId;
		
		 if($campaign = Database::dbRow($query, $parameters)){
		 	
			return $campaign;
		 }
		 
		 return false;
	}
	
	public function updateAdminFacebookCampaignByCampaignId($campaignId){
		
		
	}

	/* PUBLIC ROUTINES */

	public function findPublicFacebookCampaign($campaignShortcode) {

		$query = "SELECT MCFB.campaign_shortcode, MCFB.module_campaign_id_pk, MCFB.html, MCFB.image_gallery, MCFB.form_data, MCFB.form_message, MCFB.google_analytics_id
		,MC.module_campaign_form_id_pk
		FROM 
		
		`module_campaign_facebook` MCFB, `module_campaign` MC
		WHERE MCFB.campaign_shortcode = ?
		AND MCFB.module_campaign_id_pk = MC.module_campaign_id_pk
		
		";

		$parameters = array($campaignShortcode);

		if ($page = Database::dbRow($query, $parameters)) {

			return $page;
		}

		return false;
	}

	public function parseHTML($page) {
		// expects a database array for spliiting into components

		$html = $page['html'];
		if(strstr($html, '{{gallery}}')){
		
			$html = str_replace('{{gallery}}', $this -> showGalleryCode( $page), $html);
		
		}
		
		
		
		if(strstr($html, '{{form}}')){
		
			if(isset($_POST['sent'])){ 
		
				$html = str_replace('{{form}}', $this -> showSentMessage( $page), $html);
			
			} else { 
			
				$html = str_replace('{{form}}', $this -> showFormCode( $page), $html);
			
			}
			
		}

		


		return $html;

	}

	private function showGalleryCode($page) {

		$html = '';
		$html .= '<link rel="stylesheet" type="text/css" href="/public/javascript/uagallery/styles.css" />';
		$html .= '<script src="/public/javascript/uagallery/image-gallery.js" name="uaGallery"></script>';

		$html .= <<<EOD
					
						<style>
			#imageGallery {
				height: 533px;
				width: 520px;
			}
			
			</style>
			<script>
		
			$(window).load(function(){
							
				 $('#facebookgallery').uaGallery({
			      'outerContainer':'facebookgallery',
			      'innerContainer': 'thumbnail',
				  'innerContainerMargin' : 0,
				  'outerContainerVisibleWidth' : 730,
				  'showThumbnails' : false,
				 
				  'equalSizeContainers' : true,
				  'galleryType' : 'clicker',
				  'faderPause' : 2000,
				  'faderSpeed' : 500,
				  'arrowNavigationTop' : 230,
				  'arrowNavigationLeft' : 0,
				  'arrowNavigationRight' : 400,
				 'trackNavigation' : true
				 
		    	});  
						
				
			});
			</script>
EOD;

		$images = json_decode($page['image_gallery'], true);
		//print_r($page);
		$html .= '<div id="facebookgallery">';

		foreach ($images['images'] as $image) {

			$html .= '<div class="thumbnail">';

			$html .= '<img src="/fileserver/facebook/' . $page['module_campaign_id_pk'] . '/gallery/' . $image['image'] . '" />';

			$html .= '</div>';
		}

		$html .= '</div>';
		
		$html .= '<div class="clear"></div>';

		return $html;

	}

	 private function showFormCode($page){
	 	
		$html = '';
		
		$html .= '<form id="contactForm" name="contactForm" method="post" action="http://www.leadmonitor.com.au/_emailsave/saveemail.php">';
		
		$html .= '<input type="hidden" name="campaign" id="campaign" value="' . encrypt($page['module_campaign_id_pk']) . '" />
      	 		  <input type="hidden" name="form" id="form" value="' . encrypt($page['module_campaign_form_id_pk']) . '" />';
		
		$html .= $page['form_data'];
		
		$html .= '</form>';
		
		return $html;
	 }

	private function showSentMessage($page){
    	$html = '<div id="thankYouMessage">';
        $html .= $page['form_message'];
        $html .= '</div>';
        return $html;
    }
}
?>
