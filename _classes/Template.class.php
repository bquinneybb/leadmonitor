<?php
class Template  extends Database {

	protected $variables = array();
	protected $_controller;
	protected $_action;

	public $mode;
	public $pathString;
	public $return = false;
	public $alternativeTemplate;
	public $contentItem;

	public $template = array();

	// We are using an array with blanks or special files to supress the html containers for popups
	// This saves running heaps of helper functions and instead lets them be proper controllers
	public $blockToLoad = array('index', 'header-script', array('header', ''),  array('client-branding-bar', ''), array('main-menu', ''),  array('modulemenu', ''), array('wrapper-start','wrapper-start-no-navigation'), 'body',  array('wrapper-end', 'wrapper-end-no-navigation'), array('footer',''), 'footer-script');
	// array('shared', ''), array('submenu',''),
	/*
	 * public $blockToLoad = array('index', 'header-script', array('header', ''),  array('client-branding-bar', ''), array('main-menu', ''), array('shared', ''), array('page-options', ''),array('content-left-open', ''), array('modulemenu', ''), array('submenu',''), array('content-left-close', ''),  array('wrapper-start','wrapper-start-no-navigation'), 'body',  array('wrapper-end', 'wrapper-end-no-navigation'), array('footer',''), 'footer-script');
	 */

	function __construct($controller = '',$action = '', $mode = '') {

		$this->_controller = $controller;
		$this->_action = $action;
		$this->controller = $controller;
		/*
		 $modelName = 'Newbusiness' . 'Model';
		 $model = new $modelName;
		 //print_r('c = ' . $controller);
		 $submenu = $model->returnSubmenu();
		 //print_r($submenu);
		 $this->submenu = $submenu;
		 */
	}

	public function loadJavascript($filepath){

		$html = '';

		$realFilepath = SITE_PATH . $filepath;

		if(stristr('http', $filepath) || stristr('https', $filepath)){

			$realFilepath = $filepath;
		}

		$html .= '<script src="' . $realFilepath . '?v=' . VERSION_JAVASCRIPT . '" type="text/javascript"></script>' . "\r\n";
		echo $html;


	}

	public function concatJS($array){
		/*
		 $arrayPathed = array();
		 // ensure they're relative to the defined site path
		 foreach($array as $path){
		 	
			$arrayPathed[] = SITE_PATH . $path;
				
			}

			$booster = new Booster();


			$booster->js_hosted_minifier = TRUE;
			$booster->js_source = implode(',',$arrayPathed);
			echo $booster->js_markup();
			*/
	}

	public function concatCSS($array){

		$arrayPathed = array();
		// ensure they're relative to the defined site path
		foreach($array as $path){
				
			$arrayPathed[] = SITE_PATH . $path;
				
		}

		$booster = new Booster();
		$booster->css_hosted_minifier = TRUE;
		$booster->debug = TRUE;

		$booster->css_source = implode(',',$array);
		echo $booster->css_markup();
		//print_r($booster);
	}

	public function loadCSS($filepath){

		$html = '';

		//	$realFilepath = SITE_PATH . $filepath;
		if(SITE_PATH == 'http://localhost:8888/'){

			$realFilepath = $filepath;

		} else {

			$realFilepath = SITE_PATH . $filepath;

		}

		if(stristr('http', $filepath) || stristr('https', $filepath)){

			$realFilepath = $filepath;
		}

		//$html .= '<link rel="stylesheet" type="text/css" href="' . $realFilepath . '?v=' . VERSION_CSS . '">' . "\r\n";
		$html .= '<link rel="stylesheet" type="text/css" href="' . $realFilepath .  '">' . "\r\n";;
		echo $html;


	}

	function set($name,$value) {

		//echo 'name = ' . $name;
		$this->variables[$name] = $value;
		//print_r($this->name);
		//print_r( $this);die();
	}

	/** Display Template **/
	function setVariables(){

		extract($this->variables);
		$this->contentItem = $contentItem;

	}





	function render() {

		//	extract($this->variables);
		$template = '';

		//print_r($this);
		//	echo $alternativeTemplate;
		$this->loadBlocks();






		foreach($this->blockToLoad as $block){

			if(is_array($block)){
					
				if($this->variables['showTemplate']){

					$blockToLoad = $block[0];

				} else {

					$blockToLoad = $block[1];
				}
					
			} else {
					
				$blockToLoad = $block;
			}



			if(isset($this->template[$blockToLoad])){

				$template .= $this->template[$blockToLoad];
					
			}

		}
	
		// bugv($template);
		 eval('?>'.($template).'<?');

		 /*
		 *
		 */


	}

	function returnFilePath(){

		if(strstr($_SERVER['SCRIPT_FILENAME'], '/admin/index.php')){

			return '/admin';

		} else {

			return '';
		}
	}

	


	function loadBlocks(){

		$template = '';

		//	echo $this->pathString;die();
		if($this->mode == 'admin'){

			$this->pathString = 'admin/';
		}

		foreach($this->blockToLoad as $block){

			// 01 Feb 2011 - changing this to allow us to override some of the template blocks with non-menu item versions
			//	print_r($block);

			$this->variables['showTemplate'] = isset($this->variables['showTemplate']) ? $this->variables['showTemplate'] : true;

			if(is_array($block)){
					
				if($this->variables['showTemplate']){

					$blockToLoad = $block[0];

				} else {

					$blockToLoad = $block[1];
				}
					
			} else {
					
				$blockToLoad = $block;
			}

			if($blockToLoad == 'body'){
					
				$path = ROOT . '/' .  $this->pathString . 'application/views/' . $this->_controller . '/' . $this->_action  . '.php';
				//echo $path;
				if(file_exists($path)){

						
					$this->template[$blockToLoad] = file_get_contents($path);


				} else {

					//	$this->template[$blockToLoad] = file_get_contents(ROOT . '/' .  $this->pathString . 'application/views/error/error.php');


				}

			} else {


					


				// override individual components

				$viewOverride = ROOT . '/' . $this->pathString . '/application/views/' . $this->controller . '/views/' . $this->action . '/' . $blockToLoad . '.php';
				$controllerOverride = ROOT . '/' . $this->pathString . '/application/views/' . $this->controller . '/views/' . $blockToLoad . '.php';
				//	echo $viewOverride . '<br />';
					
				if(file_exists($viewOverride)){

					$this->template[$blockToLoad] = file_get_contents($viewOverride);
				}

				//echo $controllerOverride . '<br />';
				elseif(file_exists($controllerOverride)){

					$this->template[$blockToLoad] = file_get_contents($controllerOverride);
				}

				elseif(file_exists(ROOT . '/' .  $this->pathString . 'application/views/' . $blockToLoad . '.php')){

					$this->template[$blockToLoad] = file_get_contents(ROOT . '/' .  $this->pathString . 'application/views/' . $blockToLoad . '.php');


				}

			}

		}
	}


}

?>