<?php
class ScraperYahoo extends Scraper {

	public function findSiteLinksAsTSV($url){

		if($contents = file_get_contents("https://siteexplorer.search.yahoo.com/export?p=$url&bwm=i&fr=sfp")){
				
			$scrape = array();

			$i = 0;
			$contents = explode("\n", $contents);
			unset($contents[0]);
			unset($contents[1]);
			//	print_r($contents);die();
			foreach($contents as $results){

				$results = explode("\t", $results);
				//	print_r($results);die();
				if(isset($results['1'])){
					$scrape[$i]['title'] = $results['0'];
					$scrape[$i]['url'] = $results['1'];
					$i++;

				}
			}
			return $scrape;
			//print_r($scrape);
		}

	}
}