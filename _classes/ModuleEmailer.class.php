<?php
// select email template
// mailing list
// log outgoing
// parse through auto responders scripts

class ModuleEmailer extends ModuleCampaignsLeads {

	#####################
	### LISTS AND SORTING

	public function findAllEmailLists($groupId) {

		$query = "SELECT EL.module_email_list_id_pk, EL.email_list_label
		, COUNT(ES.module_email_list_member_id_pk) as subscribers
		FROM  module_email_list EL
		
		LEFT JOIN module_email_list_subscription ES
				ON  
		ES.subscribed = 1
		AND 	EL.module_email_list_id_pk = ES.module_email_list_id_pk
		WHERE EL.group_id_pk = ?";

		$parameters = array($groupId);
		//	Database::spe($query, $parameters);
		if ($results = Database::dbResults($query, $parameters)) {

			return $results;
		}

		return false;
	}

	public function findListName($listId, $groupId) {

		$query = "SELECT `email_list_label`
		FROM `module_email_list` WHERE 
		module_email_list_id_pk = ?
		AND group_id_pk = ?";

		$parameters = array(
			$listId,
			$groupId
		);

		if ($result = Database::dbRow($query, $parameters)) {

			return $result['email_list_label'];
		}

		return false;

	}

	###################
	### SUBSCRIBER MANAGEMENT

	public function findSubscribersInList($listId, $groupId) {

		$query = "SELECT ELM.module_email_list_member_id_pk, ELM.firstname, ELM.lastname, ELM.email_address
		, EL.email_list_label
		FROM `module_email_list_member` ELM, `module_email_list_subscription` ELS, `module_email_list` EL
		WHERE ELS.subscribed = 1
		AND EL.module_email_list_id_pk = ?
		AND EL.group_id_pk = ?
		AND ELS.module_email_list_id_pk = EL.module_email_list_id_pk
		AND ELS.module_email_list_member_id_pk = ELM.module_email_list_member_id_pk
		";

		$parameters = array(
			$listId,
			$groupId
		);

		if ($results = Database::dbResults($query, $parameters)) {

			return $results;
		}

		return false;

	}

	public function addBulkSubscribersToList($totalSubscribers, $listId) {

		// expects the format firstname, lastname, email
		$subscriberList = array();
		$totalAdded = 0;
		for ($i = 0; $i < $totalSubscribers; $i++) {

			$subscriber = isset($_POST['address_' . $i]) ? explode('|', $_POST['address_' . $i]) : null;
			//print_r($subscriber);die();
			if ($subscriber != null) {

				$added = self::checkAddNewSubscriberToList($subscriber[0], $subscriber[1], $subscriber[2], $listId);
				if (!is_array($added)) {

					$totalAdded++;
				}
			}
		}

		return $totalAdded;

	}

	public function checkAddNewSubscriberToList($firstName, $lastName, $emailAddress, $listId, $leadId = 0) {

		if ($firstName == '' || $lastName == '' || $emailAddress == '' || $listId == '') {

			return array(
				false,
				'emptyfield'
			);
		}

		// check for permanent no subscribe request
		if (self::checkEmailForNoSubscribeEver($emailAddress)) {

			return array(
				false,
				'nosubscribe'
			);
		}

		// have they previous subscribed with this email address

		$previousSubscriber = self::findSubscriberByEmailAddress($emailAddress);

		if ($previousSubscriber) {
			
			$subscriberId = $previousSubscriber['module_email_list_member_id_pk'];

			// update their status to be active
			self::updateSubscriberInformation($subscriberId, $firstName, $lastName, $emailAddress);
			
			self::resubscibeSubscriberToList($subscriberId, $listId);

		} else {
			// create a new subscriber id
			$subscriberId = self::addNewSubscriberToList($firstName, $lastName, $emailAddress, $listId, $leadId);
			// add into a particular list

			return self::addExistingSubscriberToList($subscriberId, $listId);
		}

	}

	private function updateSubscriberInformation($subscriberId, $firstName, $lastName, $emailAddress) {

		// update their name as this may have changed or may have been a typo
		// email will only change when coming from an individual edit page,
		// not a resibscribe attempt

		$query = "UPDATE `module_email_list_member` SET `firstname` = ?, `lastname` = ?, `email_address` = ?
		WHERE `module_email_list_member_id_pk` = ?";

		$dataTypes = 'sssi';
		
		$parameters = array($firstName, $lastName, $emailAddress, $subscriberId);
		
		Database::dbQuery($query, $dataTypes, $parameters);
	}

	private function resubscibeSubscriberToList($subscriberId, $listId){
		
		$query = "UPDATE `module_email_list_subscription` SET `subscribed` = 1 
		WHERE `module_email_list_member_id_pk` = ? AND `module_email_list_id_pk` = ?";
		
		$dataTypes = 'ii';
		
		$parameters = array($subscriberId, $listId);
		
		Database::dbQuery($query, $dataTypes, $parameters);
		
	}

	private function addNewSubscriberToList($firstName, $lastName, $emailAddress, $listId, $leadId = 0) {

		// add them as a general subscriber
		$query = "INSERT INTO `module_email_list_member` VALUES (
		
			'', ?, ?, ?, ?
		)";

		$dataTypes = 'isss';
		$parameters = array(
			$leadId,
			$firstName,
			$lastName,
			$emailAddress
		);

		$newSubscriberId = Database::dbInsert($query, $dataTypes, $parameters);
		return $newSubscriberId;
	}

	public function addExistingSubscriberToList($subscriberId, $listId, $alreadyCheckedNoSubscribe = false) {

		if (!$alreadyCheckedNoSubscribe) {
			// check for permanent no subscribe request
			if (self::checkEmailForNoSubscribeEver($subscriberId)) {

				return array(
					false,
					'nosubscribe'
				);
			}

		}

		$query = "INSERT INTO `module_email_list_subscription` VALUES(
			?, ?, 1, NOW(), ''
		)";

		$dataTypes = 'ii';

		$parameters = array(
			$subscriberId,
			$listId
		);

		Database::dbInsert($query, $dataTypes, $parameters);
		return true;
	}

	public function findSubscriberByEmailAddress($emailAddress) {

		$query = "SELECT * FROM `module_email_list_member` 
			WHERE `email_address` = ?";

		$parameters = array($emailAddress);

		if ($subscriber = Database::dbRow($query, $parameters)) {

			return $subscriber;
		}

		return false;

	}

	private function checkEmailForNoSubscribeEver($subscriberId = '', $emailAddress = '') {

		if ($subscriberId != '') {

			$query = "SELECT `date_requested`
			FROM `module_email_unsubscribe_always`
			WHERE `module_email_list_member_id_pk` = ?";

			$parameters = array($subscriberId);

		} else {

			$query = "SELECT `date_requested`
			FROM `module_email_unsubscribe_always`
			WHERE `email_address` = ?";

			$parameters = array($emailAddress);
		}

		if ($query = Database::dbRow($query, $parameters)) {

			return true;

		}

		return false;

	}

	public function removeBullkSubscribersFromList($listId, $groupId) {
		$i = 0;
		if ($subscribers = self::findSubscribersInList($listId, $groupId)) {

			foreach ($subscribers as $subscriber) {

				if (isset($_POST['db_subscribed_' . $subscriber['module_email_list_member_id_pk']])) {

					self::unsubscribeRecipient($subscriber['module_email_list_member_id_pk'], $listId, $subscriber['email_address']);
					$i++;
				}
			}

		}

		return $i;
	}

	public function unsubscribeRecipient($subscriberId, $listId, $emailAddress, $allLists = false) {

		$query = "UPDATE `module_email_list_subscription`
			SET `subscribed` = 0, `date_unsubscribed` = NOW()
			WHERE `module_email_list_member_id_pk` = ? ";

		$dataTypes = 'i';
		$parameters = array($subscriberId);

		if (!$allLists) {

			$query .= " AND `module_email_list_id_pk` = ?";
			$dataTypes .= 'i';
			$parameters[] = $listId;
		}

		Database::dbQuery($query, $dataTypes, $parameters);

		if ($allLists) {

			$query = "INSERT INTO `module_email_unsubscribe_always` VALUES (
				
				?, ?, NOW();
				)";

			$dataTypes = 'is';
			$parameters = array(
				$subscriberId,
				$emailAddress
			);
		}
	}

	public function cleanListOfDuplicates() {

	}

	###################
	### EBLAST MANAGEMENT

	public function addListToEmailQueue($campaignId, $emailTemplateId, $subscriberId) {

		$query = "INSERT INTO `module_email_queue` VALUES (
				
				'', ?, ?,  ?, 0, NOW()
			
			)";

		$dataTypes = 'iii';
		$parameters = array(
			$campaignId,
			$emailTemplateId,
			$subscriberId
		);

		Database::dbQuery($query, $dataTypes, $parameters);

	}

	public function getEmailForProcessing($emailCampaignId, $recipientId){
			
		$query = "SELECT
		
			ES.firstname, ES.lastname, ES.email_address
			,EM.html, EM.subject, EM.from_email, EM.from_name
			FROM `module_email_list_member` ES , `module_campaign_email` EM
			WHERE ES.module_email_list_member_id_pk = ?
			AND EM.module_campaign_email_id_pk = ?
			
		";
		
		$parameters = array($recipientId, $emailCampaignId);
		
		if($result = Database::dbRow($query, $parameters)){
			
			return $result;
		}
		
		return false;
		
	}
	
	
	public function processEmailQueue() {

		// find the next batch of unprocessed emails

	}

	private function addPrivacyAndOptOut() {

	}

}
?>