<?php
class ScraperSEO extends Scraper{
	
	public function addDomainToBlockedList($domain){
		
		$query = "INSERT INTO `module_blocked_domains` VALUES 
		(
		'', ?, ?, 1
		)";
		
		$dataTypes = 'is';
		$parameters = array($_SESSION['user']['group_id_pk'], $domain);
		
		Database::dbQuery($query, $dataTypes, $parameters);
	}
	
	public function findBlockedDomains($returnAsURLFormated = true){
		
		$query = "SELECT `domain` FROM `module_blocked_domains` WHERE `group_id_pk` = ?";
		$parameters = array($_SESSION['user']['group_id_pk']);
		
		if($results = Database::dbResults($query, $parameters)){
			
			if($returnAsURLFormated){
				
				$string = '';
				foreach($results as $result){
					
					$string .= ' -' . $result['domain'];
				}
				
				return $string;
				
			} else {
				
				return $results;
			}
		}
		
	}
	
	public function saveDomainAnalysis($results){
		
		$query = "INSERT INTO `module_site_analysis` VALUES (
		'', ?, ?, ?, 1, NOW()
		)";
		
		$dataTypes = 'iis';
		$parameters = array($_SESSION['user']['group_id_pk'], 0, json_encode($results));
		
		Database::dbQuery($query, $dataTypes, $parameters);
		
	}
	
public function returnFormatedResults($results){
		
	// find if any of these urls have been previously analysed
	$previouslyAnalysed = array();
	$parameters = array();
	
	$query = "SELECT `website_url` FROM `module_keyword_save` WHERE `website_url` IN(";
	$inQuery = '';
	foreach($results as $result){
		$parsed_url = $result['url']; 
		$parameters[] = $parsed_url;
		$inQuery .= "?,";
	}
	$inQuery = substr($inQuery, 0, -1);
	$query .= $inQuery . ") AND `group_id_pk` = ?";
	$parameters[] = $_SESSION['user']['group_id_pk'];
	//Database::spe($query, $parameters);
	if($domains = Database::dbResults($query, $parameters)){
		
		foreach($domains as $domain){
			
			$previouslyAnalysed[] = $domain['website_url'];
		}
		
	}
	
	
		$html = '';
			$html .= '<table width="100%"  class="formTable">';
			$html .= '<thead><tr>';
			
			$html .= '
						<th width="50%">Headline</th>
						<th>Domain</th>
						<th width="20%">Actions</th>';
			
			$html .= '</tr></thead>';
			
			$html .= '<tbody>';
			
			
		$i = 0;
		foreach($results as $result){
			$parsed_url = parse_url( $result['url']); 
			$description = str_replace('<br />', ' ', $result['description']);
			$html .= '<tr class="scrape_' . $i . '" id="result_' .  $i . '" href="' .  $parsed_url['host'] . '">';
				$html .= '<td><strong>' . sanitiseOutput($result['headline']) . '<strong></td>';
				$html .= '<td><a href="http://' . $parsed_url['host'] . '" target="_blank">' .  $parsed_url['host'] . '</a></td>';
				$html .= '<td>
						<a href="javascript:analysepage(' . $i . ')" title="Analyse Keywords">?</a>
						';
				
				if(in_array($result['url'], $previouslyAnalysed)){
					
					$html .= '(analyse again)';
				}
				
				$html .= '
						 | <a href="javascript:blockdomain(' . $i . ', \'' . $parsed_url['host']  . '\')" title="Block from Further searches">x</a>
						 | <a href="/admin/tools/siteanalyse/&q=' . $parsed_url['host'] . '">RANK</a>
						 | <span class="savedAnalysis" id="analyse_' . $i . '"></span>
						</td>';
			$html .= '</tr>';
			
			$html .= '<tr class="scrape_' . $i . '"><td colspan = "3">' . sanitiseOutput($result['description']). '</td></tr>';
			
		
			$i++;
		}
		
		$html .= '</tbody>';
		$html .= '</table>';
	
		$html .= '<script>$(".formTable tr:odd").addClass("alt");</script>';
		
		return $html;
	}
	
}