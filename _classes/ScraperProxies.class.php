<?php



class ScraperProxies extends Scraper {

	public function resetAllProxies(){
		
		// do this so we can test them each night
		$query = "UPDATE `proxy` SET `active` = 2, `latency` = 0";
		$parameters = array();
		$dataTypes = '';
		Database::dbQuery($query, $dataTypes, $parameters);
	}
	
	public function testProxies($onlyNew = false){

		$startTime = strtotime('now');

		$query = "SELECT `proxy_id_pk`, `address`, `port` FROM `proxy` WHERE ";
		if($onlyNew){

			$query .= " `active` = 2";
		} else {

			$query .= " `active` = 1";
		}

		$query .= " ORDER BY RAND() LIMIT 10";
		
		//echo $query;
		if($proxies = Database::dbResults($query, array())){

			foreach($proxies as $proxy){

				$contents = Scraper::getURLContents($proxy['address'] . ':' . $proxy['port']);

				if($contents == ''){

					$query = "UPDATE `proxy` SET `active` = 0, `last_checked` = NOW() WHERE `proxy_id_pk` = ?";
					$dataTypes = 'i';
					$parameters = array($proxy['proxy_id_pk']);

					Database::dbQuery($query, $dataTypes, $parameters);

				} else {

					$endTime = strtotime('now');
					$query = "UPDATE `proxy` SET `latency` = ?, `active` = 1, `last_checked` = NOW() WHERE `proxy_id_pk` = ?";
					$dataTypes = 'ii';
					$parameters = array($endTime - $startTime, $proxy['proxy_id_pk']);

					Database::dbQuery($query, $dataTypes, $parameters);
				}
				//echo $contents;
			}
		}

	}

	public function selectRandomProxy(){

		$query = "SELECT `proxy_id_pk`, `address`, `port` FROM `proxy` WHERE `active` = 1 AND `latency` < 100 ORDER BY RAND() LIMIT 1";
		if($proxy = Database::dbRow($query, array())){

			return array($proxy['address'], $proxy['port']);
		}

		return false;
	}

	public function loadFromText1Column($filename){



		$handle = fopen(ROOT . '/loadup/' . $filename, "r");
		$contents = fread($handle, filesize(ROOT . '/loadup/' . $filename));
		fclose($handle);

		//echo $contents;
		$proxies = explode("\n", $contents);
		//print_r($proxies);
		foreach($proxies as $proxy){

	
				
			$address = explode(':', $proxy);
				// see if it already exists
			$query = "SELECT `address` FROM `proxy` WHERE `address` = ?";
			$parameters = array($address[0]);
				
			if($checkExists = Database::dbRow($query, $parameters)){


			} else {
					
				//print_r($address);die();
				$query = "INSERT INTO `proxy` VALUES('', ?, ?, 0, 2, NOW())";
				$parameters =  array($address[0], $address[1]);
				$dataTypes = 'si';
				Database::dbInsert($query, $dataTypes, $parameters);
					
			}
		}
	}

}


