<?php
class Database  {

	public $connection;
	public $link;
	public $memcache;
	public $memcacheLife = 300;



	public $json = false;

	function __construct(){

		//	$this->memcacheConnect();
		$this->db = 'default';
		//print_r($this);
	}




	function memcacheConnect(){

		//	$this->memcache = new Memcache;
		//	$this->memcache->connect('127.0.0.1', 11211) or die ("Could not connect to Memcached server"); //connect to memcached server
	}


	public static function dbConnect($alternateSchema){

/*
		if($alternateSchema == 'default'){

			

		
		} else {
			
			
			
		}
*/
		if(TESTING_MODE){

			$db_login = 'bb_dev';
			$db_pass = 'bb_dev';
			$db_host = '192.168.70.45:8889';
			$db_schema = 'leadmoni_leads';

			} else {
				
			$db_host = 'localhost';	
			$db_schema = 'leadmoni_leads';		
			
			// Production credentials
			$db_pass = '4&si=,%hOArt';
			$db_login = 'leadmoni_leads';
			
			// Development credentials
			// $db_pass = 'root';
			// $db_login = 'root';
			
			if(strstr($_SERVER['DOCUMENT_ROOT'], 'httpdocs/testing')){
				
				$db_login = 'lmleadstest';
				$db_schema = 'leadmonitortesting';
			}
			
			
			}
			// bugv(array($db_host,$db_login,$db_pass,$db_schema), FALSE);
		if($link = new mysqli($db_host,$db_login,$db_pass,$db_schema)){

			//Logging::writeLogFile($link);
			return $link;

		} else {

			echo 'Error Connecting';
			die();
		}



	}



	public function spe($query, $parameters, $message = ''){



		self::showPreparedErrors($query, $parameters, $message);
	}

	public function showPreparedErrors($query, $parameters, $message = ''){

		for ($i=0; $i<count($parameters); $i++) {
			$query = preg_replace('/\?/',"'".$parameters[$i]."'",$query,1);
		}
		echo '<span style="color:#F00;background-color:#000;padding:5px;line-height:26px">' . $message . '</span><br /><br />';
		echo '<span style="color:#F00;background-color:#000;padding:5px;line-height:26px">' . $query . '</span>';
		die();
		//die('Please check your sql statement : unable to prepare:' .$query);
	}

	private static function dbPrepareAndFetch($query, $parameters, $alternateSchema ){

		//echo $query;die();
		//print_r($query) . '<hr />';
		$link = self::dbConnect($alternateSchema);

		if (!$stmt = mysqli_prepare($link, $query)) {
			mysqli_close($link);
			//	$foo = debug_backtrace();
			//	print_r($foo);
			if(!isset($_SESSION['testing'])){

				self::showPreparedErrors($query, $parameters);


			} else {

				return false;
			}
		}
		if(count($parameters) > 0){
			$types = str_repeat('s', count($parameters));
			if(!call_user_func_array('mysqli_stmt_bind_param', array_merge (array($stmt, $types), self::refValues($parameters)))){
				//	$foo = debug_backtrace();
				//	print_r($foo);

				self::spe($query, $parameters, '');
			}
		}

		//echo 'c = ' . count($parameters) . '<br />';


		//	array_unshift($parameters, $stmt);
		//	print_r($parameterVals);
		//echo phpversion();
		//	if(!call_user_func_array('mysqli_stmt_bind_param', ($parameters))){
			
		/*
		 print_r($parameters);
		 $foo = debug_backtrace();
		 print_r($foo);
		 */
		if(!mysqli_stmt_execute($stmt)){

			echo 'probs: ' . $query;

		}

		$result = mysqli_stmt_result_metadata($stmt);

		$fields = array();
		while ($field = mysqli_fetch_field($result)) {
			$name = $field->name;
			$fields[$name] = &$$name;
		}
		array_unshift($fields, $stmt);
		//print_r($fields);

		if(!call_user_func_array('mysqli_stmt_bind_result', $fields)){
			//vardump($fields);
			echo 'error on : ' . $query;die();
		}

		array_shift($fields);
		$results = array();
		while (mysqli_stmt_fetch($stmt)) {
			$temp = array();
			foreach($fields as $key => $val) {
				$temp[$key] = $val;

			}
			array_push($results, $temp);
		}

		mysqli_free_result($result);
		mysqli_stmt_close($stmt);
		mysqli_close($link);

		return $results;
	}

	static public function dbResults($query, $parameters, $alternateSchema = 'default'){
		// echo $query;

		$results = self::dbPrepareAndFetch($query, $parameters, $alternateSchema);

		// turn it into an object
		if (!empty($results)) {

			$data = new stdClass();

			foreach ($results as $akey => $aval)
			{
				$data -> {$akey} = $aval;
			}

			return $data;
		}

		return false;

		//return $results;


	}

	static public function dbRow($query, $parameters, $alternateSchema = 'default'){


		$results = self::dbPrepareAndFetch($query, $parameters, $alternateSchema);
		//print_r($results);
		// turn it into an object
		if (!empty($results)) {
			//echo $query . '<br />';
			//print_r($parameters);
			//print_r($results);
			/*
			$data = false;

			foreach ($results[0] as $akey => $aval) {
			$data -> {$akey} = $aval;
			}

			return $data;
			*/
			return $results[0];
		}

		return false;

	}

	private function db_stmt_bind_param($stmt, $dataTypes, $params){
		//echo $dataTypes;die();
		// split the options up into proper vars for inserting
		$opts[] = $stmt;
		$opts['type'] = $dataTypes;
		foreach($params as $val){


			$opts[] = $val;

		}

		return $opts;
	}

	static public function dbQuery($query, $dataTypes, $parameters, $alternateSchema = 'default'){

		$link = self::dbConnect($alternateSchema);

		if (!$stmt = mysqli_prepare($link, $query)) {
			mysqli_close($link);

			self::showPreparedErrors($query, $parameters);
			//die('Please check your sql statement : unable to prepare:' . $query . '<br />' . print_r($parameters));
		}

		//	$opts  = self::db_stmt_bind_param($stmt, $dataTypes, $parameters);
		//	print_r($opts);
		//if(!call_user_func_array('mysqli_stmt_bind_param', $opts)){
		if($dataTypes != ''){

			if(!call_user_func_array('mysqli_stmt_bind_param', array_merge (array($stmt, $dataTypes), self::refValues($parameters)))){

				self::spe($query, $parameters, 'could not bind statement');

				//echo 'could not bind statement';die();
			}
			mysqli_stmt_execute($stmt);
		} else {
			
			mysqli_stmt_execute($stmt);
		}




		mysqli_stmt_close($stmt);


	}

	public function dbInsert($query, $dataTypes, $parameters, $alternateSchema = 'default'){

		$link = self::dbConnect($alternateSchema);

		if (!$stmt = mysqli_prepare($link, $query)) {
			mysqli_close($link);
			self::showPreparedErrors($query, $parameters, 'Please check your sql statement : unable to prepare');
			//	die();
		}
		//$opts  = self::db_stmt_bind_param($stmt, $dataTypes, $parameters);

		if(!call_user_func_array('mysqli_stmt_bind_param', array_merge (array($stmt, $dataTypes), self::refValues($parameters)))){

			self::spe($query, $parameters, 'could not bind statement');

			//echo 'could not bind statement';die();
		}
		mysqli_stmt_execute($stmt);


		/*

		if(!call_user_func_array('mysqli_stmt_bind_param', $opts)){
		self::showPreparedErrors($query, $parameters, 'Could not bind statement');
		//echo ;die();
		}
		*/
		//	mysqli_stmt_execute($stmt);
		$insert_id = mysqli_stmt_insert_id($stmt);
		mysqli_stmt_close($stmt);

		return $insert_id;
		//	/	return ;

	}
	/**
	 * use memcache on objects that will be called fairly regularly, but not on every page
	 *
	 */
	public function memResults($query, $parameters, $memcacheKey){

		/**
		 * do a memcached query on a result set
		 */
		if(!$this->memcache){

			$this->memcacheConnect();
		}

		if($results = $this->memcache->get($memcacheKey)){

			return $results;

		} else {


			/**
			 * no results so fetch the results and write it to memory
			 */
			if($results = $this->dbResults($query, $parameters)){

				memcache_set($this->memcache, $memcacheKey, $results, MEMCACHE_COMPRESSED, $this->memcacheLife);
				return $results;
			}

			return false;
		}
	}

	public function memRow($query, $parameters, $memcacheKey){

		/**
		 * do a memcached query on a single row
		 */
		if(!$this->memcache){

			$this->memcacheConnect();
		}


		if($results = $this->memcache->get($memcacheKey)){
			//print_r($memcacheKey);
			return $results;

		} else {


			/**
			 * no results so fetch the row and write it to memory
			 */
			//echo 'foo: ' . $query . '<br />';
			if($results = $this->dbRow($query, $parameters)){

				memcache_set($this->memcache, $memcacheKey, $results, MEMCACHE_COMPRESSED, $this->memcacheLife);
				return $results;
			}

			return false;
		}
	}

	public function memDelete($memcacheKey){



		if(!$this->memcache){

			$this->memcacheConnect();
		}

		memcache_delete($this->memcache, $memcacheKey, 0);

	}

	/**
	 * use disk based cache on objects that will be called on every page
	 * such as menus etc
	 * this is my own basic disk caching mechanism
	 * we'll test whether this is slower or faster than using memcache for some things
	 * but it may be at risk of using all allocated memcache for lots of users
	 *
	 * also need to add a path to put this above the root on a production server
	 */

	public function diskResults($query, $parameters, $diskcacheKey){

		/**
		 * do a $diskcache query on a single row
		 */


		if($results = Database::diskQuery($query, $parameters, $diskcacheKey)){
			//print_r($results);
			return $results;

		} else {


			/**
			 * no results so fetch the row and write it to memory
			 */
			//	echo 'herte';
			if($results = Database::dbResults($query, $parameters)){

				Database::writeDiskCache($diskcacheKey, $results);
				return $results;
			}

			return false;
		}
	}

	public function diskRow($query,$parameters, $diskcacheKey){

		/**
		 * do a $diskcache query on a single row
		 */


		$cacheFile = ROOT . '/cache/' . md5($diskcacheKey) . '.php';

		//	echo $cacheFile;
		if(file_exists($cacheFile)){

			$cache = file_get_contents($cacheFile);
			return unserialize($cache);


		} else {


			/**
			 * no results so fetch the row and write it to memory
			 */

			if($results = Database::dbRow($query, $parameters)){

				Database::writeDiskCache($diskcacheKey, $results);
				return $results;
			}

			return false;
		}
	}

	public function diskQuery($query,$parameters,$diskcacheKey){

		$cacheFile = ROOT . '/cache/' . md5($diskcacheKey) . '.php';

		//	echo $cacheFile;
		if(file_exists($cacheFile)){

			$cache = file_get_contents($cacheFile);
			return unserialize($cache);

		} else {

			/**
			 * no results so fetch the results and write it to memory
			 */
			if($results = Database::dbResults($query, $parameters)){

				Database::writeDiskCache($diskcacheKey, $results);
				return $results;
			}

			return false;
		}
	}

	public function writeDiskCache($diskcacheKey, $results){
		/*
		 $cacheFile = ROOT . '/cache/' . md5($diskcacheKey) . '.php';
		 //echo $cacheFile;
		 $fp = fopen($cacheFile, 'w');
		 fwrite($fp, serialize($results));

		 fclose($fp);
		 */
	}

	public function clearDiskCache($diskcacheKey){

		$cacheFile = ROOT . '/cache/' . md5($diskcacheKey) . '.php';
		//	echo $cacheFile;
		@unlink($cacheFile);

	}

	/**
	 * misc helper functions
	 */


	static public function returnResults($results, $json){

		/**
		 * return json decoded data if requested
		 */

		if($json){

			return json_decode($results, true);

		} else {

			return $results;

		}
	}

	public function ownerQuery($alias = ''){

		if(!isset($_SESSION['user'])){
			// enables unit testing
			if(isset($_SESSION['testing'])){

				return "`foo` = 'bar'";
			}

			header('location: /');
			die();
		}

		if($alias != ''){

			$query = "
			{$alias}.group_parent_id_pk = '" . (int)$_SESSION['user']['group_parent_id_pk'] . "'
			 AND {$alias}.group_id_pk = '" . (int)$_SESSION['user']['group_id_pk'] . "'
			 ";
			/*

			*/
		} else {

			$query = "
			 `group_parent_id_pk` = '" . (int)$_SESSION['user']['group_parent_id_pk'] . "'
			 AND `group_id_pk` = '" . (int)$_SESSION['user']['group_id_pk'] . "'
			 ";
			/*

			*/
		}



		return $query;
	}

	public function groupQuery($alias = ''){

		if(!isset($_SESSION['user'])){
			// enables unit testing
			if(isset($_SESSION['testing'])){

				return "`foo` = 'bar'";
			}

			header('location: /');
			die();
		}

		if($alias != ''){

			$query = "
			{$alias}.group_id_pk = '" . (int)$_SESSION['user']['group_id_pk'] . "'
			
			";
		} else {

			$query = "
			`group_id_pk` = '" . (int)$_SESSION['user']['group_id_pk'] . "'
			
			";

		}
		return $query;
	}

	public function returnOwnerPostvars(){

		$postVars['db_group_parent_id_pk'] = (int)$_SESSION['user']['group_parent_id_pk'];
		$postVars['db_module_client_id_pk'] = (int)$_SESSION['user']['group_id_pk'];

		return $postVars;
	}

	public function convertFromArrayToKeyValue($array){
		/**
		 * Expects an array with only 2 values per pair
		 */
		$output = array();

		foreach($array as $value){

			$newArray = (array_values($value));
			$output[$newArray[0]] = $newArray[1];



		}

		return $output;
	}

	static function refValues($arr){

		if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
		{

			$refs = array();
			foreach($arr as $key => $value)
			$refs[$key] = &$arr[$key];

			return $refs;
		}
		return $arr;
	}

	static function escape_value($evalue)
	{
		$link = self::dbConnect(NULL);
		return $link->real_escape_string($evalue);
	}
}
?>