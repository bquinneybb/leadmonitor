<?php
class ModuleCampaigns {
	
	public $campaignTypes;
	
	function __construct(){
		
		$this->campaignTypes = array('Web'=>'', 'MiniWeb'=>'', 'Adwords'=>'', 'Email'=>'', 'Facebook'=>'/facebook/edit/');
		
	}
	
	private function returnCampaignSharedQuery($fullCampaign = false){
		
		$query = "SELECT 
		MP.module_project_id_pk, MP.project_name
		";
		if($fullCampaign){
			
			$query .= ",MC.module_campaign_id_pk, MC.campaign_name, MC.campaign_type, MC.website, MC.return_url, MC.analytics_id, MC.report_id, MC.module_campaign_form_id_pk, MC.active ";
			
		} else {
			
			$query .= ",MC.module_campaign_id_pk, MC.campaign_name";
		}
		$query .= "
		,CL.client_id_pk,CL.client_name, MC.lead_frequency, MC.lead_email
		FROM `module_project` MP, `module_campaign` MC, `client` CL
		WHERE 
		MP.group_id_pk = ? 
		AND MC.client_id_pk = CL.client_id_pk
		AND MP.module_project_id_pk = MC.module_project_id_pk
		AND MP.active = 1 AND MC.active = 1 ";
		
		return $query;
	}
	
	public function findActiveCampaigns($projectId = ''){
		
		$query = self::returnCampaignSharedQuery(false);
		
		if($projectId != ''){
			
			$query .= " AND MP.module_project_id_pk = ?";
		}
		
		$query .= " ORDER BY MP.project_name, MC.campaign_name
		";
		
		$parameters = array($_SESSION['user']['group_id_pk']);
		
		if($projectId != ''){
			
			$parameters[] = $projectId;
		}
		//Database::spe($query, $parameters);
		
		if($results = Database::dbResults($query, $parameters)){
			
			return $results;
		}
		
		return false;
	}
	
	public function campaignCreate($clientId, $projectId)
	{	
		//0413 838 849
	
		
		$schema = new Schema();
		$form = new ModuleCampaignsForm();
		
		$table = 'module_campaign_form';
		$postVars = array();

		// first create a blank form
		$postVars['db_group_id_pk'] = (int)$_SESSION['user']['group_id_pk'];
		$postVars['db_client_id_pk'] = $clientId;
		$postVars['db_form_data'] = $form->defaultForm;
		$returnURL = '';
		$formId = $schema->createNewDatabaseEntry($table, $postVars, $returnURL);
		
		// then create the campaign
		$table = 'module_campaign';
		$postVars = array();
		$postVars['db_module_project_id_pk'] = (int)$projectId;
		$postVars['db_client_id_pk'] = $clientId;
		$postVars['db_group_id_pk'] = (int)$_SESSION['user']['group_id_pk'];
		$postVars['db_module_campaign_form_id_pk'] = $formId;
		$postVars['db_active'] = 1;
		$returnURL = '/admin/campaigns/campaignedit/{id}';
		
		$schema->createNewDatabaseEntry($table, $postVars, $returnURL);
		
	}

	public function campaign_has_errors($campaign_id = FALSE)
	{
		if(!$campaign_id) return FALSE;

		$query 		= 'SELECT * from `log_email` WHERE campaign_id = '.(int)$campaign_id.' AND sent_by LIKE \'\' LIMIT 1';
		return ($results = Database::dbRow($query, $parameters))? $results : FALSE;		
	}
	
	public function campaignEdit($campaignId){
		/*
		$query = "SELECT * FROM `module_campaign`
		WHERE 
		`module_campaign_id_pk` = ? AND `group_id_pk` = ?";
		*/
			$query = self::returnCampaignSharedQuery(true);
			$query .= " AND MC.module_campaign_id_pk = ?";
		//	echo $query;
		$parameters = array($_SESSION['user']['group_id_pk'], $campaignId);
			//Database::spe($query, $parameters);
		if($result = Database::dbRow($query, $parameters)){
			
			return $result;
		}
		
		return false;
	}
	
	public function findCampaignAutoresponder($campaignId){
		
		$query = "SELECT * FROM `module_campaign_lead_autoresponder` WHERE `module_campaign_id_pk` = ?";
		$parameters = array($campaignId);

		if ($result = Database::dbRow($query, $parameters)) {
		
			return $result;
		}
		
		return false;
	}
	
	
	public function saveAutoresponder($campaignId){
		
		$auto_html = isset($_POST['auto_html']) ? $_POST['auto_html'] : '';
		
		$auto_html = str_replace('&nbsp;', ' ' , $auto_html);
		
		$auto_subject = isset($_POST['auto_subject']) ? $_POST['auto_subject'] : '';
		$auto_from_email = isset($_POST['auto_from_email']) ? $_POST['auto_from_email'] : '';
		$auto_from_name = isset($_POST['auto_from_name']) ? $_POST['auto_from_name'] : '';
		$query = "SELECT `module_campaign_id_pk` FROM `module_campaign_lead_autoresponder` WHERE `module_campaign_id_pk` = ?";
		$parameters = array($campaignId);
		
		if(Database::dbRow($query, $parameters)){
		
			$query = "UPDATE `module_campaign_lead_autoresponder` SET
			`html` = ?, `subject` = ?, `from_email` = ?, `from_name` = ?
			WHERE `module_campaign_id_pk` = ? 
			";
		$parameters= array(stripslashes($auto_html),$auto_subject,$auto_from_email,$auto_from_name, $campaignId );
		$dataTypes = 'ssssi';
		Database::dbQuery($query, $dataTypes, $parameters);

		
		} else {
			
			$query = "INSERT INTO `module_campaign_lead_autoresponder` VALUES (?, ?, ?, ?,?)";
		$parameters= array($campaignId, $auto_html,$auto_subject,$auto_from_email,$auto_from_name );
		$dataTypes = 'issss';
		Database::dbQuery($query, $dataTypes, $parameters);

		}	
		
				
		
		
		
	}
	public function campaignFindIdFromPost($campaignId){
		
		$query = "SELECT * FROM `module_campaign`
		WHERE 
		`module_campaign_id_pk` = ?";
		
		$parameters = array($campaignId);
		
		if($result = Database::dbRow($query, $parameters)){
			
			return $result;
		}
		
		return false;
	}
	
	public function campaignSave($campaignId){

		// check they have permission to edit it
		if(self::campaignEdit($campaignId)){
		
		$schema = new Schema();
		$table = 'module_campaign';
		$where = "`module_campaign_id_pk` = '" . (int)$campaignId . "'";
		
		
		$schema->updateDatabaseEntry($table, $where, '', '');
		
		
		}

		return false;
	}
	
	
	
}