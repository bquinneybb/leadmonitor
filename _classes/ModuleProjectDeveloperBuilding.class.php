<?php
class ModuleProjectDeveloperBuilding {
	
	public function findAllBuildingsAndApartments($projectId, $building = '', $level ='', $apartment = '', $imageMap = false,  $status = array()){
		
		// status can be individual or all
		//$projectId, 
		$parameters =  array($projectId, $_SESSION['user']['group_id_pk']);
		
		$query = "SELECT 
		
		BL.module_apartment_building_id_pk, BL.building_name, BL.uid
		,AL.module_apartment_level_id_pk , AL.level_name, AL.uid AS level_uid";
		
		if($imageMap){
			
			$query .= ", AL.imagemap";
		
		}
		$query .= " ,AP.module_apartment_unit_id_pk, AP.apartment_name, AP.status, AP.uid AS apartment_uid, AP.active ,P.module_project_id_pk
		
		FROM `module_apartment_building` AS BL

		INNER JOIN `module_project` AS P ON
		BL.module_project_id_pk = P.module_project_id_pk 
		
		LEFT JOIN  `module_apartment_level` AS AL ON 
		BL.module_apartment_building_id_pk = AL.module_apartment_building_id_pk
		
		
		LEFT JOIN `module_apartment_unit` AS AP ON
		AL.module_apartment_level_id_pk = AP.module_apartment_level_id_pk
		AND AP.active = 1
		
		
		
		WHERE BL.module_project_id_pk = ? AND BL.group_id_pk = ? 
		
		
		";
		
	if($building != ''){
			
			
			$query .= " AND BL.module_apartment_building_id_pk = ? ";
			$parameters[] = $building;
			
		}
		/*
		if($onlyActive){
			
			//$query .= "INNER JOIN ON AP.active = 1 AND "
		}
		*/
		if($level != ''){
			
			$query .= " AND AP.module_apartment_level_id_pk = ? AND  AL.module_apartment_level_id_pk = ?";
			$parameters[]= $level;
			$parameters[]= $level;
			
		} 
		
		if($apartment != ''){
			
			$query .= " AND AP.module_apartment_unit_id_pk = ?";
			$parameters[]= $apartment;
		}
		
		if(count($status) > 0){
			
			$statusFlags = '';
			foreach($status as $state){
				$parameters[]= $state;
				$statusFlags .= '?,';
			}
			$statusFlags = trim($statusFlags, ',');
			$query .= " AND AP.status IN(" . $statusFlags . ")";
			
			
			
		}
		
		
		$query .= " ORDER BY BL.ordering, AL.ordering, AP.apartment_name";
		
		//Database::spe($query, $parameters);
		if($levels = Database::dbResults($query, $parameters)){
				//print_r($levels);
			return $levels;
		}

		return false;
	}
	
	
	public function _findAllBuildings($projectId, $building = '', $level ='', $status = array()){
	
		// status can be individual or all
		
		$parameters =  array($projectId,  $_SESSION['user']['group_id_pk']);
		
		$query = "SELECT 
		
		BL.module_apartment_building_id_pk, BL.building_name, BL.uid
		
		,AP.module_apartment_unit_id_pk, AP.apartment_name, AP.status, AP.uid AS apartment_uid, AP.coords
		
		,AL.module_apartment_level_id_pk , AL.level_name, AL.uid AS level_uid
		
		
		
		FROM `module_apartment_building` BL, `module_apartment_unit` AP, `module_apartment_level` AL, `module_project` P

		WHERE
		P.module_project_id_pk  = ?
		AND BL.module_project_id_pk = P.module_project_id_pk 
		AND  BL.group_id_pk = ?
		
		
		AND BL.module_apartment_building_id_pk = AL.module_apartment_building_id_pk
		AND AL.module_apartment_level_id_pk = AP.module_apartment_level_id_pk
	
		AND AP.active = 1";
		
		
		if($building != ''){
			
			$query .= " AND BL.module_apartment_building_id_pk = ? ";
			$parameters[] = $building;
			
		}
		
		if($level != ''){
			
			$query .= " AND AP.module_apartment_level_id_pk = ? AND  AL.module_apartment_level_id_pk = ?";
			$parameters[]= $level;
			$parameters[]= $level;
			
		} 
		
		if(count($status) > 0){
			
			$statusFlags = '';
			foreach($status as $state){
				$parameters[]= $state;
				$statusFlags .= '?,';
			}
			$statusFlags = trim($statusFlags, ',');
			$query .= " AND AP.status IN(" . $statusFlags . ")";
			
			
			
		}
		
		
		$query .= " ORDER BY BL.ordering, AL.ordering, AP.apartment_name";
		
		//Database::spe($query, $parameters);
		if($levels = Database::dbResults($query, $parameters)){
				
			return $levels;
		}

		return false;
	}
	
	
	
}