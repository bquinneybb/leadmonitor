<?php
class ScraperGoogleAdwords extends Scraper {
	
	public function scrapeGoogleAdwords($searchTerm){
		
		
		$url = 'http://www.google.com.au/search?gcx=c&ie=UTF-8&q=' .urlencode($searchTerm);
	//	$url = 'http://www.urbanfreeway.com/company.php';
	//	echo $url;die();
		
		$contents = Scraper::getURLContents($url, '', false);
	//	print_r($contents);die();
	//	$contents = file_get_contents(ROOT . '/loadup/google-result.html');
		$topAds = self::findTopAds($contents);
		$sideAds = self::findSideAds($contents);
		
		return array($topAds, $sideAds);
	}
	
	
	private function findTopAds($contents){
		
		
		$html = new simple_html_dom();
		$html->load($contents);
		$i = 0;
		$ads = array();
		foreach($html->find('.vsta') as $result){
			
			
			
			$listing =  $result->innertext;
		//print_r($listing);
			$headline = getTextBetweenDelimiters($listing, '<h3>', '</h3>');
			
			$ads[$i]['headline'] = strip_tags($headline);
			$ads[$i]['url'] = strip_tags(getTextBetweenDelimiters($listing, '<cite>', '</cite>'));
			$ads[$i]['text'] = strip_tags(getTextBetweenDelimiters($listing, '<span class=ac>', '</span>'));
			$i++;
		}
		
		return($ads);
	}
	
	
	private function findSideAds($contents){
		
		$html = new simple_html_dom();
		$html->load($contents);
		$i = 0;
		$ads = array();
		foreach($html->find('.vsra') as $result){
			
			$listing =  $result->innertext;
			$headline = getTextBetweenDelimiters($listing, '<h3>', '</h3>');
			$ads[$i]['headline'] = strip_tags($headline);
			$ads[$i]['url'] = strip_tags(getTextBetweenDelimiters($listing, '<cite>', '</cite>'));
			$ads[$i]['text'] = strip_tags(getTextBetweenDelimiters($listing, '<span class=ac>', '</span>'));
			$i++;
		}
		
		return $ads;
	}
}

?>