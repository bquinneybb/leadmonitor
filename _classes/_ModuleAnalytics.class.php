<?php
include(ROOT . "/_utils/pChart/class/pData.class.php");
include(ROOT . "/_utils/pChart/class/pDraw.class.php");
include(ROOT . "/_utils/pChart/class/pImage.class.php");

class ModuleAnalytics {

	public function findNightlyAnalyticsReports(){

		$entryDate = date("Y-m-d", strtotime("yesterday"));
		
		
		$query = "SELECT
		MP.report_id AS project_report_id, MP.module_project_id_pk, MP.group_id_pk
		,MC.report_id AS report_id,  MC.module_campaign_id_pk
		FROM `module_project`MP, `module_campaign` MC
		WHERE MC.module_project_id_pk = MP.module_project_id_pk
		AND MP.active = 1 AND MC.active = 1
		";

		$parameters = array();
		$reportsToGather = array();
		if($results = Database::dbResults($query, $parameters)){

			foreach($results as $result){



				$data = self::getAnalytics($result['project_report_id']);

				$query = "INSERT INTO `module_analytics` VALUES (
 	'', ?, ?, ?, ?, ? , ?
 	
 	)";
				$dataTypes = 'iiisss';
				$parameters = array($result['group_id_pk'], $result['module_project_id_pk'], 0, json_encode($data['dimensions']), json_encode($data['metrics']), $entryDate );
				Database::dbInsert($query, $dataTypes, $parameters);
					
				// if the campaign data is different to the project, get it as well
				if($result['report_id'] != '' && $result['report_id'] != $result['project_report_id']){
						
					$data = self::getAnalytics($result['report_id']);

					$query = "INSERT INTO `module_analytics` VALUES (
 	'', ?, ?, ?, ?, ? , ?
 	
 	)";
					$dataTypes = 'iiisss';
					$parameters = array($result['group_id_pk'], $result['module_project_id_pk'], $result['report_id'], json_encode($data['dimensions']), json_encode($data['metrics']), $entryDate );
					Database::dbInsert($query, $dataTypes, $parameters);
						
				}

			}


		}
	}

	public function getAnalytics($reportId){
		//41022554

		//$ga = new gapi('nigel@urbanangles.com', 'urbananalytics');
		$ga = new gapi('social@barkingbird.com.au', 'b@rkingP@assworD');
		$dimensions = array('date',
	'country',
	'city',
	'source',
	'referralpath',
	'keyword',
	'pagepath'
	);

	$sorting = array(
	'-date',
	'country',
	'city',
	'source',
	'referralpath',
	'keyword',
	'pagepath'
	);

	$dateFrom = date("Y-m-d", strtotime("yesterday"));
	$dateTo = date("Y-m-d", strtotime("yesterday"));

	//$report_id, $dimensions, $metrics, $sort_metric=null, $filter=null, $start_date=null, $end_date=null, $start_index=1, $max_results=30
	$ga->requestReportData($reportId,$dimensions, array('pageviews', 'visits', 'newvisits', 'timeOnSite'), $sorting, null, $dateFrom, $dateTo, 1, 10000);



	$i = 0;




	$entryDate = $dateTo;
	$data = array();

	foreach($ga->results as $result){

		$resultArray = array();
		$data['dimensions'][] =   $result->dimensions;
		$data['metrics'][] =   $result->metrics;



	}

	return $data;
	}

	public function generateVisitorReport($campaignId, $dateStart = '', $dateEnd = ''){

		if($dateStart == ''){

			$dateStart = date('Y-m-d', strtotime('yesterday'));

		} else {

			$dateStart = date('Y-m-d', strtotime($dateStart));
		}

		if($dateEnd == ''){

			$dateEnd = date('Y-m-d', strtotime('today'));

		} else {

			$dateEnd = date('Y-m-d', strtotime($dateEnd));
		}

		$query = "SELECT `dimensions`, `metrics`
		FROM `module_analytics`
		WHERE `module_analytics_id_pk` = ?
		AND `group_id_pk` = ?
		AND `date` >= ? AND `date` <= ?";

		$parameters = array($campaignId, $_SESSION['user']['group_id_pk'], $dateStart, $dateEnd);

		if($data = Database::dbRow($query, $parameters)){

			$sortedData = self::analyseVisitorData($data);
			return $sortedData;
		}

		return false;

	}


	private function analyseVisitorData($visitorData){

		$dimensions = json_decode($visitorData['dimensions'], true);
		$metrics = json_decode($visitorData['metrics'], true);
		//print_r($metrics);
		$data = array();
		$sortedData = array();
		$i = 0;
		foreach($dimensions as $visitor){

			$thisDate = $visitor['date'];

			if(!isset($sortedData[$thisDate])){

				$sortedData[$thisDate] = array();
				$sortedData[$thisDate]['totalVisits'] = 0;
				$sortedData[$thisDate]['totalTime'] = 0;

			}
			if(!isset($sortedData[$thisDate]['country'][$visitor['country']])){
					
				$sortedData[$thisDate]['country'][$visitor['country']] = array();
			}
			if(!isset($sortedData[$thisDate]['country'][$visitor['country']][$visitor['city']] )){
					
					
				$sortedData[$thisDate]['country'][$visitor['country']][$visitor['city']]  = 0;
			}
			$sortedData[$thisDate]['country'][$visitor['country']][$visitor['city']] += 1;
			// = $visitor['country'];

			if(!isset($sortedData[$thisDate]['referralPath'])){
					
				$sortedData[$thisDate]['referralPath'] = array();
			}

			if(!isset($sortedData[$thisDate]['referralPath'][$visitor['referralPath']] )){
					
					
				$sortedData[$thisDate]['referralPath'][$visitor['referralPath']] = 0;
			}
			$sortedData[$thisDate]['referralPath'][$visitor['referralPath']] += 1;



			if(!isset($sortedData[$thisDate]['pagePath'])){
					
				$sortedData[$thisDate]['pagePath'] = array();
			}

			if(!isset($sortedData[$thisDate]['pagePath'][$visitor['pagePath']] )){
					
					
				$sortedData[$thisDate]['pagePath'][$visitor['pagePath']] = 0;
			}
			$sortedData[$thisDate]['pagePath'][$visitor['pagePath']] += 1;


			$sortedData[$thisDate]['totalVisits'] += 1;
			$sortedData[$thisDate]['totalTime'] += $metrics[$i]['timeOnSite'];
			//	echo $thisDate . '<br />';
			//	$data['dimensions'][$i] =$visitor;
			//	$data['metrics'][$i] = $metrics[$i];
			$i++;

		}
		$sortedData[$thisDate]['averageTime'] = $sortedData[$thisDate]['totalTime']  / $i;
		//print_r($sortedData);
		return $sortedData;
	}


	public function plotSiteVisits($data){

		$data = json_decode($data, true);

		/* Create and populate the pData object */
		$MyData = new pData();

		foreach($data as $result){

			$MyData->addPoints( $result['pageviews'],"New Visits");
			$MyData->addPoints( $result['newvisits'],"Total Visits");

		}


		$MyData->setSerieShape("New Visits",SERIE_SHAPE_FILLEDTRIANGLE);
		$MyData->setSerieShape("Total Visits",SERIE_SHAPE_FILLEDSQUARE);
		$MyData->setAxisName(0,"Visits");

		/* Create the pChart object */
		$myPicture = new pImage(1000,300,$MyData);

		/* Turn of Antialiasing */
		$myPicture->Antialias = FALSE;

		/* Add a border to the picture */
		$myPicture->drawRectangle(0,0,699,229,array("R"=>0,"G"=>0,"B"=>0));

		/* Write the chart title */
		$myPicture->setFontProperties(array("FontName"=>ROOT . "/_utils/pChart/fonts/Forgotte.ttf","FontSize"=>11));
		$myPicture->drawText(150,35,"Average temperature",array("FontSize"=>20,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));

		/* Set the default font */
		$myPicture->setFontProperties(array("FontName"=>ROOT . "/_utils/pChart/fonts/pf_arma_five.ttf","FontSize"=>6));

		/* Define the chart area */
		$myPicture->setGraphArea(60,40,650,200);

		/* Draw the scale */
		$scaleSettings = array("XMargin"=>10,"YMargin"=>5,"Floating"=>TRUE,"GridR"=>200,"GridG"=>200,"GridB"=>200,"DrawSubTicks"=>TRUE,"CycleBackground"=>TRUE);
		$myPicture->drawScale($scaleSettings);

		/* Turn on Antialiasing */
		$myPicture->Antialias = TRUE;
		$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));

		/* Draw the line chart */
		$myPicture->drawBarChart();

		/* Write the chart legend */
		$myPicture->drawLegend(580,20,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL));

		/* Render the picture (choose the best way) */
		$myPicture->Render(ROOT . '/fileserver/foo.png');

	}

}