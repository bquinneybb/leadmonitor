<?php
class ModuleCampaignsLeadExport extends ModuleCampaigns {

	public $recipient;
	

	public function findLeadRecordsForExport($formId, $startDate = '', $endDate = '', $sendEmail = false) {

		$query = "SELECT
			MCL.module_campaign_lead_id_pk, MCL.date_created, MCL.group_id_pk
			,MLD.lead_key, MLD.lead_value
			FROM `module_campaign_lead` MCL, `module_campaign_lead_data` MLD
			WHERE 
			MCL.module_campaign_lead_id_pk = MLD.module_campaign_lead_id_pk
			AND MCL.`active` = 1
			AND module_campaign_form_id_pk = ? ";
			
			$parameters = array($formId);
			
			
			if($startDate != '' && $endDate != ''){
				
				$query .= " AND MCL.date_created BETWEEN ? AND ?";
				
				$parameters[] = $startDate;
				$parameters[] = $endDate;
			}
			//print_r($query);
			
		
//GROUP BY MCL.module_campaign_lead_id_pk
		
	//	$parameters = array($formId, $startDate, $endDate);

		if ($results = Database::dbResults($query, $parameters)) {

			$cleanResult = self::cleanAndSortResults($formId, $results);
			//echo '---->'; print_r($cleanResult); die;
			//	return $results;3
			$output = fopen("php://output",'w') or die("Can't open php://output");
			$csv = self::convertLeadsToCSV($cleanResult, $formId, $results, $sendEmail, $output);
			//print_r($csv); die;
			fclose($output);
		}

		return false;

	}

	public function findLeadRecordsForView($formId, $startDate = '', $endDate = '', $sendEmail = false) 
	{
		
		$query = "SELECT
			MCL.module_campaign_lead_id_pk, MCL.date_created, MCL.group_id_pk
			,MLD.lead_key, MLD.lead_value
			FROM `module_campaign_lead` MCL, `module_campaign_lead_data` MLD
			WHERE 
			MCL.module_campaign_lead_id_pk = MLD.module_campaign_lead_id_pk
			AND MCL.`active` = 1
			AND module_campaign_form_id_pk = ? ";
			
			$parameters = array($formId);
			
			
			if($startDate != '' && $endDate != ''){
				
				$query .= " AND MCL.date_created BETWEEN ? AND ?";
				
				$parameters[] = $startDate;
				$parameters[] = $endDate;
			}
			$query .= " order by MCL.date_created";
			//print_r($query); die;
			
		
//GROUP BY MCL.module_campaign_lead_id_pk
		
	//	$parameters = array($formId, $startDate, $endDate);

		if ($results = Database::dbResults($query, $parameters)) {

			$cleanResult = self::cleanAndSortResults($formId, $results);
			//print_r($cleanResult); die;
			//	return $results;
			//$csv = self::convertLeadsToCSV($cleanResult, $formId, $results, $sendEmail);

			return $cleanResult;
		}

		return false;

	}

	public function findLeadRecordsFormClient($client_id, $startDate = '', $endDate = '', $sendEmail = false) 
	{
		
		$query = "SELECT
			MCL.module_campaign_lead_id_pk, MCL.date_created, MCL.group_id_pk
			,MLD.lead_key, MLD.lead_value
			FROM `module_campaign_lead` MCL, `module_campaign_lead_data` MLD
			WHERE 
			MCL.module_campaign_lead_id_pk = MLD.module_campaign_lead_id_pk
			AND MCL.`active` = 1
			AND module_campaign_form_id_pk = ? ";
			
			$parameters = array($formId);
			
			
			if($startDate != '' && $endDate != ''){
				
				$query .= " AND MCL.date_created BETWEEN ? AND ?";
				
				$parameters[] = $startDate;
				$parameters[] = $endDate;
			}
			//print_r($query); die;
			
		
//GROUP BY MCL.module_campaign_lead_id_pk
		
	//	$parameters = array($formId, $startDate, $endDate);

		if ($results = Database::dbResults($query, $parameters)) {

			$cleanResult = self::cleanAndSortResults($formId, $results);
			//print_r($cleanResult); die;
			//	return $results;
			//$csv = self::convertLeadsToCSV($cleanResult, $formId, $results, $sendEmail);

			return $cleanResult;
		}

		return false;

	}

	// public function deleteLeadRecords($formId = FALSE) 
	// {

	// 	if ($formId)
	// 	{
	// 		$parameters = array($formId);

	// 		$query = "DELETE FROM `module_campaign_lead_data` 
	// 		WHERE `module_campaign_lead_id_pk` 
	// 		In (select `module_campaign_lead_id_pk` 
	// 			FROM `module_campaign_lead` 
	// 			WHERE `module_campaign_form_id_pk` = ?)";

	// 		$results = Database::dbQuery($query, $parameters);


	// 		$query = "DELETE FROM `module_campaign_lead` 
	// 		WHERE `module_campaign_form_id_pk` = ?";

	// 		$results = Database::dbQuery($query, $parameters);


	// 		return $results;
	// 	}

	// }

	public function deactivateLeadRecords($formId = FALSE) 
	{
		if ($formId)
		{
			$parameters = array($formId);
			$dataTypes = 'i';

			$query = "UPDATE `module_campaign_lead` 
			SET `active`= 0 
			WHERE `module_campaign_form_id_pk` = ?";

			if ($results = Database::dbQuery($query, $dataTypes, $parameters))
			{
				return $results;
			}

			return FALSE;
		}

	}

	public function reactivateLeadRecords($formId = FALSE) 
	{
		if ($formId)
		{
			$parameters = array($formId);
			$dataTypes = 'i';

			$query = "UPDATE `module_campaign_lead` 
			SET `active`= 1 
			WHERE `module_campaign_form_id_pk` = ?";

			if ($results = Database::dbQuery($query, $dataTypes, $parameters))
			{
				return $results;
			}

			return FALSE;
		}

	}

	private function cleanAndSortResults($formId, $results) {

		
		$formFields = ModuleCampaignsLeads::findFormFields($formId, $results->{0}['group_id_pk']);

		$defaultFields = json_decode($formFields, true);
		$postFields = $defaultFields['data'];
		
		$leadData = array();
		
		// add the header row
		foreach($postFields as $postField)
		{
			$leadData[0][$postField[0]] = $postField[1];			
		}

		$leadData[0]['date_created'] = 'Date';
		//print_r($results); die;
		foreach($results as $result){
			//print_r($result); die;
		//	foreach($result as $key=>$value){
				
				//echo $key;
		//		if($key != 'module_campaign_lead_id_pk'){
						
					$leadData[$result['module_campaign_lead_id_pk']][$result['lead_key']] = addslashes($result['lead_value']);
					$leadData[$result['module_campaign_lead_id_pk']]['date_created'] = $result['date_created'];
					
			//	}
			//}
			
			
			//$leadData[$result['module_campaign_lead_id_pk']][$result['lead_key']] = $result['lead_value'];
		}
		//print_r($leadData);die();
		// find the intial sorting order
		//print_r($results);
		
		
		$orderArray = array();
		foreach($postFields as $postField){
				
				$orderArray[] = $postField[0];
			
		}
		
		$orderArray[] = 'date_created';
		
	//	print_r($orderArray);die();
		//$leadData = sortArrayByArray($leadData,$orderArray);
		$cleanData = array();
		foreach($leadData as $lead){
			
			$cleanData[] =  sortArrayByArray($lead,$orderArray);
		}
		//print_r($cleanData); die;
	
		return $cleanData;
			
	}

	private function convertLeadsToCSV($leadDataClean, $formId, $results, $sendEmail, $output){
		
		$csv = '';
		foreach($leadDataClean as $leads){
			
			//print_r($leads); die;

			foreach ($leads as $lkey => $lvalue)
			{
			 	$leads[$lkey] = preg_replace('/[^A-Za-z0-9 \/\\\\.:\-\@]/', '', $lvalue); 
			}

			if ((isset($leads['email']) && strpos($leads['email'], '@barkingbird.com') === FALSE) || !@$leads['email']) {
				$csv .= '"' . implode('","', $leads)  . '"' . "\n";
				if(!$sendEmail){
					//echo 'Line <br/>';
					fputcsv($output, $leads);
				}
			}
			
		}
		// print_r(array($formId, (int)$sendEmail, $output));
		// die($csv);
		if(!$sendEmail){
			
			//echo $csv;
			
		} else {
			
			self::emailLeadsToOwner($csv, $formId, $results);

			//print_r($defaultFields['recipient']);
			
			
		}
		
		
	}

	private function emailLeadsToOwner($csv, $formId, $results){
		
		$formFields = ModuleCampaignsLeads::findFormFields($formId, $results->{0}['group_id_pk']);
		$defaultFields = json_decode($formFields, true);
		//print_r($defaultFields);
		$email = new SendEmail();
		$email->emailSubject = 'Lead Export for : ' . $defaultFields['headers']['fromname'];
		$email->emailBody  = 'Please find attached your leads from ' . $defaultFields['headers']['fromname'] . ' in CSV format. <br /><br />';
		$email->AltBody = $email->emailBody;
		foreach( $defaultFields['recipient'] as $recipient){
			
			$email->AddAddress($recipient[0], $recipient[1]);

		}
		
		
		$csvSave = md5(microtime()) . '.csv';
		file_put_contents('/tmp/' . $csvSave, $csv);
		$email->addAttachment('/tmp/' . $csvSave,'leads.csv');
	//	$email->SMTPDebug  = 2;   
		$email->sendEmailOut();
	//	print_r($email);

	}

	public function findCampaignsWithCron(){

		$dataTypes = '';
		$parameters = array('off');

		$query = "SELECT * 
		FROM `module_campaign` 
		WHERE `lead_frequency` != ?";

		if ($results = Database::dbResults($query, $parameters))
		{
			return $results;
		}

		return "no results";
	}

	public function updateLastSentCampaign($campaign_id){

		$parameters = array($campaign_id);
		$dataTypes = 'i';

		$query = "UPDATE `module_campaign` 
		SET `lead_last_sent`= '".date("Y-m-d")."'
		WHERE `module_campaign_form_id_pk` = ?";

		if ($results = Database::dbQuery($query, $dataTypes, $parameters))
		{
			return $results;
		}

		return FALSE;
	}

}
