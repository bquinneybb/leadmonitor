<?php
class SEOWhois extends Scraper{

	public function findWhoisInformation($domain){

		$server = self::selectWhoisServer($domain);

		$domainInfo = file_get_contents($server[0]);
		$startDelimiter = $server[1];
		$endDelimiter = $server[2];
		$text = getTextBetweenDelimiters($domainInfo, $startDelimiter, $endDelimiter);
		//$whoisDate = str_replace('/', '-', $text);
		$whoisDate = date('Y-m-d', strtotime($text));
		return $whoisDate;
	}

	private function selectWhoisServer($domain){

		$domain = str_replace('http://', '', $domain);
		// url, start string, end string
		$defaultStart = "Record created on ";
		$defaultEnd = " UTC";

		$whoisServers = array();
		$whoisServers[] = array("http://centralops.net/co/DomainDossier.aspx?addr=$domain&go.x=3&go.y=9&_body=DomainDossier.aspx&dom_dns=1&dom_whois=1&net_whois=1");
		$whoisServers[] = array("http://www.ratite.com/whois/whois.cgi?domain=$domain");
		$whoisServers[] = array("http://reports.internic.net/cgi/whois?whois_nic=$domain&type=domain", "Creation Date: ", "\n");

		$rand_keys = array_rand($whoisServers);

		$selectedServer = $whoisServers[$rand_keys];
		if(!isset($selectedServer[1])){
				
			$selectedServer[1] = $defaultStart;
		}

		if(!isset($selectedServer[2])){
				
			$selectedServer[2] = $defaultEnd;
		}


		return $selectedServer;
	}


}

?>