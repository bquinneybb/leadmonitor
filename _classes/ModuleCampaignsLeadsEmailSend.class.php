<?php
class ModuleCampaignsLeadsEmailSend extends ModuleCampaignsLeads {

	public $leadId;
	public $leadSource = array();
	
	
	public function __construct(){
		
		
		$this->leadSource['lead'] = 'module_campaign_lead_autoresponder_tracking';
		$this->leadSource['eblast'] = 'module_campaign_email_tracking';
		
	}

	public function checkForAutoResponder($campaignId, $formData, $leadId) {

		$query = "SELECT * FROM `module_campaign_lead_autoresponder` WHERE `module_campaign_id_pk` = ? AND `subject` != '' AND `from_email` != ''";
		$parameters = array($campaignId);

		if ($result = Database::dbRow($query, $parameters)) {
			//print_r($result); die;
			$tracker = new ModuleCampaignsLeadsEmailSend();
			
			$tracker->addTrackingEntry($leadId);
			$tracker->constructEmailMessage($result, $formData, $leadId);

		}

		return false;

	}
	
	public function createEblastTracking($emailQueueId, $emailCampaignId, $recipientId){
		
		$tracker = new ModuleCampaignsLeadsEmailSend();
		
		if($emailDetails = ModuleEmailer::getEmailForProcessing($emailCampaignId, $recipientId)){
		
		$tracker->addTrackingEntry($emailQueueId, 'eblast');
		$tracker->constructEmailMessage($result, $formData, $leadId);
		
		$formData = self::convertEmailDataToFormData($emailDetails);
		
		$tracker->constructEmailMessage($result, $formData, $recipientId);
		
		} else {
			
			// something went wrong
		}
	}

	private function convertEmailDataToFormData($emailDetails){
		
		$formData = array();
		$formData['db_email'] = $emailDetails['email_address'];
		$formData['db_firstname'] = $emailDetails['firstname'];
		$formData['db_lastname'] = $emailDetails['lastname'];
		
	}

	private function constructEmailMessage($result, $formData, $leadId) {

		

		$messageBody = $result['html'];
		// do we want to personalise it
		if (strstr($messageBody, '{firstname}')) {

			$messageBody = str_replace('{firstname}', self::emailBodyParseRecipientName($formData), $messageBody);

		}
		$messageBody = str_replace('&nbsp;', ' ', $messageBody);
		// add the tracking pixel
		$messageBody .= $this->addTackingPixel($leadId);

		$messageBody = $this->addTrackingLinks($leadId, $messageBody);

		$email = new SendEmail();
		$email->emailSubject = $result['subject'];

		$email->AddAddress($formData['db_email'], self::parseRecipientToName($formData));

		//$email->emailNameTo =  self::parseRecipientToName($formData);
		//$email->emailEmailTo = $formData['db_email'];
		$email->replyTo = $result['from_email'];
		$email->fromEmail = $result['from_email'];
		$email->fromName = $result['from_name'];
		$email->emailBody = $messageBody;
		//print_r($email);die();
		$email->sendEmailOut();
	}

	private function parseRecipientToName($formData) {

		$name = '';
		if (isset($_POST['db_name'])) {

			return $formData['db_name'];
		}

		if (isset($formData['db_firstname'])) {

			$name = $formData['db_firstname'];

		}

		if (isset($formData['db_lastname'])) {

			$name .= ' ' . $formData['db_lastname'];

		}

		return $name;
	}

	private function emailBodyParseRecipientName($formData) {

		$name = '';

		// try for firstname

		if (isset($formData['db_firstname'])) {

			$name = $formData['db_firstname'];
			return $name;
		}

		if (isset($formData['db_name'])) {

			// split it up
			$name = explode(' ', $formData['db_name']);
			$name = $name[0];

			return $name;
		}

		return false;
	}

	private function addTackingPixel($leadId, $source='lead') {

		return ' <img src="http://www.leadmonitor.com.au/_emailsave/getimage.php?eid=' . encrypt($leadId) . '&source=' . $source . '">';
	}

	private function addTrackingLinks($leadId, $string) {
		
		$this->leadId = $leadId;
		$newString = preg_replace_callback('/href=[\'\"](.*?)[\'\"]/', array(&$this, 'encryptLinkMatches') , $string);
		//$newString = preg_replace_callback('/<a href=[\'\"](.*?)[\'\"]>(.*?)<\/a>/', array(&$this, 'encryptLinkMatches') , $string);
		//echo $string;die();

		return $newString;
	}

	private function encryptLinkMatches($matches, $source='lead') {

		$uniqueURL = 'http://www.leadmonitor.com.au/_emailsave/followlink.php?link=';
		
		if(strpos($matches[1], 'tel:') === FALSE || strpos($matches[1], 'tel:') > 3) {
			return 'href="' . $uniqueURL . encrypt('leadId=' . $this->leadId . '&link=' . $matches[1]) . '&source=' . $source .'"';
		} else {
			return 'href="' . $matches[1] .'"';
		}
		//return 'href="' . $uniqueURL . encrypt('leadId=' . $this->leadId . '&link=' . $matches[1]) . '' . $matches[2] . '</a>';
		
	}

	public function trackEmailOpen($leadId, $source) {

		
		// default is the form lead
		$table = isset($this->leadSource[$source]) ? $this->leadSource[$source] : 'module_campaign_lead_autoresponder_tracking';

		$parameters = array($leadId);
		$dataTypes = 'i';
		$query = "SELECT `opened` FROM `" . $table . "` WHERE `module_campaign_lead_id_pk` = ? ";

		if ($result = Database::dbRow($query, $parameters)) {

			if ($result['opened'] == 0) {

				$parameters = array($_SERVER['REMOTE_ADDR'], $leadId);
				$dataTypes = 'si';

				$query = "UPDATE `" . $table . "` SET `opened` = 1, `date_opened` = NOW(), `ip_address` = ? WHERE `module_campaign_lead_id_pk` = ?";
				//	Database::spe($query, $parameters);
				Database::dbQuery($query, $dataTypes, $parameters);
			}

		}
	}

	private function addTrackingEntry($leadId) {

		$parameters = array($leadId);
		$dataTypes = 'i';
		
		// default is the form lead
		$table = isset($this->leadSource[$source]) ? $this->leadSource[$source] : 'module_campaign_lead_autoresponder_tracking';
//		$table = 'module_campaign_lead_autoresponder_tracking';	

		$query = "INSERT INTO `" . $table . "`  VALUES(?, 0, NOW(), '', '')";
		Database::dbQuery($query, $dataTypes, $parameters);
	}

	public function followEmailLink($campaignId, $link, $source = 'lead') {
			
			self::saveClickedLink($campaignId, $link, $source);
			header('location: ' . $link);
			die();
	}

	private function saveClickedLink($campaignId, $link, $source){
		
		$tracker = new ModuleCampaignsLeadsEmailSend();
		// default is the form lead
		$table = isset($tracker->leadSource[$source]) ? $tracker->leadSource[$source] : 'module_campaign_lead_autoresponder_tracking';

		
		$query = "INSERT INTO  `" . $table . "` VALUES (
			 ?, ?, NOW(), NOW(), ?
		)";
		
		$parameters = array($campaignId, $link, $_SERVER['REMOTE_ADDR']);
		$dataTypes = 'sis';
		Database::dbQuery($query, $dataTypes, $parameters);
		
	}
}
