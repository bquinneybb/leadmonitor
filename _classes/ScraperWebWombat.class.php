<?php
class ScraperWebWombat extends Scraper {

	function getWombatPage($searchTerm){
	/*
		if(strstr($_SERVER['DOCUMENT_ROOT'], '/UA SERVER')){
			
			$ip = '119.17.57.86';
		} else {
			
			$ip = '117.58.255.146';
		}
		*/
		$proxy = Scraper::findProxy();
		$ip = $proxy[0];
		echo $ip;
		$riString = self::m($searchTerm, $ip);
	
	$url = 'http://s.webwombat.com.au/aus?ix=' . str_replace(' ' , '+', $searchTerm) . '&ri=' . $riString;
	//echo $url;die();
		$contents = Scraper::getURLContents($url, '', false,  $proxy);
		$html = new simple_html_dom();
		


		$html->load($contents);
		//$html->load_file();

		$scrape = array();
		$c = 0;
		foreach($html->find('td.textLrg a') as $result){

			$scrape[$c]['url'] = $result->href;
			$c++;
		}
		$i = 0;
		$c = 0;
		foreach($html->find('td.textLrg') as $result){

			if($i % 2 == 0){
					
				$scrape[$c]['headline'] = strip_tags($result->plaintext);
					
			} else {

				$scrape[$c]['description'] = strip_tags($result->plaintext);
					$c++;
			}
			
			$i++;
				
		}

		return($scrape);

	}
	
	function scrapeSearch($searchTerm){
		
		$results = $this->getWombatPage($searchTerm);
		$formatted = new ScraperSEO();
		$html = $formatted->returnFormatedResults($results);
		return $html;
	}
	
	// these replicate the web wombat javascript security

	function s($t){
	
		$il = 0;
		$v2 = 0;

		for($il = 0; $il < strlen($t); $il++){
		
			$v2 += ord(substr($t, $il, 1));
			$v2 %=  255;
		}
		return $v2 ;
		
	}
	
	function m($searchTerm, $ip){

		$r= self::s($ip . $searchTerm);
		return $r;
	}
}