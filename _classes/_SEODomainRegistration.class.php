<?php
include(ROOT . '/_utils/Netregistry/console.class.php');
include(ROOT . '/_utils/Netregistry/client.class.php');

class SEODomainRegistration {
	
	private $login = 'URBAAE-API';
	private $pass = 'bHpur8FvRdqPSYuU';
	
	private function createConnection(){
		
		$location = 'https://theconsole.netregistry.com.au/external/services/ResellerAPIService/';
		$wsdl = $location.'?wsdl';
		$username = $this->login;
		$password = $this->pass;
		
		$client = new Client($location, $wsdl, $username, $password);
		
		return $client;
	}
	
	public function checkDomainAvailability($domain){
		
		$client = $this->createConnection();
		
		//print('========== consoleMethod[domainLookup] ==========<br/>');
		$client->set('domain', $domain );
		$client->domainLookup();
		//$client->screen($client->response());
		//$client->unset('domain');
		//print_r($client->response());
		
		if(isset($client->response->return->errors)){
			
			$result =  array($client->response->return->errors->errorMsg, false);
			
		} else {
		
			$result = array($client->response->return->fields->entries[0]->value, true);
		
		}
	//	print_r($result);
		unset($client);
		return $result;
		//echo $result;
	}
	
	
	public function performWhois($domain){
		
		$client = $this->createConnection();
		$client->set('domain', $domain);
		$client->domainInfo();
		
		if(isset($client->response->return->errors)){
			
			$result =  array($client->response->return->errors->errorMsg, false);
			
		} else {
		
			$result = array($client->response->return->fields->entries, true);
		
		}
		unset($client);
		//print_r($result);
		return $result;
	}
	
	public function purchaseDomain($domain){
		
		$requiredFields = array(
			'firstName','lastName'
			,'address1', 'suburb', 'postcode', 'state'
			,'phone','email', 'organisation'
		);
		// main fields
		$contactDetails['firstName'] = isset($_POST['db_reg_firstname']) ? $_POST['db_reg_firstname'] : null;
		$contactDetails['lastName'] = isset($_POST['db_reg_lastname']) ? $_POST['db_reg_lastname'] : null;
		$contactDetails['address1'] = isset($_POST['db_reg_address1']) ? $_POST['db_reg_address1'] : null;
		$contactDetails['address2'] = isset($_POST['db_reg_address2']) ? $_POST['db_reg_address2'] : null;
		$contactDetails['suburb'] = isset($_POST['db_reg_suburb']) ? $_POST['db_reg_suburb'] : null;
		$contactDetails['state'] = isset($_POST['db_reg_state']) ? $_POST['db_reg_state'] : null;
		$contactDetails['postcode'] = isset($_POST['db_reg_postcode']) ? $_POST['db_reg_postcode'] : null;
	
		
		$contactDetails['phone'] = isset($_POST['db_reg_phone']) ? $_POST['db_reg_phone'] : null;
		$contactDetails['email'] = isset($_POST['db_reg_email']) ? $_POST['db_reg_email'] : null;
		
		$contactDetails['organisation'] = isset($_POST['db_reg_organisation']) ? $_POST['db_reg_organisation'] : null;
		
		$registration['period'] = isset($_POST['db_reg_period']) ? $_POST['db_reg_period'] : 2;
		
		$isAustralian = false;
		
		$domainComponents = explode('.', $domain);
		if($domainComponents[count($domainComponents)-1] == 'au'){
			
			$isAustralian = true;
			
			$businessDetails['companyName'] = isset($_POST['db_reg_companyname']) ? $_POST['db_reg_companyname'] : null;
			$businessDetails['companyType'] = isset($_POST['db_reg_companytype']) ? $_POST['db_reg_companytype'] : null;
			$businessDetails['eligibilityType'] = isset($_POST['db_reg_eligibilitytype']) ? $_POST['db_reg_eligibilitytype'] : null;
			$businessDetails['eligibilityNumber'] = isset($_POST['db_reg_eligibilitynumber']) ? $_POST['db_reg_eligibilitynumber'] : null;
			
			
			$requiredFields[] = array(
			
				'companyName', 'companyType', 'eligibilityType', 'eligibilityNumber'
			
			);
		}
		
		// check for the primary required fields
		foreach($requiredFields as $requiredField){
			
			
			$testField = isset($_POST['db_reg_' . $requiredField]) ? $_POST['db_reg_' . $requiredField] : null;
			
			if($testField == null){
				//unset($client);
				//return array('All required fields must be filled out', false);
			}
		}
		
		$client = $this->createConnection();
		$client->set('domain', $domain);
		$client->set('contactDetails', 
		
			array('firstName' =>$contactDetails['firstName'], 'lastName'=>$contactDetails['lastName']
			,'address1' =>$contactDetails['address1'],'address2' =>$contactDetails['address2'],
			'suburb' =>$contactDetails['suburb'],  'state' =>$contactDetails['state'], 'postcode' =>$contactDetails['postcode'],'country'=>'AU'   
			,'phone'=>$contactDetails['phone'],'email'=>$contactDetails['email']
			,'organisation'=>$contactDetails['organisation'] 
			)
		);
		
		if($isAustralian){
			
			$client->set('eligibility', array(
				array('key' => 'au.registrant.name', 'value' => $businessDetails['companyName'] )
				,array('key' => 'au.registrantid.type', 'value' => 	$businessDetails['companyType'])
				,array('key' => 'au.registrant.number', 'value' => $businessDetails['eligibilityNumber'])
				,array('key' => 'au.org.type', 'value' => $businessDetails['eligibilityType'])));
		} else {
			
				$client->set('eligibility', array());
		} 
		
		$client->set('period', $registration['period']);
		$client->set('nameServers', array('ns1.netregistry.net', 'ns2.netregistry.net', 'ns3.netregistry.net'));
		$client->registerDomain();
		$client->screen($client->response());
		unset($client);
		
	}
	
	private function checkAustralianRegistrationFields(){
		
		// au must 
	}
}
?>