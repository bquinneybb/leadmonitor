<?php
class FileUpload {



	public $realFilename;
	public $tempDir;
	public $zipUploadedFilePath;
	public $zipUploadFileName;

	public $uploadDir;
	public $uploadName;
	public $completionScript;
	public $uploadFieldId;
	public $uploadType = 'image';

	private $error;

	private $mimesImage = array(
	array('jpg','image/jpeg')
	,array('jpg','image/jpeg')
	,array('jpg','image/pjpeg')
	,array('jpg','image/pjpeg')
	,array('gif','image/gif')
	,array('png','image/png')
	,array('bmp','image/bmp')

	);

	private $mimesGeneral = array(
	array('txt'=>'text/plain')
	,array('doc','application/msword')
	,array('pdf','application/pdf')
	,array('xls','application/x-excel')
	,array('xls','application/excel')
	,array('xls','application/vnd.ms-excel')
	,array('rtf','application/rtf')
	,array('zip','application/zip')


	);

	private $mimesZip = array(

		array('zip','application/zip')
		);

		private $suffixReject = array('php', 'asp', 'pl', 'exe', 'bat', 'bin');

		function __construct(){


		}

		function showErrorMessage(){

			echo $this->error;
		}

		function uploadFile(){

			$this->mimesGeneral = array_merge($this->mimesImage,$this->mimesGeneral );

			$success = true;

			if(!$success = $this->checkForEmptyUpload()){

				$this->showErrorMessage();
				$success = false;
			}

			if(!$success = $this->checkMimeType()){

				$this->showErrorMessage();
				$success = false;
			}

			if($success){
					
				//echo 'readyt to xcytop;';die();

				if($success = $this->copyFile()){

					return true;
					//echo $this->completionScript;

				} else {

					echo 'no copy';
				}



					
					





			}	 else {

				echo 'probs';
				$_SESSION['form']['error'] =  'There were problems uploading the file.';
				$_SESSION['form']['error'] =  'You didn\'t upload a file or are not allowed to upload that type of file sorry.';
			}




		}

	 function checkForEmptyUpload(){

	 	if(!isset($_FILES[$this->uploadFieldId]) || empty($_FILES[$this->uploadFieldId]) || $_FILES[$this->uploadFieldId]['error'] != 0){

	 		$this->error = 'EMPTY_FILE';
	 			
	 		return false;
	 	} else {

	 		return true;
	 	}

	 }

	 function checkMimeType(){


	 	$allowedMimes = $this->mimesGeneral;

	 	if($this->uploadType == 'image'){

	 		$allowedMimes = $this->mimesImage;


	 	}

	 	if($this->uploadType == 'zip'){

				$allowedMimes = $this->mimesZip;

	 	}

	 	foreach($allowedMimes as $allowedMime){

	 		if($_FILES[$this->uploadFieldId]['type'] == $allowedMime[1]){

	 			return true;
	 		}
	 	}
	 	// for now, return true
	 	return true;

	 	if(in_array(substr($_FILES[$this->uploadFieldId]['type'], -3), $this->suffixReject)){

	 		return false;
	 			
	 	}
	 	///	print_r( $_FILES[$this->uploadFieldId] );die();
	 	return false;


	 }

	 function copyFile(){

	 	 

	 	//echo  $this->uploadDir . $this->uploadName;
	 	// echo $_FILES[$this->uploadFieldId]['tmp_name'];
			if(!move_uploaded_file($_FILES[$this->uploadFieldId]['tmp_name'], $this->uploadDir . $this->uploadName)){

				return false;

			} else {

			
				
				return true;
			}




	 }


	 function unpackZip($path, $zipName){

	 	$archive = new PclZip($path . '/' . $zipName);
	 	if (($v_result_list = $archive->extract(PCLZIP_OPT_PATH, $path)) == 0) {
	 		die("Error : ".$archive->errorInfo(true));
	  }

	  //  $files = new Helper();
	  $fileList = directoryToArray($path . '/', true);
	  $this->processZip($fileList);

	  delete_directory($path);
	 }

	 function processZip($fileList){

	 	$allowedSuffix = array('jpg', 'jpeg', 'png', 'gif', 'bmp');

	 	$goodFiles = array();
	 	foreach($fileList as $file){
	 		$add = true;
	 			
	 		if(strstr($file, '__MACOSX/') || strstr($file, '.zip')){

	 			$add = false;
	 		}
	 			
	 		if(!in_array(substr($file, -3) ,$allowedSuffix) && !in_array(substr($file, -4) ,$allowedSuffix)){

	 			$add = false;

	 		}
	 			

	 			
	 	}


	 }

	 public function checkForExistingUploadPath($path){
		$path = self::cleanUploadPath($path);
	 	//echo $path . "<br />";
	 	if(!file_exists($path)){

	 		mkdir($path);
	 		chmod($path, 0777);
	 	}

	 }

	 public function cleanUploadPath($path){
	 	
	 	//$badChars = array('.', '&');
	 	preg_replace("/\&/", "", $path);
	 //	echo $path;
	 return $path;
	 }
	 
	 public function checkForExistingDropboxFolder($module){
	 	
	 	self::checkForExistingUploadPath('../dropbox/'  . $_SESSION['user']['user_id_pk']);
		self::checkForExistingUploadPath('../dropbox/'  . $_SESSION['user']['user_id_pk'] . '/' . $module);
	 }
	 
	 public function sanitiseUploadName(){


	 }
	 
	 public function moveFilesFromDropboxToProject($module,  $uid, $jobId){
	 	
	 	smartCopy(ROOT . '/dropbox/' .  $_SESSION['user']['user_id_pk'] . '/' . $module . '/' , ROOT . '/fileserver/module-' . $module . '/' . $uid . '/' . $jobId . '/');
	 
	 	
	 	return true;
	 }
}