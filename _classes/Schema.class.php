<?php
class Schema  {

	private $insertId;
	public $schema;
	public $alternateDb = false;

	function __construct() {



		/** this class is used to keep track of the database tables and columns and
		 * allows quick creation and update of the forms.
		 * Columns can have an additional flag to set options to only reference it on events
		 * such as INSERT or UPDATE

		 * insert -> db entry creation
		 * insert_now -> add a time stamp on db entry creation
		 * update_now -> alter the time stamp on db update
		 * user_id -> the current user id
		 *
		 *
		 * the first column is the data type expected by mysqli prepared statements
		 * 
		 * 
		 * all schmea outlines are stored in the schema directory
		 */
	

	}

	function setSchema($table){

		if(file_exists(ROOT . '/schema/' . $table . '.php')){
			
			include(ROOT . '/schema/' . $table . '.php');
			return $schema;
		}
				/*
				 case 'module_photography_profile':
					$schema = array(
					array('module_photography_profile_id_pk', 'insert')
					,'module_photography_profile_name'
					,'active'
					);
					break;
					*/

		die('Could not load schema');
		
		if(!isset($schema)){
			
			die('Schema found but not defined');
		}
	}

	function createNewDatabaseEntry($table, $postVars = '', $returnURL = ''){

		global $_SESSION;

		$schema = $this->setSchema($table);

		$query = "INSERT INTO `" . Database::escape_value($table) . "` VALUES (";
		$dataTypes = '';
		$parameters = array();
			
		foreach($schema as $entry){

			$specialCase = isset($entry[2]) ? $entry[2] : null;
			// treat those items with special cases


			switch ($specialCase) {
				case 'insert':
					$query .= "''," ;
					//	$dataTypes .= 'i';
					break;

				case 'insert_now':
					$query .= "NOW(),";
					break;

				case 'uid':
					$query .= "'" . md5(microtime()) . "',";

					break;


				case 'user_id':

					$entryValue = isset($_SESSION['userId']) ? $_SESSION['userId'] : '0';
					$dataTypes .= 'i';
					$parameters[] = $entryValue;
					$query .= "?, ";
					break;

				case 'value':

					$entryValue = isset($entry[3]) ? $entry[3] : '';
					echo $entryValue;
					$dataTypes .= 's';
					$parameters[] = $entryValue;
					$query .= "?, ";
					break;
					
				case 'compress':

					// have we posted vars with the create

				
					$entryValue = isset($postVars['db_'. $entry[1]]) ? $postVars['db_'. $entry[1]] : ' ';
					$dataTypes .= 's';
					$parameters[] = $entryValue;
					$query .= "COMPRESS(?),";


					break;
					

				default:

					// have we posted vars with the create

					//echo $entry[1] . '|' . $postVars['db_'. $entry[1]] . '<br />';
					$entryValue = isset($postVars['db_'. $entry[1]]) ? $postVars['db_'. $entry[1]] : ' ';
					
					// clean up ? which confuses mysqli
					$entryValue = str_replace('?', urlencode('?'), $entryValue);
					
					$dataTypes .= 's';
					$parameters[] = $entryValue;
					$query .= "?,";


					break;



			}

		}
			
		// trim final comma
		$query = substr($query, 0 , -1);

		$query .= ")";
		
		//Database::spe($query, $parameters);
		//print_r($parameters);	die();
		//	print_r($query);die();
			
		if($this->alternateDb != false){

				$this->insertId = Database::dbInsert($query, $dataTypes, $parameters, $this->alternateDb);
			
		} else {
			
				$this->insertId = Database::dbInsert($query, $dataTypes, $parameters);
		}
		
	

		// do we want to go to a certain place
		if($returnURL != ''){

			$this->findReturnURL($returnURL, $this->insertId);

		} else {

			return $this->insertId;

		}

	}

	function updateDatabaseEntry($table, $where, $postVars = '', $returnURL = ''){

		global $_POST, $_SESSION;
		//$database = new Database();

		$dataTypes = '';
		$parameters = array();

		$schema = $this->setSchema($table);
		//print_r($_POST);die();
		//	$schema = $this->schema[$table];
		//	print_r($schema);die();
		$query = "UPDATE `" . Database::escape_value($table) . "` SET ";
			
		foreach($schema as $entry){

			$postVar = null;
			// treat those items with special cases
		//	echo count($entry)  . '<br />';
			if(isset($entry[2])){

				switch ($entry[2]) {
					case 'insert':
						// don't modify
						$query .= "" ;
						break;
							
						// don't modify
					case 'insert_now':
						$query .= "";
						break;

					case 'uid':
						$query .= "";
						break;
							
					case 'update_now':
						$query .= "`" . $entry[1] ."` = NOW(), ";
							
						break;

					case 'user_id':
						$userId = isset($_SESSION['userId']) ? $_SESSION['userId'] : '0';

						$dataTypes .= 's';
						$parameters[] = $userId;

						$query .= "`" . $entry[1] ."` = ?, ";
						break;
							
					case 'value':
						$postVar = isset($_POST['db_'. $entry[1]]) ? $_POST['db_'. $entry[1]] : $entry[3];
						$dataTypes .= 's';
						$parameters[] = $postVar;

						$query .= "`" . $entry[1] ."` = ?', ";
						break;
							
					case 'protected':
				//	echo 'protected - ' .$postVars['db_'. $entry[1]] . '<br />';
						$postVar = isset($postVars['db_'. $entry[1]]) ? $postVars['db_'. $entry[1]] : null;
			
						if($postVar != null){
							$dataTypes .= 's';
							$parameters[] = $postVar;

							$query .= "`" . $entry[1] ."` = ?, ";




						} else {

							$query .= '';
						}

						break;

						/**
						 * these for the cms and don't do anything here, but let's leave them in case we want to use them
						 */

					case 'configuration':
						$config = new Template('','');
						$configValues = $config->createConfigurationVariables();
						$query .= "`configuration` = '" . $configValues . "', ";
						break;

					case 'module':
						$config = new Template('','');
						$configValues = $config->createConfigurationVariables('module');
						$query .= "`modules` = '" . $configValues . "', ";
						break;

					case 'quickfields':

						$quickFields = new Template('','');
						$quickFieldsValues = $quickFields->createQuickFieldsVariables();
						//echo $quickFieldsValues;die();
						$query .= "`quickfields` = '" . Database::escape_value($quickFieldsValues ). "', ";
						break;

					case 'configfields':

						$quickFields = new Template('','');
						$quickFieldsValues = $quickFields->createQuickFieldsVariables();
						$query .= "`configuration_value` = '" . $quickFieldsValues . "', ";
						break;

						break;

				}

			} else {

				if(is_array($postVars)){


					if(!isset($_POST['db_'. $entry])){
							
						$_POST['db_'. $entry] = null;
					}

					$postVar = isset($postVars['db_'. $entry[1]]) ? $postVars['db_'. $entry[1]] : $_POST['db_'. $entry[1]];





				} else {

					$postVar = isset($_POST['db_'. $entry[1]]) ? $_POST['db_'. $entry[1]] : null;



				}

				$query .= "`" . $entry[1] ."` = ?, ";
				$dataTypes .= 's';
				$parameters[] = $postVar;




			}
		}

		// trim final space and comma
		$query = substr($query, 0 , -2);

		$query .= " WHERE " . ($where);
		
	//	echo $query;die();
	//	Database::showPreparedErrors($query, $parameters);

		//echo "<div style='background-color: white;'>".$query." -- alternateDb: ".$this->alternateDb."</h2>"; die();

		if($this->alternateDb != false){

				Database::dbQuery($query, $dataTypes, $parameters, $this->alternateDb);
				
				
		} else {
			
			Database::dbQuery($query, $dataTypes, $parameters);
		}
		
		

	//echo 'r = ' . $returnURL;
		if($returnURL != ''){

			$this->findReturnURL($returnURL);

		} else {

			return true;
		}
	}

	protected function findReturnURL($returnURL, $itemID = ''){



		if(strstr($returnURL, '{id}')){

			$returnURL = str_replace('{id}', $itemID, $returnURL );

		}

		header('location:' . $returnURL);
		die();

	}

}