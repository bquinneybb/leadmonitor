<?php
class Logging extends SessionManager {

	function logVisitor($action =  '', $username = '', $userlevel = ''){

		$db = new Database();

		$ip = $this->getRealIpAddr();

		if(isset($_SERVER['HTTP_REFERER'])){

			$refer = $_SERVER['HTTP_REFERER'];

		} else {

			$refer = '';

		}

		$query = "INSERT INTO `visitor_log`
			(`usage_log_id_pk`, `action`, `username`, `userlevel`, `ip_address`, `referring_page`, `date_created`) VALUES
			('', ?, ?, ?, ?, ?, NOW())";
		$dataTypes = 'ssiss';
		$parameters = array($action, $username, $userlevel, $ip, $refer);
		//echo $query;
		$db->dbQuery($query, $dataTypes, $parameters);

	}

	function getRealIpAddr(){

		if (!empty($_SERVER['HTTP_CLIENT_IP'])){

			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}

		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		//to check ip is pass from proxy
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];

		} else {
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	function logEmail($sentBy, $emailContent, $config){
		
		$dataTypes = 'sss';
		$parameters = array($sentBy, $emailContent, json_encode($config));
		
		$query = "INSERT INTO `log_email` VALUES
		('',?, ?, ?, NOW())";
		//echo $query;
		Database::dbQuery($query, $dataTypes, $parameters);
	}

	function writeLogFile($text){


		$fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/log.txt', 'w');
	
		fwrite($fp, $text );
		fclose($fp);

	}
}
?>