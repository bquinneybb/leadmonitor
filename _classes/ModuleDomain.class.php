<?php
include(ROOT . '/_utils/Netregistry/console.class.php');
include(ROOT . '/_utils/Netregistry/client.class.php');
class ModuleDomain {
	

	
	private $login = 'URBAAE-API';
	private $pass = 'bHpur8FvRdqPSYuU';
	
	#########################
	#### DOMAIN NAME USER FUNCTION
	
	public function saveDomainSearch($domainName, $status){
		
		// see if it exists already
		$query = "SELECT `module_domain_save_id_pk` FROM `module_domain_save`
		WHERE `domain_name` = ?
		AND `group_id_pk` = ?";
		
		if($status == 'AVAILABLE'){
			
			$status = 1;
		} else {
			
			$status = 0;
		}
		
		$parameters = array($domainName,  $_SESSION['user']['group_id_pk']);

		if($domainExists = Database::dbRow($query, $parameters)){

			$query = "UPDATE `module_domain_save` SET `status` = ?
			WHERE `module_domain_save_id_pk` = ?";
			
			$dataTypes = 'ii';
			
			$parameters = array($status,  $domainExists['module_domain_save_id_pk']);
			Database::dbQuery($query, $dataTypes, $parameters);
			
		} else {
			
			$query = "INSERT INTO `module_domain_save` VALUES(
			
			'', ?, ?, ?, 1
			)";
			
			$dataTypes = 'isi';
			
			$parameters = array($_SESSION['user']['group_id_pk'], $domainName, $status);
			Database::dbQuery($query, $dataTypes, $parameters);
		}
		

		return 1;
		
	}
	
	function findPreviouslySavedDomains(){
		
		
		$query = "SELECT `domain_name`, `status` FROM `module_domain_save` 
		WHERE  `group_id_pk` = ?
		AND `active` = 1";
		
		$parameters = array( $_SESSION['user']['group_id_pk']);
		//Database::spe($query, $parameters);
		if($domainSaves = Database::dbResults($query, $parameters)){
			
			return $domainSaves;
		}
		
		return false;
	}
	
	function saveDomainNamePurchase($domainName){
		
		$companyId = isset($_POST['select_company']) ? $_POST['select_company'] : 0;
		
		$query = "INSERT INTO `module_domain_purchase` VALUES (
		'', ?, ?, ?, ?, 2, NOW()
		
		)";
		
		$dataTypes = 'iiis';
		
		$parameters = array( $_SESSION['user']['group_id_pk'],  $_SESSION['user']['user_id_pk'], $companyId, $domainName);
		Database::dbQuery($query, $dataTypes, $parameters);
	}
	
	#########################
	#### DOMAIN NAME QUERIES AND RESLLER OPTIONS
	
	private function createConnection(){
		
		$location = 'https://theconsole.netregistry.com.au/external/services/ResellerAPIService/';
		$wsdl = $location.'?wsdl';
		$username = $this->login;
		$password = $this->pass;
		
		$client = new Client($location, $wsdl, $username, $password);
		
		return $client;
	}
	
	public function checkDomainAvailability($domain){
		
		$client = $this->createConnection();
		
		//print('========== consoleMethod[domainLookup] ==========<br/>');
		$client->set('domain', $domain );
		$client->domainLookup();
		//$client->screen($client->response());
		//$client->unset('domain');
		//print_r($client->response());
		
		if(isset($client->response->return->errors)){
			
			$result =  array($client->response->return->errors->errorMsg, false);
			
		} else {
		
			$result = array($client->response->return->fields->entries[0]->value, true);
		
		}
	//	print_r($result);
		unset($client);
		return $result;
		//echo $result;
	}
	
	
	public function performWhois($domain){
		
		$client = $this->createConnection();
		$client->set('domain', $domain);
		$client->domainInfo();
		
		if(isset($client->response->return->errors)){
			
			$result =  array($client->response->return->errors->errorMsg, false);
			
		} else {
		
			$result = array($client->response->return->fields->entries, true);
		
		}
		unset($client);
		//print_r($result);
		return $result;
	}
	
	public function purchaseDomain($domain){
		
		$requiredFields = array(
			'firstName','lastName'
			,'address1', 'suburb', 'postcode', 'state'
			,'phone','email', 'organisation'
		);
		// main fields
		$contactDetails['firstName'] = isset($_POST['db_reg_firstname']) ? $_POST['db_reg_firstname'] : null;
		$contactDetails['lastName'] = isset($_POST['db_reg_lastname']) ? $_POST['db_reg_lastname'] : null;
		$contactDetails['address1'] = isset($_POST['db_reg_address1']) ? $_POST['db_reg_address1'] : null;
		$contactDetails['address2'] = isset($_POST['db_reg_address2']) ? $_POST['db_reg_address2'] : null;
		$contactDetails['suburb'] = isset($_POST['db_reg_suburb']) ? $_POST['db_reg_suburb'] : null;
		$contactDetails['state'] = isset($_POST['db_reg_state']) ? $_POST['db_reg_state'] : null;
		$contactDetails['postcode'] = isset($_POST['db_reg_postcode']) ? $_POST['db_reg_postcode'] : null;
	
		
		$contactDetails['phone'] = isset($_POST['db_reg_phone']) ? $_POST['db_reg_phone'] : null;
		$contactDetails['email'] = isset($_POST['db_reg_email']) ? $_POST['db_reg_email'] : null;
		
		$contactDetails['organisation'] = isset($_POST['db_reg_organisation']) ? $_POST['db_reg_organisation'] : null;
		
		$registration['period'] = isset($_POST['db_reg_period']) ? $_POST['db_reg_period'] : 2;
		
		$isAustralian = false;
		
		$domainComponents = explode('.', $domain);
		if($domainComponents[count($domainComponents)-1] == 'au'){
			
			$isAustralian = true;
			
			$businessDetails['companyName'] = isset($_POST['db_reg_companyname']) ? $_POST['db_reg_companyname'] : null;
			$businessDetails['companyType'] = isset($_POST['db_reg_companytype']) ? $_POST['db_reg_companytype'] : null;
			$businessDetails['eligibilityType'] = isset($_POST['db_reg_eligibilitytype']) ? $_POST['db_reg_eligibilitytype'] : null;
			$businessDetails['eligibilityNumber'] = isset($_POST['db_reg_eligibilitynumber']) ? $_POST['db_reg_eligibilitynumber'] : null;
			
			
			$requiredFields[] = array(
			
				'companyName', 'companyType', 'eligibilityType', 'eligibilityNumber'
			
			);
		}
		
		// check for the primary required fields
		foreach($requiredFields as $requiredField){
			
			
			$testField = isset($_POST['db_reg_' . $requiredField]) ? $_POST['db_reg_' . $requiredField] : null;
			
			if($testField == null){
				//unset($client);
				//return array('All required fields must be filled out', false);
			}
		}
		
		$client = $this->createConnection();
		$client->set('domain', $domain);
		$client->set('contactDetails', 
		
			array('firstName' =>$contactDetails['firstName'], 'lastName'=>$contactDetails['lastName']
			,'address1' =>$contactDetails['address1'],'address2' =>$contactDetails['address2'],
			'suburb' =>$contactDetails['suburb'],  'state' =>$contactDetails['state'], 'postcode' =>$contactDetails['postcode'],'country'=>'AU'   
			,'phone'=>$contactDetails['phone'],'email'=>$contactDetails['email']
			,'organisation'=>$contactDetails['organisation'] 
			)
		);
		
		if($isAustralian){
			
			$client->set('eligibility', array(
				array('key' => 'au.registrant.name', 'value' => $businessDetails['companyName'] )
				,array('key' => 'au.registrantid.type', 'value' => 	$businessDetails['companyType'])
				,array('key' => 'au.registrant.number', 'value' => $businessDetails['eligibilityNumber'])
				,array('key' => 'au.org.type', 'value' => $businessDetails['eligibilityType'])));
		} else {
			
				$client->set('eligibility', array());
		} 
		
		$client->set('period', $registration['period']);
		$client->set('nameServers', array('ns1.netregistry.net', 'ns2.netregistry.net', 'ns3.netregistry.net'));
		$client->registerDomain();
	//	$response = $client->response();
	//	$client->screen($client->response());
	if(isset($client->response->return->errors)){
				
			$result =  array($client->response->return->errors->errorMsg, false);
			
		} else {
		
			$result = array($client->response->return->fields->entries, true);
		
		}
		unset($client);
		return $result;
		
	}
	
	private function checkAustralianRegistrationFields(){
		
		// au must 
	}
}
?>
