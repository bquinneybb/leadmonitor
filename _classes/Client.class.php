<?php
class Client {
	
	public	$states = array(
		'NSW'=>'NSW'
		,'VIC'=>'VIC'
		,'QLD'=>'QLD'
		,'SA'=>'SA'
		,'WA'=>'WA'
		,'ACT'=>'ACT'
		,'TAS'=>'TAS'
		,'NT'=>'NT'
	);
	public $eligibilityTypes = array(
		'ACN'=>'ACN'
		,'ABN'=>'ABN'
		,'VIC BN'=>'VIC BN'
		,'NSW BN'=>'NSW BN'
		,'SA BN'=>'SA BN'
		,'NT BN'=>'NT BN'
		,'WA BN'=>'WA BN'
		,'TAS BN'=>'TAS BN'
		,'ACT BN'=>'ACT BN'
		,'QLD BN'=>'QLD BN'
		,'TM'=>'TM'
		,'OTHER'=>'OTHER'
	);
	public $companyTypes = array(
		'Company'=>'Company'
		,'Registered Business'=>'Registered Business'
		,'Sole Trader'=>'Sole Trader'
		,'Trademark Owner'=>'Trademark Owner'
		,'Pending TM Owner'=>'Pending TM Owner'
		,'Incorporated Association'=>'Incorporated Association'
		,'Club'=>'Club'
		,'Non-profit Organisation'=>'Non-profit Organisation'
		,'Charity'=>'Charity'
		,'Trade Union'=>'Trade Union'
		,'Industry Body'=>'Industry Body'
		,'Commercial Statutory Body'=>'Commercial Statutory Body'
		,'Religous/Church Group'=>'Religous/Church Group'
		,'Political Party'=>'Political Party'
		,'Citizen/Resident'=>'Citizen/Resident'
		,'Other'=>'Other'
	);
	
	
	public function findAllClients($returnAsList = false){
		
		$query = "SELECT `client_id_pk`, `client_name` FROM `client`
		WHERE `group_id_pk` = ?
		AND `active` = 1
		AND `client_name` != ''
		ORDER BY `client_name`";
		
		$parameters = array($_SESSION['user']['group_id_pk']);
		
		if($results = Database::dbResults($query, $parameters)){
			
			if($returnAsList){
				$clientList = array();
				foreach($results as $result ){
					$clientList[$result['client_id_pk']] = $result['client_name'];
				}
				
				return $clientList;
				
			} else {
				
				return $results;
			
			}
		}
		
		return false;
	}
	
	public function createNewClient(){
		
		$schema = new Schema();
		$table = 'client';
		$postVars = array();
		$postVars['db_group_id_pk'] = (int) $_SESSION['user']['group_id_pk'];
		$postVars['db_active'] = 1;
		$returnURL= '/admin/client/clientedit/{id}';
		$schema->createNewDatabaseEntry($table, $postVars, $returnURL);
	}
	
	public function findClientInformation($clientId, $returnAsKeys = false){
		
		$query = "SELECT * FROM `client`
		WHERE `client_id_pk` = ?
		AND `group_id_pk` = ?";
		
			$parameters = array($clientId, $_SESSION['user']['group_id_pk']);
			
				if($result = Database::dbRow($query, $parameters)){
			
					if($returnAsKeys){
						$clientKeys = array();
						
						foreach($result as $key=>$value){
							
							$clientKeys[$key] = $value;
						}
						return $clientKeys;
					} else {
					
			return $result;
			
					}
		}
		
		return false;
	}
	
	public function saveClientInformation($clientId){
		
		if($_POST['db_group_id_pk_new'] != $_POST['db_group_id_pk']){
			
			self::moveClientToNewMasterGroup($_POST['db_group_id_pk'], $_POST['db_group_id_pk_new'], $clientId );
		}
		
		$schema = new Schema();
		$table = 'client';
		$where = " `client_id_pk` = '" . (int)$clientId . "' AND `group_id_pk` = '"	. (int)$_SESSION['user']['group_id_pk'] . "'";
		$postVars = '';
		$returnURL= '/admin/client/clientedit/' . (int)$clientId . '&updated';
		$schema->updateDatabaseEntry($table, $where, $postVars, $returnURL);
	}
	
	public function moveClientToNewMasterGroup($oldId, $newId, $clientId){
			
			// Used to migrate a client from one we manage, to one they manage for themselves
			
			// only someone with root privelages should be able to do this
			if(!$_SESSION['user']['group_id_pk'] === 1){
				
				die("You don't have enough privelages to make this change");
			}
			
			
			$tablesToUpdate = array(
				'client'
				,'configuration'
				,'module_campaign'
				,'module_campaign_form'
				,'module_domain_purchase'
				,'module_project'
			
				
			);
				
				// module_campaign_lead needs to find the relevant module_campaign_lead_id_pk before updating
				// module_lead_apartment needs to look up module_lead_id_pk
				//,'module_domain_save' should probably be allowed to be saved against a client
				// keywords needs a client_id
				/*
				 * ,'module_analytics'
				,'module_apartment_building'
				,'module_blocked_domains'
				,'module_campaign_lead'
				 * ,'module_email'
				,'module_email_list'
				,'module_email_sent'
				 * ,'module_keywords_keyword'
				,'module_keyword_save'
				 * ,'module_lead_apartment'
				 * 	,'module_site_analysis'
				 * ,'user'
				 */
			$parameters = array($newId, $oldId, $clientId);
			
			$dataTypes = 'iii';
			
			foreach($tablesToUpdate as $tableToUpdate){
					
				$query = "UPDATE `" . $tableToUpdate . "` SET `group_id_pk` = ? WHERE `group_id_pk` = ? AND `client_id_pk` = ?";
				Database::dbQuery($query, $dataTypes, $parameters);
			}	
				
			
	}
}