<?php
if($_SERVER['REMOTE_ADDR'] != '119.17.57.86'){
	
	//die('Nothing to see here');
	
}
include('../_config/config.php');
$export = new ModuleCampaignsLeadExport();

// 68 = 688 Inkerman

$campaignId = isset($_POST['campaignid']) ? (int)$_POST['campaignid'] : null;


if($campaignId == null){
	
	die('Not a valid campaign');
}

$campaigns = new ModuleCampaigns();
$campaign = $campaigns->campaignFindIdFromPost($campaignId);
$campaign_name = str_replace(' ', '-', $campaign['campaign_name']);

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
header("Content-Type: application/octet-stream");
header('Content-Disposition: attachment; filename="'.$campaign_name.'-lead-export.csv";' );
header("Content-Transfer-Encoding: binary"); 
$leads = $export->findLeadRecordsForExport($campaignId, '', '', false);
//$leads = $export->findLeadRecordsForExport($campaignId, '2015-07-01 00:00:01', '2016-06-30 11:59:59', false);
?>