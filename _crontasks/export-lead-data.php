<?php
if($_SERVER['REMOTE_ADDR'] != '119.17.57.86'){
	
	die('Nothing to see here');
	
}
include('../_config/config.php');
$export = new ModuleCampaignsLeadExport();

// 68 = 688 Inkerman

$campaignId = isset($_POST['campaignid']) ? (int)$_POST['campaignid'] : null;


if($campaignId == null){
	
	die('Not a valid campaign');
}

$leads = $export->findLeadRecordsForExport($campaignId, '', '', true);
?>