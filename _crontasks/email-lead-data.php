<?php

if($_SERVER["REMOTE_ADDR"]!='119.17.57.86'){
	die("unable to load this page");
}

$lead_start = '';
$lead_end = '';
$email_now = FALSE;
if(isset($_POST['email']) && $_POST['email'] != '')
{
	if((isset($_POST['email_input']) && $_POST['email_input'] != '') && (isset($_POST['start']) && $_POST['start'] != '') && (isset($_POST['end']) && $_POST['end'] != ''))
	{
		$lead_email = $_POST['email_input'];
		$lead_start = $_POST['start'];
		$lead_end = $_POST['end'];
		$email_now = TRUE;
		$lead_campaign = $_POST['campaign'];
		$lead_form = $_POST['form'];
	}
	else
	{
		$display_error = '<p><strong style="color: red;">Please fill in email, start and end then click Email to send</strong></p>';
	}
}
elseif(isset($_POST['filter']) && $_POST['filter'] != '')
{
	if((isset($_POST['start']) && $_POST['start'] != '') && (isset($_POST['end']) && $_POST['end'] != ''))
	{
		$lead_start = $_POST['start'];
		$lead_end = $_POST['end'];
	}
}

include('../_config/config.php');
$export = new ModuleCampaignsLeadExport();
$campaign = new ModuleCampaigns();

// 68 = 688 Inkerman

$client_id = isset($_POST['client_id']) ? (int)$_POST['client_id'] : null;
$campaignId = isset($_POST['campaignid']) ? (int)$_POST['campaignid'] : null;
$debug = isset($_POST['debug']) ? $_POST['debug'] : FALSE;
$debug_checked = isset($_POST['debug']) ? ' checked' : '';
//var_dump($debug);

if ($client_id) 
{
	$leads = $export->findLeadRecordsFormClient($client_id, '', '', false);
	die('---');
}

if($campaignId == null)
{
	die('Not a valid campaign');
}

$name = $campaign->campaignFindIdFromPost($campaignId);
$leads = $export->findLeadRecordsForView($campaignId, $lead_start, $lead_end, false);

?>

<p style="font-family: arial, helvetica, sans-serif"><strong>Email Campaign Leads</strong></p>

<?php echo (isset($display_error))? $display_error : ''; ?>

<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/ui-darkness/jquery-ui.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<form method="post">

	<input type="hidden" name="campaign" id="campaign" value="iSGnHcc9nvsZd+6OcMaPhQqBKqZLkxFhTgplFKHk5Qw=" />
  	<input type="hidden" name="form" id="form" value="iSGnHcc9nvsZd+6OcMaPhQqBKqZLkxFhTgplFKHk5Qw=" />

	<label for="email">Email</label>
	<input value="<?php echo (isset($_POST['email_input']) && $_POST['email_input'] != '')? $_POST['email_input'] : ''; ?>" name="email_input" id="email_input" />
	
	<label for="start">Start</label>
	<input class="datepicker" value="<?php echo (isset($_POST['start']) && $_POST['start'] != '')? $_POST['start'] : ''; ?>" name="start" id="start" />
	
	<label for="end">End</label>
	<input class="datepicker" value="<?php echo (isset($_POST['end']) && $_POST['end'] != '')? $_POST['end'] : ''; ?>" name="end" id="end" />
	
	<input type="submit" name="filter" value="Filter Results" />
	<input type="submit" name="email" value="Email" />

	<br/>
	<input type="checkbox" name="debug" value="include" <?php echo $debug_checked; ?>> Include debug leads in email

</form>
<hr/>
<script>
	$(document).ready(function(){

		        $( "input[type=submit]" ).button();
		
			$('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
			if($('#start').val() == ''){
				
				$( "#start" ).datepicker( "setDate", "-7 days" );

			}
			
			if($('#end').val() == ''){
				
				$( "#end" ).datepicker( "setDate", "-1 day" );

			}
	});
</script>

<?php

if ($name)
{
	$between = ($lead_start!='' && $lead_end!='')? ' between '.$lead_start.' - '.$lead_end : '';
	$display = '<p style="font-family: arial, helvetica, sans-serif"><strong>'.$name['campaign_name'].$between.'</strong></p>';
	$csv_filename_addition = ' - '.$name['campaign_name'].$between;
}

if ($leads)
{

	$lead_count = 0;
	$display_html = '';
	$csv_content = array();
	foreach ($leads as $lead) 
	{
		if ($debug)
		{
			$csv_content[] = $lead;

			$display_html .= '<tr>';

			foreach ($lead as $val) 
			{
				$display_html .= '<td>'.preg_replace('/[^A-Za-z0-9 \/\\\\.:\-\@]/', '', $val).'</td>';
			}

			$display_html .= '</tr>';

			$lead_count++;

		}
		elseif (strpos(@$lead['email'], '@barkingbird.com') === FALSE)
		{
			$csv_content[] = $lead;
		
			$display_html .= '<tr>';

			foreach ($lead as $val) 
			{
				$display_html .= '<td>'.preg_replace('/[^A-Za-z0-9 \/\\\\.:\-\@]/', '', $val).'</td>';
			}

			$display_html .= '</tr>';

			$lead_count++;

		}

	}

	$display .= '<p style="font-family: arial, helvetica, sans-serif; color: #999999;"><strong>'.$lead_count.' Leads</strong></p>';
	$display .= '<table cellspacing="0" cellpadding="10" border="1" style="font-family: arial, helvetica, sans-serif">';
	$display .= (isset($display_html))? $display_html : '';
	$display .= '</table>';

}
else
{

	$display .= '<p style="font-family: arial, helvetica, sans-serif">There are no leads yet...</p>';

}

if($email_now)
{
	// send the email
	$emailer 	= new SendEmailLeads();
	$emailer->emailSubject 		= $name['campaign_name'].' leads between '.$lead_start.' - '.$lead_end;
	$emailer->emailFromName 	= 'Leadmonitor';
	$emailer->emailFromEmail 	= 'admin@leadmonitor.com.au';
	$emailer->emailReplyTo 		= 'admin@leadmonitor.com.au';

	$emailer->emailEmailTo 		= $lead_email;
	$emailer->emailNameTo 		= $lead_email;

	$body = 'Selected leads attached';
	$emailText 	= $emailer->formatEmail($body);
	$emailer->emailBody 		= $body;

	// generate the csv and attach it
	$csv_created = FALSE;
	if(isset($csv_content) && $csv_content && count($csv_content) > 0)
	{
		$temp_folder = 'csv_temp';
		$middle_filename = (isset($csv_filename_addition))? $csv_filename_addition : '';
		$csv_filename = 'Leads Export'.$middle_filename.'.csv';

		if(!file_exists($temp_folder))
		{
			mkdir($temp_folder);
		}

		$file = fopen($temp_folder.'/'.$csv_filename,"w");

		foreach($csv_content as $lead)
		{
			fputcsv($file,$lead);
		}

		fclose($file);

		$emailer->emailAttachment 	= $temp_folder.'/'.$csv_filename;

		$csv_created = TRUE;
	}

	if($emailer->sendEmailOut())
	{
		echo '<p><strong style="color: green;">Your email was sent successfully</strong></p>';
	}
	else
	{
		echo '<p><strong style="color: red;">There was a problem sending your email</strong></p>';
	}

	// delete csv if created
	if($csv_created)
	{
		unlink($temp_folder.'/'.$csv_filename);
	}

}

echo $display;

?>

