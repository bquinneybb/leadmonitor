<style>
body {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	color: #333;
	background-color: #333;
}
#content {
	width: 1000px;
	background-color: #FFF;
	padding: 20px;
	margin-top: 20px;
	margin-right: auto;
	margin-bottom: 20px;
	margin-left: auto;
}
.time {
	color: #600;	
}
.register {
	color: #900;
	font-weight: bold;
}
h1 {
	font-size: 16px;	
}
</style>
<div id="content">
<h1>Warleigh Brighton Visitor Tracking</h1>
<?php
include('../_config/config.php');

$query = "SELECT * FROM module_campaign_weblinks_followed WHERE `date` >= '2012-01-30' AND `date` < '2012-02-06' ORDER BY `date`";



$parameters = array();

$paths = array();

if($results = Database::dbResults($query, $parameters)){
	//print_r($results);
	
	foreach($results as $result){
		
		$paths[$result['uid']]['link'][] = $result['href'];
		$paths[$result['uid']]['time'][]	= $result['date'];
		
	}
	$i = 1;
	$html = '';

	foreach($paths as $key=>$values){
		//print_r($paths[$key]['time'][count($paths[$key]['time'])-1]);die();
		$html .= '<strong>Visitor #' . $i . '</strong> : ' . $paths[$key]['time'][0] . ' <span class="time">(' . (strtotime($paths[$key]['time'][count($paths[$key]['time'])-1]) - strtotime($paths[$key]['time'][0]))  . ' seconds)</span> <br />';
		foreach($paths[$key]['link'] as $value){
			
			if($value == 'register'){
				
				$html .= '<span class="register">' . $value . '</span>, ';	
				
			} else {
				
				$html .= $value . ', ';	
			}
			
			
			
		}
		
		$html .= '<br /><br /><hr />';
		
		$i++;
	}

echo $html;
}

?>
</div>