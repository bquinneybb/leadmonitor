<?php

if($_SERVER['REMOTE_ADDR'] != '119.17.57.86'){
	
	die('Nothing to see here');
	
}
include('../_config/config.php');

$export = new ModuleCampaignsLeadExport();
$campaign = new ModuleCampaigns();

// 68 = 688 Inkerman

$client_id = isset($_POST['client_id']) ? (int)$_POST['client_id'] : null;
$campaignId = isset($_POST['campaignid']) ? (int)$_POST['campaignid'] : null;
$debug = isset($_POST['debug']) ? $_POST['debug'] : FALSE;
//var_dump($debug);

if ($client_id) 
{
	$leads = $export->findLeadRecordsFormClient($client_id, '', '', false);
	die('---');
}

if($campaignId == null)
{
	die('Not a valid campaign');
}

if (isset($_POST['deactivate_leads']) && isset($_POST['deactivate_campaign_id']))
{
	if($_POST['deactivate_leads'] == 'yes' && (int)$_POST['deactivate_campaign_id'] == $campaignId)
	{
		$clear_leads = $export->deactivateLeadRecords($campaignId);
	}
}

$name = $campaign->campaignFindIdFromPost($campaignId);
$leads = $export->findLeadRecordsForView($campaignId, '', '', false);

if ($name)
{
	echo '<p style="font-family: arial, helvetica, sans-serif"><strong>'.$name['campaign_name'].'</strong></p>';
}

if ($leads) :

echo '<p style="font-family: arial, helvetica, sans-serif; color: #999999;"><strong>'.(count($leads)-1).' Leads</strong></p>';

?>



<table cellspacing="0" cellpadding="10" border="1" style="font-family: arial, helvetica, sans-serif">

	<?php foreach ($leads as $lead) : //print_r($lead);
		if ($debug): ?>
		<tr>

			<?php 

				foreach ($lead as $val) 
				{
					echo '<td>';
					//echo $val; 
					echo preg_replace('/[^A-Za-z0-9 \/\\\\.:\-\@]/', '', $val);
					echo '</td>';
				}?>
		</tr>
	<?php elseif (strpos(@$lead['email'], '@barkingbird.com') === FALSE): ?>
		<tr>

			<?php 

				foreach ($lead as $val) 
				{
					echo '<td>';
					//echo $val;
					echo preg_replace('/[^A-Za-z0-9 \/\\\\.:\-\@]/', '', $val);
					echo '</td>';
				}?>
		</tr>
		<?php endif; 
	endforeach; ?>

</table>

<?php else : ?>

<p style="font-family: arial, helvetica, sans-serif">There are no leads yet...</p>

<?php endif; ?>


<br/>
<br/>
<br/>
<a style="font-family: arial, helvetica, sans-serif;" href="<?php echo '/_crontasks/view-lead-data.php?campaignid='.$campaignId.'&debug=true'; ?>">Show all Leads (Debug Mode) </a>
<br/>
<br/>

<form name="deactivate_leads" method="post" style="border: 1px solid red; padding: 10px; width: 200px; background: #ffbbbb none; font-family: arial, helvetica, sans-serif;">

	<p style="color:#ff0000; margin: 0px;"><strong>Clear All Leads</strong></p><br/>
	<label>Confirm the Campaign ID</label><br/>
	<input type="text" name="deactivate_campaign_id" style="width: 100%;" /><br/>
	<input type="hidden" name="deactivate_leads" value="yes" />

	<?php if(isset($_POST['deactivate_campaign_id']) && (int)$_POST['deactivate_campaign_id'] != $campaignId) : ?>
		<p style="color:#ff0000; margin: 0px;">Incorrect Campaign ID</p>
	<?php endif; ?>

	<br/>
	<button type="submit">Clear All Leads for this Campaign</button>

</form>

