var page = require('webpage').create(),
    system = require('system'),
    address, output, size;
var fs = require('fs');

if (system.args.length < 3 || system.args.length > 6) {
    console.log('Usage: rasterize.js URL filename [paperwidth*paperheight|paperformat]');
    console.log('  paper (pdf output) examples: "5in*7.5in", "10cm*20cm", "A4", "Letter"');
    phantom.exit(1);
} else {
    address = system.args[1];
    output = system.args[2];
    console.log(output);
    page.viewportSize = {
        width: 1000,
        height: 1000
    };
    if (system.args.length > 3 && system.args[2].substr(-4) === ".pdf") {
        size = system.args[3].split('*');
        page.dpi = 96.0;
        page.paperSize = {
            format: 'A4',
            orientation: 'portrait',
            margin: {
                top: '1cm',
                right: '1cm',
                bottom: '0.5cm',
                left: '1cm'
            },
            header: {
                height: "4cm",
                contents: phantom.callback(function(pageNum, numPages) {
                    var imgSrc = "file://" + system.args[4];
                    return "<div style='text-align:right;'><img style='width: 14cm;' src='" + imgSrc + "' /></div>"; // https://github.com/ariya/phantomjs/issues/10735
                })
            },
            footer: {
                //console.log(fs);
                height: "1cm",
                contents: phantom.callback(function(pageNum, numPages) {
                    return fs.read('tmpl/footer.html');
                })
            }
        };
    }
    if (system.args.length > 5) {
        page.zoomFactor = system.args[5];
    }
    page.open(address, function(status) {
        if (false && status !== 'success') {
            console.log(status);
            console.log('Unable to load the address!');
            phantom.exit();
        } else {
            //page.injectJs('delocal.js');
            console.log('-------------------------------');
            console.log(address);
            window.setTimeout(function() {

                page.render(output);
                phantom.exit();
            }, 200);
        }
    });
}