<?php

require __DIR__.'/vendor/gapi_v2/gapi.class.php';
require __DIR__ . '/FlowAnalytics.php';


//define('PATH_TO_PHANTOMJS', __DIR__.'/usr/local/bin/phantomjs');
//define('PATH_TO_PHANTOMJS', '/usr/local/bin/phantomjs');
//define('PATH_TO_PHANTOMJS', '/usr/local/bin/phantomjs-2.1.1-linux-x86_64/bin/phantomjs');
define('PATH_TO_PHANTOMJS', '../phantomjs');
class AnalyticsReport {

    private $ga_email;
    private $ga_password;
    private $ga_profile_id;
    public $apiKey = '';
    
    //nigel
    public $renderNow = false;
    public $ga_filename;
	public $siteName;
    private $fields = array();

    public function __construct() {
	$loader = new Twig_Loader_Filesystem(__DIR__.'/tmpl');
	$this->twig = new Twig_Environment($loader);
	$this->fields['report'] = $this;
	$this->fields['logoPath'] = realpath(__DIR__.'/img/header-image.png');
	$this->fields['promoPath'] = realpath(__DIR__.'/img/promotion.png');
	$this->fields['pageBackground'] = realpath(__DIR__.'/img/page_background.png');
	$this->fields['strategy_creative'] = realpath(__DIR__.'/img/bb_sc.png');
	$this->fields['website_technology'] = realpath(__DIR__.'/img/bb_wt.png');
	$this->fields['advertising_campaigns'] = realpath(__DIR__.'/img/bb_ac.png');
	$this->fields['online_engagement'] = realpath(__DIR__.'/img/bb_oe.png');
	$this->fields['hosting'] = realpath(__DIR__.'/img/bb_ht.png');
	$this->fields['coverpage_barking'] = realpath(__DIR__.'/img/coverpage_barking.png');

    }

    public function auth($ga_email, $ga_password, $ga_profile_id) {
	$this->ga_email = $ga_email;
	$this->ga_password = $ga_password;
	$this->ga_profile_id = $ga_profile_id;
    }

    public function setDates($startDate, $endDate) {
	$this->startDate = $startDate;
	$this->endDate = $endDate;

	$this->startTimestamp = strtotime($this->startDate);
	$this->endTimestamp = strtotime($this->endDate);

    }

    private function fetchInfo() {
	$filter = null;

	$dimensions = array('hostname');
	$metrics = array('visitors');
	$sort_metric = null;

	$start_index = 1;
	$max_results = 1;

	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
	$results = $this->ga->getResults();
	//print_r($results);die();
	foreach ($results as $v) {
		//$this->siteName = $v;
		break;
	}
    }

    private function fetchTotal() 
    {
		$total = new stdClass();
		$filter = null;

		$dimensions = array('hostname');
		$sort_metric = null;
		$start_index = 1;
		$max_results = 100;

		//$metrics = array('pageviews', 'uniquePageviews', 'visitors', 'newVisits', 'avgTimeOnSite', 'avgTimeOnPage', 'entrances', 'exitRate', 'exits', 'visitBounceRate', 'percentNewVisits');
		//$metrics = array('sessions', 'users', 'pageviews', 'avgSessionDuration', 'newVisits', 'visitors', 'avgTimeOnSite', 'avgTimeOnPage', 'entrances', 'exitRate', 'exits', 'visitBounceRate', 'percentNewVisits');
		$metrics = array('sessions', 'users', 'pageviews', 'avgSessionDuration', 'newVisits', 'visitors', 'avgSessionDuration', 'avgTimeOnPage', 'entrances', 'exitRate', 'exits', 'visitBounceRate', 'percentNewVisits');

		while (!empty($metrics)) {
		        $metrics2 = array_slice($metrics, 0, 10);
		        $metrics = array_slice($metrics, 10);
		       //print_r($metrics2); die;
			$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics2, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
			//print_r($this->ga);die();
			foreach ($metrics2 as $metric) {
				$func = 'get'.ucfirst($metric);
				$value = $this->ga->$func();
				switch ($metric) {
					case 'avgTimeOnPage':
					case 'avgSessionDuration':
					case 'avgTimeOnSite':
						$total->$metric = sprintf('%02d:%02d', $value / 60, $value % 60);
						break;
					default:
						$total->$metric = $value;
						break;
				}
			}
		}
		$this->fields['total'] = $total;
    }

    private function fetchDaily() {
	$filter = null;
	//echo 's = ' . $this->startDate . ' e = ' . $this->endDate;die();
	$dimensions = array('date');
	$sort_metric = 'date';

	//$metrics = array('pageviews', 'uniquePageviews', 'visitors', 'newVisits', 'avgTimeOnSite', 'entrances', 'exitRate', 'exits');
	$metrics = array('pageviews', 'uniquePageviews', 'visitors', 'newVisits', 'avgSessionDuration', 'entrances', 'exitRate', 'exits');
	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, 1, 365);
//$report_id, $dimensions, $metrics, $sort_metric=null, $filter=null, $start_date=null, $end_date=null, $start_index=1, $max_results=30
	$this->dailyResults = $this->ga->getResults();
	//print_r($this->dailyResults);die();
    }

    private function fetchTopVisited() {
	$filter = null;

	$dimensions = array('pagePath');
	$metrics = array('pageviews', 'uniquePageviews', 'visitors', 'newVisits', 'avgTimeOnPage', 'entrances', 'exitRate', 'exits', 'percentNewVisits', 'visitBounceRate');
	$sort_metric = '-pageviews';

	$start_index = 1;
	$max_results = 20;

	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
	$this->topVisited = $this->ga->getResults();
	
	//print_r($this->topVisited);die();
	
	$this->topVisited = $this->convertYahooPagesViewedToSearchTerms($this->topVisited);
	// convert the Yahoo tags to something useful
	
	
	
    }
    
    // MR UPDATES: $dat->dimensions is now private so just return the data
    private function convertYahooPagesViewedToSearchTerms($data){
	    
	    return $data;

	    foreach($data as $dat){
	    	//print_r($dat->dimensions['pagePath']);
	    	//die();
	    	if(strstr($dat->dimensions['pagePath'], 'OVRAW')){
		    	
		    	$s = $dat->dimensions['pagePath'];
		    	
		    	$components = explode('&OVKEY=', $s);
				$keyword = explode('/?OVRAW=', $components[0]);
			//	print_r($keyword);die();
				$campaign = explode('&OVMTC=', $components[1]);
		    	
		    	$dat->dimensions['pagePath'] = 'Yahoo Ad : Term = "' . $keyword[1] . '", Campaign Term = "' . $campaign[0] . '"';
		    	
	    	}
	    }
	    return $data;
	    //print_r($data);
	    //die();
    }

    private function fetchTopSources() {
	$filter = null;

	$dimensions = array('source');
	//$metrics = array('visitors', 'pageviews', 'avgTimeOnSite', 'entrances', 'exitRate', 'exits', 'percentNewVisits', 'visitBounceRate', 'newVisits');
	$metrics = array('visitors', 'pageviews', 'avgSessionDuration', 'entrances', 'exitRate', 'exits', 'percentNewVisits', 'visitBounceRate', 'newVisits');
	//$sort_metric = '-pageviews';
	$sort_metric = '-visitors';

	$start_index = 1;
	$max_results = 25;

	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
	$this->topSources = $this->ga->getResults();
    }

    private function fetchTopKeywords() {
	$filter = null;

	$dimensions = array('keyword');
	//$metrics = array('visitors', 'pageviews', 'avgTimeOnSite', 'entrances', 'exitRate', 'exits', 'percentNewVisits', 'visitBounceRate');
	$metrics = array('visitors', 'pageviews', 'avgSessionDuration', 'entrances', 'exitRate', 'exits', 'percentNewVisits', 'visitBounceRate');
	//$sort_metric = '-pageviews';
	$sort_metric = '-visitors';
	
	$start_index = 1;
	$max_results = 25;

	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
	$this->topKeywords = $this->ga->getResults();
    }

    private function fetchHistogram() {
	$this->max = array();
	$this->histogram = array();

	$filtering = array(
		'0-10 seconds' => '11',
		'11-30 seconds' => '31',
		'31-60 seconds' => '61',
		'61-180 seconds' => '181',
		'181-600 seconds' => '601',
		'601-1800 seconds' => '1801',
		'1801+ seconds' => '999999',
	);

	$dimensions = array('visitLength');
	$metrics = array('visitors', 'pageviews');

	$sort_metric = null;

	$start_index = 1;
	$max_results = 10;

	$lastMax = -1;
	foreach ($filtering as $caption => $max) {
		$filter = "timeOnSite > ".$lastMax." && timeOnSite < ".$max;
		$lastMax = $max - 1;
		$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
		$this->histogram[$caption] = array();
		foreach ($metrics as $metric) {
			$func = 'get'.ucfirst($metric);
			$value = $this->ga->$func();
			if (empty($this->max[$metric]) || $this->max[$metric] < $value) $this->max[$metric] = $value;
			$this->histogram[$caption][$metric] = $value;
		}
	}

    }
    private function fetchVisitsFlow() {
		//print_r($this);
		$dimensions = array('pagePath', 'previousPagePath', 'pageDepth');
		$metrics = array('pageviews', 'exits');
		$sort_metric = '-pageviews';
		$filter = '';
		$start_index = 1;
		$max_results = 200;

		$nodes = array();
		$links = array();

		$nodeCnt = 0;
		$nodeIdToCnt = array();

		$this->ga
				->requestReportData($this->ga_profile_id, $dimensions,
						$metrics, $sort_metric, $filter, $this->startDate,
						$this->endDate, $start_index, $max_results);
		$results = $this->ga->getResults();

		$flowAnalytics = new FlowAnalytics($results);
		$this->alluvialJson = $flowAnalytics->getJson();
//		print_r($this);

	}

	private function fetchDevices() {
		
        $last_month_start_date      = new DateTime("first day of last month");
        $last_month_start           = $last_month_start_date->format('Y-m-d');
        $last_month_end_date        = new DateTime("last day of last month");
        $last_month_end             = $last_month_end_date->format('Y-m-d');
        $current_month_start_date   = new DateTime("first day of this month");
        $current_month_start        = $current_month_start_date->format('Y-m-d');
        $current_month_end_date     = new DateTime("today");
        $current_month_end          = $current_month_end_date->format('Y-m-d');
        
        $sort_metric           = 'date';
        $filter                = null;

        $output                = array();
        $output[0] ['start']   = $last_month_start_date->format('M d, Y');
        $output[0] ['end'] 	   = $last_month_end_date->format('M d, Y');
        $output[0] ['desktop'] = 0;
        $output[0] ['mobile']  = 0;
        $output[1] ['start']   = $current_month_start_date->format('M d, Y');
        $output[1] ['end']     = $current_month_end_date->format('M d, Y');
        $output[1] ['desktop'] = 0;
        $output[1] ['mobile']  = 0;  
        //get the reports for last month 
        
        $get_device_info = $this->ga->requestReportData($this->ga_profile_id,array('browser','deviceCategory','date'),array('pageviews','visits') , $sort_metric, $filter, $last_month_start , $current_month_end);
		$mobile_count = 0;
		$desktop_count = 0;
        foreach($get_device_info as $i => $result)
        {
            $loop_date          = DateTime::createFromFormat('Ymd', $result->getdate());
            $loop_date_format  = $loop_date->format('Y-m-d'); 

            if($result->getdeviceCategory() == "desktop")
            {
                 if($loop_date_format < $current_month_start)
                 {
                 	$output[0] ['desktop'] = $output[0] ['desktop'] + 1; 
                 }
                 else 
                 {
                   $output[1] ['desktop'] = $output[0] ['desktop'] + 1;
                 }
                 
            } 
            else if($result->getdeviceCategory() == "mobile")
            {
                 if($loop_date_format < $current_month_start)
                 {
                 	$output[0] ['mobile'] = $output[0] ['desktop'] + 1; 
                 }
                 else 
                 {
                   $output[1] ['mobile'] = $output[0] ['desktop'] + 1;
                 }
            } 
        
        }

        $this->devicesInfo = $output; 
        
	
	}

    public function chart($field, $results = null) {
	if (empty($results)) $results = $this->dailyResults;

	$func = 'get'.ucfirst($field);
	$chart = array();
	foreach($results as $result) {
	    $dateStr = (string)$result;
	    $date = 'new Date('.substr($dateStr, 0, 4).', '.(substr($dateStr, 4, 2)-1).', '.substr($dateStr, 6, 2).')';
	    $chart[] = array($date, $result->$func());
	}
	return $chart;
    }

    private function createReport() {

		//$this->ga = new gapi($this->ga_email, $this->ga_password);
		$this->ga = new gapi('205281677360-6ft1rh294tf2d39sha14cpr5h8ecchs0@developer.gserviceaccount.com', __DIR__.'/vendor/gapi_v2/leadmonitor-53d7039196f9.p12');
		//$this->ga->apiKey = $this->apiKey;
		$this->fetchTotal();
		$this->fetchInfo();
		$this->fetchDaily();
		$this->fetchTopVisited();
		$this->fetchTopSources();
		$this->fetchTopKeywords();
		$this->fetchHistogram();
		$this->fetchVisitsFlow();
		$this->fetchDevices();
    }

    public function html() {
		$this->createReport();
		return $this->twig->render('report.twig.html', $this->fields);
    }

    public function pdf($fileName = null, $save_to_fileName = FALSE) {

    	//exec('phantomjs -v');

    	//exec('../phantomjs -v 2>&1', $output);
    	//$error = shell_exec('/usr/bin/phantomjs -v 2>&1');
		//var_dump($output);
		//die('sdf');

		$this->createReport();

		$temp = tempnam(ROOT.'/fileserver/analytics/', 'ga_');

		
		@unlink($temp);
		$pdf = $temp.'.pdf';
		//$pdf = $this->ga_filename . '.pdf';
		$temp = $temp.'.html';

		if (!empty($fileName)) {
			$pdf = $fileName;
		}

		$putted = file_put_contents($temp, $this->twig->render('report_2016.twig.html', $this->fields));
		//var_dump($temp);// die;
		//var_dump($putted); die;
		//$pass_text_js = "Barking-Bird";
		$site_name = str_replace(" ", "--", $this->siteName);
		$cmd = PATH_TO_PHANTOMJS. ' '.ROOT.'/_crontasks/analytics/rasterize.js '.$temp.' '.$pdf.' A4 '.$this->fields['pageBackground'].' '.$site_name.' '.$this->startDate.' '.$this->endDate. ' 2>&1';

		echo($cmd.'<br/>');
		die();

		exec($cmd, $error);
		//var_dump($error);

		if ($save_to_fileName === TRUE) 
		{
			return TRUE;
		}

		$pdf_array = explode("public_html/", $pdf);

        $pdf_link = 'http://'.$_SERVER['HTTP_HOST'].'/'.$pdf_array[1];

		 header('Content-type: application/pdf');
	     header('Content-Disposition: attachment; filename="'.$this->domainName.'_'.$this->startDate.'_'.$this->endDate.'.pdf"');
		 echo file_get_contents($pdf);
		
		die();

		if(!$this->renderNow)
		{	
			die('sdf');
			/* TO BE UNCOMMENTED */	
			copy($pdf, ROOT .'/temp/' .$this->ga_filename . '.pdf');
			@unlink($pdf);		
		} 
		else 
		{
			// DELETED BY NIGEL
			die($pdf);
			if (empty($fileName)) 
			{
			        header('Content-type: application/pdf');
			        header('Content-Disposition: attachment; filename="'.$this->clientName.'_'.$this->startDate.'_'.$this->endDate.'.pdf"');
					echo file_get_contents($pdf);
				//@unlink($pdf);
			}
		
		}
	
    }
}
