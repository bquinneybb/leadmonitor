var page = require('webpage').create(),
    system = require('system'),
    address, output, size;
var fs = require('fs');

if (system.args.length < 3 || system.args.length > 9) {
    console.log('Usage: rasterize.js URL filename [paperwidth*paperheight|paperformat]');
    console.log('  paper (pdf output) examples: "5in*7.5in", "10cm*20cm", "A4", "Letter"');
    phantom.exit(1);
} else {
    address = system.args[1];
    output = system.args[2];
    console.log(output);
    page.viewportSize = {
        width: 1240,
        height: 1754
    };
    if (system.args.length > 3 && system.args[2].substr(-4) === ".pdf") {
        size = system.args[3].split('*');
        page.dpi = 150.0;
        page.paperSize = {
            format: 'A4',
            orientation: 'portrait',
            margin: {
                top: '0cm',
                right: '0cm',
                bottom: '0cm',
                left: '0cm'
            },
            header: {
                height: "4cm",
                contents: phantom.callback(function(pageNum, numPages) {

                    if(pageNum == 1 || pageNum == numPages) {
                        //return '<div style="width:550px !important; height:110px !important; margin-left: 0.5cm !important; padding: 0px !important; background-color: #ffcb04 !important; border: 0 !important;"></div>';
                        //return "";//<div style='background-color: #ffcb04; height:140px; padding:15px; margin:0px; width: 1000px;'></div>";
                        var custom_header = fs.read('tmpl/alternate_header_2016.html');
                        return custom_header;
                    }

                     var header_string = system.args[4].split("---");
                     var siteName      =  header_string[0].replace(/--/g,' ');
                     var startDate     =  header_string[1].replace(/--/g,' ');
                     var endDate       =  header_string[2].replace(/--/g,' ');
                     var custom_header = fs.read('tmpl/header_2016.html');
                         custom_header = custom_header.replace(/--siteName--/g , siteName);
                         custom_header = custom_header.replace(/--startDate--/g , startDate);
                         custom_header = custom_header.replace(/--endDate--/g , endDate);
                    return custom_header;
                })
            },
            footer: {
                //console.log(fs);
                height: "1.25cm",

                contents: phantom.callback(function(pageNum, numPages) {
                    
                    if(pageNum == 1 || pageNum == numPages) {

                        return "";
                    }

                    var footer_string = system.args[4].split("---");
                    var custom_footer = fs.read('tmpl/footer_2016.html');
                        footer_html = custom_footer.replace('--pageNum--' , pageNum);
                        footer_html = footer_html.replace('--body_background--' , footer_string[3]);
                    return footer_html;
                })
            }
        };
    }
    if (system.args.length > 5) {
        page.zoomFactor = system.args[5];
    }
    page.open(address, function(status) {
        if (false && status !== 'success') {
            console.log(status);
            console.log('Unable to load the address!');
            phantom.exit();
        } else {
            //page.injectJs('delocal.js');
            console.log('-------------------------------');
            console.log(address);
            window.setTimeout(function() {

                page.render(output);
                phantom.exit();
            }, 200);
        }
    });
}