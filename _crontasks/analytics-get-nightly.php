<?php
include('../_config/config.php');

// get the analytics each night for the various sites
// this should run at 4am to ensure data is updated by Google
$analytics = new ModuleAnalytics();
$analytics->findNightlyAnalyticsReports();

?>