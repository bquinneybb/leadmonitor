<?php

include('../_config/config.php');
$export = new ModuleCampaignsLeadExport();
$campaign = new ModuleCampaigns();

// Fetch all campaigns where lead_frequency != off
$active_campaigns = $export->findCampaignsWithCron();
// echo '<pre>';
// print_r($active_campaigns);
// echo '</pre>';

if($active_campaigns == "no results" || !is_object($active_campaigns))
	die("No results to show");

$date_default = "0000-00-00";
$display = '';
$email_list = array();
foreach($active_campaigns as $campaign)
{
	// if lead email exists then proceed
	$lead_email = ($campaign['lead_email']!='')? $campaign['lead_email'] : FALSE;
	// print_r($lead_email); die();

	if($lead_email)
	{
		$email_parts = explode(',', $lead_email);
		foreach($email_parts as $email)
		{
			// gather campaign information
			$campaign_id = $campaign['module_campaign_id_pk'];
			$campaign_name = $campaign['campaign_name'];
			$frequency = ($campaign['lead_frequency']=="weekly")? '1 week' : '1 month';
			$lead_start = date("Y-m-d", strtotime('-'.$frequency));
			$lead_end = date("Y-m-d");

			// check if last sent is not default otherwise set last sent to one peroid ago so that it will send
			$last_sent = ($campaign['lead_last_sent'] != $date_default)? $campaign['lead_last_sent'] : $lead_start;
			$next_send = date("Y-m-d", strtotime($last_sent.' +'.$frequency));
			
			// check if the date last sent is equal to one period less than current date
			// echo '<p>lead_end => '.$lead_end.', next_send => '.$next_send.'</p>';
			if($lead_end >= $next_send)
			{
				$leads = $export->findLeadRecordsForView($campaign_id, $lead_start, $lead_end, false);

				if ($campaign_name)
				{
					$between = ($lead_start!='' && $lead_end!='')? ' between '.$lead_start.' - '.$lead_end : '';
					$csv_filename_addition = ' - '.$campaign_name.$between;
				}

				if ($leads)
				{
					$csv_content = array();
					foreach ($leads as $lead) 
					{
						$csv_content[] = $lead;
					}

					// generate the csv
					if(isset($csv_content) && $csv_content && count($csv_content) > 0)
					{
						$temp_folder = 'csv_cron_temp';
						$middle_filename = (isset($csv_filename_addition))? $csv_filename_addition : '';
						$csv_filename = 'Leads Export'.$middle_filename.'.csv';

						if(!file_exists($temp_folder))
						{
							mkdir($temp_folder);
						}

						$file = fopen($temp_folder.'/'.$csv_filename,"w");

						foreach($csv_content as $lead)
						{
							fputcsv($file,$lead);
						}

						fclose($file);

						$email_list[$email][] = array("report_id" => $campaign['module_campaign_id_pk'], "report_name" => $campaign_name.$between, "csv" => $temp_folder.'/'.$csv_filename);
					}
				}
			}
		}
	}
}

// echo '<pre>';
// print_r($email_list);
// echo '</pre>';

// Prepare the emails and send
if(count($email_list) > 0)
{
	foreach($email_list as $key => $email)
	{
		// send the email
		$emailer = new SendEmailLeads();
		$emailer->emailSubject 		= 'Lead Monitor summary email - leads between '.$lead_start.' - '.$lead_end;
		$emailer->emailFromName 	= 'Leadmonitor';
		$emailer->emailFromEmail 	= 'admin@leadmonitor.com.au';
		$emailer->emailReplyTo 		= 'admin@leadmonitor.com.au';

		$emailer->emailEmailTo 		= $key;
		$emailer->emailNameTo 		= $key;

		$body = 'Lead sumamries attached:<br/><br/>';

		$attachments = array();
		foreach($email as $content)
		{
			$body .= '- '.$content['report_name'].'<br/>';
			$attachments[] = $content['csv'];
			$export->updateLastSentCampaign($content['report_id']);
		}

		$emailText 	= $emailer->formatEmail($body);
		$emailer->emailBody = $body;
		$emailer->emailAttachment = $attachments;

		if($emailer->sendEmailOut())
		{
			echo '<p><strong style="color: green;">Your email to '.$key.' was sent successfully</strong></p>';
		}
		else
		{
			echo '<p><strong style="color: red;">There was a problem sending your email to '.$key.'</strong></p>';
		}
	}

	foreach($email_list as $key => $email)
	{
		// delete csvs
		foreach($email as $content)
		{
			if(is_file($content['csv']))
			{
				unlink($content['csv']);
			}
		}
	}
}
else
{
	echo '<p><strong style="color: red;">There were no lead summary emails to send</strong></p>';
}

?>

