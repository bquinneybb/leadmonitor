<?php

require __DIR__.'/vendor/autoload.php';
require __DIR__.'/AnalyticsReport.php';

$report = new AnalyticsReport();
$report->auth('example8@gmail.com', 'PASSWORD', 'PAGE_ID');
$report->setDates('2013-01-01', '2013-02-01');
$report->clientName = 'GitGis Ltd';

$report->pdf('~/gitgis.pdf');
