<?php

require __DIR__.'/vendor/gapi/gapi.class.php';
require __DIR__ . '/FlowAnalytics.php';
//define('PATH_TO_PHANTOMJS', __DIR__.'/usr/local/bin/phantomjs');
define('PATH_TO_PHANTOMJS', '/usr/local/bin/phantomjs');
class AnalyticsReport {

    private $ga_email;
    private $ga_password;
    private $ga_profile_id;
    public $apiKey = '';
    //nigel
    public $ga_filename;

    private $fields = array();

    public function __construct() {
	$loader = new Twig_Loader_Filesystem(__DIR__.'/tmpl');
	$this->twig = new Twig_Environment($loader);
	$this->fields['report'] = $this;
	$this->fields['logoPath'] = realpath(__DIR__.'/img/logo.png');
    }

    public function auth($ga_email, $ga_password, $ga_profile_id) {
	$this->ga_email = $ga_email;
	$this->ga_password = $ga_password;
	$this->ga_profile_id = $ga_profile_id;
    }

    public function setDates($startDate, $endDate) {
	$this->startDate = $startDate;
	$this->endDate = $endDate;

	$this->startTimestamp = strtotime($this->startDate);
	$this->endTimestamp = strtotime($this->endDate);

    }

    private function fetchInfo() {
	$filter = null;

	$dimensions = array('hostname');
	$metrics = array('visitors');
	$sort_metric = null;

	$start_index = 1;
	$max_results = 1;

	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
	$results = $this->ga->getResults();
	foreach ($results as $v) {
		$this->siteName = $v;
		break;
	}
    }

    private function fetchTotal() {
	$total = new stdClass();
	$filter = null;

	$dimensions = array('hostname');
	$sort_metric = null;
	$start_index = 1;
	$max_results = 100;

	$metrics = array('pageviews', 'uniquePageviews', 'visitors', 'newVisits', 'avgTimeOnSite', 'avgTimeOnPage', 'entrances', 'exitRate', 'exits', 'visitBounceRate', 'percentNewVisits');

	while (!empty($metrics)) {
	        $metrics2 = array_slice($metrics, 0, 10);
	        $metrics = array_slice($metrics, 10);

	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics2, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
	foreach ($metrics2 as $metric) {
		$func = 'get'.ucfirst($metric);
		$value = $this->ga->$func();
		switch ($metric) {
			case 'avgTimeOnPage':
			case 'avgTimeOnSite':
				$total->$metric = sprintf('%02d:%02d', $value / 60, $value % 60);
				break;
			default:
				$total->$metric = $value;
				break;
		}
	}
	}

	$this->fields['total'] = $total;
    }

    private function fetchDaily() {
	$filter = null;

	$dimensions = array('date');
	$sort_metric = 'date';

	$metrics = array('pageviews', 'uniquePageviews', 'visitors', 'newVisits', 'avgTimeOnSite', 'entrances', 'exitRate', 'exits');
	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate);

	$this->dailyResults = $this->ga->getResults();
    }

    private function fetchTopVisited() {
	$filter = null;

	$dimensions = array('pagePath');
	$metrics = array('pageviews', 'uniquePageviews', 'visitors', 'newVisits', 'avgTimeOnPage', 'entrances', 'exitRate', 'exits', 'percentNewVisits', 'visitBounceRate');
	$sort_metric = '-pageviews';

	$start_index = 1;
	$max_results = 20;

	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
	$this->topVisited = $this->ga->getResults();
	//print_r($this->topVisited);die();
    }

    private function fetchTopSources() {
	$filter = null;

	$dimensions = array('source');
	$metrics = array('visitors', 'pageviews', 'avgTimeOnSite', 'entrances', 'exitRate', 'exits', 'percentNewVisits', 'visitBounceRate', 'newVisits');
	$sort_metric = '-pageviews';

	$start_index = 1;
	$max_results = 20;

	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
	$this->topSources = $this->ga->getResults();
    }

    private function fetchTopKeywords() {
	$filter = null;

	$dimensions = array('keyword');
	$metrics = array('visitors', 'pageviews', 'avgTimeOnSite', 'entrances', 'exitRate', 'exits', 'percentNewVisits', 'visitBounceRate');
	$sort_metric = '-pageviews';

	$start_index = 1;
	$max_results = 20;

	$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
	$this->topKeywords = $this->ga->getResults();
    }

    private function fetchHistogram() {
	$this->max = array();
	$this->histogram = array();

	$filtering = array(
		'0-10 seconds' => '11',
		'11-30 seconds' => '31',
		'31-60 seconds' => '61',
		'61-180 seconds' => '181',
		'181-600 seconds' => '601',
		'601-1800 seconds' => '1801',
		'1801+ seconds' => '999999',
	);

	$dimensions = array('visitLength');
	$metrics = array('visitors', 'pageviews');

	$sort_metric = null;

	$start_index = 1;
	$max_results = 10;

	$lastMax = -1;
	foreach ($filtering as $caption => $max) {
		$filter = "timeOnSite > ".$lastMax." && timeOnSite < ".$max;
		$lastMax = $max - 1;
		$this->ga->requestReportData($this->ga_profile_id, $dimensions, $metrics, $sort_metric, $filter, $this->startDate, $this->endDate, $start_index, $max_results);
		$this->histogram[$caption] = array();
		foreach ($metrics as $metric) {
			$func = 'get'.ucfirst($metric);
			$value = $this->ga->$func();
			if (empty($this->max[$metric]) || $this->max[$metric] < $value) $this->max[$metric] = $value;
			$this->histogram[$caption][$metric] = $value;
		}
	}

/*
echo '<pre>';
print_r($this->ga->getResults());
echo '</pre>';
*/
    }
    private function fetchVisitsFlow() {
		//print_r($this);
		$dimensions = array('pagePath', 'previousPagePath', 'pageDepth');
		$metrics = array('pageviews', 'exits');
		$sort_metric = '-pageviews';
		$filter = '';
		$start_index = 1;
		$max_results = 200;

		$nodes = array();
		$links = array();

		$nodeCnt = 0;
		$nodeIdToCnt = array();

		$this->ga
				->requestReportData($this->ga_profile_id, $dimensions,
						$metrics, $sort_metric, $filter, $this->startDate,
						$this->endDate, $start_index, $max_results);
		$results = $this->ga->getResults();

		$flowAnalytics = new FlowAnalytics($results);
		$this->alluvialJson = $flowAnalytics->getJson();
//		print_r($this);

	}

    public function chart($field, $results = null) {
	if (empty($results)) $results = $this->dailyResults;

	$func = 'get'.ucfirst($field);
	$chart = array();
	foreach($results as $result) {
	    $dateStr = (string)$result;
	    $date = 'new Date('.substr($dateStr, 0, 4).', '.(substr($dateStr, 4, 2)-1).', '.substr($dateStr, 6, 2).')';
	    $chart[] = array($date, $result->$func());
	}
	return $chart;
    }

    private function createReport() {
	$this->ga = new gapi($this->ga_email, $this->ga_password);
	$this->ga->apiKey = $this->apiKey;
	$this->fetchTotal();
	$this->fetchInfo();
	$this->fetchDaily();
	$this->fetchTopVisited();
	$this->fetchTopSources();
	$this->fetchTopKeywords();
	$this->fetchHistogram();
	$this->fetchVisitsFlow();
    }

    public function html() {
	$this->createReport();
	return $this->twig->render('report.twig.html', $this->fields);
    }

    public function pdf($fileName = null) {

	$this->createReport();

	$temp = tempnam(sys_get_temp_dir(), 'ga_');
	@unlink($temp);
	$pdf = $temp.'.pdf';
	//$pdf = $this->ga_filename . '.pdf';
	$temp = $temp.'.html';

	if (!empty($fileName)) {
		$pdf = $fileName;
	}

	file_put_contents($temp, $this->twig->render('report.twig.html', $this->fields));

//	echo PATH_TO_PHANTOMJS.' rasterize.js '.$temp.' '.$pdf.' A4 '.$this->fields['logoPath']."\n";
	exec(PATH_TO_PHANTOMJS.' rasterize.js '.$temp.' '.$pdf.' A4 '.$this->fields['logoPath']);
//	exec(PATH_TO_PHANTOMJS.' rasterize.js '.  __DIR__ .'/temp/' .$this->ga_filename . '.pdf'.' A4 '.$this->fields['logoPath'] . ' 2>&1', $error);
	//print_r($error);
//	@unlink($temp);
	copy($pdf, __DIR__ .'/temp/' .$this->ga_filename . '.pdf');
	
	
	// DELETED BY NIGEL
	/*
	if (empty($fileName)) {
	        header('Content-type: application/pdf');
	        header('Content-Disposition: attachment; filename="'.$this->clientName.'_'.$this->startDate.'_'.$this->endDate.'.pdf"');
		echo file_get_contents($pdf);
		@unlink($pdf);
	}
	*/
	@unlink($pdf);
    }
}
