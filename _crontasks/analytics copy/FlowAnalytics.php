<?php

class FlowAnalytics {

    private $levels = array();

    public function __construct($results) {
        $this->results = $results;

        $this->levels[0] = $this->calcLevel();
        $this->levels[1] = $this->calcLevel($this->levels[0]);
//        unset($this->levels[0]);
/*
echo '<pre>';
print_r($this->levels);
echo '</pre>';

exit;
*/
        $this->levels[2] = $this->calcLevel($this->levels[1]);
//        unset($this->levels[1]);
        $this->levels[3] = $this->calcLevel($this->levels[2]);
//        unset($this->levels[2]);
    }

    private function calcLevel($prevLevel = array()) {
        $prevPaths = array();
        foreach ($prevLevel as $url => $row) {
            foreach ($row['paths'] as $path) $prevPaths[$path] = $url;
        }

        $level = array();

        $results = $this->results;
        $results2 = array();

        if (empty($prevLevel)) {
            foreach ($results as $row) {
                if ($row->getPreviousPagePath() == '(entrance)') {
                    $results2[] = $row;
                }
            }
        } else {
            foreach ($results as $row) {
                $prevPath = self::removeQuery($row->getPreviousPagePath());
                $url = self::removeQuery($row->getPagePath());

                if ($url == $prevPath) continue;
                if (isset($prevLevel[$url])) continue;

                if (isset($prevPaths[$prevPath])) {
                    $prev = $prevLevel[$prevPath];
                    $depth = $row->getPageDepth();

                    if ($depth > 5 + $prev['depth']) continue;
                    if ($prev['depth'] >= $depth) continue;

                    $results2[] = $row;
                }
            }
        }

/*
echo '<pre>';
print_r($results2);
echo '</pre>';
*/
        foreach ($results2 as $row) {
            $prevPath = self::removeQuery($row->getPreviousPagePath());
            $views = $row->getPageviews();

            $source = $prevPath;
            if (!empty($prevPaths[$prevPath])) $source = $prevPaths[$prevPath];

            $level = $this->addToLevel($level, $row, $source, $views);
        }


        $level = $this->aggregateLevel($level);

        return $level;
    }

    private function addToLevel($level, $row, $source, $pathCount) {
        $url = self::removeQuery($row->getPagePath());
        $path = $url;

        if (!empty($level[$path])) {
            $arr = $level[$path];
        } else {
            $arr = array();
            $arr['paths'] = array();
            $arr['srcs'] = array();
            $arr['depth'] = 9999999;
            $arr['views'] = 0;
            $arr['exits'] = 0;
        }

        $arr['path'] = $url;
        $arr['paths'][$url] = $url;
        if ($arr['depth'] > $row->getPageDepth()) $arr['depth'] = $row->getPageDepth();
        $arr['views'] += $row->getPageviews();
        $arr['exits'] += $row->getExits();

        if ($source != '(entrance)') {
            if (empty($arr['srcs'][$source])) {
                $arr['srcs'][$source] = 0;
            }
            $arr['srcs'][$source] += $pathCount;
        }

        $level[$path] = $arr;

        return $level;
    }

    private static function cmpViews($a, $b) {
        return -($a['views'] - $b['views']);
    }

    private function aggregateLevel($level) {
        uasort($level, array($this, 'cmpViews'));

        $level2 = array();
        foreach ($level as $url => $row) {
            if (sizeof($level2) < 5) {
                $level2[$url] = $row;
                continue;
            }

            $aggrUrl = '(other)';
            if (!isset($level2[$aggrUrl])) {
                $row['path'] = $aggrUrl;
                $level2[$aggrUrl] = $row;
                continue;
            }

            $aggrRow = $level2[$aggrUrl];
            $aggrRow['views'] += $row['views'];
            $aggrRow['exits'] += $row['exits'];
            foreach ($row['paths'] as $k) { $aggrRow['paths'][$k] = $k; }
            if ($aggrRow['depth'] > $row['depth']) $aggrRow['depth'] = $row['depth'];

            $level2[$aggrUrl] = $aggrRow;
        }

        return $level2;
    }

    private static function removeQuery($path) {
        if (strpos($path, '?') !== false) {
            return substr($path, 0, strpos($path, '?'));
        }
        return $path;
    }

    private static function cmpLinks($a, $b) {
        return -($a['value'] - $b['value']);
    }

    public function getJson() {
        $times = array();
        $links = array();

        foreach ($this->levels as $depth => $level) {
            $time = array();

            $levelLinks = array();
            foreach ($level as $url => $row) {
                $id = $depth.';'.$url;
                $nodeName = $url;
                $nodeValue = $row['views'];

                $displayName = self::shorten($nodeName);
                if ($nodeName == '(other)') {
                    $displayName = '(+'.count($row['paths']).' more pages)';
                }
                $time[] = array('id' => $id, 'nodeName' => $nodeName, 'nodeValue' => $nodeValue, 'exits' => $row['exits'], 'short' => $displayName);

                if (!empty($row['srcs'])) {
                    foreach ($row['srcs'] as $source => $count) {
                        $idSource = ($depth - 1).';'.$source;
                        $levelLinks[] = array('source' => $idSource, 'target' => $id, 'value' => $count);
                    }
                }
            }

            usort($levelLinks, array($this, 'cmpLinks'));

            foreach ($levelLinks as $cnt => $link) {
                if ($cnt > 6) break;
                $links[] = $link;
            }

            $times[$depth] = $time;
        }

        return json_encode(array( 'times' => $times, 'links' => $links ));
    }

    private static function shorten($text) {
        if (strlen($text) > 9 + 3 + 9) {
            $text = substr($text, 0, 9).'...'.substr($text, -9);
        }
        return $text;
    }

}
