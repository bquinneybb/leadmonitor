var page = require('webpage').create(), system = require('system'), address, output, size;

if (system.args.length < 3 || system.args.length > 6) {
	console.log('Usage: rasterize.js URL filename');
	phantom.exit(1);
} else {
	address = system.args[1];
	output = system.args[2];
	page.viewportSize = {
		width : 2000,
		height : 2000
	};
	
	page.open(address, function(status) {
		if (false && status !== 'success') {
			console.log(status);
			console.log('Unable to load the address!');
			phantom.exit();
		} else {
			window.setTimeout(function() {
				if (output != 'graph.png') {
					page.zoomFactor = 2;
				}

				var height = page.evaluate(function() {
					return document.getElementById('renderArea').offsetHeight;
				});
				var width = page.evaluate(function() {
					return document.getElementById('renderArea').offsetWidth;
				});

				page.clipRect = {
					top : 0,
					left : 0,
					width : width * page.zoomFactor,
					height : height * page.zoomFactor
				};
				
				page.render(output);
				phantom.exit();
			}, 1000);
		}
	});
}
