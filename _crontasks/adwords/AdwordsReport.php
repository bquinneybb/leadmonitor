<?php

$path = dirname(__FILE__) . '/vendor/google/adwords/src';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
define('PATH_TO_PHANTOMJS', '/usr/local/bin/phantomjs');
//define('PATH_TO_PHANTOMJS', __DIR__ . '/bin/phantomjs');

class AdwordsReport {

	public $monetaryMultip = 1.15;
	
	public function __construct() {
	}

	public function setDates($startDate, $endDate) {
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		$this->startTimestamp = strtotime($this->startDate);
		$this->endTimestamp = strtotime($this->endDate);
		
		$this->begin = date("Ymd", $this->startTimestamp);
		$this->end = date("Ymd", $this->endTimestamp);
	}

	
	public function auth($email, $password, $developerToken, $clientId) {
		$applicationToken = ""; //
		$userAgent = "curl-tutorial";
		
		$this->user = new AdWordsUser(NULL, $email, $password, $developerToken, $applicationToken, $userAgent, $clientId);
		//print_r($this->user);die();
	}
	
	public function totals() {
		$totals = array();
	
		$path = null;
		$options = array();
		$options['returnMoneyInMicros'] = true;
		
		$selector = new Selector();
		$selector->dateRange = new DateRange($this->begin, $this->end);
		$selector->fields = array(
				'AdNetworkType1',
				'Clicks', 'Impressions', 'AveragePosition',
				'Ctr', 'Cost', 'Conversions', 'AverageCpc',
				'Amount');
		
		$reportDefinition = new ReportDefinition();
		
		$reportDefinition->selector = $selector;
		$reportDefinition->reportName = 'Campaign Performance Report';
		$reportDefinition->reportType = 'CAMPAIGN_PERFORMANCE_REPORT';
		$reportDefinition->dateRangeType = 'CUSTOM_DATE';
		$reportDefinition->downloadFormat = 'CSV';
		
		$res = ReportUtils::DownloadReport(
				$reportDefinition, $path, $this->user, $options);
		$entries = explode("\n", $res);
		
		$cnt = 0;
		foreach ($entries as $entry) {
// 			echo "<pre>";
// 			print_r($entry);
// 			echo "</pre>";
			
			$cnt++;
			if ($cnt < 3) continue;
			if (empty($entry)) continue;
			$entry = explode(",", $entry);
		
			$day = array();
			$total['type'] = $entry[0];
			$total['clicks'] = $entry[1];
			$total['impressions'] = $entry[2];
			$total['averagePosition'] = $entry[3];
			$total['ctr'] = $entry[4] / 100;
			$total['cost'] = $this->calcCost($entry[5]);
			$total['conversions'] = $entry[6];
			$total['averageCpc'] = $this->calcCost($entry[7]);
			$total['budget'] = $this->calcCost($entry[8]);
				
			$totals[$total['type']] = $total;
		}
		
		return $totals;
	}
	
	public function campaigns($order = 'Clicks') {
		$campaigns = array();
		
		$campaignService = $this->user->GetCampaignService();
		
		$selector = new Selector();
		$selector->dateRange = new DateRange($this->begin, $this->end);
		$selector->fields = array('Id', 'Name', 
				'TotalBudget', 'Clicks', 'Impressions', 
				'Ctr', 'Cost', 'AverageCpc', 'AveragePosition',
				'Conversions');
		$selector->ordering[] = new OrderBy($order, 'DESCENDING');
		$selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);
		
		$page = $campaignService->get($selector);
		
		foreach ($page->entries as $entry) {
// 			echo "<pre>";
// 			print_r($entry);
// 			echo "</pre>";
			
			$campaign = array();
			$campaign['name'] = $entry->name;
			$campaign['clicks'] = $entry->campaignStats->clicks;
			$campaign['ctr'] = $entry->campaignStats->ctr;
			$campaign['impressions'] = $entry->campaignStats->impressions;
			$campaign['averagePosition'] = $entry->campaignStats->averagePosition;

			$campaign['conversions'] = $entry->campaignStats->conversions;
			
			$campaign['averageCpc'] = $this->calcCost($entry->campaignStats->averageCpc);
			$campaign['budget'] = $this->calcCost($entry->campaignStats->totalBudget);
			$campaign['cost'] = $this->calcCost($entry->campaignStats->cost);
			
			$campaigns[] = $campaign;
		}

		return $campaigns;
	}
	
	public function adGroups($order = 'Clicks') {
		$adGroups = array();
		
		$adGroupService = $this->user->GetAdGroupService();
		
		$selector = new Selector();
		$selector->dateRange = new DateRange($this->begin, $this->end);
		$selector->fields = array('Id', 'Name', 
				'Clicks', 'Impressions', 
				'Ctr', 'Cost', 'AverageCpc', 'AveragePosition',
				'Conversions');
		$selector->ordering[] = new OrderBy($order, 'DESCENDING');
		$selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);
		
		$page = $adGroupService->get($selector);
		
		foreach ($page->entries as $entry) {
// 			echo "<pre>";
// 			print_r($entry);
// 			echo "</pre>";
			
			$adGroup = array();
			$adGroup['name'] = $entry->name;
			$adGroup['clicks'] = $entry->stats->clicks;
			$adGroup['ctr'] = $entry->stats->ctr;
			$adGroup['impressions'] = $entry->stats->impressions;
			$adGroup['averagePosition'] = $entry->stats->averagePosition;
			
			$adGroup['conversions'] = $entry->stats->conversions;
				
			$adGroup['averageCpc'] = $this->calcCost($entry->stats->averageCpc);
			$adGroup['cost'] = $this->calcCost($entry->stats->cost);
				
			$adGroups[] = $adGroup;
		}
		
		return $adGroups;
	}
	
	public function topAds($order = 'Clicks') {
		$ads = array();
		
		$adGroupService = $this->user->GetAdGroupAdService();
		
		$selector = new Selector();
		$selector->dateRange = new DateRange($this->begin, $this->end);
		$selector->fields = array('Id', 'Name', 
				'Clicks', 'Impressions', 
				'Ctr', 'Cost', 'AverageCpc', 'AveragePosition',
				'Conversions');
		$selector->ordering[] = new OrderBy($order, 'DESCENDING');
		$selector->paging = new Paging(0, 20);
		
		$page = $adGroupService->get($selector);
		
		foreach ($page->entries as $entry) {
// 			echo "<pre>";
// 			print_r($entry);
// 			echo "</pre>";
			
			$adGroup = array();
			$adGroup['headline'] = $entry->ad->headline;
			$adGroup['description1'] = $entry->ad->description1;
			$adGroup['description2'] = $entry->ad->description2;
			$adGroup['url'] = $entry->ad->url;
			$adGroup['displayUrl'] = $entry->ad->displayUrl;
			$adGroup['percentServed'] = $entry->stats->percentServed;
			$adGroup['clicks'] = $entry->stats->clicks;
			$adGroup['ctr'] = $entry->stats->ctr;
			$adGroup['impressions'] = $entry->stats->impressions;
			$adGroup['averagePosition'] = $entry->stats->averagePosition;
			
			$adGroup['conversions'] = $entry->stats->conversions;
				
			$adGroup['averageCpc'] = $this->calcCost($entry->stats->averageCpc);
			$adGroup['cost'] = $this->calcCost($entry->stats->cost);
				
			$ads[] = $adGroup;
		}
		
		return $ads;
	}
	
	public function searchTerms($order = 'Cost') {
		$searchTerms = array();
		
		$path = null;
		$options = array();
		$options['returnMoneyInMicros'] = true;
		
		$selector = new Selector();
		$selector->dateRange = new DateRange($this->begin, $this->end);
		$selector->fields = array(
				'Query', 'MatchType', 'AdGroupName',
				'Clicks', 'Impressions', 'AveragePosition',
				'Ctr', 'Cost', 'Conversions', 'AverageCpc');
		
		$reportDefinition = new ReportDefinition();
		
 		$reportDefinition->selector = $selector;
 		$reportDefinition->reportName = 'Ad Performance Report';
 		$reportDefinition->reportType = 'SEARCH_QUERY_PERFORMANCE_REPORT';
 		$reportDefinition->dateRangeType = 'CUSTOM_DATE';
 		$reportDefinition->downloadFormat = 'CSV';
		
		$res = ReportUtils::DownloadReport(
				$reportDefinition, $path, $this->user, $options);
		$entries = explode("\n", $res);

		$cnt = 0;
		foreach ($entries as $entry) {
			$cnt++;
 			if ($cnt < 3) continue;
 			if (empty($entry)) continue;
			$entry = explode(",", $entry);
			if (trim($entry[1]) == '--') continue;
			
			$searchTerm = array();
			$searchTerm['searchTerm'] = $entry[0];
			$searchTerm['matchType'] = $entry[1];
			$searchTerm['adGroup'] = $entry[2];
			$searchTerm['clicks'] = $entry[3];
			$searchTerm['impressions'] = $entry[4];
			$searchTerm['averagePosition'] = $entry[5];
			$searchTerm['ctr'] = $entry[6] / 100;
			$searchTerm['cost'] = $this->calcCost($entry[7]);
			$searchTerm['conversions'] = $entry[8];
			$searchTerm['averageCpc'] = $this->calcCost($entry[9]);

// 			echo "<pre>";
// 			print_r($entry);
// 			print_r($searchTerm);
// 			echo "</pre>";

			$searchTerms[] = $searchTerm;
		}
		
		switch ($order) {
			case "Clicks":
				usort($searchTerms, array($this, 'cmpClicksDesc'));
				break;
			case "Cost":
				usort($searchTerms, array($this, 'cmpCostDesc'));
				break;
		}
		
		// paging
 		$searchTerms = array_slice($searchTerms, 0, 20);
		
		return $searchTerms;
	}
	
	public function keywords($order = 'Clicks') {
		$keywords = array();
		
		$path = null;
		$options = array();
		$options['returnMoneyInMicros'] = true;
		
		$selector = new Selector();
		$selector->dateRange = new DateRange($this->begin, $this->end);
		$selector->fields = array(
				'KeywordText',
				'Clicks', 'Impressions', 'AveragePosition',
				'Ctr', 'Cost', 'Conversions', 'AverageCpc', 
				'KeywordMatchType', 'ApprovalStatus');
		
		$reportDefinition = new ReportDefinition();
		
		$reportDefinition->selector = $selector;
		$reportDefinition->reportName = 'Keywords Performance Report';
		$reportDefinition->reportType = 'KEYWORDS_PERFORMANCE_REPORT';
		$reportDefinition->dateRangeType = 'CUSTOM_DATE';
		$reportDefinition->downloadFormat = 'CSV';
		
		$res = ReportUtils::DownloadReport(
				$reportDefinition, $path, $this->user, $options);
		$entries = explode("\n", $res);
		
		$cnt = 0;
		foreach ($entries as $entry) {
			$cnt++;
 			if ($cnt < 3) continue;
			if (empty($entry)) continue;
			$entry = explode(",", $entry);
			if (trim($entry[8]) == '--') continue;
			if (trim($entry[9]) == '--') continue;
				
			$keyword = array();
			$keyword['keyword'] = $entry[0];
			$keyword['clicks'] = $entry[1];
			$keyword['impressions'] = $entry[2];
			$keyword['averagePosition'] = $entry[3];
			$keyword['ctr'] = $entry[4] / 100;
			$keyword['cost'] = $this->calcCost($entry[5]);
			$keyword['conversions'] = $entry[6];
			$keyword['averageCpc'] = $this->calcCost($entry[7]);
		
// 			echo "<pre>";
// 			print_r($entry);
//  		print_r($keyword);
// 			echo "</pre>";
		
			$keywords[] = $keyword;
		}
		
		switch ($order) {
			case "Clicks":
				usort($keywords, array($this, 'cmpClicksDesc'));
				break;
			case "Cost":
				usort($keywords, array($this, 'cmpCostDesc'));
				break;
		}
		
		// paging
		$keywords = array_slice($keywords, 0, 20);
		
		return $keywords;
	}

	public function days() {
		$days = array();
		
		$path = null;
		$options = array();
		$options['returnMoneyInMicros'] = true;
		
		$selector = new Selector();
		$selector->dateRange = new DateRange($this->begin, $this->end);
		$selector->fields = array(
				'Date',
				'Clicks', 'Impressions', 'AveragePosition',
				'Ctr', 'Cost', 'Conversions', 'AverageCpc');
		
		$reportDefinition = new ReportDefinition();
		
		$reportDefinition->selector = $selector;
		$reportDefinition->reportName = 'Click Performance Report';
		$reportDefinition->reportType = 'ACCOUNT_PERFORMANCE_REPORT';
		$reportDefinition->dateRangeType = 'CUSTOM_DATE';
		$reportDefinition->downloadFormat = 'CSV';
		
		$res = ReportUtils::DownloadReport(
				$reportDefinition, $path, $this->user, $options);
		$entries = explode("\n", $res);
		
		$cnt = 0;
		foreach ($entries as $entry) {
// 			echo "<pre>";
// 			print_r($entry);
// 			echo "</pre>";
			
			$cnt++;
			if ($cnt < 3) continue;
			if (empty($entry)) continue;
			$entry = explode(",", $entry);
			if (trim($entry[0]) == 'Total') continue;
		
			$day = array();
			$day['date'] = $entry[0];
			$day['clicks'] = $entry[1];
			$day['impressions'] = $entry[2];
			$day['averagePosition'] = $entry[3];
			$day['ctr'] = $entry[4] / 100;
			$day['cost'] = $this->calcCost($entry[5]);
			$day['conversions'] = $entry[6];
			$day['averageCpc'] = $this->calcCost($entry[7]);
		
			$days[] = $day;
		}
		
		return $days;
	}

	public function png($reportName) {
		$temp = tempnam(sys_get_temp_dir(), 'ga_');
		@unlink($temp);
		$png = $reportName . '.png';
		$temp = $temp . '.html';
		
		$fields = array();
		if ($reportName == 'graph') {
			$fields['days'] = $this->days();
		 	$fields['output'] = $reportName;
		} else {
			$fields['totals'] = $this->totals();
			$fields[$reportName] = $this->$reportName();
 			$fields['output'] = $reportName;
		}

 		$loader = new Twig_Loader_Filesystem(__DIR__.'/tmpl');
 		$twig = new Twig_Environment($loader);
		
		file_put_contents($temp, $twig->render('main.twig.html', $fields));
		
	//	echo PATH_TO_PHANTOMJS.' rasterize.js '.$temp.' '.$png."\n";
		exec(PATH_TO_PHANTOMJS . ' rasterize.js ' . $temp . ' ' . $png);
// 		@unlink($temp);
	}
	
	private function cmpCostDesc($a, $b) {
		return -($a['cost'] - $b['cost']);
	}
	
	private function cmpClicksDesc($a, $b) {
		return -($a['clicks'] - $b['clicks']);
	}
	
	private function calcCost($obj) {
		if (isset($obj->microAmount)) $obj = $obj->microAmount;
		return round(($this->monetaryMultip * ($obj / 1000000)),2);
	}
	
}
