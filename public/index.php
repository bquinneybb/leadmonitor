<?php
ob_start("ob_gzhandler");
ob_implicit_flush(0);
error_reporting(E_ALL);

if( $_SERVER['REMOTE_ADDR'] != '202.126.96.74'){
	//die();
}

//ini_set('display_errors','On');
ini_set('display_errors','Off');
define('ROOT', dirname(dirname(__FILE__)));

/**
 * CORE INCLUDES
 */
include (ROOT . '/_config/config.php');

//require_once (ROOT . '/shared/helpers.php');




// this will divert to the appropriate controller
function callHook(){

	global $url,$subcontentType, $subcontentId, $reservedControllers;

	// reserved controllers



	$urlArray = array();
	$urlArray = explode("/",$url);



	$controller = $urlArray[0];
	$action = $urlArray[1];

	$queryString = $urlArray;

	// if the last component is blank, delete it
	if($queryString[count($queryString)-1] == ''){
		unset($queryString[count($queryString)-1]);

	}
	//print_r($queryString);
	$subcontentId = $queryString[count($queryString)-1];

	if(count($queryString) > 2){

		for($i=2;$i<count($queryString);$i++){

			if($queryString[$i] != ''){

				$parameters[] = $queryString[$i];
			}
		}
		//$subcontentId = $queryString[count($queryString)-1];
	}

	// we are accessing offset 0 and 1 all the time, so set them to avoid trap errors
	if(!isset($parameters[0])){

		$parameters[0] = '';
	}
	if(!isset($parameters[1])){

		$parameters[1] = '';
	}

	if(!isset($parameters[2])){

		$parameters[2] = '';
	}


	$model = ucwords($controller);


	$controllerName = ucwords($controller.'Controller');


	if($controller == 'default'){
		$controller = 'default';
		$controllerName = ucwords('Controller');
		$model = 'Model';

	}

	// we need some custom model overrides for clean public urls without needing for logins
	// so we list the contrller and the default action we want it to take
	$publicOverrides = array('content'=>'index', 'video'=>'dashboard');



	if(isset($publicOverrides[$controller])){
			
		$action = $publicOverrides[$controller];
			
	}




	if (method_exists($controllerName, $action)) {
		$dispatch = new $controllerName($model, $controller, $action);
		call_user_func_array(array($dispatch,$action),array($queryString,$parameters));

	} else {
		/* Error Generation Code Here */
		header('location: /content/index/');die();
		//echo 'THIS CONTROLLER DOESN\'T EXIST :/ ' . $controllerName ;die();
	}


}

/**
 * THIRD PARTY INCLUDES WHICH WE'LL USE A LOT
 */


$url = isset($_GET['url']) ? $_GET['url'] : null;

if($url == null){
	header('location: http://www.barkingbird.com.au'); die();
	header('location: /content/index/');die();
}
/*
 $subcontentType = isset($_GET['subcontentType']) ? $_GET['subcontentType'] : null;
 $subcontentId = isset($_GET['subcontentId']) ? $_GET['subcontentId'] : null;
 */



// clean any urls
foreach($_GET as $get){

	$_GET[$get] = escapeshellarg($get);
}
foreach($_POST as $post){

	$_POST[$post] = escapeshellarg($post);
}

$session = new SessionManager();
session_start();




callHook();

print_gzipped_page();
//ob_flush();

?>