/* LAST MODIFIED 24/02/2012
 * 
 * 
 */
;(function($) {

	$.uaSlider = function(element, options) {

		var defaults = {
			menuClass : '',
			slideContainer : '',
			slideClass : '',
			captionContainer : ''
		}

		var plugin = this;

		plugin.settings = {}

		var $element = $(element), element = element;

		plugin.init = function() {
			plugin.settings = $.extend({}, defaults, options);
			offsets = new Array();
			// code goes here
			bindMenu();
			calculateInitialWidths();
			setIntialCaption();
		}
		var bindMenu = function() {

			$(plugin.settings.menuClass + ' a').each(function() {

				if(!$(this).hasClass('external')) {

					$(this).bind('click', function() {

						moveSlideTo($(this).data('slider'));

					});
				}

			});
		}
		var calculateInitialWidths = function() {

			var i = 0;
			var totalWidth = 0;
			$(plugin.settings.slideClass).each(function() {
				totalWidth += $(this).width();

			});

			$(plugin.settings.slideContainer).width(totalWidth);

			//    console.log(offsets);
		}
		var setIntialCaption = function() {
			//alert($(plugin.settings.slideContainer + ' img').eq(0).attr('title'));
			$(plugin.settings.captionContainer).html($(plugin.settings.slideContainer + ' img').eq(0).attr('title'));
		}
		var moveSlideTo = function(whichSlider) {
			var i = 0;
			var totalWidth = 0;
			var found = false;
			var caption = '';

			$(plugin.settings.slideClass).each(function() {
				//	alert($(plugin.settings.slideClass + ' img').eq(i).attr('rel'));

				if(!found && i > 0) {
					totalWidth += $(this).width();

				}
				if($(plugin.settings.slideClass + ' img').eq(i).attr('rel') == whichSlider) {
					found = true;
					caption = $(plugin.settings.slideClass + ' img').eq(i).attr('title');
				}
				i++;
			});
			//alert(totalWidth);
			$(plugin.settings.slideContainer).stop(true).animate({

				left : (totalWidth * -1) + 'px'

			}, 500);
			//alert(caption);
			$(plugin.settings.captionContainer).stop(true).fadeTo(500, 0, function() {

				$(plugin.settings.captionContainer).html(caption);
			}).fadeTo(500, 1);

			//alert(totalWidth);
		}

		plugin.init();

	}

	$.fn.uaSlider = function(options) {

		return this.each(function() {
			if(undefined == $(this).data('uaSlider')) {
				var plugin = new $.uaSlider(this, options);
				$(this).data('uaSlider', plugin);
			}
		});
	}
})(jQuery);
