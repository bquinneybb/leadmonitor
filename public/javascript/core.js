/* CORE */
$(document).ready(function(){

	$('#dump').html('')

	
	$.prettyLoader();
	
	if($('.modalimage'.length > 0)){
		
		$('.modalimage').fancybox();
	}

	// add style to any items that need extra padding
	$('.subMenuItems li').wrapInner('<div class="subMenuIndent" />');
	$('.moduleMenuItems li').wrapInner('<div class="moduleMenuIndent" />');


	$('.formTable tr:odd').addClass('alt');

	
	// fade the update messages
	$('.formsave').fadeTo(2000, 0);

});


function dump(string){
	$('#dump').append(string + '<br >');
}

/* NAVIGATION */

$(document).ready(function(){
	
	$('.moduleMenuItems li').live('mouseenter', function(){
		
		$(this).addClass('moduleMenuHover');
	
	});
	
		$('.moduleMenuItems li').live('mouseleave', function(){
		
		$(this).removeClass('moduleMenuHover');
	
	});
	
		$('.subMenuItems li').live('mouseenter', function(){
		
		$(this).addClass('subMenuHover');
	
	});
	
		$('.subMenuItems li').live('mouseleave', function(){
		
		$(this).removeClass('subMenuHover');
	
	});
	/*
	$('.formTable').tablesorter().addClass('tablesorter').bind('sortEnd',function() { 
       $('.formTable tr').removeClass('alt');
       $('.formTable tr:odd').addClass('alt');
    }).tablesorterFilter({ filterContainer: $("#filterBoxOne"),
       
        filterColumns: [0, 1],
        filterCaseSensitive: false
	
    });
    */
	$('.formTable th').live('mouseenter', function(){
		
		$(this).addClass('hover');
	
	});
	
		$('.formTable th').live('mouseleave', function(){
		
		$(this).removeClass('hover');
	
	});
	
	
	
});

/* Jquery-ify Forms */

jQuery.fn.checkboxify = function() {
	return this.each(function() {
		jQuery(this).data('checked', jQuery(this).is(':checked'));
		
		jQuery(this).click(function(){
			var radio = jQuery(this);
			
			if (radio.data('checked')) {
				radio.attr('checked', false).data('checked', false);
			} else {
				jQuery('input[name="' + radio.attr('name') + '"]', radio.parent()).attr('checked', false).data('checked', false);
				radio.attr('checked', true).data('checked', true);
			}
			
			
		
		});
		
	});
}

/* These check a certain criteria on form items */

function checkboxCheckAtLeastOneByClass(className){
	
	var success = false;

	 jQuery.each($('.' + className), function(){
		
		var checked = $(this).attr('checked');
	
		if(checked){
			
			success =  true;
			
		}
	
	})
	
	return success;

}

function radioCheckAtLeastOneByName(name){

	var success = false;

	 jQuery.each($('input[name=' + name + ']'), function(){
		
		var checked = $(this).attr('checked');
	
		if(checked){
			
			success =  true;
			
		}
	
	})
//	alert(success);
	return success;
	
}

/* these perform a task on a form group */
function checkboxDoSelectAllByClass(className){

	jQuery.each($("." + className), function(){
		
		$(this).attr('checked', true);	
	
	})

}

function checkboxDoUnselectAllByClass(className){

	jQuery.each($("." + className), function(){
		
		$(this).attr('checked', false);	
	
	})

}

/* these perform a task on an individual form item  */
function checkboxUnselectOneById(id){

	$('#' + id).attr('checked', false);

}


/* show a dialogue */

function showDialogue(text){
	
	alert(text);
}

/* miscellaneous helper classes */

function returnFormattedDate(inputString){

	// return nice date and time 

	 var datestr = inputString;
     var dateOb = Date.parse(datestr);
     var newDateStr = dateOb.toString('ddd, MMM d, yyyy');
     
	//alert(newDateStr);
	return newDateStr;
	
}

function humanTimeToDatepicker(datePicker){
//alert(datePicker.length);break;

	if(datePicker != ''){
		var datePickerString =  datePicker[6] + datePicker[7] + datePicker[8] + datePicker[9] + '/' + datePicker[3] + datePicker[4] + '/' + datePicker[0] + datePicker[1];
		
	
		
		if(datePicker.length > 9){
		
		for(var i=10;i < datePicker.length;i++){
		
			
			datePickerString += datePicker[i];
		}
		
		} else {
			
			datePickerString(Date.parse('now'));
		}
	
	}
	//alert(datePicker);
	return datePickerString;

}

/* AUTO SAVING */
function throttle(fn, delay) {
  var timer = null;
  return function () {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(context, args);
    }, delay);
  };
}
function showSaveMessage(fieldId){
	$('#' + fieldId).css('opacity', 1);
	$('#' + fieldId).html('SAVED / UPDATED').addClass('error').fadeTo(1000, 0);
	
}
/* File upload */


/* INTERFACE NICETIES */

function showPreloadImageById(id, topMargin){
	
	marginTop = returnTopMargin(topMargin);
	
	var $target = $('#' + id);
	$target.css('display', 'none');
	$target.wrap(returnPreloadWrapper('preload_' + id));
	$('#preload_' + id).append(returnPreloadImage(marginTop));
	//
	$target.load(function() { 
	
		$('#preload_' + id + ' .generic_preloaderImage').css('display', 'none');
		$target.fadeTo(500,1);	
		
	});
	

	//
	
}

function showPreloadImageByClass(imageClass, topMargin){
	
	
	marginTop = returnTopMargin(topMargin);
	var i=0;
	
	$('.' + imageClass + ' img').each(function(){
	
		var id = 'generic_preloaderWrapper_' + i;
		
		var $target = $(this);
		$target.css('display', 'none');
		
		
		$target.wrap(returnPreloadWrapper(id));
		$('#' + id).append(returnPreloadImage(marginTop));

	
		i++;
	});
	
	$(window).load(function(){
	
		$('.' + imageClass + ' img').each(function(){
	
		
		var $target = $(this);
		
		$target.fadeTo(500,1);	
		});
			$('.generic_preloaderImage').css('display', 'none');
	});
	
	}
	
	
	

function returnTopMargin(topMargin){
	
	var marginTop = 20;
	
	if(topMargin != undefined){
		
		marginTop = topMargin;
	}
	
	return marginTop; 
}

function returnPreloadWrapper(id){
	
	return '<div class="generic_preloader" id="' + id + '"></div>';
}

function returnPreloadImage(marginTop){

	return '<img src="' + SITE_PATH + '/public/template-core/interface/loading-icon-white.gif" class="generic_preloaderImage" style="margin-top:' + marginTop + 'px" />'

}
/* MISC HELPERS */

function itemRGBToHex(rgbString){

	var parts = rgbString.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	
	
	delete (parts[0]);
	for(var i=1;i<=3;i++){
	    parts[i] = parseInt(parts[i]).toString(16);
	    if (parts[i].length == 1) parts[i] = '0' + parts[i];
	}
	var hexString = parts.join('');
	
	return hexString;

}