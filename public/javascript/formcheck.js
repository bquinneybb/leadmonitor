/* LAST MODIFIED 24/02/2012
 * 
 * 
 */
;(function($) {

    $.formCheck = function(element, options) {

        var defaults = {
           
        }

        var plugin = this;

        plugin.settings = {}

        var $element = $(element),
             element = element;

		formValid = true;
	errorMessage = '';

        plugin.init = function() {
            plugin.settings = $.extend({}, defaults, options);
            bindform();
            // code goes here
        }

      

       
		
		
		 var bindform = function(){
       	
      		$element.submit(function(e){
      			 
      			 checkform();
      			
      			if(formValid){
      				
      				return true;
      				
      			} else {
      				e.preventDefault();
      				alert(errorMessage);
      				return false;
      			}
      			
      		});
      		
      		  $('input').each(function(){
      		  	
      		  	if($(this).hasClass('required')){
      		  		
      		  		$(this).bind('keyup', function(){
      		  			
      		  			if($(this).val() != ''){
				
				$(this).removeClass('error');
				
			}
      		  			
      		  		});
      		  		
      		  	};
      		  	
      		  });
       	
       }

        var checkform = function() {
          errorMessage = '';
          $('input').each(function(){
          	
          		$(this).removeClass('error');
          	
          		if($(this).hasClass('required')){
          			
          			if($(this).val() == ''){
          				$(this).addClass('error');
          				if(errorMessage == ''){
          				errorMessage += 'Please fill in all fields marked with an *';
          				}
          				formValid = false;
          			}
          			
          			if($(this).hasClass('email')){
          				var validEmail = isValidEmailAddress($(this).val());
          			
          				if(!validEmail){
          						$(this).addClass('error');
          					formValid = false;
          					if(errorMessage != ''){
          						
          						errorMessage += "\n";
          					}
          					
          					errorMessage += 'Please enter a valid email address';
          					
          				}
          			}
          			
          		}
          });
          
        }
		
		
		
		 plugin.init();
		
    }

	var  isValidEmailAddress = function(emailAddress) {
 		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
 		return pattern.test(emailAddress);
	}

    $.fn.formCheck = function(options) {

        return this.each(function() {
            if (undefined == $(this).data('formCheck')) {
                var plugin = new $.formCheck(this, options);
                $(this).data('formCheck', plugin);
            }
        });

    }

})(jQuery);