/* SEARCH AND MISC HELPERS */

function showNotFoundAddress(){
	
		
		alert('The address couldn\'t be found. Please check the address or manually place the Estate Marker');
		
	
	
}

function setInitialView(zoom){
	
	if(zoom == undefined){
		
		zoom = 15;
	}
	
	// if there's nothing set, zoom in on the Client site
	scrollMap(clientOfficeLatitude,clientOfficeLongitude, zoom);

}

function scrollMap(latitude, longitude, zoom){
	
if(zoom == undefined){
		
		zoom = 10;
	}
	
	 $.goMap.setMap({latitude:latitude, longitude:longitude, zoom:zoom}); 
}

function findCurrentMapCentre(){
	
	var cords = $.goMap.map.getCenter().toUrlValue(6).split(',');
	
	
	return cords; 
}