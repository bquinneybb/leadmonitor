;(function($) {

	/* LAST EDIT 12/02/22 */

	/* TODO
	 * clear filter after fade to stop grungey effect
	 * preloader image
	 * why are there extra arrows showing up in the background?
	 * preloader doesn't really do anything...
	 *
	 *
	 * editing line 685
	 */

	$.uaGallery = function(element, options) {

		var defaults = {
			outerContainer : '',
			innerContainer : '',
			innerContainerMargin : 0,
			outerContainerVisibleWidth : 0,
			
			sliderSpeed : 500,
			centerFirstItem : false,
			dimNonActiveItems : false,
			showSubNavigation : false,
			subNavigationPosition : 'top',
			showThumbnails : false,
			thumbnailDirectory : '',
			equalSizeContainers : false,
			centerContainerContent : true,
			gallerySpeed : 1000,
			rowGap : 10,
			columnGap : 10,
			fixedNumberOfColumns : 0,
			arrowContainer : '',
			allowWrap : true,
			trackNavigation : false
			
		}

		var plugin = this;

		plugin.settings = {}

		var $element = $(element), element = element;

		plugin.init = function() {
			plugin.settings = $.extend({}, defaults, options);
			// define variables to use throughout
			IOS = isIOS();
			
			initialiseDom();

		}
		
		
		
		
		/* PRIVATE METHODS */
		var initialiseDom = function() {
			// create variables for container names
			outerContainer = $('#' + plugin.settings.outerContainer);
			outerContainerHeight = parseInt(outerContainer.css('height'), 10);
			outerContainerVisibleWidth = parseInt(outerContainer.css('width'), 10);
			refreshContainerElements();
			// wrap it for our own uses
			outerContainer.wrap('<div id="uagalleryWrapper" style="position:relative"><div id="uagallery" style="position:relative;width:' + outerContainerVisibleWidth + 'px;height:' + innerContainerHeight + 'px;overflow:hidden" /></div>');
			wrapperContainer = $('#uagallery');
			outerWrapperContainer = $('#uagalleryWrapper');

			// calculate and set what else we need for later on
			findInnerContainersLength();

			// plugin various settings
			sliderSpeed = plugin.settings.sliderSpeed;
			currentPanel = 1;
			totalPanels = innerContainersLength;
			imagePath = findScriptPath();
			dimNonActiveItems = plugin.settings.dimNonActiveItems;
			centerFirstItem = plugin.settings.centerFirstItem;
			equalSizeContainers = plugin.settings.equalSizeContainers;
			centerContainerContent = plugin.settings.centerContainerContent;
			showNavigationArrows = true;
			arrowNavigationTop = plugin.settings.arrowNavigationTop;
			arrowNavigationLeft = plugin.settings.arrowNavigationLeft;
			arrowNavigationRight = plugin.settings.arrowNavigationRight;
			allowWrap = plugin.settings.allowWrap;

			/* AUTO FADER */
			faderPause = plugin.settings.faderPause;
			faderSpeed = plugin.settings.faderSpeed;

			/* THUMBNAILS */
			rowGap = plugin.settings.rowGap;
			columnGap = plugin.settings.columnGap;
			fixedNumberOfColumns = plugin.settings.fixedNumberOfColumns;
			/*
			 showSubNavigation = plugin.settings.showSubNavigation;
			 subNavigationPosition = plugin.settings.subNavigationPosition;
			 showThumbnails = plugin.settings.showThumbnails;
			 thumbnailDirectory = plugin.settings.thumbnailDirectory;
			 */
			/* THIS WILL SOON SET SOME DEFAULTS */
			galleryType = plugin.settings.galleryType;
			
			// do we want to track the gallery clicks
			trackNavigation = plugin.settings.trackNavigation;

			// set up the containers

			switch(galleryType) {

				case 'fader' :
					showSubNavigation = false;
					showThumbnails = false;
					showNavigationArrows = false;
					innerContainerMargin = 0;
					centerContainerContent = true;
					setOuterContainerWidth();
					createFaderGallery();

					break;

				case 'thumbnail' :
					showSubNavigation = false;
					// dummy setting until styles for on states are fixed
					showThumbnails = false;
					thumbnailDirectory = plugin.settings.thumbnailDirectory;
					setOuterContainerWidth();
					createThumbnailNavigation();
					if(showNavigationArrows && !IOS) {

						createArrowNavigation();
						
					} else {
						
						createIOSNavigation();
						
					}

					break;

				case 'clicker' :
					innerContainerMargin = 0;
					showSubNavigation = false;
					showThumbnails = false;
					setOuterContainerWidth();
					
					if(!IOS){
						
						createArrowNavigation();
					
					} else {
						
						createIOSNavigation();
						
					}

					break;

				default :
					// numbered navigation
					showSubNavigation = true;
					subNavigationPosition = plugin.settings.subNavigationPosition;
					showThumbnails = false;

					setOuterContainerWidth();
					createSubNavigation();

					break;
			}
			/* MOVING THIS HERE MAY STUFF UP THE OTHER GALLERIES... */
			outerContainer.css('position', 'absolute').data('moving', 0);
			innerContainer.css('float', 'left').css('margin-right', innerContainerMargin);

			// complete the setup
			// this needs to make some decisions to only show thumbails or numbers

			if(centerFirstItem) {

				setInitialPosition();
			}

			createPreloader();

		}
		
		var findScriptPath = function(){
			
			//http://lead:8888/public/javascript/uagallery/image-gallery.js
		 	var scriptPath = document.scripts['uaGallery'].src;
			var paths = scriptPath.split('/');
			
			var fullPath = '';
			for(var i=0;i<paths.length-1;i++){
				
				fullPath += paths[i] + '/';
			}
			return fullPath + '/images';
			
		}
		var isIOS = function() {

			var ios = false;
			if((navigator.userAgent.indexOf("iPod") > 0) || (navigator.userAgent.indexOf("iPhone") > 0 || navigator.userAgent.indexOf("iPad") > 0)) {
				ios = true;

			}
			
			return ios;

		}
		var createPreloader = function() {

			innerContainer.css('background-image', 'url(' + imagePath + 'loading.gif)').css('background-position', 'center 200px').css('background-repeat', 'no-repeat');
			var timeout = setInterval(function() {
				//   foo();
			}, 500);
		}
		var refreshContainerElements = function() {
			innerContainer = $('.' + plugin.settings.innerContainer);
			innerContainerMargin = plugin.settings.innerContainerMargin;
			innerContainerWidth = innerContainer.width();
			innerContainerTotalWidth = innerContainerWidth + innerContainerMargin;
			innerContainerHeight = innerContainer.height();
			//alert('FOO');
		}
		var findInnerContainersLength = function() {
			// we've created a function in case the dom is later updated
			innerContainersLength = innerContainer.length;

		}
		var setOuterContainerWidth = function() {
			var outerContainerWidth = 0;
			var widestContainer = findWidestContainer();

			innerContainer.each(function() {

				// are we forcing equal size containers - which we'd want for slideshows generally

				if(equalSizeContainers) {

					$(this).width(widestContainer);
					outerContainerWidth += widestContainer + innerContainerMargin;

				} else {
					outerContainerWidth += $(this).width() + innerContainerMargin;
				}

				if(centerContainerContent) {

					$(this).css('text-align', 'center');
				}

			});
			// 	alert(innerContainer.length);
			//alert(outerContainerWidth);
			outerContainer.width(outerContainerWidth);
		}
		var findWidestContainer = function() {

			var widestContainer = 0;
			innerContainer.each(function() {

				if($(this).width() > widestContainer) {
					widestContainer = $(this).width();
				}
			});
			return widestContainer;
		}
		
		var createIOSNavigation = function(){
			
			var innerContainerWidth = innerContainer.width();
			var innerContanerHeight = innerContainer.height();
			var imageMargin = 10;
			//alert(innerContainer.length);
			$(innerContainer).css({'float' : 'left', 'width' : innerContainerWidth + 'px', 'margin-right' : imageMargin + 'px'});
			
			var galleryCode = '<div id="uaIOSSlider" style="position:relative;height:' + innerContanerHeight + 'px;width:' + (innerContainerWidth + imageMargin) * innerContainer.length  + 'px;">' + outerContainer.html() + '</div>';
			outerContainer.html(galleryCode);
			outerContainer.css({'position':'absolute','height': + innerContanerHeight + 'px', 'width': innerContainerWidth + 'px', 'overflow':'auto', '-webkit-overflow-scrolling':'touch'});
			
			
		}
		
		var createArrowNavigation = function() {
		
			
			$('body').append('<img id="uagallery_next_temp" style="display:none">');
			//$(img).load( function() {
			$('#uagallery_next_temp').bind('load', function() {

				//			alert(imageDirectory + 'nav_right.png');
				$('#uagallery_next_temp').css('display', 'none');
				var html = '<img src="' + imagePath + '/nav_left.png" id="uagallery_previous" alt="Previous" title="Previous" />';
				html += '<img src="' + imagePath + '/nav_right.png" id="uagallery_next" alt="Next" title="Next" />';
				//            	// have we overridden the container to append it to
				//	alert(plugin.settings.arrowContainer);
				if(plugin.settings.arrowContainer != '') {

					$(plugin.settings.arrowContainer).append(html);

				} else {
					//wrapperContainer.append(html);
					outerWrapperContainer.append(html);

				}

				//	alert(html);
				previousButton = $('#uagallery_previous');
				nextButton = $('#uagallery_next');
				
					// add tracking classes if set
		//	alert(trackNavigation);
			if(trackNavigation){
				
				previousButton.addClass('logLink').data('linkid', 'gallery_previous');
				nextButton.addClass('logLink').data('linkid', 'gallery_next');
				
				// trigger the external script expecting tracking links as this has loaded after it's fired
				addTrackingLinks();
			}

				var navigationItemWidth = nextButton.width();
				var navigationItemWidth = nextButton.height();
				var navigationYOffset = innerContainerHeight / 2 - navigationItemWidth / 2;
				previousButton.css('position', 'absolute').css('left', '0px').css('top', navigationYOffset).css('z-index', 1);
				nextButton.css('position', 'absolute').css('left', wrapperContainer.width() - navigationItemWidth + 'px').css('top', navigationYOffset).css('z-index', 1);

				// if the position has been set in the config then use that instead of making it's own decision
				var useCustomArrowPosition = plugin.settings.arrowNavigationTop === undefined ? false : true;
				if(useCustomArrowPosition) {

					previousButton.css('left', arrowNavigationLeft + 'px').css('top', arrowNavigationTop + 'px');
					nextButton.css('left', arrowNavigationRight + 'px').css('top', arrowNavigationTop + 'px');
				}

				nextButton.bind('click', function() {

					showNextPanel();

				});

				nextButton.bind('mouseenter', function() {nextButton.css('display') == 'block' ? $('html').css('cursor', 'pointer') : $('html').css('cursor', 'default');

				});

				nextButton.bind('mouseleave', function() {

					$('html').css('cursor', 'default');

				});

				previousButton.bind('click', function() {

					showPreviousPanel();

				});

				previousButton.bind('mouseenter', function() {previousButton.css('display') == 'block' ? $('html').css('cursor', 'pointer') : $('html').css('cursor', 'default');

				});

				previousButton.bind('mouseleave', function() {

					$('html').css('cursor', 'default');

				});
				afterMoveCleanup();

			}).attr('src', imagePath + '/nav_right.png');

		

		}
		var createFaderGallery = function() {
			// set them all to invisible to start with
			var i = 1;

			innerContainer.each(function() {

				$(this).css('position', 'absolute').css('opacity', 0).css('left', '0px').attr('rel', i);
				i++;
			});

			$('.' + plugin.settings.innerContainer + '[rel="' + currentPanel + '"]').css('opacity', 1).css('z-index', 1);
			//	crossFadeImages();
			var timeout = setInterval(function() {
				crossFadeImages();
			}, faderPause);
		}
		var crossFadeImages = function() {

			$('.' + plugin.settings.innerContainer + '[rel="' + currentPanel + '"]').css('z-index', 2).fadeTo(faderSpeed, 0, function() {

			});
			if(currentPanel == totalPanels) {
				currentPanel = 1;
			} else {
				currentPanel++;
			}
			$('.' + plugin.settings.innerContainer + '[rel="' + currentPanel + '"]').css('opacity', 1).css('z-index', 1);
		}
		var createSubNavigation = function() {

			var html = '';
			if(subNavigationPosition == 'top') {
				html += '<div id="uagallery_subnavigation" style="top:0px">';

			} else {
				html += '<div id="uagallery_subnavigation" style="top:' + outerContainerHeight + 'px">';
			}

			for(var i = 0; i < innerContainer.length; i++) {
				html += '<span class="uagallery_subnavigation" rel="' + (i + 1) + '">' + (i + 1) + '</span>';
			}
			html += '</div>';

			wrapperContainer.append(html);
			var subNavigation = $('#uagallery_subnavigation');
			var subNavigationClass = $('.uagallery_subnavigation');

			var subNavigationHeight = parseInt(subNavigation.css('height'), 10);
			if(subNavigationPosition == 'top') {
				outerContainer.css('top', subNavigationHeight + 'px');
			}
			wrapperContainer.css('height', outerContainerHeight + subNavigationHeight + 'px');
			updateSubNavigation();

			subNavigationClass.bind('click', function() {

				jumpToItem(parseInt($(this).attr('rel'), 10));

			});

			subNavigationClass.bind('mouseenter', function() {$(this).attr('rel') != currentPanel ? $('html').css('cursor', 'pointer') : $('html').css('cursor', 'default');

			});

			subNavigationClass.bind('mouseleave', function() {

				$('html').css('cursor', 'default');

			});
		}
		var createThumbnailNavigation = function() {

			// it's better to create them as absolute positioning so we can apply
			// dropshadows etc and not have ****ing explorer mess up their positions

			var row = 0;
			var column = 0;
			//	var rowGap = 10;
			//	var columnGap = 10;

			// append it first to get the values
			var html = '<div id="uagallery_thumbnailnavigation_wrapper">';
			var html = '<div id="uagallery_thumbnailnavigation" style="top:' + (outerContainerHeight + 20) + 'px;">';
			var i = 0;
			$('.' + plugin.settings.innerContainer + ' img').each(function() {
				var path = $(this).attr('src');

				var pathArray = path.split("/");
				//alert(pathArray);
				html += '<div class="uagallery_thumbnailnavigation"  rel="' + (i + 1) + '"><img src="' + thumbnailDirectory + pathArray[pathArray.length - 1] + '" /></div>';
				i++;
			});
			html += '</div>';
			html += '</div>';
			// we append to the outer wrapper to allow some overflow on dropshadwos, borders etc
			outerWrapperContainer.append(html);

			var thumbnailNavigation = $('.uagallery_thumbnailnavigation');
			var thumbnailImageHeight = parseInt(thumbnailNavigation.css('height'), 10) + rowGap;

			if(fixedNumberOfColumns != 0) {

				// set the outer container to overflow visible so dropshadows and borders are seen
				// to do this we'll need to create an inner wrapper so the outer wrapper can overflow

				var thumbnailImageWidth = parseInt(thumbnailNavigation.css('width'), 10);
				totalImagesPerLine = fixedNumberOfColumns;

			} else {

				var thumbnailImageWidth = parseInt(thumbnailNavigation.css('width'), 10) + columnGap;
				var totalImagesPerLine = Math.floor((outerContainerVisibleWidth + columnGap) / thumbnailImageWidth);

				if(totalImagesPerLine > $('.uagallery_thumbnailnavigation').length) {
					totalImagesPerLine = $('.uagallery_thumbnailnavigation').length;

				}
			}

			// append a div with the hover class to find out the information we need
			thumbnailNavigation.append('<div id="uagallery_thumbnailnavigation_temp_active" class="uagallery_thumbnailnavigation active" style="display:none"></div><div id="uagallery_thumbnailnavigation_temp_hover" class="uagallery_thumbnailnavigation hover" style="display:none"></div>');
			imageBorderActive = parseInt($('.uagallery_thumbnailnavigation.active').css('border-right-width'), 10);
			imageBorderActive = isNaN(imageBorderActive) ? 0 : imageBorderActive;
			imageBorderHover = parseInt($('.uagallery_thumbnailnavigation.hover').css('border-right-width'), 10);
			imageBorderHover = isNaN(imageBorderHover) ? 0 : imageBorderHover;

			$('#uagallery_thumbnailnavigation_temp').remove();

			thumbnailNavigation.each(function() {

				// if it's a fixed number of columns then spread them evenly
				if(fixedNumberOfColumns != 0) {
					// outer width - 2 thumbnails and then space the rest evenly

					// first column
					if(column == 0) {

						var leftPos = 0;
					} else if(column == fixedNumberOfColumns - 1) {
						// last column

						var leftPos = outerContainerVisibleWidth - thumbnailImageWidth;
						//alert(leftPos);
					} else {
						// in-between columns
						var remaingSpace = outerContainerVisibleWidth - (thumbnailImageWidth * 2);
						var thumbnailSpace = remaingSpace - (fixedNumberOfColumns - 2) * thumbnailImageWidth;
						var margins = thumbnailSpace / (fixedNumberOfColumns - 1);

						var leftPos = (thumbnailImageWidth + margins) * column;
					}
					//alert(margins);
					$(this).css('left', leftPos).css('top', row * thumbnailImageHeight);
				} else {

					$(this).css('left', column * thumbnailImageWidth).css('top', row * thumbnailImageHeight);

				}

				//	alert(i%totalImagesPerLine + '===' + column);
				if(column % totalImagesPerLine === totalImagesPerLine - 1) {
					row++;
					column = 0;

				} else {
					column++;
				}

				// bind the actions
				thumbnailNavigation.bind('click', function() {

					jumpToItem(parseInt($(this).attr('rel'), 10));

				});
				// we are binding the images to be able to add the border and not have them move
				// there's an assumption the border is equally applied

				thumbnailNavigation.bind('mouseenter', function() {

					if(!$(this).hasClass('active')) {

						var currentLeft = parseFloat($(this).css('left'), 10);
						var currentTop = parseFloat($(this).css('top'), 10); $(this).attr('rel') != currentPanel ? $('html').css('cursor', 'pointer') : $('html').css('cursor', 'default');
						var offsetDistance = $(this).data('bound') === 1 ? 0 : imageBorderHover;

						//alert(imageBorderHover);
						$(this).addClass('hover').data('bound', 1).css('left', currentLeft - offsetDistance + 'px').css('top', currentTop - offsetDistance + 'px');
						//	alert(offsetDistance);

					}

				});

				thumbnailNavigation.bind('mouseleave', function() {

					var currentLeft = parseFloat($(this).css('left'), 10);
					var currentTop = parseFloat($(this).css('top'), 10);
					$('html').css('cursor', 'default').removeClass('hover');
					var offsetDistance = $(this).data('bound') === 1 ? imageBorderHover : 0;
					$(this).removeClass('hover').data('bound', 0).css('left', currentLeft + offsetDistance + 'px').css('top', currentTop + offsetDistance + 'px');

				});
			});

			wrapperContainer.css('height', outerContainerHeight + ((row + 1) * thumbnailImageHeight) + 10 + 'px');
			//        	$('#uagallery_thumbnailnavigation').css('left', '20px');
			// now shuffle it across to the center if we're not forcing a set number
			if(fixedNumberOfColumns == 0) {
				var thumbnailMargin = (outerContainerVisibleWidth - (totalImagesPerLine * thumbnailImageWidth)) / 2;
				$('#uagallery_thumbnailnavigation').css('left', thumbnailMargin + 'px');

			}
		}
		var showNextPanel = function() {

			if(!outerContainer.data('moving') && (currentPanel < totalPanels || allowWrap)) {

				// if we're wrapping then we need to move the first item to the end

				if(allowWrap && currentPanel == totalPanels) {

					var clonedContainer = innerContainer.eq(0).clone().wrap('<div>').parent().html();

					outerContainer.append(clonedContainer);
					refreshContainerElements();
					setOuterContainerWidth();

					//alert(clonedContainer);
				}

				// do we slide the current distance, or calculate it based on what's around it
				if(centerFirstItem) {

					var nextContainerWidth = (innerContainer.eq(currentPanel - 1).width() + innerContainer.eq(currentPanel).width()) / 2 + innerContainerMargin;

				} else {

					var nextContainerWidth = innerContainer.eq(currentPanel - 1).width() + innerContainerMargin;
				}

				outerContainer.data('moving', 1);
				outerContainer.animate({

					left : '-=' + nextContainerWidth + 'px'

				}, sliderSpeed, function() {

					// now remove the extra container
					if(allowWrap && currentPanel == totalPanels) {

						outerContainer.css('left', '0px');
						currentPanel = 1;
						innerContainer.eq(innerContainer.length - 1).remove();
						refreshContainerElements();
						setOuterContainerWidth();

					} else {
						currentPanel++;

					}

					afterMoveCleanup();

				});
			}

		}
		var showPreviousPanel = function() {

			if(!outerContainer.data('moving') && (currentPanel > 0 || allowWrap)) {

				if(allowWrap && currentPanel == 1) {

					var clonedContainer = innerContainer.eq(innerContainer.length - 1).clone().wrap('<div>').parent().html();
					//alert(clonedContainer);
					outerContainer.prepend(clonedContainer);
					refreshContainerElements();

					setOuterContainerWidth();
					var firstContainerWidth = innerContainer.eq(0).outerWidth();
					outerContainer.css('left', firstContainerWidth * -1 + 'px');
					//alert(outerContainer.css('left'));
					//	alert(firstContainerWidth);
				}

				if(centerFirstItem) {

					var previousContainerWidth = (innerContainer.eq(currentPanel - 2).width() + innerContainer.eq(currentPanel - 1).width()) / 2 + innerContainerMargin;

				} else {

					var previousContainerWidth = innerContainer.eq(currentPanel - 2).width() + innerContainerMargin;
				}
				outerContainer.data('moving', 1);
				outerContainer.animate({

					left : '+=' + previousContainerWidth + 'px'

				}, sliderSpeed, function() {

					// now remove the extra container
					if(allowWrap && currentPanel == 1) {

						// needs to jump back to the last container
						// best done by finding outerContainer.width - last image width
						var totalCurrentWidth = outerContainer.width();
						var lastItemWidth = innerContainer.eq(innerContainer.length - 1).outerWidth() + innerContainerMargin;
						//  alert(lastItemWidth);
						// alert(innerContainerMargin);
						// this may not work with margins
						//	outerContainer.css('left', '-7985px');
						outerContainer.css('left', ((totalCurrentWidth - (lastItemWidth * 2) ) * -1) + 'px');
						innerContainer.eq(0).remove();
						//	alert(totalCurrentWidth - (lastItemWidth * 2));

						//	innerContainer.eq(0).remove();
						refreshContainerElements();
						currentPanel = innerContainer.length;

						setOuterContainerWidth();

					} else {
						currentPanel--;

					}

					afterMoveCleanup();

				});
			}

		}
		var jumpToItem = function(whichItem) {

			var distanceToMove = 0;
			if(whichItem != currentPanel) {
				if(whichItem > currentPanel) {

					for(var i = currentPanel; i < whichItem; i++) {
						distanceToMove += innerContainer.eq(i - 1).width() + innerContainerMargin;
						if(centerFirstItem) {
							distanceToMove -= innerContainer.eq(i - 1).width() / 2 - innerContainer.eq(i).width() / 2;

						}

					}

				} else {

					for(var i = whichItem; i < currentPanel; i++) {
						distanceToMove += innerContainer.eq(i - 1).width() + innerContainerMargin;
						if(centerFirstItem) {
							distanceToMove -= innerContainer.eq(i - 1).width() / 2 - innerContainer.eq(i).width() / 2;

						}

					}

				}
				//alert(distanceToMove);

				if(whichItem > currentPanel) {
					distanceToMove *= -1;
				}

				if(!outerContainer.data('moving')) {

					outerContainer.data('moving', 1);
					outerContainer.animate({

						left : '+=' + distanceToMove + 'px'

					}, sliderSpeed, function() {
						currentPanel = whichItem;

						afterMoveCleanup();

					});
				}

			}
			//alert(currentPanel);

		}
		var afterMoveCleanup = function() {
			outerContainer.data('moving', 0);
			checkNavigationVisibility();
			if(dimNonActiveItems) {
				innerContainer.css('opacity', 0.5);
				innerContainer.eq(currentPanel - 1).fadeTo(1000, 1);
			}
			innerContainer.css('filter', '');
			updateSubNavigation();

			//	thumbnailNavigation.eq(currentPanel-1).addClass('hover');

		}
		var checkNavigationVisibility = function() {
			// we don't always show this so check if it exists so it doesn't bug when displaying
			if( typeof previousButton != 'undefined') {

				if(!allowWrap) {currentPanel === 1 ? previousButton.css('display', 'none') : previousButton.css('display', 'block'); currentPanel === totalPanels ? nextButton.css('display', 'none') : nextButton.css('display', 'block');

				}
			}

		}
		var updateSubNavigation = function() {

			if(showSubNavigation) {
				//   alert('f');
				$('.uagallery_subnavigation').removeClass('active');
				$('.uagallery_subnavigation').eq(currentPanel - 1).addClass('active');

			}

			if(showThumbnails) {

				var thumbnailNavigation = $('.uagallery_thumbnailnavigation');

				var offsetDistance = imageBorderActive;
				//	offsetDistance = imageBorderHover === 0 ? 0 : imageBorderActive;
				thumbnailNavigation.each(function() {
					if($(this).hasClass('active')) {

						var currentLeft = parseInt($(this).css('left'), 10);
						var currentTop = parseInt($(this).css('top'), 10);

						$(this).removeClass('active').css('left', currentLeft + offsetDistance + 'px').css('top', currentTop + offsetDistance + 'px');

						// remove the class and the offset
					}
				});
				var newActiveThumbnail = $('.uagallery_thumbnailnavigation[rel="' + currentPanel + '"]');
				//alert(newActiveThumbnail.data('bound'));
				currentLeft = parseInt(newActiveThumbnail.css('left'), 10);
				currentTop = parseInt(newActiveThumbnail.css('top'), 10);

				if(!newActiveThumbnail.data('bound')) {

					newActiveThumbnail.addClass('active');

				} else {

					newActiveThumbnail.addClass('active').css('left', currentLeft - offsetDistance + 'px').css('top', currentTop - offsetDistance + 'px');
				}

			}
		}
		var setInitialPosition = function() {

			outerContainer.css('left', outerContainerVisibleWidth / 2 - innerContainer.eq(0).width() / 2 + 'px');
		}
		// do your magic
		plugin.init();

	}

	$.fn.uaGallery = function(options) {

		return this.each(function() {
			if(undefined == $(this).data('uaGallery')) {
				var plugin = new $.uaGallery(this, options);
				$(this).data('uaGallery', plugin);
			}
		});
	}
})(jQuery);
// JavaScript Document