$(document).ready(function() {
    
    // bind the fields to be saved and updated
    $('#reorderable .text-field').live('blur keyup', function(){
    	
    	var fieldId = $(this).attr('rel');
    	var fieldValue = $(this).val();
    	
    	
    		queryString = SITE_PATH + '/admin/helpers/core/update-list.php?';
	    	queryString += 'table=' + reorderTable + '&where=' + reorderWhere;
	    	queryString += '&fieldId=' + fieldId;
	    	queryString += '&fieldValue=' + fieldValue;
	    	queryString += SID_STRING;
	

	
	    	$.post(queryString);
	    	
    });
    
    // bind the deletable buttons
    $('#reorderable .delete').live('click', function(){
    	
    	var fieldId = $(this).attr('rel');
    	deleteNode(fieldId);
    	$($(this).closest("tr")).remove();
    	
    	
    
    });
    
    // Initialise the table
    $("#reorderable").tableDnD();


	// Initialise the second table specifying a dragClass and an onDrop function that will display an alert
	$("#reorderable").tableDnD({
	    onDragClass: "myDragClass",

	    onDrop: function(table, row) {
	    
	    	queryString = SITE_PATH + '/admin/helpers/core/reorder-list.php?';
	    	queryString += 'table=' + reorderTable + '&where=' + reorderWhere + '&'+$.tableDnD.serialize() + '';
	    	queryString += SID_STRING;
			//alert(queryString);
	    	$.post(queryString);
	    	
	    	
	    	}
    	
	});
    
});

function addListOption(additionalField){


		queryString = SITE_PATH + '/admin/helpers/core/add-to-list.php?';
		queryString += 'table=' + reorderTable + '&type=' + reorderType;
	    queryString += SID_STRING;
	    
	    if(additionalField != undefined){
			
			
}
	    
	 
	 	$.post(queryString, function(data) {
 			
 			// get a return value so we can append it to the DOM
 			createNode(data, '');
		
		});

		$("#reorderable").tableDnD();
}

function createNode(nodeId, value){

	nodeHTML = '<tr  id="' + nodeId + '">';
	nodeHTML += '<td><div class="reorder"></div></td>';

	nodeHTML += '<td><label>' + reorderLabel + '&nbsp;<input type="text" id="' + reorderField + '[' + nodeId + ']" name="' + reorderField + '[' + nodeId + ']" rel="' + nodeId + '" ';
	nodeHTML += 'value="' + value + '" class="text-field" /></label></td>';

	nodeHTML += '<td><a href="#" onclick="return false" class="delete" rel="' + nodeId + '"><img src="' + SITE_PATH + '/public/template-core/icons/cross.png" /></a></td>';
	nodeHTML += '</tr>';
	//alert(nodeHTML);
	$('#reorderable').append(nodeHTML);
}

function deleteNode(nodeId){

	   		queryString = SITE_PATH + '/admin/helpers/core/delete-node-from-list.php?';
	    	queryString += 'table=' + reorderTable + '&where=' + reorderWhere;
	    	queryString += '&fieldId=' + nodeId;
	    	queryString += SID_STRING;
	

	
	    	$.post(queryString);
	    	
}