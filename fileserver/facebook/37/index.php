
<link rel="stylesheet" href="/fileserver/facebook/37/css/core.css">

<script src="/public/javascript/uaslider/ua-slider.js"></script>
<script src="/public/javascript/formcheck.js"></script>

</head>

<body>
	
<div id="content">
	
    <div class="contentInner">
    <img src="/fileserver/facebook/37/images/haines-hunter-logo.png" alt="Haines Hunter - Creating the Difference" width="222" height="44" />
    <div id="mainMenu">
        	
      <div class="menuItem"><a href="#" data-slider="prowler">Prowler</a></div>
      <div class="menuItem"><a href="#" data-slider="profish">Profish</a></div>
      <div class="menuItem"><a href="#" data-slider="rseries">R-Series</a></div>
       <div class="menuItem"><a href="#" data-slider="offshore">Offshore</a></div>
      <div class="menuItem"><a href="#" data-slider="patriot">Patriot</a></div>
      <div class="menuItem"><a href="#" data-slider="enclosed">Enclosed</a></div>
     
        <div class="menuItem limited"><a href="#" data-slider="limited">LIMITED</a></div>
      <div class="menuItem last"><a href="http://www.haineshunter.com.au/" target="_blank" class="external">Website</a></div>
          
        
      </div>
      
      <div id="captions"><div class="caption" id="caption"></div></div>
      
      <div id="slideshow">
      	
      	<div id="slideshowInner">
      
      		<div class="slide"><a href="http://www.haineshunter.com.au/models/prowler/" target="_blank"><img src="/fileserver/facebook/37/images/boats/prowler.jpg" rel="prowler" title="Prowler" /></a></div>
            
            <div class="slide"><a href="http://www.haineshunter.com.au/models/profish/" target="_blank"><img src="/fileserver/facebook/37/images/boats/profish.jpg" rel="profish" title="Profish" /></a></div>
            
          <div class="slide"><a href="http://www.haineshunter.com.au/models/r-series/" target="_blank"><img src="/fileserver/facebook/37/images/boats/r-series.jpg" rel="rseries" title="R-Series" /></a></div>
                
          <div class="slide"><a href="http://www.haineshunter.com.au/models/offshore/" target="_blank"><img src="/fileserver/facebook/37/images/boats/offshore.jpg" rel="offshore" title="Offshore" /></a></div>
        
            <div class="slide"><a href="http://www.haineshunter.com.au/models/patriot/" target="_blank"><img src="/fileserver/facebook/37/images/boats/patriot.jpg" rel="patriot" title="Patriot" /></a></div>
        
        	 <div class="slide"><a href="http://www.haineshunter.com.au/models/enclosed/" target="_blank"><img src="/fileserver/facebook/37/images/boats/enclosed.jpg" rel="enclosed" title="Enclosed" /></a></div>
            
            <div class="slide"><a href="http://www.haineshunter.com.au/limited/limited-range/" target="_blank"><img src="/fileserver/facebook/37/images/boats/limited.jpg" rel="limited" title="Limited" /></a></div>
         
             
            
        	
        </div>
        
        </div>
    

    
   	  <div id="submenu">
       	<div class="submenuItem">Newsletter Sign Up</div>
            
      </div>
    <h1>Fill in the form below to be kept up to date with<br />
      the latest developments from Haines Hunter.    </h1>
    <div class="contentInner">
<form action="http://hh.leadmonitor.com.au/proc.php" method="post" id="_form_1026" accept-charset="utf-8" enctype="multipart/form-data">
				<input type="hidden" name="f" value="1026">
				<input type="hidden" name="s" value="">
				<input type="hidden" name="c" value="0">
				<input type="hidden" name="m" value="0">
				<input type="hidden" name="act" value="sub">
				<input type="hidden" name="nlbox[]" value="2">
                <h2>Firstname*</h2>
					<input type="text" name="firstname" title="First Name" class="text-input required auto-hint"><div class="clr"></div>
                    <h2>Surname*</h2>
					<input type="text" name="lastname" title="Last Name" class="text-input required auto-hint"><div class="clr"></div>
                    <h2>Email Address*</h2>
					<input type="text" name="email" title="Email Address" class="text-input required auto-hint"><div class="clr"></div>
					<div class="clr"></div>
					
					<input type="submit" value="Submit">
		
        			
<hr />
</form>
<div id="terms">
By subscribing to our database you are opting in to receive correspondence from Haines Hunter. Your information will not be used for any other purpose, and will not be passed on to any third parties.



  </div>

</div>
