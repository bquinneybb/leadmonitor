<link href="/fileserver/facebook/41/css/core.css" rel="stylesheet" type="text/css" />
<script src="/fileserver/facebook/41/javascript/core.js"></script>
<link href="/fileserver/facebook/41/javascript/ua-image-gallery/styles.css" rel="stylesheet" type="text/css">
<script src="/fileserver/facebook/41/javascript/ua-image-gallery/image-gallery.js" name="uaGallery"></script>
<div id="arrownavigation" style="position:relative"><div id="arrownavigationinner" style="position:absolute;z-index:999;width:4900px"></div></div>
<div id="content">
	
    <div class="contentInner">
    
      <div align="center"><a href="http://www.thewarleighbrighton.com.au/?ref=facebookiframe"><img src="/fileserver/facebook/41/images/warleigh-brighton-logo.png" alt="The Warleigh Brighton" border="0" id="warleighLogo"></a></div>
   <div class="bodytext" id="warleighIntro">
      <h1>Welcome to The Warleigh Brighton</h1>
      
      <p> Commanding pride of place in a leafy grove off vibrant Bay Street, North Brighton, a breathtaking residential opportunity is about to take shape as the stylish, architecturally designed apartments and private townhouses of The Warleigh Brighton are finally revealed.</p>
      <p>Be amongst the first to discover the sophistication of Luxe, the relaxed charm of Domaine and the spacious comfort and privacy of 
        The Residences by visiting the website or registering your interest in an exclusive, one-on-one viewing of the onsite display suite.</p>
      <p>The Warleigh Brighton is the combined vision of Cbus Property, SJB Architecture, Jack Merlo Design, SJB Interiors and Marshall White. </p>
       </div>  
      <div class="bodytext">
      <div id="slideshow">
    
    		<div class="slideshow"><img src="/fileserver/facebook/41/images/images_old/domaine-external.jpg"></div>
            <div class="slideshow"><img src="/fileserver/facebook/41/images/images_old/domaine-internal.jpg"></div>
            <div class="slideshow"><img src="/fileserver/facebook/41/images/images_old/luxe-external.jpg"></div>
            <div class="slideshow"><img src="/fileserver/facebook/41/images/images_old/luxe-internal.jpg"></div>
             <div class="slideshow"><img src="/fileserver/facebook/41/images/images_old/residences-external.jpg"></div>
            <div class="slideshow"><img src="/fileserver/facebook/41/images/images_old/residences-internal.jpg"></div>
            
            </div>
            </div>
            <form action='http://warleigh.leadmonitor.com.au/proc.php' method='post' id='_form_1012' accept-charset='utf-8' enctype='multipart/form-data' class="registrationForm">
  <input type='hidden' name='f' value='1012'>
  <input type='hidden' name='s' value=''>
  <input type='hidden' name='c' value='0'>
  <input type='hidden' name='m' value='0'>
  <input type='hidden' name='act' value='sub'>
  <input type='hidden' name='nlbox[]' value='1'>
    
    <!-- registration form -->
    
    <label for="db_name">*Name</label>
      <input type="text" name="fullname" id="db_name" class="text-input required" autocomplete="on" />
   
      <label for="db_phone">*Phone Number</label>
      <input type="text" name="field[2]" id="db_phone" class="text-input required" autocomplete="on" />
      <label for="db_email">*Email</label>
      <input type="text" name="email" id="db_email" class="text-input required" autocomplete="on" />
      
      <label for="db_message">Comments</label>
      <input type="text" name="field[4]" id="db_message" class="text-input" autocomplete="on" />
      
      <label for="db_pricerange">*Price Range</label>
      <select id="db_pricerange" name="field[5]" class="text-input required" autocomplete="on">
      	<option value="$500K - $800K">$500K - $800K</option>
        <option value="$801K - $1.25M">$801K - $1.25M</option>
        <option value="$1.25M+">$1.25M+</option>
      </select>
        <input type='hidden' name='field[6][]' value='~|'>
      <label for="interested_in">Interested In</label>
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="10"><input type="checkbox" name="field[6][]" value="LUXE" id="interestedLuxe"></td>
    <td width="100">Luxe</td>
    <td width="10"><input type="checkbox" name="field[6][]" value="DOMAINE" id="interestedDomaine"></td>
        <td width="100">Domaine</td>
    <td width="10"><input type="checkbox" name="field[6][]" value="THE RESIDENCES" id="interestedResidences"></td>
    <td>The Residences</td>
  </tr>
</table>
     

     
      
      <br /><br />
     <input type="submit" value="REGISTER INTEREST" />
    </form>
    </div>
     <!-- /registration form -->
    
    </div>
    
    
</div>