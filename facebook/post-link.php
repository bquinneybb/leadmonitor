<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1); 
require_once('../_config/config.php');
require_once('../shared/helpers.php');

$db = new Database();

 $_POST = array_merge($_GET, $_POST);
$whichArticle = isset($_POST['id']) ? (int)$_POST['id'] : null; 
$linkType = isset($_POST['type']) ? $_POST['type'] : 'blog';
//echo $linkType;
if($whichArticle == null){
	die();	
}

include('shared.php');
$links = array();
$links['blog'] = 'code-link.php';
$links['gallery'] = 'code-gallery.php';

if(isset($links[$linkType])){
	
	include($links[$linkType]);
	
}


 ?>
