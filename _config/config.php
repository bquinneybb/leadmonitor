<?php
$_POST = array_merge($_GET, $_POST);
//echo $_SERVER['DOCUMENT_ROOT'];
define('DS', DIRECTORY_SEPARATOR); // I always use this short form in my code.

date_default_timezone_set('Australia/Victoria');

if(strstr($_SERVER['DOCUMENT_ROOT'], 'benquinney')){

	define('SITE_PATH', 'http://leadmonitor54.local:8888/');
	define('SITE_URL', 'http://leadmonitor54.local:8888/');
	define('TESTING_MODE', true);
	define('FFMPEG', 'ffmpeg');
	define('FREEWAY_EMAIL_SERVER', '{mail.urbanangles.com:143/imap}INBOX');

} else {

	
	define('SITE_PATH', '');
	define('SITE_URL', 'http://dev.urbanfreeway.com/');
	define('TESTING_MODE', false);
	define('FFMPEG', '/usr/local/bin/ffmpeg');

	define('FREEWAY_EMAIL_SERVER', '{mail.urbanangles.com:143/imap/tls/novalidate-cert}INBOX');

}
if(!defined('ROOT')){

	define('ROOT', dirname(dirname(__FILE__)));
}

// if it's a single client, there may be lots of info we don't want to show
define('SINGLE_CLIENT_MODE', false);

include (ROOT . '/application/shared.php');
include(ROOT . '/_utils/phpThumb/phpthumb.class.php');

define('SHOW_ERRORS', true);

if(SHOW_ERRORS){
	error_reporting(E_ALL);
	ini_set("display_errors", 1);

} else {
	error_reporting(E_NONE);
	ini_set("display_errors", 0);
}


define('CURRENT_CLIENT', 'leadtracker');


//print_r(get_declared_classes());
define('VERSION_JAVASCRIPT', '1.0');
define('VERSION_CSS', '1.0');

define('SALT', 'L3@dTracK3rD2ta');

define('DATE_NULL', '0000-00-00 00:00:00');

define('UTIL_PHPTHUMB',  '/_utils/phpThumb/phpThumb.php');
define('PLUGIN_PATH_ROOT', ROOT . '/public/plugins/');
define('FILESERVER_ROOT', ROOT . '/fileserver/');
define('PLUGIN_PATH_LOCAL', SITE_PATH . '/public/plugins/');
define('FILESERVER_LOCAL', SITE_PATH . '/fileserver/');
define('FILESERVER_RAW', '/fileserver/');
define('UPLOAD_PATH_ROOT', ROOT . '/dropbox/');
define('UPLOAD_PATH_LOCAL', '/dropbox/');

// see conditionals above for server
define('FREEWAY_EMAIL_LOGIN', 'freezer@urbanangles.com');
define('FREEWAY_EMAIL_PASSWORD', 'fr33z3r');

// whitelist tables that have permission to update in the re-ordering
$tableWhitelist = array();


$columnWhitelist = array();


/**
 * CORE FUNCTIONS
 */
function autoLoader($className){

	// remove camel case
	if(substr($className, -5) == "Model"){

		$className = ucwords(strtolower(substr($className, 0, -5))) . 'Model';

	}

	if(substr($className, -10) == "Controller"){

		$className = ucwords(strtolower(substr($className, 0, -10))) . 'Controller';
	}

	if(strstr($_SERVER['PHP_SELF'], 'admin')){

		$directories = array(
     	'',
      	'_classes/'
      	,'_classes_third_party/'
 
      	,'admin/application/controllers/'
      	,'admin/application/models/'
      	,'admin/application/views/'
      	,'application/controllers/'
      	,'application/models/'
      	,'application/views/'
      	,'_utils/'
	
      	);

	} else {

		$directories = array(
     	'',
      	'_classes/'
      	,'_classes_third_party/'
      	,'application/controllers/'
      	,'application/models/'
      	,'application/views/'
      	,'admin/application/controllers/'
      	,'admin/application/models/'
      	,'admin/application/views/'
      	,'_utils/'
	
      	);
	}



	//Add your file naming formats here
	$fileNameFormats = array(
      '%s.php',
      '%s.class.php'
      );

      // this is to take care of the PEAR style of naming classes
      $path = str_ireplace('_', '/', $className);
       
      if(@include_once ROOT . '/' . $path.'.php'){
      	return;
      }
       
      foreach($directories as $directory){
      	foreach($fileNameFormats as $fileNameFormat){
      		$path = ROOT . '/'  . $directory.sprintf($fileNameFormat, $className);
      		//  echo 'look = ' . $path . '<br />';
      		if(file_exists($path)){
      			//	echo 'found = ' . $path . '<br />';
      			include_once $path;
      			return;
      		}
      	}
      }
}

spl_autoload_register('autoLoader');

?>