<?php
function checkAccessAllowed(){

	/**
	 * TO DO - but to stop direct access of modal windows without login
	 */
	if(!isset($_SESSION['user']) || !isset($_SESSION['user']['user_id_pk']) || $_SESSION['user']['user_id_pk'] == ''){

		die();
	}
}



/*** DOM FUNCTIONS */
function xml2array($contents, $get_attributes=1, $priority = 'tag') {
    if(!$contents) return array();

    if(!function_exists('xml_parser_create')) {
        //print "'xml_parser_create()' function not found!";
        return array();
    }

    //Get the XML parser of PHP - PHP must have this module for the parser to work
    $parser = xml_parser_create('');
    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, trim($contents), $xml_values);
    xml_parser_free($parser);

    if(!$xml_values) return;//Hmm...

    //Initializations
    $xml_array = array();
    $parents = array();
    $opened_tags = array();
    $arr = array();

    $current = &$xml_array; //Refference

    //Go through the tags.
    $repeated_tag_index = array();//Multiple tags with same name will be turned into an array
    foreach($xml_values as $data) {
        unset($attributes,$value);//Remove existing values, or there will be trouble

        //This command will extract these variables into the foreach scope
        // tag(string), type(string), level(int), attributes(array).
        extract($data);//We could use the array by itself, but this cooler.

        $result = array();
        $attributes_data = array();
        
        if(isset($value)) {
            if($priority == 'tag') $result = $value;
            else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
        }

        //Set the attributes too.
        if(isset($attributes) and $get_attributes) {
            foreach($attributes as $attr => $val) {
                if($priority == 'tag') $attributes_data[$attr] = $val;
                else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
            }
        }

        //See tag status and do the needed.
        if($type == "open") {//The starting of the tag '<tag>'
            $parent[$level-1] = &$current;
            if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
                $current[$tag] = $result;
                if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
                $repeated_tag_index[$tag.'_'.$level] = 1;

                $current = &$current[$tag];

            } else { //There was another element with the same tag name

                if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                    $repeated_tag_index[$tag.'_'.$level]++;
                } else {//This section will make the value an array if multiple tags with the same name appear together
                    $current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
                    $repeated_tag_index[$tag.'_'.$level] = 2;
                    
                    if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                        $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                        unset($current[$tag.'_attr']);
                    }

                }
                $last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
                $current = &$current[$tag][$last_item_index];
            }

        } elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
            //See if the key is already taken.
            if(!isset($current[$tag])) { //New Key
                $current[$tag] = $result;
                $repeated_tag_index[$tag.'_'.$level] = 1;
                if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

            } else { //If taken, put all things inside a list(array)
                if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

                    // ...push the new element into that array.
                    $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                    
                    if($priority == 'tag' and $get_attributes and $attributes_data) {
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                    }
                    $repeated_tag_index[$tag.'_'.$level]++;

                } else { //If it is not an array...
                    $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
                    $repeated_tag_index[$tag.'_'.$level] = 1;
                    if($priority == 'tag' and $get_attributes) {
                        if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                            
                            $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                            unset($current[$tag.'_attr']);
                        }
                        
                        if($attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                        }
                    }
                    $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
                }
            }

        } elseif($type == 'close') { //End of tag '</tag>'
            $current = &$parent[$level-1];
        }
    }
    
    return($xml_array);
}  



/**
 * SECONDARY FUNCTIONS
 */
//// CAREFUL AS THESE 2 DELETE EXISTING FOLDERS
function rrmdir($dir) {
  if (is_dir($dir)) {
    $files = scandir($dir);
    foreach ($files as $file)
    if ($file != "." && $file != "..") rrmdir("$dir/$file");
    rmdir($dir);
  }
  else if (file_exists($dir)) unlink($dir);
} 

function rcopy($src, $dst) {
  if (file_exists($dst)) rrmdir($dst);
  if (is_dir($src)) {
    mkdir($dst);
    $files = scandir($src);
    foreach ($files as $file)
    if ($file != "." && $file != "..") rcopy("$src/$file", "$dst/$file");
  }
  else if (file_exists($src)) copy($src, $dst);
}
//
function smartCopy($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0755))
    {
        $result=false;
       
        if (is_file($source)) {
            if ($dest[strlen($dest)-1]=='/') {
                if (!file_exists($dest)) {
                    cmfcDirectory::makeAll($dest,$options['folderPermission'],true);
                }
                $__dest=$dest."/".basename($source);
            } else {
                $__dest=$dest;
            }
            $result=copy($source, $__dest);
            chmod($__dest,$options['filePermission']);
           
        } elseif(is_dir($source)) {
            if ($dest[strlen($dest)-1]=='/') {
                if ($source[strlen($source)-1]=='/') {
                    //Copy only contents
                } else {
                    //Change parent itself and its contents
                    $dest=$dest.basename($source);
                    @mkdir($dest);
                    chmod($dest,$options['filePermission']);
                }
            } else {
                if ($source[strlen($source)-1]=='/') {
                    //Copy parent directory with new name and all its content
                    @mkdir($dest,$options['folderPermission']);
                    chmod($dest,$options['filePermission']);
                } else {
                    //Copy parent directory with new name and all its content
                    @mkdir($dest,$options['folderPermission']);
                    chmod($dest,$options['filePermission']);
                }
            }

            $dirHandle=opendir($source);
            while($file=readdir($dirHandle))
            {
                if($file!="." && $file!="..")
                {
                     if(!is_dir($source."/".$file)) {
                        $__dest=$dest."/".$file;
                    } else {
                        $__dest=$dest."/".$file;
                    }
                    //echo "$source/$file ||| $__dest<br />";
                    $result=smartCopy($source."/".$file, $__dest, $options);
                }
            }
            closedir($dirHandle);
           
        } else {
            $result=false;
        }
        return $result;
    } 

function get_server_load()
{

	$serverload = array();

	// DIRECTORY_SEPARATOR checks if running windows
	if(DIRECTORY_SEPARATOR != '\\')
	{
		if(function_exists("sys_getloadavg"))
		{
			// sys_getloadavg() will return an array with [0] being load within the last minute.
			$serverload = sys_getloadavg();
			$serverload[0] = round($serverload[0], 4);
		}
		else if(@file_exists("/proc/loadavg") && $load = @file_get_contents("/proc/loadavg"))
		{
			$serverload = explode(" ", $load);
			$serverload[0] = round($serverload[0], 4);
		}
		if(!is_numeric($serverload[0]))
		{
			if(@ini_get('safe_mode') == 'On')
			{
				return "Unknown";
			}

			// Suhosin likes to throw a warning if exec is disabled then die - weird
			if($func_blacklist = @ini_get('suhosin.executor.func.blacklist'))
			{
				if(strpos(",".$func_blacklist.",", 'exec') !== false)
				{
					return "Unknown";
				}
			}
			// PHP disabled functions?
			if($func_blacklist = @ini_get('disable_functions'))
			{
				if(strpos(",".$func_blacklist.",", 'exec') !== false)
				{
					return "Unknown";
				}
			}

			$load = @exec("uptime");
			$load = explode("load average: ", $load);
			$serverload = explode(",", $load[1]);
			if(!is_array($serverload))
			{
				return "Unknown";
			}
		}
	}
	else
	{
		return "Unknown";
	}

	$returnload = trim($serverload[0]);

	return $returnload;
}



function createCleanUrl($parts){

	$slug = '';
	foreach($parts as $part){

		$slug .= makeSlug($part) . '/';
	}

	return $slug;


}

function makeSlug($string, $force_lowercase = true, $anal = false) {
	$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                   "���", "���", ",", "<", ".", ">", "/", "?");

	$clean = str_replace('&', 'and', $string);
	// echo 's = ' . $string;die();
	$clean = trim(str_replace($strip, "", strip_tags($clean)));
	$clean = preg_replace('/\s+/', "-", $clean);
	$clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
	return ($force_lowercase) ?
	(function_exists('mb_strtolower')) ?
	mb_strtolower($clean, 'UTF-8') :
	strtolower($clean) :
	$clean;

}

function print_gzipped_page() {

	global $HTTP_ACCEPT_ENCODING;
	if( headers_sent() ){
		$encoding = false;
	}elseif( strpos($HTTP_ACCEPT_ENCODING, 'x-gzip') !== false ){
		$encoding = 'x-gzip';
	}elseif( strpos($HTTP_ACCEPT_ENCODING,'gzip') !== false ){
		$encoding = 'gzip';
	}else{
		$encoding = false;
	}

	if( $encoding ){
		$contents = ob_get_contents();
		ob_end_clean();
		header('Content-Encoding: '.$encoding);
		print("\x1f\x8b\x08\x00\x00\x00\x00\x00");
		$size = strlen($contents);
		$contents = gzcompress($contents, 9);
		$contents = substr($contents, 0, $size);
		print($contents);
		exit();
	}else{
		ob_end_flush();
		exit();
	}
}

/**
 * HELPERS
 */

function convert($size)
{
	$unit=array('b','kb','mb','gb','tb','pb');
	return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

/*
 *
 *
 Credits: Bit Repository
 URL: http://www.bitrepository.com/web-programming/php/extracting-content-between-two-delimiters.html

 */


function getTextBetweenDelimiters($string, $start, $end){
	$pos = stripos($string, $start);

	$str = substr($string, $pos);

	$str_two = substr($str, strlen($start));

	$second_pos = stripos($str_two, $end);

	$str_three = substr($str_two, 0, $second_pos);

	$unit = trim($str_three); // remove whitespaces

	return $unit;
}


/**
 *
 * @get text between tags
 *
 * @param string $tag The tag name
 *
 * @param string $html The XML or XHTML string
 *
 * @param int $strict Whether to use strict mode
 *
 * @return array
 *
 */
function getTextBetweenTags($tag, $html, $strict=0){
	/*** a new dom object ***/
	$dom = new domDocument;

	/*** load the html into the object ***/
	if($strict==1)
	{
		$dom->loadXML($html);
	}
	else
	{
		$dom->loadHTML($html);
	}

	/*** discard white space ***/
	$dom->preserveWhiteSpace = false;

	/*** the tag by its tag name ***/
	$content = $dom->getElementsByTagname($tag);

	/*** the array to return ***/
	$out = array();
	foreach ($content as $item)
	{
		/*** add node value to the out array ***/
		$out[] = $item->nodeValue;
	}
	/*** return the results ***/
	return $out;
}


function encrypt($text){
	return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
}

function decrypt($text){
	// it gets messed up with URLD encoding, so change it back to + signs
	$text = str_replace(' ', '+', $text);

	return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
}

function sanitiseOutput($string){
//	print_r($string);
	$string = stripslashes($string);
	//$string = str_replace('&amp;', '&', $string);
	//$string = html_decode($string);
	return str_replace(array("&lt;", "&gt;", "&nbsp;", "&amp;"), array("<", ">", " ", "&"), htmlspecialchars($string, ENT_NOQUOTES, "UTF-8"));
	//return stripslashes($string);
}

// sort an array by keys in the data
function orderArrayByData($data, $field) {

	if(is_array($data)){

		$field = $field;
		$code = "return strnatcmp(\$a['$field'], \$b['$field']);";
		usort($data, create_function('$a,$b', $code));

	}
	return $data;


}
function sortArrayByArray($array,$orderArray) {
	ini_set('memory_limit', '1024M');
    $ordered = array();
    foreach($orderArray as $key) {
        if(array_key_exists($key,$array)) {
                $ordered[$key] = $array[$key];
                unset($array[$key]);
        }
    }
    return $ordered + $array;
}
################## URL FUNCTIONS
function replace_urls_callback($text, $callback) {
	// Start off with a regex
	preg_match_all('#(?:(?:https?|ftps?)://[^.\s]+\.[^\s]+|(?:[^.\s/]+\.)+(?:museum|travel|[a-z]{2,4})(?:[:/][^\s]*)?)#i', $text, $matches);
	
	// Then clean up what the regex left behind
	$offset = 0;
	foreach($matches[0] as $url) {
		$url = htmlspecialchars_decode($url);
		
		// Remove trailing punctuation
		$url = rtrim($url, '.?!,;:\'"`');

		// Remove surrounding parens and the like
		preg_match('/[)\]>]+$/', $url, $trailing);
		if (isset($trailing[0])) {
			preg_match_all('/[(\[<]/', $url, $opened);
			preg_match_all('/[)\]>]/', $url, $closed);
			$unopened = count($closed[0]) - count($opened[0]);

			// Make sure not to take off more closing parens than there are at the end
			$unopened = ($unopened > strlen($trailing[0])) ? strlen($trailing[0]):$unopened;

			$url = ($unopened > 0) ? substr($url, 0, $unopened * -1):$url;
		}

		// Remove trailing punctuation again (in case there were some inside parens)
		$url = rtrim($url, '.?!,;:\'"`');
		
		// Make sure we didn't capture part of the next sentence
		preg_match('#((?:[^.\s/]+\.)+)(museum|travel|[a-z]{2,4})#i', $url, $url_parts);
		
		// Were the parts capitalized any?
		$last_part = (strtolower($url_parts[2]) !== $url_parts[2]) ? true:false;
		$prev_part = (strtolower($url_parts[1]) !== $url_parts[1]) ? true:false;
		
		// If the first part wasn't cap'd but the last part was, we captured too much
		if ((!$prev_part && $last_part)) {
			$url = substr_replace($url, '', strpos($url, '.'.$url_parts[2], 0));
		}
		
		// Capture the new TLD
		preg_match('#((?:[^.\s/]+\.)+)(museum|travel|[a-z]{2,4})#i', $url, $url_parts);
		
		$tlds = array('ac', 'ad', 'ae', 'aero', 'af', 'ag', 'ai', 'al', 'am', 'an', 'ao', 'aq', 'ar', 'arpa', 'as', 'asia', 'at', 'au', 'aw', 'ax', 'az', 'ba', 'bb', 'bd', 'be', 'bf', 'bg', 'bh', 'bi', 'biz', 'bj', 'bm', 'bn', 'bo', 'br', 'bs', 'bt', 'bv', 'bw', 'by', 'bz', 'ca', 'cat', 'cc', 'cd', 'cf', 'cg', 'ch', 'ci', 'ck', 'cl', 'cm', 'cn', 'co', 'com', 'coop', 'cr', 'cu', 'cv', 'cx', 'cy', 'cz', 'de', 'dj', 'dk', 'dm', 'do', 'dz', 'ec', 'edu', 'ee', 'eg', 'er', 'es', 'et', 'eu', 'fi', 'fj', 'fk', 'fm', 'fo', 'fr', 'ga', 'gb', 'gd', 'ge', 'gf', 'gg', 'gh', 'gi', 'gl', 'gm', 'gn', 'gov', 'gp', 'gq', 'gr', 'gs', 'gt', 'gu', 'gw', 'gy', 'hk', 'hm', 'hn', 'hr', 'ht', 'hu', 'id', 'ie', 'il', 'im', 'in', 'info', 'int', 'io', 'iq', 'ir', 'is', 'it', 'je', 'jm', 'jo', 'jobs', 'jp', 'ke', 'kg', 'kh', 'ki', 'km', 'kn', 'kp', 'kr', 'kw', 'ky', 'kz', 'la', 'lb', 'lc', 'li', 'lk', 'lr', 'ls', 'lt', 'lu', 'lv', 'ly', 'ma', 'mc', 'md', 'me', 'mg', 'mh', 'mil', 'mk', 'ml', 'mm', 'mn', 'mo', 'mobi', 'mp', 'mq', 'mr', 'ms', 'mt', 'mu', 'museum', 'mv', 'mw', 'mx', 'my', 'mz', 'na', 'name', 'nc', 'ne', 'net', 'nf', 'ng', 'ni', 'nl', 'no', 'np', 'nr', 'nu', 'nz', 'om', 'org', 'pa', 'pe', 'pf', 'pg', 'ph', 'pk', 'pl', 'pm', 'pn', 'pr', 'pro', 'ps', 'pt', 'pw', 'py', 'qa', 're', 'ro', 'rs', 'ru', 'rw', 'sa', 'sb', 'sc', 'sd', 'se', 'sg', 'sh', 'si', 'sj', 'sk', 'sl', 'sm', 'sn', 'so', 'sr', 'st', 'su', 'sv', 'sy', 'sz', 'tc', 'td', 'tel', 'tf', 'tg', 'th', 'tj', 'tk', 'tl', 'tm', 'tn', 'to', 'tp', 'tr', 'travel', 'tt', 'tv', 'tw', 'tz', 'ua', 'ug', 'uk', 'us', 'uy', 'uz', 'va', 'vc', 've', 'vg', 'vi', 'vn', 'vu', 'wf', 'ws', 'ye', 'yt', 'yu', 'za', 'zm', 'zw');

		if (isset($url_parts[2]) && !in_array($url_parts[2], $tlds)) continue;
		
		// Call user specified func
		$modified_url = $callback($url);
		
		// Replace it!
		$start = strpos($text, $url, $offset);
		$text = substr_replace($text, $modified_url, $start, strlen($url));
		$offset = $start + strlen($modified_url);
	}
	
	return $text;
}

function linkify($url) {

	if (!preg_match('#^[a-z]+://#i', $url)) {
		return "<a href=\"http://$url\" class=\"extlink\" target=\"_blank\">$url</a>";
	}
	return "<a href=\"$url\" class=\"extlink\" target=\"_blank\">$url</a>";
}

function addslashes_array(&$arr_r){
	foreach ($arr_r as &$val) is_array($val) ? addslashes_array($val):$val=addslashes($val);
	unset($val);
}

# returns most common value in input array
function multidimensional_array_diff($a1,$a2)
{
	$r = array();

	foreach ($a2 as $key => $second)
	{
		foreach ($a1 as $key => $first)
		{

			if (isset($a2[$key]))
			{
				foreach ($first as $first_value)
				{
					foreach ($second as $second_value)
					{
						if ($first_value == $second_value)
						{
							$true = true;
							break;
						}
					}
					if (!isset($true))
					{

						$r[$key][] = $first_value;
					}
					unset($true);
				}
			}
			else
			{
				$r[$key] = $first;
			}
		}
	}
	return $r;
}


function array_most_common($input)
{
	$counted = array_count_values($input);
	arsort($counted);
	return(key($counted));
}
function super_unique($array)
{
	$result = array_map("unserialize", array_unique(array_map("serialize", $array)));

	foreach ($result as $key => $value)
	{
		if ( is_array($value) )
		{
			$result[$key] = super_unique($value);
		}
	}

	return $result;
}

############### DISTANCES
function findHaversineDistance($latitude1, $longitude1, $latitude2, $longitude2) {
	$earth_radius = 6371;

	$dLat = deg2rad($latitude2 - $latitude1);
	$dLon = deg2rad($longitude2 - $longitude1);

	$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
	$c = 2 * asin(sqrt($a));
	$d = $earth_radius * $c;

	return $d;

}

//Calculates distance in KM between postcodes
function postcode_dist($postcode1,$postcode2, $suburb1 = '', $suburb2 = '') {

	$parameters = array($postcode1);
	$query = "SELECT `latitude`, `longitude` FROM `helper_postcode` WHERE `latitude` <> 0 AND `longitude` <> 0 AND `postcode` = ?";

	if($suburb1 != ''){

		$query = " AND `suburb` = ?";
		$parameters[] = $suburb1;

	}

	if($cords1 = Database::dbRow($query, $parameters)){

		$parameters = array($postcode2);
		$query = "SELECT `latitude`, `longitude` FROM `helper_postcode` WHERE `latitude` <> 0 AND `longitude` <> 0 AND `postcode` = ?";

		if($suburb2 != ''){

			$query = " AND `suburb` = ?";
			$parameters[] = $suburb2;
		}
	}

	if($cords2 = Database::dbRow($query, $parameters)){

		$distance = findHaversineDistance($cords1['latitude'], $cords1['longitude'], $cords2['latitude'], $cords2['longitude']);
		return $distance;
	}

	return false;
}

function findSuburbsWithinDistance($latitude, $longitude, $distance){



	$query = "SELECT `postcode`,(((acos(sin(( ? *pi()/180)) * sin((`latitude`*pi()/180))+cos(( ? *pi()/180))
	* cos((`latitude`*pi()/180)) * cos(((? - `longitude`)*pi()/180))))*180/pi())*60*
	-  1.1515*1.609344) as `distance` FROM `helper_postcode` HAVING `distance` <= ? AND `distance` >= ? ORDER BY `distance` ASC";


	//$latitude, $latitude, $longitude, $distanceFromOrigin
	$parameters = array($latitude, $latitude, $longitude, $distance, $distance * -1);
	if($suburbs = Database::dbResults($query, $parameters)){
		//print_r($suburbs);
		return $suburbs;
	}
}


#########################
### COLOUR FUNCTIONS

function getContrastYIQ($hexcolor){

	if(substr($hexcolor, 0, 1) == '#'){

		$hexcolor = substr($hexcolor, 1);
	}

	$r = hexdec(substr($hexcolor,0,2));
	$g = hexdec(substr($hexcolor,2,2));
	$b = hexdec(substr($hexcolor,4,2));
	$yiq = (($r*299)+($g*587)+($b*114))/1000;
	return ($yiq >= 128) ? 'black' : 'white';
}


function color_inverse($color){
	$color = str_replace('#', '', $color);
	if (strlen($color) != 6){ return '000000'; }
	$rgb = '';
	for ($x=0;$x<3;$x++){
		$c = 255 - hexdec(substr($color,(2*$x),2));
		$c = ($c < 0) ? 0 : dechex($c);
		$rgb .= (strlen($c) < 2) ? '0'.$c : $c;
	}
	return '#'.$rgb;
}



function _color_rgb2hsl($rgb) {

	$r = $rgb[0];

	$g = $rgb[1];

	$b = $rgb[2];

	$min = min($r, min($g, $b));

	$max = max($r, max($g, $b));

	$delta = $max - $min;

	$l = ($min + $max) / 2;

	$s = 0;

	if ($l > 0 && $l < 1) {

		$s = $delta / ($l < 0.5 ? (2 * $l) : (2 - 2 * $l));

	}

	$h = 0;

	if ($delta > 0) {

		if ($max == $r && $max != $g) $h += ($g - $b) / $delta;

		if ($max == $g && $max != $b) $h += (2 + ($b - $r) / $delta);

		if ($max == $b && $max != $r) $h += (4 + ($r - $g) / $delta);

		$h /= 6;

	}

	return array($h, $s, $l);

}



function _color_hsl2rgb($hsl) {

	$h = $hsl[0];

	$s = $hsl[1];

	$l = $hsl[2];

	$m2 = ($l <= 0.5) ? $l * ($s + 1) : $l + $s - $l*$s;

	$m1 = $l * 2 - $m2;

	return array(_color_hue2rgb($m1, $m2, $h + 0.33333),

	_color_hue2rgb($m1, $m2, $h),

	_color_hue2rgb($m1, $m2, $h - 0.33333));

}

/**

* Helper function for _color_hsl2rgb().

*/

function _color_hue2rgb($m1, $m2, $h) {

	$h = ($h < 0) ? $h + 1 : (($h > 1) ? $h - 1 : $h);

	if ($h * 6 < 1) return $m1 + ($m2 - $m1) * $h * 6;

	if ($h * 2 < 1) return $m2;

	if ($h * 3 < 2) return $m1 + ($m2 - $m1) * (0.66666 - $h) * 6;

	return $m1;

}

/**

* Convert a hex color into an RGB triplet.

*/

function _color_unpack($hex, $normalize = false) {
	$out = array();
	if (strlen($hex) == 4) {

		$hex = $hex[1] . $hex[1] . $hex[2] . $hex[2] . $hex[3] . $hex[3];

	}

	$c = hexdec($hex);

	for ($i = 16; $i >= 0; $i -= 8) {

		$out[] = (($c >> $i) & 0xFF) / ($normalize ? 255 : 1);

	}

	return $out;

}

/**

* Convert an RGB triplet to a hex color.

*/

function _color_pack($rgb, $normalize = false) {
	$out = '';
	foreach ($rgb as $k => $v) {

		$out |= (($v * ($normalize ? 255 : 1)) << (16 - $k * 8));

	}

	return '#'. str_pad(dechex($out), 6, 0, STR_PAD_LEFT);

}

function findDominantImageColour($imagePath){

	$rTotal = 0;
	$gTotal = 0;
	$bTotal = 0;
	$total = 0;

	$rValues = array();
	$gValues = array();
	$bValues = array();

	$original = imagecreatefromjpeg($imagePath);
	list($width, $height) = getimagesize($imagePath);
	$scaled = imagecreatetruecolor($width / 20, $height / 20);

	imagecopyresized($scaled, $original, 0,0, 0, 0, $width / 20, $height / 20, $width, $height);

	for ($x=0;$x<imagesx($scaled);$x++) {

		for ($y=0;$y<imagesy($scaled);$y++) {



			$rgb = imagecolorat($scaled,$x,$y);

			$r   = ($rgb >> 16) & 0xFF;

			$g   = ($rgb >>  8) & 0xFF;

			$b   = $rgb & 0xFF;

			//echo 'r = ' . $r % 25 . '<br />';
			//	$rMod = $r %2;
			if(isset($rValues[$r % 25])){

				$rValues[$r % 25] += 1;
			} else {

				$rValues[$r % 25]  = 1;
			}

			if(isset($gValues[$g % 25])){

				$gValues[$g % 25] += 1;
			} else {

				$gValues[$g % 25]  = 1;
			}

			if(isset($bValues[$b % 25])){

				$bValues[$b % 25] += 1;
			} else {

				$bValues[$b % 25]  = 1;
			}

			$rTotal += $r;

			$gTotal += $g;

			$bTotal += $b;

			$total++;



		}

	}
	//	print_r($rValues);print_r($gValues);
	$rAverage = round($rTotal/$total);

	$gAverage = round($gTotal/$total);

	$bAverage = round($bTotal/$total);

	ImageDestroy($original);
	ImageDestroy($scaled);

	//imagejpeg($scaled, ROOT . '/dropbox/1/signregister/1/foo.jpg');

	echo $rTotal . '|' .$gTotal . '|' .$bTotal;die();
	// db query
	//select * from image_table order by ABS(".$r."-rVal)+ABS(".$g."-gVal)+ABS(".$b."-bVal);


}

####################
### IMAGE FUCNTIONS

function findBestScale($maxWidth, $maxHeight, $width, $height){
	// Recalculate new size with default ratio
	if($width < $maxWidth && $height < $maxHeight){

		return(array($width, $height));
	}

	$xscale=$width/$maxWidth;
	$yscale=$height/$maxHeight;

	if ($yscale>$xscale){
		$new_width = round($width * (1/$yscale));
		$new_height = round($height * (1/$yscale));

	}
	else {
		$new_width = round($width * (1/$xscale));
		$new_height = round($height * (1/$xscale));

	}
	// echo $new_height . '|' . $new_height;
	return array($new_width, $new_height);
}
#####################
#### TIME
function roundToQuarterHour($timestring) {
    $minutes = date('i', strtotime($timestring));
    return $minutes - ($minutes % 15);
}


function sec2hms ($sec, $padHours = false, $roundUp = false)
{

	// round it up
	if($roundUp){

		$sec = ceil($sec);
	}
	// start with a blank string
	$hms = "";

	// do the hours first: there are 3600 seconds in an hour, so if we divide
	// the total number of seconds by 3600 and throw away the remainder, we're
	// left with the number of hours in those seconds
	$hours = intval(intval($sec) / 3600);

	// add hours to $hms (with a leading 0 if asked for)
	$hms .= ($padHours)
	? str_pad($hours, 2, "0", STR_PAD_LEFT). ":"
	: $hours. ":";

	// dividing the total seconds by 60 will give us the number of minutes
	// in total, but we're interested in *minutes past the hour* and to get
	// this, we have to divide by 60 again and then use the remainder
	$minutes = intval(($sec / 60) % 60);

	// add minutes to $hms (with a leading 0 if needed)
	$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ":";

	// seconds past the minute are found by dividing the total number of seconds
	// by 60 and using the remainder
	$seconds = intval($sec % 60);

	// add seconds to $hms (with a leading 0 if needed)
	$hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

	// done!
	return $hms;

}
function hms2sec($time, $delimiter = ':'){

	$parts = explode($delimiter, $time);
	$totalSeconds = 0;
	$totalSeconds += $parts[0] * 3600;
	$totalSeconds += $parts[1] * 60;
	$totalSeconds += $parts[2];

	return $totalSeconds;

}
#################
### FILE HELPERS
function getFileHeaders($url){
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,            $url);
	curl_setopt($ch, CURLOPT_HEADER,         true);
	curl_setopt($ch, CURLOPT_NOBODY,         true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT,        10);

	$r = curl_exec($ch);
	//echo $r;
	//print_r($r);
	return $r;

}