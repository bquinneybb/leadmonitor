<?php
class Controller {

	protected $_model;
	protected $_modelName;
	protected $_controller;
	protected $_action;
	protected $_template;

	protected $noPermissionRedirect = array( 'ErrorModel', 'LoginModel', 'ContentModel','VideoModel');
	public $hideMenu = array('login');
	function __construct($model, $controller, $action) {



		$this->_controller = $controller;
		$this->_action = $action;
		$this->_model = $model;
		$this->_modelName = $model;

		//echo $model;

		if($model != 'Model'){

			$model .= 'Model';
			$this->_model = $model;
		}


		$this->$model = new $model;




		$this->_template = new Template($controller,$action);
		$this->_template->model = $model;
		$this->_template->action = $action;
		$this->_template->noPermissionRedirect = $this->noPermissionRedirect;
		// check the permissions
		//echo 'm = ' . $model;die();
		if(!in_array($model, $this->noPermissionRedirect) && is_callable(array($this->$model, 'setPermissions'))){

				
			$permissions = $this->$model->setPermissions();
				
				
		} else {
				
				
			$thisModel = new Model();
			$permissions = $thisModel->setPermissions();
				
		}
		$this->set('hideNavigation', false);

		if(in_array($this->_controller, $this->hideMenu)){

			$this->set('hideNavigation', true);
		}
			
		// set some a variable so we can supress the menus etc
		$this->set('showTemplate', true);

		$this->_template->permissions = $permissions;

	}

	function set($name,$value) {

		$this->_template->set($name,$value);
	}

	function __destruct() {

			

		$this->_template->render();
	}



}