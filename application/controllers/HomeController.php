<?php
class HomeController extends Controller {
	
function dashboard($queryString, $parameters){
	
		$this->set('activeLabel', 'Dashboard');
		
		$modelName = $this->_model;
		$model = new $this->$modelName;
		
		$tasks = $model->findAllOutstandingTasks();
		$this->set('tasks', $tasks);
		
	}
}