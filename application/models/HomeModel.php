<?php
class HomeModel extends Model {
	
	public function findAllOutstandingTasks(){
		
		$dashboard = new ModuleDashboard();
		$tasks = $dashboard->findUserOutstandingTasks();
		
		return $tasks;
		
	}
	
}