<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>LeadMonitor.com.au Facebook Application Privacy Policy</title>
<style type="text/css">
body {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
}
#content {
	width: 1000px;
	margin: 10px auto 10px auto;	
}
</style>
</head>

<body>
<div id="content">
<p><strong>Effective Date: March 6th, 2012</strong></p>
<p><strong>LeadMonitor.com.au Facebook Application Privacy Policy</strong></p>
<p>We at LeadMonitor.com.au (&ldquo;we&rdquo;) respect your privacy and value the relationship we have with you. Your use of this application is subject to this Privacy Policy. Your use of Facebook in general and your relationship with Facebook are subject to Facebook&rsquo;s own Privacy Policy and Facebook&rsquo;s other terms and policies, which LeadMonitor.com.au does not control. This Privacy Policy describes how LeadMonitor.com.au may use the information we collect with this application on Facebook and with whom we may share it.</p>
<p><strong>Information We Collect</strong></p>
<p>We may collect two types of personal information with this application: (i) information and content we receive from Facebook that was provided by you or other users to Facebook or was associated by Facebook with a particular user (&ldquo;Information We Receive From Facebook&rdquo;) and (ii) information and content you provide directly to us, such as by submitting it to us through a form in this application (&ldquo;Independent Information&rdquo;). </p>
<p><strong>Information We Receive From Facebook</strong></p>
<p>When you visit the application, it may collect any information you have made visible to &ldquo;Everyone&rdquo; on Facebook as well as the information Facebook designates as your publicly available information. This includes your name, profile picture, gender, current city, networks, friend list, and pages you are a fan of. Even if you have not allowed this application access, it may collect Information We Receive From Facebook about you when friends on Facebook allow access to and use the application, subject to your Facebook settings. We use this information to operate the application, such as posting to your wall or sharing with other users. We may share this information on your wall or with other users without your explicit approval. </p>
<p><strong>Independent Information</strong></p>
<p>The application may ask you to explicitly provide certain information to us, such as your email address from a pre-defined list to send to your friends. We generally use Independent Information for the same purposes as we use Information We Receive From Facebook.</p>
<p><strong>How We Share Information We Receive From Facebook</strong></p>
<p>We may disclose Information We Receive From Facebook to our affiliates and service providers to the extent permitted by Facebook. We will not provide such information to third parties for their use in marketing their products or services to you without your consent, but we do not control Facebook&rsquo;s own use of content and information it collects in relation to your use of this application. We also may share Information We Receive From Facebook with other third parties with your consent. For example, we may share some Information We Receive From Facebook with other users of Facebook, including other users of the application, as described above.</p>
<p>We may disclose any Information We Receive From Facebook we deem necessary, in our sole discretion, to comply with any applicable law, regulation, legal process or governmental request. In addition, we may disclose Information We Receive From Facebook when we believe, in our sole discretion, disclosure is necessary or appropriate to prevent physical harm or financial loss or in connection with suspected or actual illegal activity.</p>
<p><strong>Updates To This Privacy Policy</strong></p>
<p>This Privacy Policy may be updated periodically and without prior notice to you to reflect changes in our personal information practices.</p>
<p><strong>How to Contact Us</strong></p>
<p>If you have any questions or comments about this privacy notice, please contact us by email at admin@leadmonitor.com.au</p>
</div>
</body>
</html>
