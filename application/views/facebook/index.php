<?php

if(!isset($_SESSION['facebookvisitor'])){
	
	$_SESSION['facebookvisitor'] = md5(microtime());
}	

?>
<html>
	<head></head>
	<body>
		<script src="/public/javascript/jquery/jquery.min.js"></script>
		<link href="/public/modules/facebook/css/core.css" rel="stylesheet" type="text/css" media="all" />
		
		<script src="/public/modules/facebook/javascript/core.js"></script>
        <script src="/public/javascript/formcheck.js"></script>
        
        
        
        
		<?php
		$page = $this -> variables['page'];

		if ($page['html'] == '') {

			echo "This page doesn't exist";
			die();
		}
		/*
		 $images = array();
		 $images['images'][0]['image'] = 'profish.jpg';
		 $images['images'][0]['caption'] = 'Profish';
		 $images['images'][0]['url'] = 'http://www.haineshunter.com.au/model.php?mid=1';

		 $images['images'][1]['image'] = '560-classic.jpg';
		 $images['images'][1]['caption'] = '560 Classic Offshore';
		 $images['images'][1]['url'] = 'http://www.haineshunter.com.au/dealerpackage.php?id=174';

		 $images['configuration']['gallerytype'] = '';

		 print_r(json_encode($images));
		 */

		// process the html for template items
		$faceBook = new ModuleFacebook();
		$html = $faceBook -> parseHTML($page);
		
		
	?>
    <?php if($page['google_analytics_id'] != ''){ ?>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php echo $page['google_analytics_id']; ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    <?php } ?>
	<?php
		$clientCSSFile = '/fileserver/facebook/' . $page['module_campaign_id_pk'] . '/css/core.css';
		if(file_exists(ROOT . $clientCSSFile)){
		?>
		<link href="<?php echo $clientCSSFile ?>" rel="stylesheet" type="text/css" media="all" />
		<?php } ?>
        <?php
		$clientJavascriptFile = '/fileserver/facebook/' . $page['module_campaign_id_pk'] . '/javascript/core.js';
		if(file_exists(ROOT . $clientJavascriptFile)){
		?>
		<script src="<?php echo $clientJavascriptFile ?>"></script>
		<?php } ?>
	<script>var siteId = '<?php echo $page['module_campaign_id_pk'] ?>';var uid = '<?php echo $_SESSION['facebookvisitor']  ?>';</script>

<div id="content">
		<?php echo $html;?>
		</div>
	</body>
</html>
